﻿using System.Reflection;
using Orms.DataAccessHelper;
using Xtensive.Orm;
using Xtensive.Orm.Configuration;

namespace Orms.DataAccessDataObjectsNet
{
    public class DataObjectsNetHelper : SessionWorker<Domain>
    {
        private Domain _domain;

        public DataObjectsNetHelper(string connectionString) : base(connectionString)
        {
        }

        public override void CloseSession()
        {
            _domain.Dispose();
            isOpen = false;
        }

        public override void OpenSession()
        {
            var configuration = new DomainConfiguration("sqlserver", _connectionString);

            configuration.Types.Register(Assembly.GetExecutingAssembly(), "Orms.DataAccessDataObjectsNet.EntityClasses");
            //configuration.UpgradeMode = DomainUpgradeMode.Recreate;
            _domain = Domain.Build(configuration);
            isOpen = true;
        }

        public override Domain Read()
        {
            return _domain;
        }

        public override Domain ReadAndWrite()
        {
            return _domain;
        }

        public override void SaveChanges()
        {
            throw new System.NotImplementedException();
        }
    }
}
