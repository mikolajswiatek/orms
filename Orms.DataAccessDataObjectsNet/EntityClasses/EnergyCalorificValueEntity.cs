﻿using System;
using Xtensive.Orm;

namespace Orms.DataAccessDataObjectsNet.EntityClasses
{
    [Serializable]
    [HierarchyRoot]
    [TableMapping("CalorificValueValues")]
    public class EnergyCalorificValueEntity : BaseEntity
    {
        [Field(Length = Int32.MaxValue, Nullable = false)]
        public EnergyUnitValue Energy { get; set; }

        [Field(Length = Int32.MaxValue, Nullable = false)]
        [FieldMapping("HourIndex")]
        public int HourIndex { get; set; }

        [Field(Length = Int32.MaxValue, Nullable = false)]
        [FieldMapping("CalorificValueId")]
        public CalorificValueEntity CalorificValue { get; set; }
    }
}