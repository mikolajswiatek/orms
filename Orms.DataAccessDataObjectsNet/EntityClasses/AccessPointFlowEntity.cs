﻿using System;
using Orms.Domains.Enums;
using Xtensive.Orm;

namespace Orms.DataAccessDataObjectsNet.EntityClasses
{
    [Serializable]
    [HierarchyRoot]
    [TableMapping("AccessPointFlows")]
    public class AccessPointFlowEntity : BaseEntity
    {
        [Field(Length = 255, Nullable = false)]
        [FieldMapping("Direction")]
        public DirectionType Direction { get; set; }

        [Field(Length = Int32.MaxValue, Nullable = false)]
        [FieldMapping("AccessPointId")]
        public AccessPointEntity AccessPoint { get; set; }
    }
}