﻿using System;
using Xtensive.Orm;

namespace Orms.DataAccessDataObjectsNet.EntityClasses
{
    [Serializable]
    [HierarchyRoot]
    [TableMapping("Agreements")]
    public class AgreementEntity : BaseEntity
    {
        [Field(Length = Int32.MaxValue, Nullable = false)]
        [FieldMapping("CreateDatetime")]
        public DateTime CreateDate { get; set; }

        [Field(Length = Int32.MaxValue, Nullable = false)]
        [FieldMapping("StartDatetime")]
        public DateTime Start { get; set; }

        [Field(Length = Int32.MaxValue, Nullable = false)]
        [FieldMapping("EndDatetime")]
        public DateTime End { get; set; }

        [Field(Length = Int32.MaxValue, Nullable = false)]
        [FieldMapping("ContractorId")]
        public ContractorEntity Contractor { get; set; }

        [Field(Length = Int32.MaxValue, Nullable = false)]
        [FieldMapping("GasStorageId")]
        public GasStorageEntity GasStorage { get; set; }
    }
}