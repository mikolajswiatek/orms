﻿using System;
using Xtensive.Orm;

namespace Orms.DataAccessDataObjectsNet.EntityClasses
{
    [Serializable]
    [HierarchyRoot]
    [TableMapping("ContractorContacts")]
    public class ContractorContactEntity : BaseEntity
    {
        [Field(Length = 255, Nullable = false)]
        [FieldMapping("Name")]
        public string Name { get; set; }

        [Field(Length = 255, Nullable = false)]
        [FieldMapping("FirstName")]
        public string FirstName { get; set; }

        [Field(Length = 255, Nullable = false)]
        [FieldMapping("LastName")]
        public string LastName { get; set; }

        [Field(Length = 255, Nullable = false)]
        [FieldMapping("Adress")]
        public string Adress { get; set; }

        [Field(Length = 255, Nullable = false)]
        [FieldMapping("Email")]
        public string Email { get; set; }

        [Field(Length = 255, Nullable = false)]
        [FieldMapping("Phone")]
        public string Phone { get; set; }

        [Field(Length = Int32.MaxValue, Nullable = false)]
        [Association(PairTo = "Contacts")]
        [FieldMapping("ContractorId")]
        public ContractorEntity Contractor { get; set; }
    }
}