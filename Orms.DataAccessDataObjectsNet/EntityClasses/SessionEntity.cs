﻿using System;
using Orms.Models;
using Xtensive.Orm;

namespace Orms.DataAccessDataObjectsNet.EntityClasses
{
    [Serializable]
    [HierarchyRoot]
    [TableMapping("Sessions")]
    public class SessionEntity : BaseEntity
    {
        [Field(Length = 255, Nullable = false)]
        [FieldMapping("Guid")]
        public string Guid { get; set; }

        [Field(Length = 255, Nullable = false)]
        [FieldMapping("Ip")]
        public string Ip { get; set; }

        [Field(Length = 255, Nullable = false)]
        [FieldMapping("Browser")]
        public string Browser { get; set; }

        [Field(Nullable = false)]
        [FieldMapping("CreateDatetime")]
        public DateTime CreateDatetime { get; set; }

        [Field(Nullable = false)]
        [FieldMapping("LastActivityDatetime")]
        public DateTime LastActivityDatetime { get; set; }

        [Field(Length = Int32.MaxValue, Nullable = false)]
        [FieldMapping("UserId")]
        public UserEntity User { get; set; }
    }
}
