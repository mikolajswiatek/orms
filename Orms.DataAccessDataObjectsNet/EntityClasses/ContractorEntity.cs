﻿using System;
using Xtensive.Orm;

namespace Orms.DataAccessDataObjectsNet.EntityClasses
{
    [Serializable]
    [HierarchyRoot]
    [TableMapping("Contractors")]
    public class ContractorEntity : BaseEntity
    {
        [Field(Length = 255, Nullable = false)]
        [FieldMapping("Name")]
        public string Name { get; set; }

        [Field(Length = 255, Nullable = false)]
        [FieldMapping("Code")]
        public string Code { get; set; }

        [Field(Length = 255, Nullable = false)]
        [FieldMapping("Adress")]
        public string Adress { get; set; }

        [Field]
        public EntitySet<AgreementEntity> Agreements { get; set; }

        [Field]
        public EntitySet<ContractorContactEntity> Contacts { get; set; }

        [Field]
        public EntitySet<UserEntity> Users { get; set; }
    }
}