﻿using System;
using Orms.Domains.Enums;
using Xtensive.Orm;

namespace Orms.DataAccessDataObjectsNet.EntityClasses
{
    [Serializable]
    [HierarchyRoot]
    [TableMapping("Nominations")]
    public class NominationEntity : BaseEntity
    {
        [Field(Length = 255, Nullable = false)]
        [FieldMapping("Name")]
        public string Name { get; set; }

        [Field(Nullable = false)]
        [FieldMapping("GasDay")]
        public DateTime GasDay { get; set; }

        [Field(Length = Int32.MaxValue, Nullable = false)]
        [FieldMapping("ContractorId")]
        public ContractorEntity Contractor { get; set; }

        [Field(Length = Int32.MaxValue, Nullable = false)]
        [FieldMapping("AccessPointFlowId")]
        public AccessPointFlowEntity AccessPointFlow { get; set; }

        [Field(Nullable = false)]
        [FieldMapping("CreateDateTime")]
        public DateTime CreateDateTime { get; set; }

        [Field(Nullable = false)]
        [FieldMapping("IsLastOrder")]
        public bool IsLastOrder { get; set; }

        [Field(Nullable = false)]
        [FieldMapping("IsAccepted")]
        public bool IsAccepted { get; set; }

        [Field(Length = Int32.MaxValue, Nullable = true)]
        [FieldMapping("ParentNominationId")]
        public NominationEntity ParentNomination { get; set; }

        [Field]
        public EntitySet<NominationValueEntity> Values { get; set; }

        [Field(Length = Int32.MaxValue, Nullable = false)]
        [FieldMapping("UserId")]
        public UserEntity Creator { get; set; }

        [Field(Length = 255, Nullable = false)]
        [FieldMapping("Channel")]
        public Channel Channel { get; set; }

        [Field(Length = Int32.MaxValue, Nullable = false)]
        [FieldMapping("MajorVersion")]
        public int MajorVersion { get; set; }

        [Field(Length = 255, Nullable = false)]
        [FieldMapping("Version")]
        public int Version { get; set; }

        [Field(Length = 255, Nullable = false)]
        [FieldMapping("Comment")]
        public string Comment { get; set; }

        [Field(Length = Int32.MaxValue, Nullable = false)]
        [FieldMapping("CalorificValueId")]
        public CalorificValueEntity CalorificValue { get; set; }

    }
}