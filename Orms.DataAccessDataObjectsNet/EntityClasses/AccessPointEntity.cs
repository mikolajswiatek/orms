﻿using System;
using Orms.Models;
using Xtensive.Orm;

namespace Orms.DataAccessDataObjectsNet.EntityClasses
{
    [Serializable]
    [HierarchyRoot]
    [TableMapping("AccessPoints")]
    public class AccessPointEntity : BaseEntity
    {
        [Field(Length = 255, Nullable = false)]
        [FieldMapping("Code")]
        public string Code { get; set; }

        [Field]
        public EntitySet<AccessPointFlowEntity> AccessPointFlows { get; set; }

        [Field(Length = Int32.MaxValue, Nullable = false)]
        [FieldMapping("GasStorageId")]
        public GasStorageEntity GasStorage { get; set; }
    }
}