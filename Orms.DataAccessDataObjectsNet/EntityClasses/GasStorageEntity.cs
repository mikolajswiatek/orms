﻿using System;
using Xtensive.Orm;

namespace Orms.DataAccessDataObjectsNet.EntityClasses
{
    [Serializable]
    [HierarchyRoot]
    [TableMapping("GasStorages")]
    public class GasStorageEntity : BaseEntity
    {
        [Field(Length = 255, Nullable = false)]
        [FieldMapping("Name")]
        public string Name { get; set; }

        [Field]
        public EntitySet<AccessPointEntity> AccessPoints { get; set; }

        [Field]
        public EntitySet<AgreementEntity> Agreements { get; set; }
    }
}