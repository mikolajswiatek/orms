﻿using System;
using Orms.Models;
using Xtensive.Orm;

namespace Orms.DataAccessDataObjectsNet.EntityClasses
{
    [Serializable]
    [HierarchyRoot]
    [TableMapping("CalorificValues")]
    public class CalorificValueEntity : BaseEntity
    {
        [Field(Nullable = false)]
        [FieldMapping("GasDay")]
        public DateTime GasDay { get; set; }

        [Field(Length = Int32.MaxValue, Nullable = false)]
        [FieldMapping("GasStorageId")]
        public GasStorageEntity GasStorage { get; set; }

        [Field]
        public EntitySet<EnergyCalorificValueEntity> Values { get; set; }
    }
}