﻿using Xtensive.Orm;

namespace Orms.DataAccessDataObjectsNet.EntityClasses
{
    public class BaseEntity : Entity
    {
        [Key]
        [Field]
        [FieldMapping("Id")]
        public int Id { get; private set; }
    }
}