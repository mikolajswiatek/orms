﻿using System;
using Xtensive.Orm;

namespace Orms.DataAccessDataObjectsNet.EntityClasses
{
    [Serializable]
    [HierarchyRoot]
    [TableMapping("Users")]
    public class UserEntity : BaseEntity
    {
        [Field(Length = 255, Nullable = false)]
        [FieldMapping("Name")]
        public string Name { get; set; }

        [Field(Length = 255, Nullable = false)]
        [FieldMapping("Password")]
        public string Password { get; set; }

        [Field(Length = 255, Nullable = false)]
        [FieldMapping("Email")]
        public string Email { get; set; }

        [Field(Nullable = false)]
        [FieldMapping("IsActive")]
        public bool IsActive { get; set; }

        [Field(Nullable = false)]
        [FieldMapping("CreateDatetime")]
        public DateTime CreateDatetime { get; set; }

        [Field(Nullable = false)]
        [FieldMapping("LastActivityDatetime")]
        public DateTime LastActivityDatetime { get; set; }

        [Field(Length = Int32.MaxValue, Nullable = false)]
        [FieldMapping("ContractorId")]
        public ContractorEntity Contractor { get; set; }
    }
}
