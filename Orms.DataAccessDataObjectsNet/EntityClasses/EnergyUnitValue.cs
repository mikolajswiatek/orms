﻿using Orms.Domains.Enums;
using Xtensive.Orm;

namespace Orms.DataAccessDataObjectsNet.EntityClasses
{
    public class EnergyUnitValue : Structure
    {
        [Field(Nullable = false)]
        [FieldMapping("EnergyValue")]
        public decimal Value { get; set; }

        [Field(Nullable = false)]
        [FieldMapping("EnergyUnit")]
        public Unit Unit { get; set; }

        public EnergyUnitValue(decimal value, Unit unit)
        {
            Value = value;
            Unit = unit;
        }
    }
}