﻿using System;
using Xtensive.Orm;

namespace Orms.DataAccessDataObjectsNet.EntityClasses
{
    [Serializable]
    [HierarchyRoot]
    [TableMapping("NominationValues")]
    public class NominationValueEntity : BaseEntity
    {
        [Field(Nullable = false)]
        public EnergyUnitValue Energy { get; set; }

        [Field(Nullable = false)]
        public VolumeUnitValue Volume { get; set; }

        [Field(Length = Int32.MaxValue, Nullable = false)]
        [FieldMapping("HourIndex")]
        public int HourIndex { get; set; }

        [Field(Length = Int32.MaxValue, Nullable = false)]
        [FieldMapping("NominationId")]
        public NominationEntity Nomination { get; set; }
    }
}
