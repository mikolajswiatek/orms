﻿using System;
using Xtensive.Orm;

namespace Orms.DataAccessDataObjectsNet.EntityClasses
{
    [Serializable]
    [HierarchyRoot]
    [TableMapping("GasAccounts")]
    public class GasAccountEntity : BaseEntity
    {
        [Field(Nullable = false)]
        public EnergyUnitValue Energy { get; set; }

        [Field(Nullable = false)]
        public VolumeUnitValue Volume { get; set; }

        [Field(Nullable = false)]
        public DateTime GasDay { get; set; }

        [Field(Length = Int32.MaxValue, Nullable = false)]
        [FieldMapping("HourIndex")]
        public int HourIndex { get; set; }

        [Field(Length = Int32.MaxValue, Nullable = false)]
        [FieldMapping("GasStorageId")]
        public GasStorageEntity GasStorage { get; set; }
    }
}