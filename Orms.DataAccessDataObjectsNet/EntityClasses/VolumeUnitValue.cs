﻿using Orms.Domains.Enums;
using Xtensive.Orm;

namespace Orms.DataAccessDataObjectsNet.EntityClasses
{
    public class VolumeUnitValue : Structure
    {
        [Field(Nullable = false)]
        [FieldMapping("VolumeValue")]
        public decimal Value { get; set; }

        [Field(Nullable = false)]
        [FieldMapping("VolumeUnit")]
        public Unit Unit { get; set; }

        public VolumeUnitValue(decimal value, Unit unit)
        {
            Value = value;
            Unit = unit;
        }
    }
}