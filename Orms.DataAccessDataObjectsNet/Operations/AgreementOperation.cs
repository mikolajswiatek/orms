﻿using System.Collections.Generic;
using Orms.DataAccessHelper;
using Orms.DataAccessHelper.Operations.Interfaces;
using Orms.DataAccessDataObjectsNet.Providers;
using Orms.Models;
using Orms.Services;
using Orms.Services.Interfaces;
using Xtensive.Orm;
using Session = Xtensive.Orm.Session;

namespace Orms.DataAccessDataObjectsNet.Operations
{
    public class AgreementOperation : Operation<Session>, IAgreementOperation
    {
        private IAgreementService _service;
        private readonly SessionWorker<Domain> _helperWorker;

        public AgreementOperation(SessionWorker<Domain> helper)
        {
            _helperWorker = helper;
        }

        protected override void SetProviders(Session session)
        {
            _service = new AgreementService(
                new AgreementProvider(session));
        }

        public void Add(Agreement agreement)
        {
            using (var session = _helperWorker.ReadAndWrite().OpenSession())
            {
                using (session.Activate())
                {
                    using (var transaction = session.OpenTransaction())
                    {
                        SetProviders(session);

                        _service.Add(agreement);
                        transaction.Complete();
                    }
                }
            }
        }

        public IEnumerable<Agreement> GetAll(Contractor contractor)
        {
            using (var session = _helperWorker.Read().OpenSession())
            {
                using (session.Activate())
                {
                    using (session.OpenTransaction())
                    {
                        SetProviders(session);

                        return _service.GetAll(contractor);
                    }
                }
            }
        }

        public IEnumerable<Agreement> GetAll(GasStorage gasStorage)
        {
            using (var session = _helperWorker.Read().OpenSession())
            {
                using (session.Activate())
                {
                    using (session.OpenTransaction())
                    {
                        SetProviders(session);

                        return _service.GetAll(gasStorage);
                    }
                }
            }
        }

        public IEnumerable<Agreement> GetAll(Contractor contractor, GasStorage gasStorage)
        {
            using (var session = _helperWorker.Read().OpenSession())
            {
                using (session.Activate())
                {
                    using (session.OpenTransaction())
                    {
                        SetProviders(session);

                        return _service.GetAll(contractor, gasStorage);
                    }
                }
            }
        }

        public Agreement Get(int id)
        {
            using (var session = _helperWorker.Read().OpenSession())
            {
                using (session.Activate())
                {
                    using (session.OpenTransaction())
                    {
                        SetProviders(session);

                        return _service.Get(id);
                    }
                }
            }
        }

        public IEnumerable<Agreement> GetAll()
        {
            using (var session = _helperWorker.Read().OpenSession())
            {
                using (session.Activate())
                {
                    using (session.OpenTransaction())
                    {
                        SetProviders(session);

                        return _service.GetAll();
                    }
                }
            }
        }
    }
}