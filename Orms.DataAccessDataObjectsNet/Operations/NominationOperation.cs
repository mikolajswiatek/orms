﻿using System;
using System.Collections.Generic;
using Orms.DataAccessDataObjectsNet.Providers;
using Orms.DataAccessHelper;
using Orms.DataAccessHelper.Operations.Interfaces;
using Orms.Models;
using Orms.Services;
using Orms.Services.Interfaces;
using Xtensive.Orm;
using Session = Xtensive.Orm.Session;

namespace Orms.DataAccessDataObjectsNet.Operations
{
    public class NominationOperation : Operation<Session>, INominationOperation
    {
        private INominationService _service;
        private readonly SessionWorker<Domain> _helper;

        public NominationOperation(SessionWorker<Domain> helper)
        {
            _helper = helper;
        }

        protected override void SetProviders(Session session)
        {
            _service = new NominationService(
                new NominationProvider(session),
                new NominationValueProvider(session));
        }

        public void Add(Nomination nomination)
        {
            using (var session = _helper.ReadAndWrite().OpenSession())
            {
                using (session.Activate())
                {
                    using (var transaction = session.OpenTransaction())
                    {
                        SetProviders(session);

                        _service.Add(nomination);
                        transaction.Complete();
                    }
                }
            }
        }

        public void Add(IEnumerable<NominationValue> values)
        {
            using (var session = _helper.ReadAndWrite().OpenSession())
            {
                using (session.Activate())
                {
                    using (var transaction = session.OpenTransaction())
                    {
                        SetProviders(session);

                        _service.Add(values);
                        transaction.Complete();
                    }
                }
            }
        }

        public void Add(Nomination nomination, IEnumerable<NominationValue> values)
        {
            using (var session = _helper.ReadAndWrite().OpenSession())
            {
                using (session.Activate())
                {
                    using (var transaction = session.OpenTransaction())
                    {
                        SetProviders(session);

                        _service.Add(nomination, values);
                        transaction.Complete();
                    }
                }
            }
        }

        public void RefreshNomiation(Nomination nomination, int parentNominationId)
        {
            using (var session = _helper.ReadAndWrite().OpenSession())
            {
                using (session.Activate())
                {
                    using (var transaction = session.OpenTransaction())
                    {
                        SetProviders(session);

                        _service.RefreshNomiation(nomination, parentNominationId);
                        transaction.Complete();
                    }
                }
            }
        }

        public void AcceptedNomination(Nomination nomination)
        {
            using (var session = _helper.ReadAndWrite().OpenSession())
            {
                using (session.Activate())
                {
                    using (var transaction = session.OpenTransaction())
                    {
                        SetProviders(session);

                        _service.AcceptedNomination(nomination);
                        transaction.Complete();
                    }
                }
            }
        }

        public IEnumerable<Nomination> GetBy(Contractor contractor)
        {
            using (var session = _helper.Read().OpenSession())
            {
                using (session.Activate())
                {
                    using (session.OpenTransaction())
                    {
                        SetProviders(session);

                        return _service.GetBy(contractor);
                    }
                }
            }
        }

        public IEnumerable<Nomination> GetBy(GasStorage gasStorage)
        {
            using (var session = _helper.Read().OpenSession())
            {
                using (session.Activate())
                {
                    using (session.OpenTransaction())
                    {
                        SetProviders(session);

                        return _service.GetBy(gasStorage);
                    }
                }
            }
        }

        public IEnumerable<Nomination> GetBy(User user)
        {
            using (var session = _helper.Read().OpenSession())
            {
                using (session.Activate())
                {
                    using (session.OpenTransaction())
                    {
                        SetProviders(session);

                        return _service.GetBy(user);
                    }
                }
            }
        }

        public IEnumerable<Nomination> GetBy(DateTime gasDay)
        {
            using (var session = _helper.Read().OpenSession())
            {
                using (session.Activate())
                {
                    using (session.OpenTransaction())
                    {
                        SetProviders(session);

                        return _service.GetBy(gasDay);
                    }
                }
            }
        }

        public Nomination Get(int id)
        {
            using (var session = _helper.Read().OpenSession())
            {
                using (session.Activate())
                {
                    using (session.OpenTransaction())
                    {
                        SetProviders(session);

                        return _service.Get(id);
                    }
                }
            }
        }

        public Nomination GetLast(Contractor contractor)
        {
            using (var session = _helper.Read().OpenSession())
            {
                using (session.Activate())
                {
                    using (session.OpenTransaction())
                    {
                        SetProviders(session);

                        return _service.GetLast(contractor);
                    }
                }
            }
        }

        public Nomination GetLast(GasStorage gasStorage)
        {
            using (var session = _helper.Read().OpenSession())
            {
                using (session.Activate())
                {
                    using (session.OpenTransaction())
                    {
                        SetProviders(session);

                        return _service.GetLast(gasStorage);
                    }
                }
            }
        }

        public Nomination GetLast(User user)
        {
            using (var session = _helper.Read().OpenSession())
            {
                using (session.Activate())
                {
                    using (session.OpenTransaction())
                    {
                        SetProviders(session);

                        return _service.GetLast(user);
                    }
                }
            }
        }

        public Nomination GetLast(Contractor contractor, DateTime gasDay)
        {
            using (var session = _helper.Read().OpenSession())
            {
                using (session.Activate())
                {
                    using (session.OpenTransaction())
                    {
                        SetProviders(session);

                        return _service.GetLast(contractor, gasDay);
                    }
                }
            }
        }

        public IEnumerable<Nomination> GetForGasDay(Contractor contractor, DateTime gasDay)
        {
            using (var session = _helper.Read().OpenSession())
            {
                using (session.Activate())
                {
                    using (session.OpenTransaction())
                    {
                        SetProviders(session);

                        return _service.GetForGasDay(contractor, gasDay);
                    }
                }
            }
        }

        public IEnumerable<Nomination> GetForGasDay(GasStorage gasStorage, DateTime gasDay)
        {
            using (var session = _helper.Read().OpenSession())
            {
                using (session.Activate())
                {
                    using (session.OpenTransaction())
                    {
                        SetProviders(session);

                        return _service.GetForGasDay(gasStorage, gasDay);
                    }
                }
            }
        }

        public IEnumerable<Nomination> GetForGasDay(Contractor contractor, GasStorage gasStorage, DateTime gasDay)
        {
            using (var session = _helper.Read().OpenSession())
            {
                using (session.Activate())
                {
                    using (session.OpenTransaction())
                    {
                        SetProviders(session);

                        return _service.GetForGasDay(contractor, gasStorage, gasDay);
                    }
                }
            }
        }
    }
}