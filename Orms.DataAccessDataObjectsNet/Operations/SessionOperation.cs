﻿using Orms.DataAccessDataObjectsNet.Providers;
using Orms.DataAccessHelper;
using Orms.DataAccessHelper.Operations.Interfaces;
using Orms.Services;
using Xtensive.Orm;
using ISessionService = Orms.Services.Interfaces.ISessionService;
using Session = Orms.Models.Session;

namespace Orms.DataAccessDataObjectsNet.Operations
{
    public class SessionOperation : Operation<Xtensive.Orm.Session>, ISessionOperation
    {
        private ISessionService _service;
        private readonly SessionWorker<Domain> _helper;

        public SessionOperation(SessionWorker<Domain> helper)
        {
            _helper = helper;
        }

        protected override void SetProviders(Xtensive.Orm.Session session)
        {
            _service = new SessionService(
                new SessionProvider(session),
                new UserProvider(session));
        }

        public void Add(Session s)
        {
            using (var session = _helper.ReadAndWrite().OpenSession())
            {
                using (session.Activate())
                {
                    using (var transaction = session.OpenTransaction())
                    {
                        SetProviders(session);

                        _service.Add(s);
                        transaction.Complete();
                    }
                }
            }
        }

        public void Refresh(string guid)
        {
            using (var session = _helper.ReadAndWrite().OpenSession())
            {
                using (session.Activate())
                {
                    using (var transaction = session.OpenTransaction())
                    {
                        SetProviders(session);

                        _service.Refresh(guid);
                        transaction.Complete();
                    }
                }
            }
        }

        public Session GetBy(string guid)
        {
            using (var session = _helper.Read().OpenSession())
            {
                using (session.Activate())
                {
                    using (session.OpenTransaction())
                    {
                        SetProviders(session);

                        return _service.GetBy(guid);
                    }
                }
            };
        }

        public void Delete(Session s)
        {
            using (var session = _helper.ReadAndWrite().OpenSession())
            {
                using (session.Activate())
                {
                    using (var transaction = session.OpenTransaction())
                    {
                        SetProviders(session);

                        _service.Delete(s);
                        transaction.Complete();
                    }
                }
            }
        }
    }
}