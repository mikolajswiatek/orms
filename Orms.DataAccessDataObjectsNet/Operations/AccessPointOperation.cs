﻿using System.Collections.Generic;
using Orms.DataAccessDataObjectsNet.Providers;
using Orms.DataAccessHelper;
using Orms.DataAccessHelper.Operations.Interfaces;
using Orms.Models;
using Orms.Services;
using Orms.Services.Interfaces;
using Xtensive.Orm;
using Session = Xtensive.Orm.Session;

namespace Orms.DataAccessDataObjectsNet.Operations
{
    public class AccessPointOperation : Operation<Session>, IAccessPointOperation
    {
        private IAccessPointService _service;
        private readonly SessionWorker<Domain> _helperWorker;

        public AccessPointOperation(SessionWorker<Domain> helper)
        {
            _helperWorker = helper;
        }

        protected override void SetProviders(Session session)
        {
            _service = new AccessPointService(
                new AccessPointProvider(session),
                new AccessPointFlowProvider(session));
        }

        public IEnumerable<AccessPoint> GetBy(GasStorage gasStorage)
        {
            using (var session = _helperWorker.Read().OpenSession())
            {
                using (session.Activate())
                {
                    using (session.OpenTransaction())
                    {
                        SetProviders(session);

                        return _service.GetBy(gasStorage);
                    }
                }
            }
        }

        public IEnumerable<AccessPoint> GetAll()
        {
            using (var session = _helperWorker.Read().OpenSession())
            {
                using (session.Activate())
                {
                    using (session.OpenTransaction())
                    {
                        SetProviders(session);

                        return _service.GetAll();
                    }
                }
            }
        }

        public AccessPoint Get(int id)
        {
            using (var session = _helperWorker.Read().OpenSession())
            {
                using (session.Activate())
                {
                    using (session.OpenTransaction())
                    {
                        SetProviders(session);

                        return _service.Get(id);
                    }
                }
            }
        }

        public void Add(AccessPoint accessPoint)
        {
            using (var session = _helperWorker.ReadAndWrite().OpenSession())
            {
                using (session.Activate())
                {
                    using (var transaction = session.OpenTransaction())
                    {
                        SetProviders(session);

                        _service.Add(accessPoint);
                        transaction.Complete();
                    }
                }
            }
        }

        public void Add(AccessPointFlow apf, int accessPointId)
        {
            using (var session = _helperWorker.ReadAndWrite().OpenSession())
            {
                using (session.Activate())
                {
                    using (var transaction = session.OpenTransaction())
                    {
                        SetProviders(session);

                        _service.Add(apf, accessPointId);
                        transaction.Complete();
                    }
                }
            }
        }
    }
}