﻿using System.Collections.Generic;
using Orms.DataAccessDataObjectsNet.Providers;
using Orms.DataAccessHelper;
using Orms.DataAccessHelper.Operations.Interfaces;
using Orms.Models;
using Orms.Services;
using Orms.Services.Interfaces;
using Xtensive.Orm;
using Session = Xtensive.Orm.Session;

namespace Orms.DataAccessDataObjectsNet.Operations
{
    public class ContractorOperation : Operation<Session>, IContractorOperation
    {
        private IContractorService _service;
        private readonly SessionWorker<Domain> _helper;

        public ContractorOperation(SessionWorker<Domain> helper)
        {
            _helper = helper;
        }

        protected override void SetProviders(Session session)
        {
            _service = new ContractorService(
                new ContractorContactProvider(session),
                new ContractorProvider(session));
        }

        public IEnumerable<Contractor> GetAll()
        {
            using (var session = _helper.Read().OpenSession())
            {
                using (session.Activate())
                {
                    using (session.OpenTransaction())
                    {
                        SetProviders(session);

                        return _service.GetAll();
                    }
                }
            }
        }

        public IEnumerable<ContractorContact> GetAll(Contractor contractor)
        {
            using (var session = _helper.Read().OpenSession())
            {
                using (session.Activate())
                {
                    using (session.OpenTransaction())
                    {
                        SetProviders(session);

                        return _service.GetAll(contractor);

                    }
                }
            }
        }

        public Contractor Get(int id)
        {
            using (var session = _helper.Read().OpenSession())
            {
                using (session.Activate())
                {
                    using (session.OpenTransaction())
                    {
                        SetProviders(session);

                        return _service.Get(id);
                    }
                }
            }
        }

        public ContractorContact GetContact(int id)
        {
            using (var session = _helper.ReadAndWrite().OpenSession())
            {
                using (session.Activate())
                {
                    using (session.OpenTransaction())
                    {
                        SetProviders(session);

                        return _service.GetContact(id);
                    }
                }
            }
        }

        public void Update(ContractorContact contact)
        {
            using (var session = _helper.ReadAndWrite().OpenSession())
            {
                using (session.Activate())
                {
                    using (var transaction = session.OpenTransaction())
                    {
                        SetProviders(session);

                        _service.Update(contact);
                        transaction.Complete();
                    }
                }
            }
        }

        public void Add(Contractor contractor)
        {
            using (var session = _helper.ReadAndWrite().OpenSession())
            {
                using (session.Activate())
                {
                    using (var transaction = session.OpenTransaction())
                    {
                        SetProviders(session);

                        _service.Add(contractor);
                        transaction.Complete();
                    }
                }
            }
        }

        public void Add(ContractorContact contact, int contractorId)
        {
            using (var session = _helper.ReadAndWrite().OpenSession())
            {
                using (session.Activate())
                {
                    using (var transaction = session.OpenTransaction())
                    {
                        SetProviders(session);

                        _service.Add(contact, contractorId);
                        transaction.Complete();
                    }
                }
            }
        }
    }
}