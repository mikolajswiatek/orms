﻿using System.Collections.Generic;
using Orms.DataAccessDataObjectsNet.Providers;
using Orms.DataAccessHelper;
using Orms.DataAccessHelper.Operations.Interfaces;
using Orms.Models;
using Orms.Services;
using Orms.Services.Interfaces;
using Xtensive.Orm;
using Session = Xtensive.Orm.Session;

namespace Orms.DataAccessDataObjectsNet.Operations
{
    public class GasStorageOperation : Operation<Session>, IGasStorageOperation
    {
        private IGasStorageService _service;
        private readonly SessionWorker<Domain> _helper;

        public GasStorageOperation(SessionWorker<Domain> helper)
        {
            _helper = helper;
        }

        protected override void SetProviders(Session session)
        {
            _service = new GasStorageService(
                new GasStorageProvider(session));
        }

        public void Add(GasStorage gasStorage)
        {
            using (var session = _helper.ReadAndWrite().OpenSession())
            {
                using (session.Activate())
                {
                    using (var transaction = session.OpenTransaction())
                    {
                        SetProviders(session);

                        _service.Add(gasStorage);
                        transaction.Complete();
                    }
                }
            }
        }

        public GasStorage Get(int id)
        {
            using (var session = _helper.Read().OpenSession())
            {
                using (session.Activate())
                {
                    using (session.OpenTransaction())
                    {
                        SetProviders(session);

                        return _service.Get(id);
                    }
                }
            }
        }

        public IEnumerable<GasStorage> GetAll()
        {
            using (var session = _helper.Read().OpenSession())
            {
                using (session.Activate())
                {
                    using (session.OpenTransaction())
                    {
                        SetProviders(session);

                        return _service.GetAll();
                    }
                }
            }
        }
    }
}