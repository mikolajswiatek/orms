﻿using System;
using System.Collections.Generic;
using Orms.DataAccessDataObjectsNet.Providers;
using Orms.DataAccessHelper;
using Orms.DataAccessHelper.Operations.Interfaces;
using Orms.Models;
using Orms.Services;
using Orms.Services.Interfaces;
using Xtensive.Orm;
using Session = Xtensive.Orm.Session;

namespace Orms.DataAccessDataObjectsNet.Operations
{
    public class CalorificValueOperation : Operation<Session>, ICalorificValueOperation
    {
        private ICalorificValueService _service;
        private readonly SessionWorker<Domain> _helper;

        public CalorificValueOperation(SessionWorker<Domain> helper)
        {
            _helper = helper;
        }

        protected override void SetProviders(Session session)
        {
            _service = new CalorificValueService(new CalorificValueProvider(session));
        }

        public void Add(CalorificValue cv, IEnumerable<EnergyCalorificValue> values)
        {
            using (var session = _helper.ReadAndWrite().OpenSession())
            {
                using (session.Activate())
                {
                    using (var transaction = session.OpenTransaction())
                    {
                        SetProviders(session);

                        _service.Add(cv, values);
                        transaction.Complete();
                    }
                }
            }
        }

        public CalorificValue GetLastBy(GasStorage gasStorage)
        {
            using (var session = _helper.Read().OpenSession())
            {
                using (session.Activate())
                {
                    using (session.OpenTransaction())
                    {
                        SetProviders(session);

                        return _service.GetLastBy(gasStorage);
                    }
                }
            }
        }

        public CalorificValue GetForGasDay(GasStorage gasStorage, DateTime gasDay)
        {
            using (var session = _helper.Read().OpenSession())
            {
                using (session.Activate())
                {
                    using (session.OpenTransaction())
                    {
                        SetProviders(session);

                        return _service.GetForGasDay(gasStorage, gasDay);
                    }
                }
            }
        }

        public CalorificValue Get(int id)
        {
            using (var session = _helper.Read().OpenSession())
            {
                using (session.Activate())
                {
                    using (session.OpenTransaction())
                    {
                        SetProviders(session);

                        return _service.Get(id);
                    }
                }
            }
        }
    }
}