﻿using System;
using System.Collections.Generic;
using Orms.DataAccessDataObjectsNet.Providers;
using Orms.DataAccessHelper;
using Orms.DataAccessHelper.Operations.Interfaces;
using Orms.Models;
using Orms.Services;
using Orms.Services.Interfaces;
using Xtensive.Orm;
using Session = Xtensive.Orm.Session;

namespace Orms.DataAccessDataObjectsNet.Operations
{
    public class GasAccountOperation : Operation<Session>, IGasAccountOperation
    {
        private IGasAccountService _service;
        private readonly SessionWorker<Domain> _helper;

        public GasAccountOperation(SessionWorker<Domain> helper)
        {
            _helper = helper;
        }

        protected override void SetProviders(Session session)
        {
            _service = new GasAccountService(
                new GasAccountProvider(session),
                new GasStorageProvider(session),
                new NominationProvider(session));
        }

        public void Update(IEnumerable<GasAccount> gasAccounts)
        {
            using (var session = _helper.ReadAndWrite().OpenSession())
            {
                using (session.Activate())
                {
                    using (var transaction = session.OpenTransaction())
                    {
                        SetProviders(session);

                        _service.Update(gasAccounts);
                        transaction.Complete();
                    }
                }
            }
        }

        public IEnumerable<GasAccount> Get(DateTime gasDay)
        {
            using (var session = _helper.Read().OpenSession())
            {
                using (session.Activate())
                {
                    using (session.OpenTransaction())
                    {
                        SetProviders(session);

                        return _service.Get(gasDay);
                    }
                }
            }
        }

        public IEnumerable<GasAccount> Get(GasStorage gasStorage, DateTime gasDay)
        {
            using (var session = _helper.Read().OpenSession())
            {
                using (session.Activate())
                {
                    using (session.OpenTransaction())
                    {
                        SetProviders(session);

                        return _service.Get(gasStorage, gasDay);
                    }
                }
            }
        }

        public void Refresh(GasStorage gasStorage)
        {
            using (var session = _helper.ReadAndWrite().OpenSession())
            {
                using (session.Activate())
                {
                    using (var transaction = session.OpenTransaction())
                    {
                        SetProviders(session);

                        _service.Refresh(gasStorage);
                        transaction.Complete();
                    }
                }
            }
        }

        public void Refresh()
        {
            using (var session = _helper.ReadAndWrite().OpenSession())
            {
                using (session.Activate())
                {
                    using (var transaction = session.OpenTransaction())
                    {
                        SetProviders(session);

                        _service.Refresh();
                        transaction.Complete();
                    }
                }
            }
        }

        public void Refresh(GasStorage gasStorage, GasDay gasDay)
        {
            using (var session = _helper.ReadAndWrite().OpenSession())
            {
                using (session.Activate())
                {
                    using (var transaction = session.OpenTransaction())
                    {
                        SetProviders(session);

                        _service.Refresh(gasStorage, gasDay);
                        transaction.Complete();
                    }
                }
            }
        }

        public void Refresh(GasDay gasDay)
        {
            using (var session = _helper.ReadAndWrite().OpenSession())
            {
                using (session.Activate())
                {
                    using (var transaction = session.OpenTransaction())
                    {
                        SetProviders(session);

                        _service.Refresh(gasDay);
                        transaction.Complete();
                    }
                }
            }
        }
    }
}