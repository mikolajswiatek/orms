﻿using Orms.DataAccessDataObjectsNet.EntityClasses;
using Xtensive.Orm;

namespace Orms.DataAccessDataObjectsNet.Utils
{
    public class TempEntity : Entity
    {
        public EntitySet<AccessPointEntity> AccessPoints { get; set; }

        public EntitySet<AccessPointFlowEntity> AccessPointFlows { get; set; }

        public EntitySet<AgreementEntity> Agreements { get; set; }

        public EntitySet<ContractorContactEntity> Contacts { get; set; }

        public EntitySet<EnergyCalorificValueEntity> EnergyCalorificValues { get; set; }

        public EntitySet<NominationValueEntity> NominationValues { get; set; }

        public EntitySet<UserEntity> Users { get; set; }
    }
}