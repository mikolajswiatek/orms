﻿using Orms.DataAccessDataObjectsNet.Repositories;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;
using Orms.Providers;
using System;
using System.Collections.Generic;
using Session = Xtensive.Orm.Session;

namespace Orms.DataAccessDataObjectsNet.Providers
{
    public class NominationProvider : INominationProvider
    {
        private readonly INominationRepository<Session> _repository;
        private readonly Session _session;

        public NominationProvider(Session session)
        {
            _session = session;
            _repository = new NominationRepository();
        }

        public IEnumerable<Nomination> GetAll()
        {
            return _repository.GetAll(_session);
        }

        public Nomination Get(int id)
        {
            return _repository.Get(_session, id);
        }

        public void Update(Nomination nomination)
        {
            _repository.Update(_session, nomination);
        }

        public void Add(Nomination nomination)
        {
            _repository.Add(_session, nomination);
        }

        public IEnumerable<Nomination> GetBy(Contractor contractor)
        {
            return _repository.GetBy(_session, contractor);
        }

        public IEnumerable<Nomination> GetBy(GasStorage gasStorage)
        {
            return _repository.GetBy(_session, gasStorage);
        }

        public IEnumerable<Nomination> GetBy(User user)
        {
            return _repository.GetBy(_session, user);
        }

        public IEnumerable<Nomination> GetBy(DateTime gasDay)
        {
            return _repository.GetBy(_session, gasDay);
        }

        public Nomination GetLast(Contractor contractor)
        {
            return _repository.GetLast(_session, contractor);
        }

        public Nomination GetLast(GasStorage gasStorage)
        {
            return _repository.GetLast(_session, gasStorage);
        }

        public Nomination GetLast(User user)
        {
            return _repository.GetLast(_session, user);
        }

        public Nomination GetLast(Contractor contractor, DateTime gasDay)
        {
            return _repository.GetLast(_session, contractor, gasDay);
        }

        public IEnumerable<Nomination> GetForGasDay(Contractor contractor, DateTime gasDay)
        {
            return _repository.GetForGasDay(_session, contractor, gasDay);
        }

        public IEnumerable<Nomination> GetForGasDay(GasStorage gasStorage, DateTime gasDay)
        {
            return _repository.GetForGasDay(_session, gasStorage, gasDay);
        }

        public IEnumerable<Nomination> GetForGasDay(Contractor contractor, GasStorage gasStorage, DateTime gasDay)
        {
            return _repository.GetForGasDay(_session, contractor, gasStorage, gasDay);
        }
    }
}