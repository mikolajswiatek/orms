﻿using System.Collections.Generic;
using Orms.DataAccessDataObjectsNet.Repositories;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;
using Orms.Providers;
using Session = Xtensive.Orm.Session;

namespace Orms.DataAccessDataObjectsNet.Providers
{
    public class AccessPointProvider : IAccessPointProvider
    {
        private readonly IAccessPointRepository<Session> _repository;
        private readonly Session _session;

        public AccessPointProvider(Session session)
        {
            _session = session;
            _repository = new AccessPointRepository();
        }

        public IEnumerable<AccessPoint> GetAll()
        {
            return _repository.GetAll(_session);
        }

        public AccessPoint Get(int id)
        {
            return _repository.Get(_session, id);
        }

        public void Update(AccessPoint accessPoint)
        {
            _repository.Update(_session, accessPoint);
        }

        public void Add(AccessPoint accessPoint)
        {
            _repository.Add(_session, accessPoint);
        }

        public IEnumerable<AccessPoint> GetBy(GasStorage gasStorage)
        {
            return _repository.GetBy(_session, gasStorage);
        }
    }
}