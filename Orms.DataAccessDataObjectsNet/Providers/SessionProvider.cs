﻿using System;
using Orms.DataAccessDataObjectsNet.Repositories;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Providers;
using System.Collections.Generic;
using Xtensive.Orm;

namespace Orms.DataAccessDataObjectsNet.Providers
{
    public class SessionProvider : ISessionProvider
    {
        private readonly ISessionRepository<Session> _repository;
        private readonly Session _session;

        public SessionProvider(Session session)
        {
            _session = session;
            _repository = new SessionRepository();
        }

        public IEnumerable<Models.Session> GetAll()
        {
            return _repository.GetAll(_session);
        }

        public Models.Session Get(int id)
        {
            return _repository.Get(_session, id);
        }

        public void Update(Models.Session s)
        {
            if (s == null) throw new ArgumentNullException(nameof(s));

            _repository.Update(_session, s);
        }

        public void Add(Models.Session s)
        {
            if (s == null) throw new ArgumentNullException(nameof(s));

            _repository.Add(_session, s);
        }

        public Models.Session GetBy(string guid)
        {
            return _repository.GetBy(_session, guid);
        }

        public void Delete(Models.Session s)
        {
            _repository.Delete(_session, s);
        }
    }
}