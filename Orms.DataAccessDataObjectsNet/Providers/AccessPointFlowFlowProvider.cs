﻿using System.Collections.Generic;
using Orms.DataAccessDataObjectsNet.Repositories;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;
using Orms.Providers;
using Session = Xtensive.Orm.Session;

namespace Orms.DataAccessDataObjectsNet.Providers
{
    public class AccessPointFlowProvider : IAccessPointFlowProvider
    {
        private readonly IAccessPointFlowRepository<Session> _repository;
        private readonly Session _session;

        public AccessPointFlowProvider(Session session)
        {
            _session = session;
            _repository = new AccessPointFlowRepository();
        }

        public IEnumerable<AccessPointFlow> GetAll()
        {
            return _repository.GetAll(_session);
        }

        public AccessPointFlow Get(int id)
        {
            return _repository.Get(_session, id);
        }

        public void Update(AccessPointFlow accessPointFlow)
        {
            _repository.Update(_session, accessPointFlow);
        }

        public void Add(AccessPointFlow accessPointFlow)
        {
            _repository.Add(_session, accessPointFlow);
        }
    }
}