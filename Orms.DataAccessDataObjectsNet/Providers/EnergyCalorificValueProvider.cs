﻿using System;
using System.Collections.Generic;
using Orms.DataAccessDataObjectsNet.Repositories;
using Orms.DataAccessHelper;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;
using Orms.Providers;
using Xtensive.Orm;
using Session = Xtensive.Orm.Session;

namespace Orms.DataAccessDataObjectsNet.Providers
{
    public class EnergyCalorificValueProvider : IEnergyCalorificValueProvider
    {
        private readonly IEnergyCalorificValueRepository<Session> _repository;
        private readonly Session _session;

        public EnergyCalorificValueProvider(Session session)
        {
            _session = session;
            _repository = new EnergyCalorificValueRepository();
        }

        public IEnumerable<EnergyCalorificValue> GetAll()
        {
            return _repository.GetAll(_session);
        }

        public EnergyCalorificValue Get(int id)
        {
            return _repository.Get(_session, id);
        }

        public void Update(EnergyCalorificValue ecv)
        {
            _repository.Update(_session, ecv);
        }

        public void Add(EnergyCalorificValue ecv)
        {
            _repository.Add(_session, ecv);
        }

        public void Add(IEnumerable<EnergyCalorificValue> ecvs)
        {
            _repository.Add(_session, ecvs);
        }

        public IEnumerable<EnergyCalorificValue> GetAll(CalorificValue cv)
        {
            return _repository.GetAll(_session, cv);
        }
    }
}