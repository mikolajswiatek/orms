﻿using System.Collections.Generic;
using Orms.DataAccessDataObjectsNet.Repositories;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;
using Orms.Providers;
using Session = Xtensive.Orm.Session;

namespace Orms.DataAccessDataObjectsNet.Providers
{
    public class AgreementProvider : IAgreementProvider
    {
        private readonly IAgreementRepository<Session> _repository;
        private readonly Session _session;

        public AgreementProvider(Session session)
        {
            _session = session;
            _repository = new AgreementRepository();
        }

        public IEnumerable<Agreement> GetAll()
        {
            return _repository.GetAll(_session);
        }

        public Agreement Get(int id)
        {
            return _repository.Get(_session, id);

        }

        public void Update(Agreement agreement)
        {
            _repository.Update(_session, agreement);

        }

        public void Add(Agreement agreement)
        {
            _repository.Add(_session, agreement);

        }

        public IEnumerable<Agreement> GetBy(Contractor contractor)
        {
            return _repository.GetBy(_session, contractor);

        }

        public IEnumerable<Agreement> GetBy(GasStorage gasStorage)
        {
            return _repository.GetBy(_session, gasStorage);

        }
    }
}