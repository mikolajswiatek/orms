﻿using System;
using System.Collections.Generic;
using Orms.DataAccessDataObjectsNet.Repositories;
using Orms.DataAccessHelper;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;
using Orms.Providers;
using Xtensive.Orm;
using Session = Xtensive.Orm.Session;

namespace Orms.DataAccessDataObjectsNet.Providers
{
    public class GasStorageProvider : IGasStorageProvider
    {
        private readonly IGasStorageRepository<Session> _repository;
        private readonly Session _session;

        public GasStorageProvider(Session session)
        {
            _session = session;
            _repository = new GasStorageRepository();
        }

        public IEnumerable<GasStorage> GetAll()
        {
            return _repository.GetAll(_session);
        }

        public GasStorage Get(int id)
        {
            return _repository.Get(_session, id);
        }

        public void Update(GasStorage gasStorage)
        {
            _repository.Update(_session, gasStorage);
        }

        public void Add(GasStorage gasStorage)
        {
            _repository.Add(_session, gasStorage);
        }
    }
}