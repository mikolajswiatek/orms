﻿using Orms.DataAccessDataObjectsNet.Repositories;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;
using Orms.Providers;
using System.Collections.Generic;
using Session = Xtensive.Orm.Session;

namespace Orms.DataAccessDataObjectsNet.Providers
{
    public class ContractorContactProvider : IContractorContactProvider
    {
        private readonly IContractorContactRepository<Session> _repository;
        private readonly Session _session;

        public ContractorContactProvider(Session session)
        {
            _session = session;
            _repository = new ContractorContactRepository();
        }

        public IEnumerable<ContractorContact> GetAll()
        {
            return _repository.GetAll(_session);
        }

        public ContractorContact Get(int id)
        {
            return _repository.Get(_session, id);
        }

        public void Update(ContractorContact contact)
        {
            _repository.Update(_session, contact);
        }

        public void Add(ContractorContact contact)
        {
            _repository.Add(_session, contact);
        }

        public IEnumerable<ContractorContact> GetByContractor(Contractor contractor)
        {
            return _repository.GetByContractor(_session, contractor);
        }
    }
}