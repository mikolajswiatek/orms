﻿using System;
using System.Collections.Generic;
using Orms.DataAccessDataObjectsNet.Repositories;
using Orms.DataAccessHelper;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;
using Orms.Providers;
using Xtensive.Orm;
using Session = Xtensive.Orm.Session;

namespace Orms.DataAccessDataObjectsNet.Providers
{
    public class UserProvider : IUserProvider
    {
        private readonly IUserRepository<Session> _repository;
        private readonly Session _session;

        public UserProvider(Session session)
        {
            _session = session;
            _repository = new UserRepository();
        }

        public IEnumerable<User> GetAll()
        {
            return _repository.GetAll(_session);
        }

        public User Get(int id)
        {
            return _repository.Get(_session, id);
        }

        public void Update(User user)
        {
            _repository.Update(_session, user);
        }

        public void Add(User user)
        {
            _repository.Add(_session, user);
        }

        public IEnumerable<User> GetBy(Contractor contractor)
        {
            return _repository.GetBy(_session, contractor);
        }

        public IEnumerable<User> GetBy(string nameOrEmail)
        {
            return _repository.GetBy(_session, nameOrEmail);
        }
    }
}