﻿using Orms.DataAccessDataObjectsNet.Repositories;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;
using Orms.Providers;
using System;
using System.Collections.Generic;
using Session = Xtensive.Orm.Session;

namespace Orms.DataAccessDataObjectsNet.Providers
{
    public class CalorificValueProvider : ICalorificValueProvider
    {
        private readonly ICalorificValueRepository<Session> _repository;
        private readonly Session _session;

        public CalorificValueProvider(Session session)
        {
            _session = session;
            _repository = new CalorificValueRepository();
        }

        public IEnumerable<CalorificValue> GetAll()
        {
            return _repository.GetAll(_session);
        }

        public CalorificValue Get(int id)
        {
            return _repository.Get(_session, id);
        }

        public void Update(CalorificValue calorificValue)
        {
            _repository.Update(_session, calorificValue);
        }

        public void Add(CalorificValue calorificValue)
        {
            _repository.Add(_session, calorificValue);
        }

        public CalorificValue GetLastBy(GasStorage gasStorage)
        {
            return _repository.GetLastBy(_session, gasStorage);
        }

        public CalorificValue GetForGasDay(GasStorage gasStorage, DateTime gasDay)
        {
            return _repository.GetForGasDay(_session, gasStorage, gasDay);
        }
    }
}