﻿using System;
using System.Collections.Generic;
using Orms.DataAccessDataObjectsNet.Repositories;
using Orms.DataAccessHelper;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;
using Orms.Providers;
using Xtensive.Orm;
using Session = Xtensive.Orm.Session;

namespace Orms.DataAccessDataObjectsNet.Providers
{
    public class GasAccountProvider : IGasAccountProvider
    {
        private readonly IGasAccountRepository<Session> _repository;
        private readonly Session _session;

        public GasAccountProvider(Session session)
        {
            _session = session;
            _repository = new GasAccountRepository();
        }

        public IEnumerable<GasAccount> GetAll()
        {
            return _repository.GetAll(_session);
        }

        public GasAccount Get(int id)
        {
            return _repository.Get(_session, id);
        }

        public void Update(GasAccount gasAccount)
        {
            _repository.Update(_session, gasAccount);
        }

        public void Add(GasAccount gasAccount)
        {
            _repository.Add(_session, gasAccount);
        }

        public void Add(IEnumerable<GasAccount> gasAccounts)
        {
            _repository.Add(_session, gasAccounts);
        }

        public void Update(IEnumerable<GasAccount> gasAccounts)
        {
            _repository.Update(_session, gasAccounts);
        }

        public IEnumerable<GasAccount> Get(DateTime gasDay)
        {
            return _repository.Get(_session, gasDay);
        }

        public IEnumerable<GasAccount> Get(GasStorage gasStorage, DateTime gasDay)
        {
            return _repository.Get(_session, gasStorage, gasDay);
        }
    }
}