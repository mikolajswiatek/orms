﻿using Orms.DataAccessDataObjectsNet.Repositories;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;
using Orms.Providers;
using System.Collections.Generic;
using Session = Xtensive.Orm.Session;

namespace Orms.DataAccessDataObjectsNet.Providers
{
    public class NominationValueProvider : INominationValueProvider
    {
        private readonly INominationValueRepository<Session> _repository;
        private readonly Session _session;

        public NominationValueProvider(Session session)
        {
            _session = session;
            _repository = new NominationValueRepository();
        }

        public IEnumerable<NominationValue> GetAll()
        {
            return _repository.GetAll(_session);
        }

        public NominationValue Get(int id)
        {
            return _repository.Get(_session, id);
        }

        public void Update(NominationValue nominationValue)
        {
            _repository.Update(_session, nominationValue);
        }

        public void Add(NominationValue nominationValue)
        {
            _repository.Add(_session, nominationValue);
        }

        public void Add(IEnumerable<NominationValue> nominationValues)
        {
            _repository.Add(_session, nominationValues);
        }

        public IEnumerable<NominationValue> GetBy(Nomination nomination)
        {
            return _repository.GetBy(_session, nomination);
        }
    }
}