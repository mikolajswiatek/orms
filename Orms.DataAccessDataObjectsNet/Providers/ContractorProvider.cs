﻿using Orms.DataAccessDataObjectsNet.Repositories;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;
using Orms.Providers;
using System.Collections.Generic;
using Session = Xtensive.Orm.Session;

namespace Orms.DataAccessDataObjectsNet.Providers
{
    public class ContractorProvider : IContractorProvider
    {
        private readonly IContractorRepository<Session> _repository;
        private readonly Session _session;

        public ContractorProvider(Session session)
        {
            _session = session;
            _repository = new ContractorRepository();
        }

        public IEnumerable<Contractor> GetAll()
        {
            return _repository.GetAll(_session);
        }

        public Contractor Get(int id)
        {
            return _repository.Get(_session, id);
        }

        public void Update(Contractor contractor)
        {
            _repository.Update(_session, contractor);
        }

        public void Add(Contractor contractor)
        {
            _repository.Add(_session, contractor);
        }
    }
}