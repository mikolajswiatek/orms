﻿using Orms.DataAccessDataObjectsNet.EntityClasses;
using Orms.Models;

namespace Orms.DataAccessDataObjectsNet.Converter
{
    public static partial class Converter
    {
        public static SessionEntity ToEntity(this Session session)
        {
            return new SessionEntity()
            {
                Guid = session.Guid,
                Ip = session.Ip,
                Browser = session.Browser,
                LastActivityDatetime = session.LastActivityDatetime,
                CreateDatetime = session.CreateDatetime
            };
        }

        public static SessionEntity ToEntity(this Session session, UserEntity user)
        {
            return new SessionEntity()
            {
                Guid = session.Guid,
                User = user,
                Ip = session.Ip,
                Browser = session.Browser,
                LastActivityDatetime = session.LastActivityDatetime,
                CreateDatetime = session.CreateDatetime
            };
        }

        public static Session ToModel(this SessionEntity session)
        {
            if (session == null) return null;

            return new Session()
            {
                Id = session.Id,
                Guid = session.Guid,
                User = session.User.ToModel(),
                Ip = session.Ip,
                Browser = session.Browser,
                LastActivityDatetime = session.LastActivityDatetime,
                CreateDatetime = session.CreateDatetime,
            };
        }
    }
}