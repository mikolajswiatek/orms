﻿using Orms.DataAccessDataObjectsNet.EntityClasses;
using Orms.Models;

namespace Orms.DataAccessDataObjectsNet.Converter
{
    public static partial class Converter
    {
        public static UserEntity ToEntity(this User user)
        {
            return new UserEntity()
            {
                Name = user.Name,
                CreateDatetime = user.CreateDateTime,
                Email = user.Email,
                IsActive = user.IsActive,
                LastActivityDatetime = user.LastActivityDateTime,
                Password = user.Password
            };
        }

        public static UserEntity ToEntity(this User user, ContractorEntity contractor)
        {
            return new UserEntity()
            {
                Name = user.Name,
                Contractor = contractor,
                CreateDatetime = user.CreateDateTime,
                Email = user.Email,
                IsActive = user.IsActive,
                LastActivityDatetime = user.LastActivityDateTime,
                Password = user.Password
            };
        }

        public static User ToModel(this UserEntity user)
        {
            return new User()
            {
                Id = user.Id,
                Name = user.Name,
                Contractor = user.Contractor.ToModel(),
                CreateDateTime = user.CreateDatetime,
                Email = user.Email,
                IsActive = user.IsActive,
                LastActivityDateTime = user.LastActivityDatetime,
                Password = user.Password,
            };
        }
    }
}