﻿using System.Linq;
using Orms.DataAccessDataObjectsNet.EntityClasses;
using Orms.Models;
using Xtensive.Orm;

namespace Orms.DataAccessDataObjectsNet.Converter
{
    public static partial class Converter
    {
        public static NominationEntity ToEntity(this Nomination nomination)
        {
            var result = new NominationEntity()
            {
                Name = nomination.Name,
                Channel = nomination.Channel,
                GasDay = nomination.GasDay,
                MajorVersion = nomination.MajorVersion,
                Version = nomination.Version,
                IsAccepted = nomination.IsAccepted,
                Comment = nomination.Comment,
                IsLastOrder = nomination.IsLast,
                CreateDateTime = nomination.CreateDateTime,
            };

            return result;
        }

        public static NominationEntity ToEntity(this Nomination nomination, NominationEntity parentNomination)
        {
            var result = new NominationEntity()
            {
                Name = nomination.Name,
                Channel = nomination.Channel,
                GasDay = nomination.GasDay,
                MajorVersion = nomination.MajorVersion,
                Version = nomination.Version,
                ParentNomination = parentNomination,
                IsAccepted = nomination.IsAccepted,
                Comment = nomination.Comment,
                CalorificValue = nomination.CalorificValue.ToEntity(),
                Contractor = nomination.Contractor.ToEntity(),
                IsLastOrder = nomination.IsLast,
                CreateDateTime = nomination.CreateDateTime,
                Creator = nomination.Creator.ToEntity(),
                AccessPointFlow = nomination.AccessPointFlow.ToEntity()
            };

            if (nomination.Values != null) result.Values.AddRange(nomination.Values.Select(nv => nv.ToEntity()));

            return result;
        }

        public static NominationEntity ToEntity(this Nomination nomination, CalorificValueEntity cv)
        {
            var result = new NominationEntity()
            {
                Name = nomination.Name,
                Channel = nomination.Channel,
                GasDay = nomination.GasDay,
                MajorVersion = nomination.MajorVersion,
                Version = nomination.Version,
                ParentNomination = nomination.ParentNomination.ToEntity(),
                IsAccepted = nomination.IsAccepted,
                Comment = nomination.Comment,
                CalorificValue = cv,
                Contractor = nomination.Contractor.ToEntity(),
                IsLastOrder = nomination.IsLast,
                CreateDateTime = nomination.CreateDateTime,
                Creator = nomination.Creator.ToEntity(),
                AccessPointFlow = nomination.AccessPointFlow.ToEntity()
            };

            if (nomination.Values != null) result.Values.AddRange(nomination.Values.Select(nv => nv.ToEntity()));

            return result;
        }

        public static NominationEntity ToEntity(this Nomination nomination, ContractorEntity contractor)
        {
            var result = new NominationEntity()
            {
                Name = nomination.Name,
                Channel = nomination.Channel,
                GasDay = nomination.GasDay,
                MajorVersion = nomination.MajorVersion,
                Version = nomination.Version,
                ParentNomination = nomination.ParentNomination.ToEntity(),
                IsAccepted = nomination.IsAccepted,
                Comment = nomination.Comment,
                CalorificValue = nomination.CalorificValue.ToEntity(),
                Contractor = contractor,
                IsLastOrder = nomination.IsLast,
                CreateDateTime = nomination.CreateDateTime,
                Creator = nomination.Creator.ToEntity(),
                AccessPointFlow = nomination.AccessPointFlow.ToEntity()
            };

            if (nomination.Values != null) result.Values.AddRange(nomination.Values.Select(nv => nv.ToEntity()));

            return result;
        }

        public static NominationEntity ToEntity(this Nomination nomination, UserEntity creator)
        {
            var result = new NominationEntity()
            {
                Name = nomination.Name,
                Channel = nomination.Channel,
                GasDay = nomination.GasDay,
                MajorVersion = nomination.MajorVersion,
                Version = nomination.Version,
                ParentNomination = nomination.ParentNomination.ToEntity(),
                IsAccepted = nomination.IsAccepted,
                Comment = nomination.Comment,
                CalorificValue = nomination.CalorificValue.ToEntity(),
                Contractor = nomination.Contractor.ToEntity(),
                IsLastOrder = nomination.IsLast,
                CreateDateTime = nomination.CreateDateTime,
                Creator = creator,
                AccessPointFlow = nomination.AccessPointFlow.ToEntity()
            };

            if (nomination.Values != null) result.Values.AddRange(nomination.Values.Select(nv => nv.ToEntity()));

            return result;
        }

        public static NominationEntity ToEntity(this Nomination nomination, AccessPointFlowEntity accessPoint)
        {
            var result = new NominationEntity()
            {
                Name = nomination.Name,
                Channel = nomination.Channel,
                GasDay = nomination.GasDay,
                MajorVersion = nomination.MajorVersion,
                Version = nomination.Version,
                ParentNomination = nomination.ParentNomination.ToEntity(),
                IsAccepted = nomination.IsAccepted,
                Comment = nomination.Comment,
                CalorificValue = nomination.CalorificValue.ToEntity(),
                Contractor = nomination.Contractor.ToEntity(),
                IsLastOrder = nomination.IsLast,
                CreateDateTime = nomination.CreateDateTime,
                Creator = nomination.Creator.ToEntity(),
                AccessPointFlow = accessPoint
            };

            if (nomination.Values != null) result.Values.AddRange(nomination.Values.Select(nv => nv.ToEntity()));

            return result;
        }

        public static NominationEntity ToEntity(this Nomination nomination, EntitySet<NominationValueEntity> values)
        {
            var result = new NominationEntity()
            {
                Name = nomination.Name,
                Channel = nomination.Channel,
                GasDay = nomination.GasDay,
                MajorVersion = nomination.MajorVersion,
                Version = nomination.Version,
                ParentNomination = nomination.ParentNomination.ToEntity(),
                IsAccepted = nomination.IsAccepted,
                Comment = nomination.Comment,
                CalorificValue = nomination.CalorificValue.ToEntity(),
                Contractor = nomination.Contractor.ToEntity(),
                IsLastOrder = nomination.IsLast,
                CreateDateTime = nomination.CreateDateTime,
                Creator = nomination.Creator.ToEntity(),
                AccessPointFlow = nomination.AccessPointFlow.ToEntity(),
                Values = values
            };

            return result;
        }

        public static NominationEntity ToEntity(
            this Nomination nomination,
            NominationEntity parentNomination,
            CalorificValueEntity cv,
            ContractorEntity contractor,
            UserEntity creator,
            AccessPointFlowEntity accessPoint,
            EntitySet<NominationValueEntity> values)
        {
            return new NominationEntity()
            {
                Name = nomination.Name,
                Channel = nomination.Channel,
                GasDay = nomination.GasDay,
                MajorVersion = nomination.MajorVersion,
                Version = nomination.Version,
                ParentNomination = parentNomination,
                IsAccepted = nomination.IsAccepted,
                Comment = nomination.Comment,
                CalorificValue = cv,
                Contractor = contractor,
                IsLastOrder = nomination.IsLast,
                CreateDateTime = nomination.CreateDateTime,
                Creator = creator,
                AccessPointFlow = accessPoint,
                Values = values
            };
        }

        public static Nomination ToModel(this NominationEntity nomination)
        {
            return new Nomination()
            {
                Id = nomination.Id,
                Name = nomination.Name,
                AccessPointFlow = nomination.AccessPointFlow.ToModel(),
                Channel = nomination.Channel,
                Contractor = nomination.Contractor.ToModel(),
                GasDay = nomination.GasDay,
                MajorVersion = nomination.MajorVersion,
                Version = nomination.Version,
                ParentNomination = nomination.ParentNomination?.ToModel(),
                Creator = nomination.Creator.ToModel(),
                IsLast = nomination.IsLastOrder,
                IsAccepted = nomination.IsAccepted,
                Comment = nomination.Comment,
                CreateDateTime = nomination.CreateDateTime
            };
        }
    }
}