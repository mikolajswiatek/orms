﻿using System.Collections.Generic;
using System.Linq;
using Orms.DataAccessDataObjectsNet.EntityClasses;
using Orms.Models;
using Xtensive.Orm;

namespace Orms.DataAccessDataObjectsNet.Converter
{
    public static partial class Converter
    {
        public static AccessPointEntity ToEntity(this AccessPoint accessPoint)
        {
            var result = new AccessPointEntity()
            {
                Code = accessPoint.Code
            };

            return result;
        }

        public static AccessPointEntity ToEntity(this AccessPoint accessPoint, GasStorageEntity gasStorage, EntitySet<AccessPointFlowEntity> accessPointFlow)
        {
            return new AccessPointEntity()
            {
                Code = accessPoint.Code,
                GasStorage = gasStorage,
                AccessPointFlows = accessPointFlow
            };
        }

        public static AccessPointEntity ToEntity(this AccessPoint accessPoint, GasStorageEntity gasStorage, IEnumerable<AccessPointFlow> accessPointFlow)
        {
            var result = new AccessPointEntity()
            {
                Code = accessPoint.Code,
                GasStorage = gasStorage,
            };

            if (accessPoint.AccessPointFlows != null) result.AccessPointFlows.AddRange(accessPoint.AccessPointFlows.Select(apf => apf.ToEntity()));

            return result;
        }

        public static AccessPoint ToModel(this AccessPointEntity accessPoint)
        {
            return new AccessPoint()
            {
                Id = accessPoint.Id,
                Code = accessPoint.Code,
                GasStorage = accessPoint.GasStorage.ToModel()
            };
        }
    }
}