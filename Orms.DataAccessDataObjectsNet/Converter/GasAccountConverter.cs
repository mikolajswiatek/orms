﻿using Orms.DataAccessDataObjectsNet.EntityClasses;
using Orms.Domains.Units;
using Orms.Models;

namespace Orms.DataAccessDataObjectsNet.Converter
{
    public static partial class Converter
    {
        public static GasAccountEntity ToEntity(this GasAccount gasAccount)
        {
            return new GasAccountEntity()
            {
                GasDay = gasAccount.GasDay,
                Energy = new EnergyUnitValue(gasAccount.Energy.Value, gasAccount.Energy.Unit),
                Volume = new VolumeUnitValue(gasAccount.Volume.Value, gasAccount.Volume.Unit),
                HourIndex = gasAccount.HourIndex,
            };
        }

        public static GasAccountEntity ToEntity(this GasAccount gasAccount, GasStorageEntity gasStorage)
        {
            return new GasAccountEntity()
            {
                GasDay = gasAccount.GasDay,
                Energy = new EnergyUnitValue(gasAccount.Energy.Value, gasAccount.Energy.Unit),
                Volume = new VolumeUnitValue(gasAccount.Volume.Value, gasAccount.Volume.Unit),
                HourIndex = gasAccount.HourIndex,
                GasStorage = gasStorage
            };
        }

        public static GasAccount ToModel(this GasAccountEntity gasAccount)
        {
            return new GasAccount()
            {
                Id = gasAccount.Id,
                GasDay = gasAccount.GasDay,
                Energy = new UnitValue(gasAccount.Energy.Value, gasAccount.Energy.Unit),
                Volume = new UnitValue(gasAccount.Volume.Value, gasAccount.Volume.Unit),
                GasStorage = gasAccount.GasStorage.ToModel(),
                HourIndex = gasAccount.HourIndex
            };
        }
    }
}