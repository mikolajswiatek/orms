﻿using System.Collections.Generic;
using System.Linq;
using Orms.DataAccessDataObjectsNet.EntityClasses;
using Orms.Models;
using Xtensive.Orm;

namespace Orms.DataAccessDataObjectsNet.Converter
{
    public static partial class Converter
    {
        public static ContractorEntity ToEntity(this Contractor contractor)
        {
            var result = new ContractorEntity()
            {
                Name = contractor.Name,
                Adress = contractor.Adress,
                Code = contractor.Code
            };

            return result;
        }

        public static ContractorEntity ToEntity(
            this Contractor contractor,
            EntitySet<AgreementEntity> agreements,
            EntitySet<ContractorContactEntity> contacts,
            EntitySet<UserEntity> users)
        {
            var result = new ContractorEntity()
            {
                Name = contractor.Name,
                Adress = contractor.Adress,
                Code = contractor.Code
            };

            if (agreements != null) result.Agreements = agreements;
            if (contacts != null) result.Contacts = contacts;
            if (users != null) result.Users = users;

            return result;
        }

        public static Contractor ToModel(this ContractorEntity contractor)
        {
            return new Contractor()
            {
                Id = contractor.Id,
                Name = contractor.Name,
                Adress = contractor.Adress,
                Code = contractor.Code
            };
        }
    }
}