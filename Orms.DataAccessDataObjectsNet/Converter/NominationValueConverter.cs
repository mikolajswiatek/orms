﻿using Orms.DataAccessDataObjectsNet.EntityClasses;
using Orms.Domains.Units;
using Orms.Models;

namespace Orms.DataAccessDataObjectsNet.Converter
{
    public static partial class Converter
    {
        public static NominationValueEntity ToEntity(this NominationValue value)
        {
            return new NominationValueEntity()
            {
                Energy = new EnergyUnitValue(value.Energy.Value, value.Energy.Unit),
                Volume = new VolumeUnitValue(value.Volume.Value, value.Volume.Unit),
                HourIndex = value.HourIndex,
            };
        }

        public static NominationValueEntity ToEntity(this NominationValue value, NominationEntity nomination)
        {
            return new NominationValueEntity()
            {
                Energy = new EnergyUnitValue(value.Energy.Value, value.Energy.Unit),
                Volume = new VolumeUnitValue(value.Volume.Value, value.Volume.Unit),
                HourIndex = value.HourIndex,
                Nomination = nomination
            };
        }

        public static NominationValue ToModel(this NominationValueEntity value)
        {
            return new NominationValue()
            {
                Id = value.Id,
                Nomination = value.Nomination.ToModel(),
                Energy = new UnitValue(value.Energy.Value, value.Energy.Unit),
                Volume = new UnitValue(value.Volume.Value, value.Volume.Unit),
                HourIndex = value.HourIndex
            };
        }
    }
}