﻿using Orms.DataAccessDataObjectsNet.EntityClasses;
using Orms.Models;

namespace Orms.DataAccessDataObjectsNet.Converter
{
    public static partial class Converter
    {
        public static AccessPointFlowEntity ToEntity(this AccessPointFlow accessPointFlow)
        {
            return new AccessPointFlowEntity()
            {
                Direction = accessPointFlow.Direction,
                //AccessPoint = accessPointFlow.AccessPoint.ToEntity()
            };
        }

        public static AccessPointFlowEntity ToEntity(this AccessPointFlow accessPointFlow, AccessPointEntity accessPoint)
        {
            return new AccessPointFlowEntity()
            {
                Direction = accessPointFlow.Direction,
                AccessPoint = accessPoint
            };
        }

        public static AccessPointFlow ToModel(this AccessPointFlowEntity accessPoint)
        {
            return new AccessPointFlow()
            {
                Id = accessPoint.Id,
                Direction = accessPoint.Direction,
                AccessPoint = accessPoint.AccessPoint.ToModel()
            };
        }
    }
}