﻿using System.Linq;
using Orms.DataAccessDataObjectsNet.EntityClasses;
using Orms.Models;
using Xtensive.Orm;

namespace Orms.DataAccessDataObjectsNet.Converter
{
    public static partial class Converter
    {
        public static GasStorageEntity ToEntity(this GasStorage gasStorage)
        {
            var result = new GasStorageEntity()
            {
                Name = gasStorage.Name,
            };

            return result;
        }

        public static GasStorageEntity ToEntity(this GasStorage gasStorage, EntitySet<AccessPointEntity> accessPoints)
        {
            var result = new GasStorageEntity()
            {
                Name = gasStorage.Name,
                AccessPoints = accessPoints,
            };

            return result;
        }

        public static GasStorageEntity ToEntity(this GasStorage gasStorage, EntitySet<AgreementEntity> agreements)
        {
            var result = new GasStorageEntity()
            {
                Name = gasStorage.Name,
                Agreements = agreements
            };

            if (gasStorage.AccessPoints != null) result.AccessPoints.AddRange(gasStorage.AccessPoints.Select(ap => ap.ToEntity()));

            return result;
        }

        public static GasStorageEntity ToEntity(this GasStorage gasStorage, EntitySet<AccessPointEntity> accessPoints, EntitySet<AgreementEntity> agreements)
        {
            return new GasStorageEntity()
            {
                Name = gasStorage.Name,
                AccessPoints = accessPoints,
                Agreements = agreements
            };
        }

        public static GasStorage ToModel(this GasStorageEntity gasStorage)
        {
            return new GasStorage()
            {
                Id = gasStorage.Id,
                Name = gasStorage.Name,
                //AccessPoints = gasStorage.AccessPoints.Select(ap => ap.ToModel())
            };
        }
    }
}