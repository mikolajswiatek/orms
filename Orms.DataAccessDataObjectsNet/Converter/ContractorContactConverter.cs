﻿using Orms.DataAccessDataObjectsNet.EntityClasses;
using Orms.Models;

namespace Orms.DataAccessDataObjectsNet.Converter
{
    public static partial class Converter
    {
        public static ContractorContactEntity ToEntity(this ContractorContact contact)
        {
            return new ContractorContactEntity()
            {
                Name = contact.Name,
                Adress = contact.Adress,
                Email = contact.Email,
                FirstName = contact.FirstName,
                LastName = contact.LastName,
                Phone = contact.Phone,
                //Contractor = contact.Contractor.ToEntity(),
            };
        }

        public static ContractorContactEntity ToEntity(this ContractorContact contact, ContractorEntity contractor)
        {
            return new ContractorContactEntity()
            {
                Name = contact.Name,
                Adress = contact.Adress,
                Email = contact.Email,
                FirstName = contact.FirstName,
                LastName = contact.LastName,
                Phone = contact.Phone,
                Contractor = contractor
            };
        }

        public static ContractorContact ToModel(this ContractorContactEntity contact)
        {
            return new ContractorContact()
            {
                Id = contact.Id,
                Name = contact.Name,
                Adress = contact.Adress,
                Email = contact.Email,
                Contractor = contact.Contractor.ToModel(),
                FirstName = contact.FirstName,
                LastName = contact.LastName,
                Phone = contact.Phone
            };
        }
    }
}