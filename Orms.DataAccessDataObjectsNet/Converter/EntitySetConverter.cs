﻿using System.Collections.Generic;
using System.Linq;
using Orms.DataAccessDataObjectsNet.EntityClasses;
using Orms.DataAccessDataObjectsNet.Utils;
using Orms.Models;
using Xtensive.Orm;

namespace Orms.DataAccessDataObjectsNet.Converter
{
    public partial class Converter
    {
        public static List<AccessPoint> ToModel(this EntitySet<AccessPointEntity> aps)
        {
            var result = new List<AccessPoint>();

            if (aps == null) return result;

            result.AddRange(aps.Select(ap => ap.ToModel()));

            return result;
        }

        public static List<AccessPointFlow> ToModel(this EntitySet<AccessPointFlowEntity> apfs)
        {
            var result = new List<AccessPointFlow>();

            if (apfs == null) return result;

            foreach (var apf in apfs.ToList())
            {
                result.Add(apf.ToModel());
            }

            return result;
        }

        public static List<Agreement> ToModel(this EntitySet<AgreementEntity> agreements)
        {
            var result = new List<Agreement>();

            if (agreements == null) return result;

            result.AddRange(agreements.Select(agreement => agreement.ToModel()));

            return result;
        }

        public static List<ContractorContact> ToModel(this EntitySet<ContractorContactEntity> contacts)
        {
            var result = new List<ContractorContact>();

            if (contacts == null) return result;

            result.AddRange(contacts.Select(contact => contact.ToModel()));

            return result;
        }

        public static List<EnergyCalorificValue> ToModel(this EntitySet<EnergyCalorificValueEntity> ecvs)
        {
            var result = new List<EnergyCalorificValue>();

            if (ecvs == null) return result;

            result.AddRange(ecvs.Select(ecv => ecv.ToModel()));

            return result;
        }

        public static List<NominationValue> ToModel(this EntitySet<NominationValueEntity> values)
        {
            var result = new List<NominationValue>();

            if (values == null) return result;

            result.AddRange(values.Select(value => value.ToModel()));

            return result;
        }

        public static List<User> ToModel(this EntitySet<UserEntity> users)
        {
            var result = new List<User>();

            if (users == null) return result;

            result.AddRange(users.Select(user => user.ToModel()));

            return result;
        }
    }
}