﻿using Orms.DataAccessDataObjectsNet.EntityClasses;
using Orms.Domains.Units;
using Orms.Models;

namespace Orms.DataAccessDataObjectsNet.Converter
{
    public static partial class Converter
    {
        public static EnergyCalorificValueEntity ToEntity(this EnergyCalorificValue ecv)
        {
            return new EnergyCalorificValueEntity()
            {
                HourIndex = ecv.HourIndex,
                Energy = new EnergyUnitValue(ecv.Energy.Value, ecv.Energy.Unit),
            };
        }

        public static EnergyCalorificValueEntity ToEntity(this EnergyCalorificValue ecv, CalorificValueEntity cv)
        {
            return new EnergyCalorificValueEntity()
            {
                CalorificValue = cv,
                HourIndex = ecv.HourIndex,
                Energy = new EnergyUnitValue(ecv.Energy.Value, ecv.Energy.Unit),
            };
        }

        public static EnergyCalorificValue ToModel(this EnergyCalorificValueEntity ecv)
        {
            return new EnergyCalorificValue()
            {
                Id = ecv.Id,
                CalorificValue = ecv.CalorificValue.ToModel(),
                Energy = new UnitValue(ecv.Energy.Value, ecv.Energy.Unit),
                HourIndex = ecv.HourIndex
            };
        }
    }
}