﻿using System.Collections.Generic;
using System.Linq;
using Orms.DataAccessDataObjectsNet.EntityClasses;
using Orms.Models;
using Xtensive.Orm;

namespace Orms.DataAccessDataObjectsNet.Converter
{
    public static partial class Converter
    {
        public static CalorificValueEntity ToEntity(this CalorificValue cv)
        {
            var result = new CalorificValueEntity()
            {
                GasDay = cv.GasDay,
            };

            return result;
        }

        public static CalorificValueEntity ToEntity(this CalorificValue cv, GasStorageEntity gasStorage, IEnumerable<EnergyCalorificValue> values)
        {
            var result = new CalorificValueEntity()
            {
                GasStorage = gasStorage,
                GasDay = cv.GasDay
            };

            if (cv.Values != null) result.Values.AddRange(values.Select(c => c.ToEntity()));

            return result;
        }

        public static CalorificValueEntity ToEntity(this CalorificValue cv, GasStorageEntity gasStorage, EntitySet<EnergyCalorificValueEntity> values)
        {
            return new CalorificValueEntity()
            {
                GasStorage = gasStorage,
                GasDay = cv.GasDay,
                Values = values
            };
        }

        public static CalorificValue ToModel(this CalorificValueEntity cv)
        {
            return new CalorificValue()
            {
                Id = cv.Id,
                GasStorage = cv.GasStorage.ToModel(),
                GasDay = cv.GasDay,
                //Values = cv.Values.ToModel()
            };
        }
    }
}