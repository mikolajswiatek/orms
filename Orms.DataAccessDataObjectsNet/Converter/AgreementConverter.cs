﻿using Orms.DataAccessDataObjectsNet.EntityClasses;
using Orms.Models;

namespace Orms.DataAccessDataObjectsNet.Converter
{
    public static partial class Converter
    {
        public static AgreementEntity ToEntity(this Agreement agreement)
        {
            return new AgreementEntity()
            {
                CreateDate = agreement.CreateDate,
                End = agreement.End,
                Start = agreement.Start,
            };
        }

        public static AgreementEntity ToEntity(this Agreement agreement, ContractorEntity contractor, GasStorageEntity gasStorage)
        {
            return new AgreementEntity()
            {
                Contractor = contractor,
                GasStorage = gasStorage,
                CreateDate = agreement.CreateDate,
                End = agreement.End,
                Start = agreement.Start,
            };
        }

        public static Agreement ToModel(this AgreementEntity agreement)
        {
            return new Agreement()
            {
                Id = agreement.Id,
                CreateDate = agreement.CreateDate,
                End = agreement.End,
                GasStorage = agreement.GasStorage.ToModel(),
                Start = agreement.Start,
                Contractor = agreement.Contractor.ToModel()
            };
        }
    }
}