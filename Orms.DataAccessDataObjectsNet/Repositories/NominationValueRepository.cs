﻿using System.Collections.Generic;
using System.Linq;
using Orms.DataAccessDataObjectsNet.Converter;
using Orms.DataAccessDataObjectsNet.EntityClasses;
using Orms.DataAccessHelper.Exceptions;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;
using Session = Xtensive.Orm.Session;

namespace Orms.DataAccessDataObjectsNet.Repositories
{
    public class NominationValueRepository : INominationValueRepository<Session>
    {
        public void Add(Session session, NominationValue nominationValue)
        {
            nominationValue.ToEntity();
        }

        public NominationValue Get(Session session, int id)
        {
            var queryResult = session.Query.All<NominationValueEntity>()
                .FirstOrDefault(c => c.Id == id);

            var value = queryResult.ToModel();

            return value;
        }

        public IEnumerable<NominationValue> GetAll(Session session)
        {
            var queryResult = session.Query.All<NominationValueEntity>()
                .ToList();

            var values = queryResult.Select(v => v.ToModel());

            return values;
        }

        public void Update(Session session, NominationValue nominationValue)
        {
            throw new DatabaseOperationException("Todo?");
        }

        public void Add(Session session, IEnumerable<NominationValue> values)
        {
            var nomination = session.Query.All<NominationEntity>()
                .FirstOrDefault(c => c.Id == values.First().Id);

            foreach (var value in values)
            {
                value.ToEntity(nomination);
            }
        }

        public IEnumerable<NominationValue> GetBy(Session session, Nomination nomination)
        {
            var queryResult = session.Query.All<NominationValueEntity>()
                .Where(nv => nv.Nomination.Id == nomination.Id)
                .OrderByDescending(nv => nv.HourIndex)
                .ToList();

            var values = queryResult.Select(v => v.ToModel());

            return values;
        }
    }
}
