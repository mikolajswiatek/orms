﻿using System;
using System.Collections.Generic;
using System.Linq;
using Orms.DataAccessDataObjectsNet.Converter;
using Orms.DataAccessDataObjectsNet.EntityClasses;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;
using Xtensive.Orm.BulkOperations;
using Session = Xtensive.Orm.Session;

namespace Orms.DataAccessDataObjectsNet.Repositories
{
    public class GasAccountRepository : IGasAccountRepository<Session>
    {
        public void Add(Session session, GasAccount gasAccount)
        {
            var gasStorage = session.Query.All<GasStorageEntity>()
                .FirstOrDefault(c => c.Id == gasAccount.GasStorage.Id);

            var ga = gasAccount.ToEntity();
            ga.GasStorage = gasStorage;
        }

        public GasAccount Get(Session session, int id)
        {
            var queryResult = session.Query.All<GasAccountEntity>()
                .FirstOrDefault(ecv => ecv.Id == id);

            var result = queryResult.ToModel();

            return result;
        }

        public IEnumerable<GasAccount> GetAll(Session session)
        {
            var results = new List<GasAccount>();

            var gasAccounts = from ga in session.Query.All<GasAccountEntity>()
                              select ga;

            foreach (var ga in gasAccounts)
            {
                results.Add(ga.ToModel());
            }

            return results;
        }

        public void Update(Session session, GasAccount gasAccount)
        {
            Update(session, new List<GasAccount>() { gasAccount });
        }

        public void Add(Session session, IEnumerable<GasAccount> gasAccounts)
        {
            var gasStorage = session.Query.SingleOrDefault<GasStorageEntity>(gasAccounts.First().GasStorage.Id);

            foreach (var gasAccount in gasAccounts)
            {
                var ga = gasAccount.ToEntity();
                ga.GasStorage = gasStorage;
            }
        }

        public void Update(Session session, IEnumerable<GasAccount> gasAccounts)
        {
            foreach (var gasAccount in gasAccounts)
            {
                session.Query.All<GasAccountEntity>()
                    .Where(ga => ga.Id == gasAccount.Id)
                    .Set(ga => ga.Energy.Value, gasAccount.Energy.Value)
                    .Set(ga => ga.Volume.Value, gasAccount.Volume.Value)
                    .Set(ga => ga.Energy.Unit, gasAccount.Energy.Unit)
                    .Set(ga => ga.Volume.Unit, gasAccount.Volume.Unit)
                    .Update();
            }
        }

        public IEnumerable<GasAccount> Get(Session session, GasStorage gasStorage, DateTime gasDay)
        {
            var results = new List<GasAccount>();

            var gasAccounts = session.Query.All<GasAccountEntity>()
                .Where(ga => ga.GasStorage.Id == gasStorage.Id && ga.GasDay == gasDay)
                .OrderBy(ga => ga.HourIndex);

            foreach (var ga in gasAccounts)
            {
                results.Add(ga.ToModel());
            }

            return results;
        }

        public IEnumerable<GasAccount> Get(Session session, DateTime gasDay)
        {
            var results = new List<GasAccount>();

            var gasAccounts = from ga in session.Query.All<GasAccountEntity>()
                              where ga.GasDay == gasDay
                              select ga;

            foreach (var ga in gasAccounts)
            {
                results.Add(ga.ToModel());
            }

            return results;
        }
    }
}
