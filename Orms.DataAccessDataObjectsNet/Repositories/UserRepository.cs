﻿using System.Collections.Generic;
using System.Linq;
using Orms.DataAccessDataObjectsNet.Converter;
using Orms.DataAccessDataObjectsNet.EntityClasses;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;
using Xtensive.Orm.BulkOperations;
using Session = Xtensive.Orm.Session;

namespace Orms.DataAccessDataObjectsNet.Repositories
{
    public class UserRepository : IUserRepository<Session>
    {
        public void Add(Session session, User user)
        {
            var userEntity = user.ToEntity();
            var contractor = session.Query.SingleOrDefault<ContractorEntity>(user.Contractor.Id);
            contractor.Users.Add(userEntity);
            userEntity.Contractor = contractor;
        }

        public User Get(Session session, int id)
        {
            var queryResult = session.Query.All<UserEntity>()
                .FirstOrDefault(u => u.Id == id);

            var user = queryResult.ToModel();

            return user;
        }

        public IEnumerable<User> GetAll(Session session)
        {
            var results = new List<User>();

            var users = session.Query.All<UserEntity>().Select(u => u);

            foreach (var user in users)
            {
                results.Add(user.ToModel());
            }

            return results;
        }

        public void Update(Session session, User user)
        {
            session.Query.All<UserEntity>()
                .Where(u => u.Id == user.Id)
                .Set(u => u.Password, user.Password)
                .Set(u => u.LastActivityDatetime, user.LastActivityDateTime)
                .Set(u => u.IsActive, user.IsActive)
                .Set(u => u.Email, user.Email)
                .Update();
        }

        public IEnumerable<User> GetBy(Session session, Contractor contractor)
        {
            var results = new List<User>();

            var users = session.Query.All<UserEntity>()
                .Where(u => u.Contractor.Id == contractor.Id)
                .Select(u => u);

            foreach (var user in users)
            {
                results.Add(user.ToModel());
            }

            return results;
        }

        public IEnumerable<User> GetBy(Session session, string nameOrEmail)
        {
            var results = new List<User>();

            var users = session.Query.All<UserEntity>()
                .Where(u => u.Name == nameOrEmail || u.Email == nameOrEmail)
                .Select(u => u);

            foreach (var user in users)
            {
                results.Add(user.ToModel());
            }

            return results;
        }
    }
}
