﻿using System.Collections.Generic;
using System.Linq;
using Orms.DataAccessDataObjectsNet.Converter;
using Orms.DataAccessDataObjectsNet.EntityClasses;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;
using Xtensive.Orm.BulkOperations;
using Session = Xtensive.Orm.Session;

namespace Orms.DataAccessDataObjectsNet.Repositories
{
    public class ContractorRepository : IContractorRepository<Session>
    {
        public void Add(Session session, Contractor contractor)
        {
            contractor.ToEntity();
        }

        public Contractor Get(Session session, int id)
        {
            var queryResult = session.Query.All<ContractorEntity>()
                .FirstOrDefault(c => c.Id == id);

            var contractor = queryResult.ToModel();

            session.Query.All(typeof(SessionEntity));

            return contractor;
        }

        public IEnumerable<Contractor> GetAll(Session session)
        {
            var results = new List<Contractor>();

            var contractors = from c in session.Query.All<ContractorEntity>()
                              select c;

            foreach (var contractor in contractors)
            {
                results.Add(contractor.ToModel());
            }

            return results;
        }

        public void Update(Session session, Contractor contractor)
        {
            session.Query.All<ContractorEntity>()
                .Where(c => c.Id == contractor.Id)
                .Set(c => c.Adress, contractor.Adress)
                .Set(c => c.Name, contractor.Name)
                .Update();
        }
    }
}