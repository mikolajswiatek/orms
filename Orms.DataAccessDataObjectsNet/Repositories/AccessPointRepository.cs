﻿using System.Collections.Generic;
using System.Linq;
using Orms.DataAccessDataObjectsNet.Converter;
using Orms.DataAccessDataObjectsNet.EntityClasses;
using Orms.DataAccessHelper.Exceptions;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;
using Session = Xtensive.Orm.Session;

namespace Orms.DataAccessDataObjectsNet.Repositories
{
    public class AccessPointRepository : IAccessPointRepository<Session>
    {
        public IEnumerable<AccessPoint> GetAll(Session session)
        {
            var results = new List<AccessPoint>();

            var aps = from c in session.Query.All<AccessPointEntity>()
                select c;

            foreach (var ap in aps)
            {
                results.Add(ap.ToModel());
            }

            return results;
        }

        public AccessPoint Get(Session session, int id)
        {
            var queryResult = session.Query.SingleOrDefault<AccessPointEntity>(id);

            var ap = queryResult.ToModel();

            return ap;
        }

        public void Update(Session session, AccessPoint accessPoint)
        {
            throw new DatabaseOperationException("Todo?");
        }

        public void Add(Session session, AccessPoint accessPoint)
        {
            var accessPointEntity = accessPoint.ToEntity();
            var gasStorage = session.Query.SingleOrDefault<GasStorageEntity>(accessPoint.GasStorage.Id);
            gasStorage.AccessPoints.Add(accessPointEntity);
            accessPointEntity.GasStorage = gasStorage;
            //gaStorage.AccessPoints.Add(accessPointEntity);
        }

        public IEnumerable<AccessPoint> GetBy(Session session, GasStorage gasStorage)
        {
            var results = new List<AccessPoint>();

            var aps = session.Query.All<AccessPointEntity>()
                .Where(ap => ap.GasStorage.Id == gasStorage.Id)
                .ToList();

            foreach (var ap in aps)
            {
                var apModel = ap.ToModel();
                apModel.GasStorage = ap.GasStorage.ToModel();
                apModel.AccessPointFlows = ap.AccessPointFlows.ToModel();
                results.Add(apModel);
            }

            return results;
        }
    }
}