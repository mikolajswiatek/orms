﻿using System.Collections.Generic;
using System.Linq;
using Orms.DataAccessDataObjectsNet.Converter;
using Orms.DataAccessDataObjectsNet.EntityClasses;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;
using Xtensive.Orm.BulkOperations;
using Session = Xtensive.Orm.Session;

namespace Orms.DataAccessDataObjectsNet.Repositories
{
    public class ContractorContactRepository : IContractorContactRepository<Session>
    {
        public IEnumerable<ContractorContact> GetAll(Session session)
        {
            var results = new List<ContractorContact>();

            var contacts = from cc in session.Query.All<ContractorContactEntity>()
                select cc;

            foreach (var contact in contacts)
            {
                results.Add(contact.ToModel());
            }

            return results;
        }

        public ContractorContact Get(Session session, int id)
        {
            var queryResult = session.Query.All<ContractorContactEntity>()
                .FirstOrDefault(c => c.Id == id);

            var contact = queryResult.ToModel();

            return contact;
        }

        public void Update(Session session, ContractorContact contractorContact)
        {
            session.Query.All<ContractorContactEntity>()
                .Where(c => c.Id == contractorContact.Id)
                .Set(c => c.Adress, contractorContact.Adress)
                .Set(c => c.Phone, contractorContact.Phone)
                .Set(c => c.Email, contractorContact.Email)
                .Update();
        }

        public void Add(Session session, ContractorContact contractorContact)
        {
            var contact = contractorContact.ToEntity();
            var contractor = session.Query.SingleOrDefault<ContractorEntity>(contractorContact.Contractor.Id);
            //contractor.Contacts.Add(contact);
            contact.Contractor = contractor;
        }

        public IEnumerable<ContractorContact> GetByContractor(Session session, Contractor contractor)
        {
            var results = new List<ContractorContact>();

            var contacts = session.Query.All<ContractorContactEntity>()
                .Where(a => a.Contractor.Id == contractor.Id)
                .ToList();

            foreach (var contact in contacts)
            {
                results.Add(contact.ToModel());
            }

            return results;
        }
    }
}