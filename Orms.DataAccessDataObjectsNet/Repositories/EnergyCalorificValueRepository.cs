﻿using System;
using System.Collections.Generic;
using System.Linq;
using Orms.DataAccessDataObjectsNet.Converter;
using Orms.DataAccessDataObjectsNet.EntityClasses;
using Orms.DataAccessHelper.Exceptions;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;
using Session = Xtensive.Orm.Session;

namespace Orms.DataAccessDataObjectsNet.Repositories
{
    public class EnergyCalorificValueRepository : IEnergyCalorificValueRepository<Session>
    {
        public IEnumerable<EnergyCalorificValue> GetAll(Session session)
        {
            var results = new List<EnergyCalorificValue>();

            var values = from ecv in session.Query.All<EnergyCalorificValueEntity>()
                         select ecv;

            foreach (var value in values)
            {
                results.Add(value.ToModel());
            }

            return results;
        }

        public EnergyCalorificValue Get(Session session, int id)
        {
            var queryResult = session.Query.All<EnergyCalorificValueEntity>()
                .FirstOrDefault(ecv => ecv.Id == id);

            var result = queryResult.ToModel();

            return result;
        }

        public void Update(Session session, EnergyCalorificValue value)
        {
            throw new DatabaseOperationException("Todo?");
        }

        public void Add(Session session, EnergyCalorificValue value)
        {
            Add(session, new List<EnergyCalorificValue>() { value });
        }

        public void Add(Session session, IEnumerable<EnergyCalorificValue> values)
        {
            var cv = session.Query.All<CalorificValueEntity>()
                .FirstOrDefault(ecv => ecv.Id == values.FirstOrDefault().CalorificValue.Id);

            foreach (var value in values)
            {
                value.ToEntity(cv);
            }
        }

        public IEnumerable<EnergyCalorificValue> GetAll(Session session, CalorificValue cv)
        {
            var results = new List<EnergyCalorificValue>();

            var values = from ecv in session.Query.All<EnergyCalorificValueEntity>()
                         where ecv.CalorificValue.Id == cv.Id
                         orderby ecv.HourIndex
                         select ecv;

            foreach (var value in values)
            {
                results.Add(value.ToModel());
            }

            return results;
        }
    }
}