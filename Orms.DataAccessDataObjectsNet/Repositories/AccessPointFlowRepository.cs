﻿using System.Collections.Generic;
using System.Linq;
using Orms.DataAccessDataObjectsNet.Converter;
using Orms.DataAccessDataObjectsNet.EntityClasses;
using Orms.DataAccessHelper.Exceptions;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;
using Session = Xtensive.Orm.Session;

namespace Orms.DataAccessDataObjectsNet.Repositories
{
    public class AccessPointFlowRepository : IAccessPointFlowRepository<Session>
    {
        public IEnumerable<AccessPointFlow> GetAll(Session session)
        {
            var results = new List<AccessPointFlow>();

            var apfs = from apf in session.Query.All<AccessPointFlowEntity>()
                select apf;

            foreach (var apf in apfs)
            {
                results.Add(apf.ToModel());
            }

            return results;
        }

        public AccessPointFlow Get(Session session, int id)
        {
            var queryResult = session.Query.All<AccessPointFlowEntity>()
                .FirstOrDefault(c => c.Id == id);

            var apf = queryResult.ToModel();

            return apf;
        }

        public void Update(Session session, AccessPointFlow accessPointFlow)
        {
            throw new DatabaseOperationException("Todo?");
        }

        public void Add(Session session, AccessPointFlow accessPointFlow)
        {
            var accessPointFlowEntity = accessPointFlow.ToEntity();
            var accessPoint = session.Query.SingleOrDefault<AccessPointEntity>(accessPointFlow.AccessPoint.Id);
            accessPoint.AccessPointFlows.Add(accessPointFlowEntity);
            accessPointFlowEntity.AccessPoint = accessPoint;
        }
    }
}