﻿using System;
using System.Collections.Generic;
using System.Linq;
using Orms.DataAccessDataObjectsNet.Converter;
using Orms.DataAccessDataObjectsNet.EntityClasses;
using Orms.DataAccessHelper.Exceptions;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;
using Xtensive.Core;
using Xtensive.Orm;
using Session = Xtensive.Orm.Session;

namespace Orms.DataAccessDataObjectsNet.Repositories
{
    public class CalorificValueRepository : ICalorificValueRepository<Session>
    {
        public void Add(Session session, CalorificValue cv)
        {
            var calorificValue = cv.ToEntity();
            var gasStorage = session.Query.SingleOrDefault<GasStorageEntity>(cv.GasStorage.Id);
            calorificValue.GasStorage = gasStorage;
            var values = cv.Values.Select(v => v.ToEntity());

            foreach (var val in values)
            {
                calorificValue.Values.Add(val);
                val.CalorificValue = calorificValue;
            }
        }

        public CalorificValue Get(Session session, int id)
        {
            var queryResult = session.Query.All<CalorificValueEntity>()
                .FirstOrDefault(c => c.Id == id);

            var cv = queryResult.ToModel();

            return cv;
        }

        public IEnumerable<CalorificValue> GetAll(Session session)
        {
            var results = new List<CalorificValue>();

            var calorificValues = from cv in session.Query.All<CalorificValueEntity>()
                                  select cv;

            foreach (var calorificValue in calorificValues)
            {
                results.Add(calorificValue.ToModel());
            }

            return results;
        }

        public void Update(Session session, CalorificValue cv)
        {
            throw new DatabaseOperationException("Todo?");
        }

        public IEnumerable<CalorificValue> GetBy(Session session, GasStorage gasStorage)
        {
            var results = new List<CalorificValue>();

            var calorificValues = from cv in session.Query.All<CalorificValueEntity>()
                                  where cv.GasStorage.Id == gasStorage.Id
                                  select cv;

            foreach (var calorificValue in calorificValues)
            {
                results.Add(calorificValue.ToModel());
            }

            return results;
        }

        public CalorificValue GetLastBy(Session session, GasStorage gasStorage)
        {
            var calorificValue = session.Query.All<CalorificValueEntity>()
                .Where(cv => cv.GasStorage.Id == gasStorage.Id)
                .OrderByDescending(cv => cv.Id)
                .FirstOrDefault()
                ?.ToModel();

            return calorificValue;
        }

        public CalorificValue GetForGasDay(Session session, GasStorage gasStorage, DateTime gasDay)
        {
            var calorificValue = session.Query.All<CalorificValueEntity>()
                .Prefetch(cv => cv.Values)
                .Where(cv => cv.GasStorage.Id == gasStorage.Id && cv.GasDay == gasDay)
                .OrderByDescending(cv => cv.Id)
                .FirstOrDefault();

            if (calorificValue != null)
            {
                var c = calorificValue.ToModel(); ;

                c.Values = calorificValue.Values.ToModel();

                return c;
            }

            return null;
        }
    }
}