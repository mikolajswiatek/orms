﻿using System.Collections.Generic;
using System.Linq;
using Orms.DataAccessDataObjectsNet.Converter;
using Orms.DataAccessDataObjectsNet.EntityClasses;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Xtensive.Orm.BulkOperations;
using S = Orms.Models.Session;
using Session = Xtensive.Orm.Session;

namespace Orms.DataAccessDataObjectsNet.Repositories
{
    public class SessionRepository : ISessionRepository<Session>
    {
        public void Add(Session session, S s)
        {
            var sessionEntity = s.ToEntity();
            var user = session.Query.SingleOrDefault<UserEntity>(s.User.Id);
            sessionEntity.User = user;
        }

        public S Get(Session session, int id)
        {
            var queryResult = session.Query.All<SessionEntity>()
                .FirstOrDefault(c => c.Id == id);

            var s = queryResult.ToModel();

            return s;
        }

        public IEnumerable<S> GetAll(Session session)
        {
            var results = new List<S>();

            var sessions = from n in session.Query.All<SessionEntity>()
                           select n;

            foreach (var s in sessions)
            {
                results.Add(s.ToModel());
            }

            return results;
        }

        public void Update(Session session, S s)
        {
            session.Query.All<SessionEntity>()
                .Where(x => x.Id == s.Id || x.Guid == s.Guid)
                .Set(x => x.LastActivityDatetime, s.LastActivityDatetime)
                .Update();
        }

        public S GetBy(Session session, string guid)
        {
            var queryResult = session.Query.All<SessionEntity>()
                .FirstOrDefault(c => c.Guid == guid);

            var s = queryResult.ToModel();

            return s;
        }

        public void Delete(Session session, S s)
        {
            session.Query.All<SessionEntity>()
                .Where(c => s.Id == s.Id || s.Guid == s.Guid)
                .Delete();
        }
    }
}
