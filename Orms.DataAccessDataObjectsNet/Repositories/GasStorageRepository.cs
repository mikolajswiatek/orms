﻿using System.Collections.Generic;
using System.Linq;
using Orms.DataAccessDataObjectsNet.Converter;
using Orms.DataAccessDataObjectsNet.EntityClasses;
using Orms.DataAccessHelper.Exceptions;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;
using Session = Xtensive.Orm.Session;

namespace Orms.DataAccessDataObjectsNet.Repositories
{
    public class GasStorageRepository : IGasStorageRepository<Session>
    {
        public void Add(Session session, GasStorage gasStorage)
        {
            gasStorage.ToEntity();
        }

        public GasStorage Get(Session session, int id)
        {
            var queryResult = session.Query.All<GasStorageEntity>()
                .FirstOrDefault(c => c.Id == id);

            var gasStorage = queryResult.ToModel();

            return gasStorage;
        }

        public IEnumerable<GasStorage> GetAll(Session session)
        {
            var results = new List<GasStorage>();

            var gasStorages = from g in session.Query.All<GasStorageEntity>()
                              select g;

            foreach (var gasStorage in gasStorages)
            {
                results.Add(gasStorage.ToModel());
            }

            return results;
        }

        public void Update(Session session, GasStorage gasStorage)
        {
            throw new DatabaseOperationException("Todo?");
        }
    }
}
