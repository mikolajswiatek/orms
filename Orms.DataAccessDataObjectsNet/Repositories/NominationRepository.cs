﻿using System;
using System.Collections.Generic;
using System.Linq;
using Orms.DataAccessDataObjectsNet.Converter;
using Orms.DataAccessDataObjectsNet.EntityClasses;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;
using Xtensive.Orm.BulkOperations;
using Session = Xtensive.Orm.Session;

namespace Orms.DataAccessDataObjectsNet.Repositories
{
    public class NominationRepository : INominationRepository<Session>
    {
        public void Add(Session session, Nomination nomination)
        {
            var nominationEntity = nomination.ToEntity();
            var contractor = session.Query.SingleOrDefault<ContractorEntity>(nomination.Contractor.Id);
            var accessPoint = session.Query.SingleOrDefault<AccessPointFlowEntity>(nomination.AccessPointFlow.Id);
            var user = session.Query.SingleOrDefault<UserEntity>(nomination.Creator.Id);
            var cv = session.Query.SingleOrDefault<CalorificValueEntity>(nomination.CalorificValue.Id);
            nominationEntity.AccessPointFlow = accessPoint;
            nominationEntity.Contractor = contractor;
            nominationEntity.CalorificValue = cv;
            nominationEntity.Creator = user;

            var values = nomination.Values.Select(v => v.ToEntity());

            foreach (var val in values)
            {
                nominationEntity.Values.Add(val);
                val.Nomination = nominationEntity;
            }

            if (nomination.ParentNomination != null)
            {
                nominationEntity.ParentNomination = session.Query.SingleOrDefault<NominationEntity>(nomination.ParentNomination.Id);
            }

        }

        public Nomination Get(Session session, int id)
        {
            var queryResult = session.Query.All<NominationEntity>()
                .FirstOrDefault(c => c.Id == id);

            var nomination = queryResult.ToModel();
            nomination.Values = queryResult.Values.ToModel();
            nomination.AccessPointFlow.AccessPoint.GasStorage.AccessPoints =
                queryResult.AccessPointFlow.AccessPoint.GasStorage.AccessPoints.ToModel();

            foreach (var ap in nomination.AccessPointFlow.AccessPoint.GasStorage.AccessPoints)
            {
                ap.AccessPointFlows = queryResult.AccessPointFlow.AccessPoint.GasStorage.AccessPoints
                    .FirstOrDefault(a => a.Id == ap.Id).AccessPointFlows.ToModel();
            }

            return nomination;
        }

        public IEnumerable<Nomination> GetAll(Session session)
        {
            var results = new List<Nomination>();

            var nominations = from n in session.Query.All<NominationEntity>()
                             select n;

            foreach (var nomination in nominations)
            {
                results.Add(nomination.ToModel());
            }

            return results;
        }

        public void Update(Session session, Nomination nomination)
        {
            session.Query.All<NominationEntity>()
                .Where(n => n.Id == nomination.Id)
                .Set(n => n.IsAccepted, nomination.IsAccepted)
                .Set(n => n.IsLastOrder, nomination.IsLast)
                .Update();
        }

        public IEnumerable<Nomination> GetBy(Session session, Contractor contractor)
        {
            var results = new List<Nomination>();

            var nominations = session.Query.All<NominationEntity>()
                .Where(n => n.Contractor.Id == contractor.Id);

            foreach (var nomination in nominations)
            {
                results.Add(nomination.ToModel());
            }

            return results;
        }

        public IEnumerable<Nomination> GetBy(Session session, GasStorage gasStorage)
        {
            var results = new List<Nomination>();
            var apfIds = new List<int>();

            foreach (var accessPoint in gasStorage.AccessPoints)
            {
                foreach (var accessPointFlow in accessPoint.AccessPointFlows)
                {
                    apfIds.Add(accessPointFlow.Id);
                }
            }

            var nominations = from n in session.Query.All<NominationEntity>()
                              where apfIds.Contains(gasStorage.Id)
                              select n;

            foreach (var nomination in nominations)
            {
                results.Add(nomination.ToModel());
            }

            return results;
        }

        public IEnumerable<Nomination> GetBy(Session session, User user)
        {
            var results = new List<Nomination>();

            var nominations = session.Query.All<NominationEntity>().Where(n => n.Creator.Id == user.Id);

            foreach (var nomination in nominations)
            {
                results.Add(nomination.ToModel());
            }

            return results;
        }

        public IEnumerable<Nomination> GetBy(Session session, DateTime gasDay)
        {
            var results = new List<Nomination>();

            var nominations = session.Query.All<NominationEntity>()
                .Where(n => n.GasDay == gasDay)
                .ToList();

            foreach (var nomination in nominations)
            {
                var n = nomination.ToModel();
                n.Values = nomination.Values.ToModel();
                results.Add(n);
            }

            return results;
        }

        public Nomination GetLast(Session session, Contractor contractor)
        {
            var queryResult = (from n in session.Query.All<NominationEntity>()
                              where n.Contractor.Id == contractor.Id
                              orderby n.Id descending
                              select n)
                              .FirstOrDefault();

            var nomination = queryResult.ToModel();

            return nomination;
        }

        public Nomination GetLast(Session session, GasStorage gasStorage)
        {
            var apfIds = new List<int>();

            foreach (var accessPoint in gasStorage.AccessPoints)
            {
                foreach (var accessPointFlow in accessPoint.AccessPointFlows)
                {
                    apfIds.Add(accessPointFlow.Id);
                }
            }

            var queryResult = (from n in session.Query.All<NominationEntity>()
                               where apfIds.Contains(gasStorage.Id)
                               orderby n.Id descending
                               select n)
                               .FirstOrDefault();

            var nomination = queryResult.ToModel();

            return nomination;
        }

        public Nomination GetLast(Session session, User user)
        {
            var queryResult = (from n in session.Query.All<NominationEntity>()
                               where n.Creator.Id == user.Id
                               orderby n.Id descending
                               select n)
                               .FirstOrDefault();

            var nomination = queryResult.ToModel();

            return nomination;
        }

        public Nomination GetLast(Session session, Contractor contractor, DateTime gasDay)
        {
            var queryResult = (from n in session.Query.All<NominationEntity>()
                               where n.Contractor.Id == contractor.Id && n.GasDay == gasDay
                               orderby n.Id descending
                               select n)
                               .FirstOrDefault();

            var nomination = queryResult.ToModel();

            return nomination;
        }

        public IEnumerable<Nomination> GetForGasDay(Session session, Contractor contractor, DateTime gasDay)
        {
            var results = new List<Nomination>();

            var nominations = from n in session.Query.All<NominationEntity>()
                              where n.Contractor.Id == contractor.Id && n.GasDay == gasDay
                              orderby n.Id descending
                              select n;


            foreach (var nomination in nominations)
            {
                results.Add(nomination.ToModel());
            }

            return results;
        }

        public IEnumerable<Nomination> GetForGasDay(Session session, GasStorage gasStorage, DateTime gasDay)
        {
            var apfIds = session.Query.All<AccessPointFlowEntity>()
                .Where(apf => apf.AccessPoint.GasStorage.Id == gasStorage.Id)
                .Select(apf => apf.Id)
                .ToList();

            var results = new List<Nomination>();

            var nominations = from n in session.Query.All<NominationEntity>()
                              where apfIds.Contains(n.AccessPointFlow.Id) && n.GasDay == gasDay
                              orderby n.Id descending
                              select n;

            foreach (var nomination in nominations)
            {
                var n = nomination.ToModel();
                n.Values = nomination.Values.ToModel();
                results.Add(n);
            }

            return results;
        }

        public IEnumerable<Nomination> GetForGasDay(Session session, Contractor contractor, GasStorage gasStorage, DateTime gasDay)
        {
            var apfIds = new List<int>();

            foreach (var accessPoint in gasStorage.AccessPoints)
            {
                foreach (var accessPointFlow in accessPoint.AccessPointFlows)
                {
                    apfIds.Add(accessPointFlow.Id);
                }
            }

            var results = new List<Nomination>();

            var nominations = from n in session.Query.All<NominationEntity>()
                              where apfIds.Contains(n.AccessPointFlow.Id) && n.GasDay == gasDay && n.Contractor.Id == contractor.Id
                              orderby n.Id descending
                              select n;

            foreach (var nomination in nominations)
            {
                var n = nomination.ToModel();
                n.Values = nomination.Values.ToModel();
                results.Add(n);
            }

            return results;
        }
    }
}
