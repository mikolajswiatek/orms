﻿using System.Collections.Generic;
using System.Linq;
using Orms.DataAccessDataObjectsNet.Converter;
using Orms.DataAccessDataObjectsNet.EntityClasses;
using Orms.DataAccessHelper.Exceptions;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;
using Session = Xtensive.Orm.Session;

namespace Orms.DataAccessDataObjectsNet.Repositories
{
    public class AgreementRepository : IAgreementRepository<Session>
    {
        public IEnumerable<Agreement> GetAll(Session session)
        {
            var results = new List<Agreement>();

            var agreements = from a in session.Query.All<AgreementEntity>()
                select a;

            foreach (var agreement in agreements)
            {
                results.Add(agreement.ToModel());
            }

            return results;
        }

        public Agreement Get(Session session, int id)
        {
            var queryResult = session.Query.All<AgreementEntity>()
                .FirstOrDefault(c => c.Id == id);

            var ap = queryResult.ToModel();

            return ap;
        }

        public void Update(Session session, Agreement agreement)
        {
            throw new DatabaseOperationException("Todo?");
        }

        public void Add(Session session, Agreement agreement)
        {
            var agreementEntity = agreement.ToEntity();

            var contractor = session.Query.SingleOrDefault<ContractorEntity>(agreement.Contractor.Id);
            var gasStorage = session.Query.SingleOrDefault<GasStorageEntity>(agreement.GasStorage.Id);

            agreementEntity.GasStorage = gasStorage;
            agreementEntity.Contractor = contractor;
        }

        public IEnumerable<Agreement> GetBy(Session session, GasStorage gasStorage)
        {
            var results = new List<Agreement>();

            var agreements = from a in session.Query.All<AgreementEntity>()
                where a.GasStorage.Id == gasStorage.Id
                select a;

            foreach (var agreement in agreements)
            {
                results.Add(agreement.ToModel());
            }

            return results;
        }

        public IEnumerable<Agreement> GetBy(Session session, Contractor contractor)
        {
            var results = new List<Agreement>();

            var agreements = from a in session.Query.All<AgreementEntity>()
                where a.Contractor.Id == contractor.Id
                select a;

            foreach (var agreement in agreements)
            {
                results.Add(agreement.ToModel());
            }

            return results;
        }
    }
}