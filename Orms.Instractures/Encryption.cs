﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Orms.Instractures
{
    public static class Encryption
    {
        public static string Hash(string plainText)
        {
            UnicodeEncoding uEncode = new UnicodeEncoding();
            byte[] bytPassword = uEncode.GetBytes(plainText);
            SHA512Managed sha = new SHA512Managed();
            byte[] hash = sha.ComputeHash(bytPassword);
            return Convert.ToBase64String(hash);
        }
    }
}
