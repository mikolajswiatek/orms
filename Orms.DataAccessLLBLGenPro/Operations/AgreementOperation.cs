﻿using System.Collections.Generic;
using Orms.DataAccessHelper;
using Orms.DataAccessHelper.Operations.Interfaces;
using Orms.DataAccessLLBLGenPro.DatabaseSpecific;
using Orms.DataAccessLLBLGenPro.Providers;
using Orms.Models;
using Orms.Services;
using Orms.Services.Interfaces;

namespace Orms.DataAccessLLBLGenPro.Operations
{
    public class AgreementOperation : Operation<DataAccessAdapter>, IAgreementOperation
    {
        private IAgreementService _service;
        private readonly SessionWorker<DataAccessAdapter> _adapter;

        public AgreementOperation(SessionWorker<DataAccessAdapter> session)
        {
            _adapter = session;
        }

        protected override void SetProviders(DataAccessAdapter adapter)
        {
            _service = new AgreementService(
                new AgreementProvider(adapter));
        }

        public void Add(Agreement agreement)
        {
            using (var session = _adapter.ReadAndWrite())
            {
                SetProviders(session);

                _service.Add(agreement);
            }
        }

        public IEnumerable<Agreement> GetAll(Contractor contractor)
        {
            using (var session = _adapter.Read())
            {
                SetProviders(session);

                return _service.GetAll(contractor);
            }
        }

        public IEnumerable<Agreement> GetAll(GasStorage gasStorage)
        {
            using (var session = _adapter.Read())
            {
                SetProviders(session);

                return _service.GetAll(gasStorage);
            }
        }

        public IEnumerable<Agreement> GetAll(Contractor contractor, GasStorage gasStorage)
        {
            using (var session = _adapter.Read())
            {
                SetProviders(session);

                return _service.GetAll(contractor, gasStorage);
            }
        }

        public Agreement Get(int id)
        {
            using (var session = _adapter.Read())
            {
                SetProviders(session);

                return _service.Get(id);
            }
        }

        public IEnumerable<Agreement> GetAll()
        {
            using (var session = _adapter.Read())
            {
                SetProviders(session);

                return _service.GetAll();
            }
        }
    }
}