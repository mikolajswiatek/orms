﻿using Orms.DataAccessHelper;
using Orms.DataAccessHelper.Operations.Interfaces;
using Orms.DataAccessLLBLGenPro.DatabaseSpecific;
using Orms.DataAccessLLBLGenPro.Providers;
using Orms.Models;
using Orms.Services;
using Orms.Services.Interfaces;

namespace Orms.DataAccessLLBLGenPro.Operations
{
    public class SessionOperation : Operation<DataAccessAdapter>, ISessionOperation
    {
        private ISessionService _service;
        private readonly SessionWorker<DataAccessAdapter> _adapter;

        public SessionOperation(SessionWorker<DataAccessAdapter> adapter)
        {
            _adapter = adapter;
        }

        protected override void SetProviders(DataAccessAdapter adapter)
        {
            _service = new SessionService(
                new SessionProvider(adapter),
                new UserProvider(adapter));
        }

        public void Add(Session s)
        {
            using (var adapter = _adapter.ReadAndWrite())
            {
                SetProviders(adapter);

                _service.Add(s);
            }
        }

        public void Refresh(string guid)
        {
            using (var adapter = _adapter.ReadAndWrite())
            {
                SetProviders(adapter);

                _service.Refresh(guid);
            }
        }

        public Session GetBy(string guid)
        {
            using (var adapter = _adapter.Read())
            {
                SetProviders(adapter);

                return _service.GetBy(guid);
            }
        }

        public void Delete(Session s)
        {
            using (var adapter = _adapter.ReadAndWrite())
            {
                SetProviders(adapter);

                _service.Delete(s);
            }
        }
    }
}