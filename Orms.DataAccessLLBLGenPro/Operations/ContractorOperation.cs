﻿using System.Collections.Generic;
using Orms.DataAccessHelper;
using Orms.DataAccessHelper.Operations.Interfaces;
using Orms.DataAccessLLBLGenPro.DatabaseSpecific;
using Orms.DataAccessLLBLGenPro.Providers;
using Orms.Models;
using Orms.Services;
using Orms.Services.Interfaces;

namespace Orms.DataAccessLLBLGenPro.Operations
{
    public class ContractorOperation : Operation<DataAccessAdapter>, IContractorOperation
    {
        private IContractorService _service;
        private readonly SessionWorker<DataAccessAdapter> _adapter;

        public ContractorOperation(SessionWorker<DataAccessAdapter> adapter)
        {
            _adapter = adapter;
        }

        protected override void SetProviders(DataAccessAdapter adapter)
        {
            _service = new ContractorService(
                new ContractorContactProvider(adapter),
                new ContractorProvider(adapter));
        }

        public IEnumerable<Contractor> GetAll()
        {
            using (var adapter = _adapter.Read())
            {
                SetProviders(adapter);

                return _service.GetAll();
            }
        }

        public IEnumerable<ContractorContact> GetAll(Contractor contractor)
        {
            using (var adapter = _adapter.Read())
            {
                SetProviders(adapter);

                return _service.GetAll(contractor);
            }
        }

        public Contractor Get(int id)
        {
            using (var adapter = _adapter.Read())
            {
                SetProviders(adapter);

                return _service.Get(id);
            }
        }

        public ContractorContact GetContact(int id)
        {
            using (var adapter = _adapter.Read())
            {
                SetProviders(adapter);

                return _service.GetContact(id);
            }
        }

        public void Update(ContractorContact contact)
        {
            using (var adapter = _adapter.ReadAndWrite())
            {
                SetProviders(adapter);

                _service.Update(contact);
            }
        }

        public void Add(Contractor contractor)
        {
            using (var adapter = _adapter.ReadAndWrite())
            {
                SetProviders(adapter);

                _service.Add(contractor);
            }
        }

        public void Add(ContractorContact contact, int contractorId)
        {
            using (var adapter = _adapter.Read())
            {
                SetProviders(adapter);

                _service.Add(contact, contractorId);
            }
        }
    }
}