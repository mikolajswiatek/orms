﻿using System;
using System.Collections.Generic;
using Orms.DataAccessHelper;
using Orms.DataAccessHelper.Operations.Interfaces;
using Orms.DataAccessLLBLGenPro.DatabaseSpecific;
using Orms.DataAccessLLBLGenPro.Providers;
using Orms.Models;
using Orms.Services;
using Orms.Services.Interfaces;

namespace Orms.DataAccessLLBLGenPro.Operations
{
    public class CalorificValueOperation : Operation<DataAccessAdapter>, ICalorificValueOperation
    {
        private ICalorificValueService _service;
        private readonly SessionWorker<DataAccessAdapter> _adapter;

        public CalorificValueOperation(SessionWorker<DataAccessAdapter> adapter)
        {
            _adapter = adapter;
        }

        protected override void SetProviders(DataAccessAdapter adapter)
        {
            _service = new CalorificValueService(new CalorificValueProvider(adapter));
        }

        public void Add(CalorificValue cv, IEnumerable<EnergyCalorificValue> values)
        {
            using (var adapter = _adapter.ReadAndWrite())
            {
                SetProviders(adapter);

                _service.Add(cv, values);
            }
        }

        public CalorificValue GetLastBy(GasStorage gasStorage)
        {
            using (var adapter = _adapter.Read())
            {
                SetProviders(adapter);

                return _service.GetLastBy(gasStorage);
            }
        }

        public CalorificValue GetForGasDay(GasStorage gasStorage, DateTime gasDay)
        {
            using (var adapter = _adapter.Read())
            {
                SetProviders(adapter);

                return _service.GetForGasDay(gasStorage, gasDay);
            }
        }

        public CalorificValue Get(int id)
        {
            using (var adapter = _adapter.Read())
            {
                SetProviders(adapter);

                return _service.Get(id);
            }
        }
    }
}