﻿using System;
using System.Collections.Generic;
using Orms.DataAccessHelper;
using Orms.DataAccessHelper.Operations.Interfaces;
using Orms.DataAccessLLBLGenPro.DatabaseSpecific;
using Orms.DataAccessLLBLGenPro.Providers;
using Orms.Models;
using Orms.Services;
using Orms.Services.Interfaces;

namespace Orms.DataAccessLLBLGenPro.Operations
{
    public class GasAccountOperation : Operation<DataAccessAdapter>, IGasAccountOperation
    {
        private IGasAccountService _service;
        private readonly SessionWorker<DataAccessAdapter> _adapter;

        public GasAccountOperation(SessionWorker<DataAccessAdapter> adapter)
        {
            _adapter = adapter;
        }

        protected override void SetProviders(DataAccessAdapter adapter)
        {
            _service = new GasAccountService(
                new GasAccountProvider(adapter),
                new GasStorageProvider(adapter),
                new NominationProvider(adapter));
        }

        public void Update(IEnumerable<GasAccount> gasAccounts)
        {
            using (var adapter = _adapter.ReadAndWrite())
            {
                SetProviders(adapter);

                _service.Update(gasAccounts);
            }
        }

        IEnumerable<GasAccount> IGasAccountOperation.Get(GasStorage gasStorage, DateTime gasDay)
        {
            return Get(gasStorage, gasDay);
        }

        public IEnumerable<GasAccount> Get(DateTime gasDay)
        {
            using (var adapter = _adapter.Read())
            {
                SetProviders(adapter);

                return _service.Get(gasDay);
            }
        }

        public IEnumerable<GasAccount> Get(GasStorage gasStorage, DateTime gasDay)
        {
            using (var adapter = _adapter.Read())
            {
                SetProviders(adapter);

                return _service.Get(gasStorage, gasDay);
            }
        }

        public void Refresh(GasStorage gasStorage)
        {
            using (var adapter = _adapter.ReadAndWrite())
            {
                SetProviders(adapter);

                _service.Refresh(gasStorage);
            }
        }

        public void Refresh()
        {
            using (var adapter = _adapter.ReadAndWrite())
            {
                SetProviders(adapter);

                _service.Refresh();
            }
        }

        public void Refresh(GasStorage gasStorage, GasDay gasDay)
        {
            using (var adapter = _adapter.ReadAndWrite())
            {
                SetProviders(adapter);

                _service.Refresh(gasStorage, gasDay);
            }
        }

        public void Refresh(GasDay gasDay)
        {
            using (var adapter = _adapter.ReadAndWrite())
            {
                SetProviders(adapter);

                _service.Refresh(gasDay);
            }
        }
    }
}