﻿using System;
using System.Collections.Generic;
using Orms.DataAccessHelper;
using Orms.DataAccessHelper.Operations.Interfaces;
using Orms.DataAccessLLBLGenPro.DatabaseSpecific;
using Orms.DataAccessLLBLGenPro.Providers;
using Orms.Models;
using Orms.Services;
using Orms.Services.Interfaces;

namespace Orms.DataAccessLLBLGenPro.Operations
{
    public class AccessPointOperation : Operation<DataAccessAdapter>, IAccessPointOperation
    {
        private IAccessPointService _service;
        private readonly SessionWorker<DataAccessAdapter> _adapter;

        public AccessPointOperation(SessionWorker<DataAccessAdapter> adapter)
        {
            _adapter = adapter;
        }

        protected override void SetProviders(DataAccessAdapter adapter)
        {
            _service = new AccessPointService(
                new AccessPointProvider(adapter),
                new AccessPointFlowProvider(adapter));
        }

        public IEnumerable<AccessPoint> GetBy(GasStorage gasStorage)
        {
            using (var adapter = _adapter.Read())
            {
                SetProviders(adapter);

                return _service.GetBy(gasStorage);
            }
        }

        public IEnumerable<AccessPoint> GetAll()
        {
            using (var adapter = _adapter.Read())
            {
                SetProviders(adapter);

                return _service.GetAll();
            }
        }

        public AccessPoint Get(int id)
        {
            using (var adapter = _adapter.Read())
            {
                SetProviders(adapter);

                return _service.Get(id);
            }
        }

        public void Add(AccessPoint accessPoint)
        {
            using (var adapter = _adapter.ReadAndWrite())
            {
                SetProviders(adapter);

                _service.Add(accessPoint);
            }
        }

        public void Add(AccessPointFlow apf, int accessPointId)
        {
            using (var adapter = _adapter.ReadAndWrite())
            {
                SetProviders(adapter);

                _service.Add(apf, accessPointId);
            }
        }
    }
}