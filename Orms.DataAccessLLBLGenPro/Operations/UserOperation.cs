﻿using System.Collections.Generic;
using Orms.DataAccessHelper;
using Orms.DataAccessHelper.Operations.Interfaces;
using Orms.DataAccessLLBLGenPro.DatabaseSpecific;
using Orms.DataAccessLLBLGenPro.Providers;
using Orms.Models;
using Orms.Services;
using Orms.Services.Interfaces;

namespace Orms.DataAccessLLBLGenPro.Operations
{
    public class UserOperation : Operation<DataAccessAdapter>, IUserOperation
    {
        private IUserService _service;
        private readonly SessionWorker<DataAccessAdapter> _adapter;

        public UserOperation(SessionWorker<DataAccessAdapter> adapter)
        {
            _adapter = adapter;
        }

        protected override void SetProviders(DataAccessAdapter adapter)
        {
            _service = new UserService(
                new UserProvider(adapter),
                new SessionProvider(adapter));
        }

        public User Exist(string nameOrEmail)
        {
            using (var adapter = _adapter.Read())
            {
                SetProviders(adapter);

                return _service.Exist(nameOrEmail);
            }
        }

        public void Create(User user)
        {
            using (var adapter = _adapter.ReadAndWrite())
            {
                SetProviders(adapter);

                _service.Create(user);
            }
        }

        public void Update(User user)
        {
            using (var adapter = _adapter.ReadAndWrite())
            {
                SetProviders(adapter);

                _service.Update(user);
            }
        }

        public User Get(int id)
        {
            using (var adapter = _adapter.Read())
            {
                SetProviders(adapter);

                return _service.Get(id);
            }
        }

        public IEnumerable<User> Get(Contractor contractor)
        {
            using (var adapter = _adapter.Read())
            {
                SetProviders(adapter);

                return _service.Get(contractor);
            }
        }

        public IEnumerable<User> GetAll()
        {
            using (var adapter = _adapter.Read())
            {
                SetProviders(adapter);

                return _service.GetAll();
            }
        }
    }
}