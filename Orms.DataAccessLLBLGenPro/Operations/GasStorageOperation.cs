﻿using System;
using System.Collections.Generic;
using Orms.DataAccessHelper;
using Orms.DataAccessHelper.Operations.Interfaces;
using Orms.DataAccessLLBLGenPro.DatabaseSpecific;
using Orms.DataAccessLLBLGenPro.Providers;
using Orms.Models;
using Orms.Services;
using Orms.Services.Interfaces;

namespace Orms.DataAccessLLBLGenPro.Operations
{
    public class GasStorageOperation : Operation<DataAccessAdapter>, IGasStorageOperation, IDisposable
    {
        private IGasStorageService _service;
        private readonly SessionWorker<DataAccessAdapter> _adapter;

        public GasStorageOperation(SessionWorker<DataAccessAdapter> adapter)
        {
            _adapter = adapter;
        }

        protected override void SetProviders(DataAccessAdapter adapter)
        {
            _service = new GasStorageService(
                new GasStorageProvider(adapter));
        }

        public void Add(GasStorage gasStorage)
        {
            using (var adapter = _adapter.ReadAndWrite())
            {
                SetProviders(adapter);

                _service.Add(gasStorage);
            }
        }

        public GasStorage Get(int id)
        {
            using (var adapter = _adapter.Read())
            {
                SetProviders(adapter);

                return _service.Get(id);
            }
        }

        public IEnumerable<GasStorage> GetAll()
        {
            using (var adapter = _adapter.Read())
            {
                SetProviders(adapter);

                return _service.GetAll();
            }
        }

        public void Dispose()
        {
            _adapter.CloseSession();
        }
    }
}