﻿using System;
using System.Collections.Generic;
using Orms.DataAccessHelper;
using Orms.DataAccessHelper.Operations.Interfaces;
using Orms.DataAccessLLBLGenPro.DatabaseSpecific;
using Orms.DataAccessLLBLGenPro.Providers;
using Orms.Models;
using Orms.Services;
using Orms.Services.Interfaces;

namespace Orms.DataAccessLLBLGenPro.Operations
{
    public class NominationOperation : Operation<DataAccessAdapter>, INominationOperation
    {
        private INominationService _service;
        private readonly SessionWorker<DataAccessAdapter> _adapter;

        public NominationOperation(SessionWorker<DataAccessAdapter> adapter)
        {
            _adapter = adapter;
        }

        protected override void SetProviders(DataAccessAdapter adapter)
        {
            _service = new NominationService(
                new NominationProvider(adapter),
                new NominationValueProvider(adapter));
        }

        public void Add(Nomination nomination)
        {
            using (var adapter = _adapter.ReadAndWrite())
            {
                SetProviders(adapter);

                _service.Add(nomination);
            }
        }

        public void Add(IEnumerable<NominationValue> values)
        {
            using (var adapter = _adapter.ReadAndWrite())
            {
                SetProviders(adapter);

                _service.Add(values);
            }
        }

        public void Add(Nomination nomination, IEnumerable<NominationValue> values)
        {
            using (var adapter = _adapter.ReadAndWrite())
            {
                SetProviders(adapter);

                _service.Add(nomination, values);
            }
        }

        public void RefreshNomiation(Nomination nomination, int parentNominationId)
        {
            using (var adapter = _adapter.ReadAndWrite())
            {
                SetProviders(adapter);

                _service.RefreshNomiation(nomination, parentNominationId);
            }
        }

        public void AcceptedNomination(Nomination nomination)
        {
            using (var adapter = _adapter.ReadAndWrite())
            {
                SetProviders(adapter);

                _service.AcceptedNomination(nomination);
            }
        }

        public IEnumerable<Nomination> GetBy(Contractor contractor)
        {
            using (var adapter = _adapter.Read())
            {
                SetProviders(adapter);

                return _service.GetBy(contractor);
            }
        }

        public IEnumerable<Nomination> GetBy(GasStorage gasStorage)
        {
            using (var adapter = _adapter.Read())
            {
                SetProviders(adapter);

                return _service.GetBy(gasStorage);
            }
        }

        public IEnumerable<Nomination> GetBy(User user)
        {
            using (var adapter = _adapter.Read())
            {
                SetProviders(adapter);

                return _service.GetBy(user);
            }
        }

        public IEnumerable<Nomination> GetBy(DateTime gasDay)
        {
            using (var adapter = _adapter.Read())
            {
                SetProviders(adapter);

                return _service.GetBy(gasDay);
            }
        }

        public Nomination Get(int id)
        {
            using (var adapter = _adapter.Read())
            {
                SetProviders(adapter);

                return _service.Get(id);
            }
        }

        public Nomination GetLast(Contractor contractor)
        {
            using (var adapter = _adapter.Read())
            {
                SetProviders(adapter);

                return _service.GetLast(contractor);
            }
        }

        public Nomination GetLast(GasStorage gasStorage)
        {
            using (var adapter = _adapter.Read())
            {
                SetProviders(adapter);

                return _service.GetLast(gasStorage);
            }
        }

        public Nomination GetLast(User user)
        {
            using (var adapter = _adapter.Read())
            {
                SetProviders(adapter);

                return _service.GetLast(user);
            }
        }

        IEnumerable<Nomination> INominationOperation.GetForGasDay(Contractor contractor, DateTime gasDay)
        {
            return GetForGasDay(contractor, gasDay);
        }

        IEnumerable<Nomination> INominationOperation.GetForGasDay(GasStorage gasStorage, DateTime gasDay)
        {
            return GetForGasDay(gasStorage, gasDay);
        }

        IEnumerable<Nomination> INominationOperation.GetForGasDay(Contractor contractor, GasStorage gasStorage, DateTime gasDay)
        {
            return GetForGasDay(contractor, gasStorage, gasDay);
        }

        public Nomination GetLast(Contractor contractor, DateTime gasDay)
        {
            using (var adapter = _adapter.Read())
            {
                SetProviders(adapter);

                return _service.GetLast(contractor, gasDay);
            }
        }

        public IEnumerable<Nomination> GetForGasDay(Contractor contractor, DateTime gasDay)
        {
            using (var adapter = _adapter.Read())
            {
                SetProviders(adapter);

                return _service.GetForGasDay(contractor, gasDay);
            }
        }

        public IEnumerable<Nomination> GetForGasDay(GasStorage gasStorage, DateTime gasDay)
        {
            using (var adapter = _adapter.Read())
            {
                SetProviders(adapter);

                return _service.GetForGasDay(gasStorage, gasDay);
            }
        }

        public IEnumerable<Nomination> GetForGasDay(Contractor contractor, GasStorage gasStorage, DateTime gasDay)
        {
            using (var adapter = _adapter.Read())
            {
                SetProviders(adapter);

                return _service.GetForGasDay(contractor, gasStorage, gasDay);
            }
        }
    }
}