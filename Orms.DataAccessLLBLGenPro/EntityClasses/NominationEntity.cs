﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Orms.DataAccessLLBLGenPro;
using Orms.DataAccessLLBLGenPro.HelperClasses;
using Orms.DataAccessLLBLGenPro.FactoryClasses;
using Orms.DataAccessLLBLGenPro.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Orms.DataAccessLLBLGenPro.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	
	/// <summary>Entity class which represents the entity 'Nomination'.<br/><br/></summary>
	[Serializable]
	public partial class NominationEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
			
	{
		#region Class Member Declarations
		private EntityCollection<NominationEntity> _nominations;
		private EntityCollection<NominationValueEntity> _nominationValues;
		private AccessPointFlowEntity _accessPointFlow;
		private CalorificValueEntity _calorificValue;
		private ContractorEntity _contractor;
		private NominationEntity _nomination;
		private UserEntity _user;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name AccessPointFlow</summary>
			public static readonly string AccessPointFlow = "AccessPointFlow";
			/// <summary>Member name CalorificValue</summary>
			public static readonly string CalorificValue = "CalorificValue";
			/// <summary>Member name Contractor</summary>
			public static readonly string Contractor = "Contractor";
			/// <summary>Member name Nomination</summary>
			public static readonly string Nomination = "Nomination";
			/// <summary>Member name User</summary>
			public static readonly string User = "User";
			/// <summary>Member name Nominations</summary>
			public static readonly string Nominations = "Nominations";
			/// <summary>Member name NominationValues</summary>
			public static readonly string NominationValues = "NominationValues";
		}
		#endregion
		
		/// <summary> Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static NominationEntity()
		{
			SetupCustomPropertyHashtables();
		}
		
		/// <summary> CTor</summary>
		public NominationEntity():base("NominationEntity")
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <remarks>For framework usage.</remarks>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public NominationEntity(IEntityFields2 fields):base("NominationEntity")
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this NominationEntity</param>
		public NominationEntity(IValidator validator):base("NominationEntity")
		{
			InitClassEmpty(validator, null);
		}
				
		/// <summary> CTor</summary>
		/// <param name="id">PK value for Nomination which data should be fetched into this Nomination object</param>
		/// <remarks>The entity is not fetched by this constructor. Use a DataAccessAdapter for that.</remarks>
		public NominationEntity(System.Int32 id):base("NominationEntity")
		{
			InitClassEmpty(null, null);
			this.Id = id;
		}

		/// <summary> CTor</summary>
		/// <param name="id">PK value for Nomination which data should be fetched into this Nomination object</param>
		/// <param name="validator">The custom validator object for this NominationEntity</param>
		/// <remarks>The entity is not fetched by this constructor. Use a DataAccessAdapter for that.</remarks>
		public NominationEntity(System.Int32 id, IValidator validator):base("NominationEntity")
		{
			InitClassEmpty(validator, null);
			this.Id = id;
		}

		/// <summary> Protected CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected NominationEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			if(SerializationHelper.Optimization != SerializationOptimization.Fast) 
			{
				_nominations = (EntityCollection<NominationEntity>)info.GetValue("_nominations", typeof(EntityCollection<NominationEntity>));
				_nominationValues = (EntityCollection<NominationValueEntity>)info.GetValue("_nominationValues", typeof(EntityCollection<NominationValueEntity>));
				_accessPointFlow = (AccessPointFlowEntity)info.GetValue("_accessPointFlow", typeof(AccessPointFlowEntity));
				if(_accessPointFlow!=null)
				{
					_accessPointFlow.AfterSave+=new EventHandler(OnEntityAfterSave);
				}
				_calorificValue = (CalorificValueEntity)info.GetValue("_calorificValue", typeof(CalorificValueEntity));
				if(_calorificValue!=null)
				{
					_calorificValue.AfterSave+=new EventHandler(OnEntityAfterSave);
				}
				_contractor = (ContractorEntity)info.GetValue("_contractor", typeof(ContractorEntity));
				if(_contractor!=null)
				{
					_contractor.AfterSave+=new EventHandler(OnEntityAfterSave);
				}
				_nomination = (NominationEntity)info.GetValue("_nomination", typeof(NominationEntity));
				if(_nomination!=null)
				{
					_nomination.AfterSave+=new EventHandler(OnEntityAfterSave);
				}
				_user = (UserEntity)info.GetValue("_user", typeof(UserEntity));
				if(_user!=null)
				{
					_user.AfterSave+=new EventHandler(OnEntityAfterSave);
				}
				this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance());
			}
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
			
		}

		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((NominationFieldIndex)fieldIndex)
			{
				case NominationFieldIndex.AccessPointFlowId:
					DesetupSyncAccessPointFlow(true, false);
					break;
				case NominationFieldIndex.CalorificValueId:
					DesetupSyncCalorificValue(true, false);
					break;
				case NominationFieldIndex.ContractorId:
					DesetupSyncContractor(true, false);
					break;
				case NominationFieldIndex.ParentNominationId:
					DesetupSyncNomination(true, false);
					break;
				case NominationFieldIndex.UserId:
					DesetupSyncUser(true, false);
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "AccessPointFlow":
					this.AccessPointFlow = (AccessPointFlowEntity)entity;
					break;
				case "CalorificValue":
					this.CalorificValue = (CalorificValueEntity)entity;
					break;
				case "Contractor":
					this.Contractor = (ContractorEntity)entity;
					break;
				case "Nomination":
					this.Nomination = (NominationEntity)entity;
					break;
				case "User":
					this.User = (UserEntity)entity;
					break;
				case "Nominations":
					this.Nominations.Add((NominationEntity)entity);
					break;
				case "NominationValues":
					this.NominationValues.Add((NominationValueEntity)entity);
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}
		
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "AccessPointFlow":
					toReturn.Add(Relations.AccessPointFlowEntityUsingAccessPointFlowId);
					break;
				case "CalorificValue":
					toReturn.Add(Relations.CalorificValueEntityUsingCalorificValueId);
					break;
				case "Contractor":
					toReturn.Add(Relations.ContractorEntityUsingContractorId);
					break;
				case "Nomination":
					toReturn.Add(Relations.NominationEntityUsingIdParentNominationId);
					break;
				case "User":
					toReturn.Add(Relations.UserEntityUsingUserId);
					break;
				case "Nominations":
					toReturn.Add(Relations.NominationEntityUsingParentNominationId);
					break;
				case "NominationValues":
					toReturn.Add(Relations.NominationValueEntityUsingNominationId);
					break;
				default:
					break;				
			}
			return toReturn;
		}

		/// <summary>Checks if the relation mapped by the property with the name specified is a one way / single sided relation. If the passed in name is null, it/ will return true if the entity has any single-sided relation</summary>
		/// <param name="propertyName">Name of the property which is mapped onto the relation to check, or null to check if the entity has any relation/ which is single sided</param>
		/// <returns>true if the relation is single sided / one way (so the opposite relation isn't present), false otherwise</returns>
		protected override bool CheckOneWayRelations(string propertyName)
		{
			int numberOfOneWayRelations = 0;
			switch(propertyName)
			{
				case null:
					return ((numberOfOneWayRelations > 0) || base.CheckOneWayRelations(null));
				default:
					return base.CheckOneWayRelations(propertyName);
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "AccessPointFlow":
					SetupSyncAccessPointFlow(relatedEntity);
					break;
				case "CalorificValue":
					SetupSyncCalorificValue(relatedEntity);
					break;
				case "Contractor":
					SetupSyncContractor(relatedEntity);
					break;
				case "Nomination":
					SetupSyncNomination(relatedEntity);
					break;
				case "User":
					SetupSyncUser(relatedEntity);
					break;
				case "Nominations":
					this.Nominations.Add((NominationEntity)relatedEntity);
					break;
				case "NominationValues":
					this.NominationValues.Add((NominationValueEntity)relatedEntity);
					break;
				default:
					break;
			}
		}

		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "AccessPointFlow":
					DesetupSyncAccessPointFlow(false, true);
					break;
				case "CalorificValue":
					DesetupSyncCalorificValue(false, true);
					break;
				case "Contractor":
					DesetupSyncContractor(false, true);
					break;
				case "Nomination":
					DesetupSyncNomination(false, true);
					break;
				case "User":
					DesetupSyncUser(false, true);
					break;
				case "Nominations":
					this.PerformRelatedEntityRemoval(this.Nominations, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "NominationValues":
					this.PerformRelatedEntityRemoval(this.NominationValues, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity2 objects, referenced by this entity</returns>
		protected override List<IEntity2> GetDependingRelatedEntities()
		{
			List<IEntity2> toReturn = new List<IEntity2>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These
		/// entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity2 objects, referenced by this entity</returns>
		protected override List<IEntity2> GetDependentRelatedEntities()
		{
			List<IEntity2> toReturn = new List<IEntity2>();
			if(_accessPointFlow!=null)
			{
				toReturn.Add(_accessPointFlow);
			}
			if(_calorificValue!=null)
			{
				toReturn.Add(_calorificValue);
			}
			if(_contractor!=null)
			{
				toReturn.Add(_contractor);
			}
			if(_nomination!=null)
			{
				toReturn.Add(_nomination);
			}
			if(_user!=null)
			{
				toReturn.Add(_user);
			}
			return toReturn;
		}
		
		/// <summary>Gets a list of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection2 objects, referenced by this entity</returns>
		protected override List<IEntityCollection2> GetMemberEntityCollections()
		{
			List<IEntityCollection2> toReturn = new List<IEntityCollection2>();
			toReturn.Add(this.Nominations);
			toReturn.Add(this.NominationValues);
			return toReturn;
		}

		/// <summary>ISerializable member. Does custom serialization so event handlers do not get serialized. Serializes members of this entity class and uses the base class' implementation to serialize the rest.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (SerializationHelper.Optimization != SerializationOptimization.Fast) 
			{
				info.AddValue("_nominations", ((_nominations!=null) && (_nominations.Count>0) && !this.MarkedForDeletion)?_nominations:null);
				info.AddValue("_nominationValues", ((_nominationValues!=null) && (_nominationValues.Count>0) && !this.MarkedForDeletion)?_nominationValues:null);
				info.AddValue("_accessPointFlow", (!this.MarkedForDeletion?_accessPointFlow:null));
				info.AddValue("_calorificValue", (!this.MarkedForDeletion?_calorificValue:null));
				info.AddValue("_contractor", (!this.MarkedForDeletion?_contractor:null));
				info.AddValue("_nomination", (!this.MarkedForDeletion?_nomination:null));
				info.AddValue("_user", (!this.MarkedForDeletion?_user:null));
			}
			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			base.GetObjectData(info, context);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new NominationRelations().GetAllRelations();
		}

		/// <summary> Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Nomination' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoNominations()
		{
			IRelationPredicateBucket bucket = new RelationPredicateBucket();
			bucket.PredicateExpression.Add(new FieldCompareValuePredicate(NominationFields.ParentNominationId, null, ComparisonOperator.Equal, this.Id));
			return bucket;
		}

		/// <summary> Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'NominationValue' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoNominationValues()
		{
			IRelationPredicateBucket bucket = new RelationPredicateBucket();
			bucket.PredicateExpression.Add(new FieldCompareValuePredicate(NominationValueFields.NominationId, null, ComparisonOperator.Equal, this.Id));
			return bucket;
		}

		/// <summary> Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'AccessPointFlow' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoAccessPointFlow()
		{
			IRelationPredicateBucket bucket = new RelationPredicateBucket();
			bucket.PredicateExpression.Add(new FieldCompareValuePredicate(AccessPointFlowFields.Id, null, ComparisonOperator.Equal, this.AccessPointFlowId));
			return bucket;
		}

		/// <summary> Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'CalorificValue' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoCalorificValue()
		{
			IRelationPredicateBucket bucket = new RelationPredicateBucket();
			bucket.PredicateExpression.Add(new FieldCompareValuePredicate(CalorificValueFields.Id, null, ComparisonOperator.Equal, this.CalorificValueId));
			return bucket;
		}

		/// <summary> Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Contractor' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoContractor()
		{
			IRelationPredicateBucket bucket = new RelationPredicateBucket();
			bucket.PredicateExpression.Add(new FieldCompareValuePredicate(ContractorFields.Id, null, ComparisonOperator.Equal, this.ContractorId));
			return bucket;
		}

		/// <summary> Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Nomination' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoNomination()
		{
			IRelationPredicateBucket bucket = new RelationPredicateBucket();
			bucket.PredicateExpression.Add(new FieldCompareValuePredicate(NominationFields.Id, null, ComparisonOperator.Equal, this.ParentNominationId));
			return bucket;
		}

		/// <summary> Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'User' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoUser()
		{
			IRelationPredicateBucket bucket = new RelationPredicateBucket();
			bucket.PredicateExpression.Add(new FieldCompareValuePredicate(UserFields.Id, null, ComparisonOperator.Equal, this.UserId));
			return bucket;
		}
		

		/// <summary>Creates a new instance of the factory related to this entity</summary>
		protected override IEntityFactory2 CreateEntityFactory()
		{
			return EntityFactoryCache2.GetEntityFactory(typeof(NominationEntityFactory));
		}

		/// <summary>Adds the member collections to the collections queue (base first)</summary>
		/// <param name="collectionsQueue">The collections queue.</param>
		protected override void AddToMemberEntityCollectionsQueue(Queue<IEntityCollection2> collectionsQueue) 
		{
			base.AddToMemberEntityCollectionsQueue(collectionsQueue);
			collectionsQueue.Enqueue(this._nominations);
			collectionsQueue.Enqueue(this._nominationValues);
		}
		
		/// <summary>Gets the member collections queue from the queue (base first)</summary>
		/// <param name="collectionsQueue">The collections queue.</param>
		protected override void GetFromMemberEntityCollectionsQueue(Queue<IEntityCollection2> collectionsQueue)
		{
			base.GetFromMemberEntityCollectionsQueue(collectionsQueue);
			this._nominations = (EntityCollection<NominationEntity>) collectionsQueue.Dequeue();
			this._nominationValues = (EntityCollection<NominationValueEntity>) collectionsQueue.Dequeue();

		}
		
		/// <summary>Determines whether the entity has populated member collections</summary>
		/// <returns>true if the entity has populated member collections.</returns>
		protected override bool HasPopulatedMemberEntityCollections()
		{
			bool toReturn = false;
			toReturn |=(this._nominations != null);
			toReturn |=(this._nominationValues != null);
			return toReturn ? true : base.HasPopulatedMemberEntityCollections();
		}
		
		/// <summary>Creates the member entity collections queue.</summary>
		/// <param name="collectionsQueue">The collections queue.</param>
		/// <param name="requiredQueue">The required queue.</param>
		protected override void CreateMemberEntityCollectionsQueue(Queue<IEntityCollection2> collectionsQueue, Queue<bool> requiredQueue) 
		{
			base.CreateMemberEntityCollectionsQueue(collectionsQueue, requiredQueue);
			collectionsQueue.Enqueue(requiredQueue.Dequeue() ? new EntityCollection<NominationEntity>(EntityFactoryCache2.GetEntityFactory(typeof(NominationEntityFactory))) : null);
			collectionsQueue.Enqueue(requiredQueue.Dequeue() ? new EntityCollection<NominationValueEntity>(EntityFactoryCache2.GetEntityFactory(typeof(NominationValueEntityFactory))) : null);
		}

		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("AccessPointFlow", _accessPointFlow);
			toReturn.Add("CalorificValue", _calorificValue);
			toReturn.Add("Contractor", _contractor);
			toReturn.Add("Nomination", _nomination);
			toReturn.Add("User", _user);
			toReturn.Add("Nominations", _nominations);
			toReturn.Add("NominationValues", _nominationValues);
			return toReturn;
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			OnInitClassMembersComplete();
		}


		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AccessPointFlowId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CalorificValueId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Channel", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Comment", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ContractorId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreateDatetime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("GasDay", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Id", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsAccepted", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsLast", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MajorVersion", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParentNominationId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UserId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Version", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _accessPointFlow</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncAccessPointFlow(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _accessPointFlow, new PropertyChangedEventHandler( OnAccessPointFlowPropertyChanged ), "AccessPointFlow", Orms.DataAccessLLBLGenPro.RelationClasses.StaticNominationRelations.AccessPointFlowEntityUsingAccessPointFlowIdStatic, true, signalRelatedEntity, "Nominations", resetFKFields, new int[] { (int)NominationFieldIndex.AccessPointFlowId } );
			_accessPointFlow = null;
		}

		/// <summary> setups the sync logic for member _accessPointFlow</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncAccessPointFlow(IEntityCore relatedEntity)
		{
			if(_accessPointFlow!=relatedEntity)
			{
				DesetupSyncAccessPointFlow(true, true);
				_accessPointFlow = (AccessPointFlowEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _accessPointFlow, new PropertyChangedEventHandler( OnAccessPointFlowPropertyChanged ), "AccessPointFlow", Orms.DataAccessLLBLGenPro.RelationClasses.StaticNominationRelations.AccessPointFlowEntityUsingAccessPointFlowIdStatic, true, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnAccessPointFlowPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _calorificValue</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCalorificValue(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _calorificValue, new PropertyChangedEventHandler( OnCalorificValuePropertyChanged ), "CalorificValue", Orms.DataAccessLLBLGenPro.RelationClasses.StaticNominationRelations.CalorificValueEntityUsingCalorificValueIdStatic, true, signalRelatedEntity, "Nominations", resetFKFields, new int[] { (int)NominationFieldIndex.CalorificValueId } );
			_calorificValue = null;
		}

		/// <summary> setups the sync logic for member _calorificValue</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCalorificValue(IEntityCore relatedEntity)
		{
			if(_calorificValue!=relatedEntity)
			{
				DesetupSyncCalorificValue(true, true);
				_calorificValue = (CalorificValueEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _calorificValue, new PropertyChangedEventHandler( OnCalorificValuePropertyChanged ), "CalorificValue", Orms.DataAccessLLBLGenPro.RelationClasses.StaticNominationRelations.CalorificValueEntityUsingCalorificValueIdStatic, true, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCalorificValuePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _contractor</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncContractor(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _contractor, new PropertyChangedEventHandler( OnContractorPropertyChanged ), "Contractor", Orms.DataAccessLLBLGenPro.RelationClasses.StaticNominationRelations.ContractorEntityUsingContractorIdStatic, true, signalRelatedEntity, "Nominations", resetFKFields, new int[] { (int)NominationFieldIndex.ContractorId } );
			_contractor = null;
		}

		/// <summary> setups the sync logic for member _contractor</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncContractor(IEntityCore relatedEntity)
		{
			if(_contractor!=relatedEntity)
			{
				DesetupSyncContractor(true, true);
				_contractor = (ContractorEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _contractor, new PropertyChangedEventHandler( OnContractorPropertyChanged ), "Contractor", Orms.DataAccessLLBLGenPro.RelationClasses.StaticNominationRelations.ContractorEntityUsingContractorIdStatic, true, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnContractorPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _nomination</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncNomination(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _nomination, new PropertyChangedEventHandler( OnNominationPropertyChanged ), "Nomination", Orms.DataAccessLLBLGenPro.RelationClasses.StaticNominationRelations.NominationEntityUsingIdParentNominationIdStatic, true, signalRelatedEntity, "Nominations", resetFKFields, new int[] { (int)NominationFieldIndex.ParentNominationId } );
			_nomination = null;
		}

		/// <summary> setups the sync logic for member _nomination</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncNomination(IEntityCore relatedEntity)
		{
			if(_nomination!=relatedEntity)
			{
				DesetupSyncNomination(true, true);
				_nomination = (NominationEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _nomination, new PropertyChangedEventHandler( OnNominationPropertyChanged ), "Nomination", Orms.DataAccessLLBLGenPro.RelationClasses.StaticNominationRelations.NominationEntityUsingIdParentNominationIdStatic, true, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnNominationPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _user</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncUser(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _user, new PropertyChangedEventHandler( OnUserPropertyChanged ), "User", Orms.DataAccessLLBLGenPro.RelationClasses.StaticNominationRelations.UserEntityUsingUserIdStatic, true, signalRelatedEntity, "Nominations", resetFKFields, new int[] { (int)NominationFieldIndex.UserId } );
			_user = null;
		}

		/// <summary> setups the sync logic for member _user</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncUser(IEntityCore relatedEntity)
		{
			if(_user!=relatedEntity)
			{
				DesetupSyncUser(true, true);
				_user = (UserEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _user, new PropertyChangedEventHandler( OnUserPropertyChanged ), "User", Orms.DataAccessLLBLGenPro.RelationClasses.StaticNominationRelations.UserEntityUsingUserIdStatic, true, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnUserPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this NominationEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END
			

			OnInitialized();

		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static NominationRelations Relations
		{
			get	{ return new NominationRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Nomination' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathNominations
		{
			get	{ return new PrefetchPathElement2( new EntityCollection<NominationEntity>(EntityFactoryCache2.GetEntityFactory(typeof(NominationEntityFactory))), (IEntityRelation)GetRelationsForField("Nominations")[0], (int)Orms.DataAccessLLBLGenPro.EntityType.NominationEntity, (int)Orms.DataAccessLLBLGenPro.EntityType.NominationEntity, 0, null, null, null, null, "Nominations", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany);	}
		}

		/// <summary> Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'NominationValue' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathNominationValues
		{
			get	{ return new PrefetchPathElement2( new EntityCollection<NominationValueEntity>(EntityFactoryCache2.GetEntityFactory(typeof(NominationValueEntityFactory))), (IEntityRelation)GetRelationsForField("NominationValues")[0], (int)Orms.DataAccessLLBLGenPro.EntityType.NominationEntity, (int)Orms.DataAccessLLBLGenPro.EntityType.NominationValueEntity, 0, null, null, null, null, "NominationValues", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany);	}
		}

		/// <summary> Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'AccessPointFlow' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathAccessPointFlow
		{
			get	{ return new PrefetchPathElement2(new EntityCollection(EntityFactoryCache2.GetEntityFactory(typeof(AccessPointFlowEntityFactory))),	(IEntityRelation)GetRelationsForField("AccessPointFlow")[0], (int)Orms.DataAccessLLBLGenPro.EntityType.NominationEntity, (int)Orms.DataAccessLLBLGenPro.EntityType.AccessPointFlowEntity, 0, null, null, null, null, "AccessPointFlow", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'CalorificValue' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathCalorificValue
		{
			get	{ return new PrefetchPathElement2(new EntityCollection(EntityFactoryCache2.GetEntityFactory(typeof(CalorificValueEntityFactory))),	(IEntityRelation)GetRelationsForField("CalorificValue")[0], (int)Orms.DataAccessLLBLGenPro.EntityType.NominationEntity, (int)Orms.DataAccessLLBLGenPro.EntityType.CalorificValueEntity, 0, null, null, null, null, "CalorificValue", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Contractor' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathContractor
		{
			get	{ return new PrefetchPathElement2(new EntityCollection(EntityFactoryCache2.GetEntityFactory(typeof(ContractorEntityFactory))),	(IEntityRelation)GetRelationsForField("Contractor")[0], (int)Orms.DataAccessLLBLGenPro.EntityType.NominationEntity, (int)Orms.DataAccessLLBLGenPro.EntityType.ContractorEntity, 0, null, null, null, null, "Contractor", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Nomination' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathNomination
		{
			get	{ return new PrefetchPathElement2(new EntityCollection(EntityFactoryCache2.GetEntityFactory(typeof(NominationEntityFactory))),	(IEntityRelation)GetRelationsForField("Nomination")[0], (int)Orms.DataAccessLLBLGenPro.EntityType.NominationEntity, (int)Orms.DataAccessLLBLGenPro.EntityType.NominationEntity, 0, null, null, null, null, "Nomination", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'User' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathUser
		{
			get	{ return new PrefetchPathElement2(new EntityCollection(EntityFactoryCache2.GetEntityFactory(typeof(UserEntityFactory))),	(IEntityRelation)GetRelationsForField("User")[0], (int)Orms.DataAccessLLBLGenPro.EntityType.NominationEntity, (int)Orms.DataAccessLLBLGenPro.EntityType.UserEntity, 0, null, null, null, null, "User", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The AccessPointFlowId property of the Entity Nomination<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Nominations"."AccessPointFlowId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 AccessPointFlowId
		{
			get { return (System.Int32)GetValue((int)NominationFieldIndex.AccessPointFlowId, true); }
			set	{ SetValue((int)NominationFieldIndex.AccessPointFlowId, value); }
		}

		/// <summary> The CalorificValueId property of the Entity Nomination<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Nominations"."CalorificValueId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CalorificValueId
		{
			get { return (System.Int32)GetValue((int)NominationFieldIndex.CalorificValueId, true); }
			set	{ SetValue((int)NominationFieldIndex.CalorificValueId, value); }
		}

		/// <summary> The Channel property of the Entity Nomination<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Nominations"."Channel"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Channel
		{
			get { return (System.String)GetValue((int)NominationFieldIndex.Channel, true); }
			set	{ SetValue((int)NominationFieldIndex.Channel, value); }
		}

		/// <summary> The Comment property of the Entity Nomination<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Nominations"."Comment"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 1<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Comment
		{
			get { return (System.String)GetValue((int)NominationFieldIndex.Comment, true); }
			set	{ SetValue((int)NominationFieldIndex.Comment, value); }
		}

		/// <summary> The ContractorId property of the Entity Nomination<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Nominations"."ContractorId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ContractorId
		{
			get { return (System.Int32)GetValue((int)NominationFieldIndex.ContractorId, true); }
			set	{ SetValue((int)NominationFieldIndex.ContractorId, value); }
		}

		/// <summary> The CreateDatetime property of the Entity Nomination<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Nominations"."CreateDatetime"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime CreateDatetime
		{
			get { return (System.DateTime)GetValue((int)NominationFieldIndex.CreateDatetime, true); }
			set	{ SetValue((int)NominationFieldIndex.CreateDatetime, value); }
		}

		/// <summary> The GasDay property of the Entity Nomination<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Nominations"."GasDay"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime GasDay
		{
			get { return (System.DateTime)GetValue((int)NominationFieldIndex.GasDay, true); }
			set	{ SetValue((int)NominationFieldIndex.GasDay, value); }
		}

		/// <summary> The Id property of the Entity Nomination<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Nominations"."Id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 Id
		{
			get { return (System.Int32)GetValue((int)NominationFieldIndex.Id, true); }
			set	{ SetValue((int)NominationFieldIndex.Id, value); }
		}

		/// <summary> The IsAccepted property of the Entity Nomination<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Nominations"."IsAccepted"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsAccepted
		{
			get { return (System.Boolean)GetValue((int)NominationFieldIndex.IsAccepted, true); }
			set	{ SetValue((int)NominationFieldIndex.IsAccepted, value); }
		}

		/// <summary> The IsLast property of the Entity Nomination<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Nominations"."IsLast"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsLast
		{
			get { return (System.Boolean)GetValue((int)NominationFieldIndex.IsLast, true); }
			set	{ SetValue((int)NominationFieldIndex.IsLast, value); }
		}

		/// <summary> The MajorVersion property of the Entity Nomination<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Nominations"."MajorVersion"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 MajorVersion
		{
			get { return (System.Int32)GetValue((int)NominationFieldIndex.MajorVersion, true); }
			set	{ SetValue((int)NominationFieldIndex.MajorVersion, value); }
		}

		/// <summary> The Name property of the Entity Nomination<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Nominations"."Name"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)NominationFieldIndex.Name, true); }
			set	{ SetValue((int)NominationFieldIndex.Name, value); }
		}

		/// <summary> The ParentNominationId property of the Entity Nomination<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Nominations"."ParentNominationId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ParentNominationId
		{
			get { return (Nullable<System.Int32>)GetValue((int)NominationFieldIndex.ParentNominationId, false); }
			set	{ SetValue((int)NominationFieldIndex.ParentNominationId, value); }
		}

		/// <summary> The UserId property of the Entity Nomination<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Nominations"."UserId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UserId
		{
			get { return (System.Int32)GetValue((int)NominationFieldIndex.UserId, true); }
			set	{ SetValue((int)NominationFieldIndex.UserId, value); }
		}

		/// <summary> The Version property of the Entity Nomination<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Nominations"."Version"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Version
		{
			get { return (System.Int32)GetValue((int)NominationFieldIndex.Version, true); }
			set	{ SetValue((int)NominationFieldIndex.Version, value); }
		}

		/// <summary> Gets the EntityCollection with the related entities of type 'NominationEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(NominationEntity))]
		public virtual EntityCollection<NominationEntity> Nominations
		{
			get { return GetOrCreateEntityCollection<NominationEntity, NominationEntityFactory>("Nomination", true, false, ref _nominations);	}
		}

		/// <summary> Gets the EntityCollection with the related entities of type 'NominationValueEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(NominationValueEntity))]
		public virtual EntityCollection<NominationValueEntity> NominationValues
		{
			get { return GetOrCreateEntityCollection<NominationValueEntity, NominationValueEntityFactory>("Nomination", true, false, ref _nominationValues);	}
		}

		/// <summary> Gets / sets related entity of type 'AccessPointFlowEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual AccessPointFlowEntity AccessPointFlow
		{
			get	{ return _accessPointFlow; }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncAccessPointFlow(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Nominations", "AccessPointFlow", _accessPointFlow, true); 
				}
			}
		}

		/// <summary> Gets / sets related entity of type 'CalorificValueEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual CalorificValueEntity CalorificValue
		{
			get	{ return _calorificValue; }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncCalorificValue(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Nominations", "CalorificValue", _calorificValue, true); 
				}
			}
		}

		/// <summary> Gets / sets related entity of type 'ContractorEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual ContractorEntity Contractor
		{
			get	{ return _contractor; }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncContractor(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Nominations", "Contractor", _contractor, true); 
				}
			}
		}

		/// <summary> Gets / sets related entity of type 'NominationEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual NominationEntity Nomination
		{
			get	{ return _nomination; }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncNomination(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Nominations", "Nomination", _nomination, true); 
				}
			}
		}

		/// <summary> Gets / sets related entity of type 'UserEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual UserEntity User
		{
			get	{ return _user; }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncUser(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Nominations", "User", _user, true); 
				}
			}
		}
	
		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}
		
		/// <summary>Returns the Orms.DataAccessLLBLGenPro.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Orms.DataAccessLLBLGenPro.EntityType.NominationEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		
		#endregion

		#region Included code

		#endregion
	}
}
