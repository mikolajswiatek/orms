﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Orms.DataAccessLLBLGenPro;
using Orms.DataAccessLLBLGenPro.HelperClasses;
using Orms.DataAccessLLBLGenPro.FactoryClasses;
using Orms.DataAccessLLBLGenPro.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Orms.DataAccessLLBLGenPro.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	
	/// <summary>Entity class which represents the entity 'GasStorage'.<br/><br/></summary>
	[Serializable]
	public partial class GasStorageEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
			
	{
		#region Class Member Declarations
		private EntityCollection<AccessPointEntity> _accessPoints;
		private EntityCollection<AgreementEntity> _agreements;
		private EntityCollection<CalorificValueEntity> _calorificValues;
		private EntityCollection<GasAccountEntity> _gasAccounts;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name AccessPoints</summary>
			public static readonly string AccessPoints = "AccessPoints";
			/// <summary>Member name Agreements</summary>
			public static readonly string Agreements = "Agreements";
			/// <summary>Member name CalorificValues</summary>
			public static readonly string CalorificValues = "CalorificValues";
			/// <summary>Member name GasAccounts</summary>
			public static readonly string GasAccounts = "GasAccounts";
		}
		#endregion
		
		/// <summary> Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static GasStorageEntity()
		{
			SetupCustomPropertyHashtables();
		}
		
		/// <summary> CTor</summary>
		public GasStorageEntity():base("GasStorageEntity")
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <remarks>For framework usage.</remarks>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public GasStorageEntity(IEntityFields2 fields):base("GasStorageEntity")
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this GasStorageEntity</param>
		public GasStorageEntity(IValidator validator):base("GasStorageEntity")
		{
			InitClassEmpty(validator, null);
		}
				
		/// <summary> CTor</summary>
		/// <param name="id">PK value for GasStorage which data should be fetched into this GasStorage object</param>
		/// <remarks>The entity is not fetched by this constructor. Use a DataAccessAdapter for that.</remarks>
		public GasStorageEntity(System.Int32 id):base("GasStorageEntity")
		{
			InitClassEmpty(null, null);
			this.Id = id;
		}

		/// <summary> CTor</summary>
		/// <param name="id">PK value for GasStorage which data should be fetched into this GasStorage object</param>
		/// <param name="validator">The custom validator object for this GasStorageEntity</param>
		/// <remarks>The entity is not fetched by this constructor. Use a DataAccessAdapter for that.</remarks>
		public GasStorageEntity(System.Int32 id, IValidator validator):base("GasStorageEntity")
		{
			InitClassEmpty(validator, null);
			this.Id = id;
		}

		/// <summary> Protected CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected GasStorageEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			if(SerializationHelper.Optimization != SerializationOptimization.Fast) 
			{
				_accessPoints = (EntityCollection<AccessPointEntity>)info.GetValue("_accessPoints", typeof(EntityCollection<AccessPointEntity>));
				_agreements = (EntityCollection<AgreementEntity>)info.GetValue("_agreements", typeof(EntityCollection<AgreementEntity>));
				_calorificValues = (EntityCollection<CalorificValueEntity>)info.GetValue("_calorificValues", typeof(EntityCollection<CalorificValueEntity>));
				_gasAccounts = (EntityCollection<GasAccountEntity>)info.GetValue("_gasAccounts", typeof(EntityCollection<GasAccountEntity>));
				this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance());
			}
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
			
		}


		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "AccessPoints":
					this.AccessPoints.Add((AccessPointEntity)entity);
					break;
				case "Agreements":
					this.Agreements.Add((AgreementEntity)entity);
					break;
				case "CalorificValues":
					this.CalorificValues.Add((CalorificValueEntity)entity);
					break;
				case "GasAccounts":
					this.GasAccounts.Add((GasAccountEntity)entity);
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}
		
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "AccessPoints":
					toReturn.Add(Relations.AccessPointEntityUsingGasStorageId);
					break;
				case "Agreements":
					toReturn.Add(Relations.AgreementEntityUsingGasStorageId);
					break;
				case "CalorificValues":
					toReturn.Add(Relations.CalorificValueEntityUsingGasStorageId);
					break;
				case "GasAccounts":
					toReturn.Add(Relations.GasAccountEntityUsingGasStorageId);
					break;
				default:
					break;				
			}
			return toReturn;
		}

		/// <summary>Checks if the relation mapped by the property with the name specified is a one way / single sided relation. If the passed in name is null, it/ will return true if the entity has any single-sided relation</summary>
		/// <param name="propertyName">Name of the property which is mapped onto the relation to check, or null to check if the entity has any relation/ which is single sided</param>
		/// <returns>true if the relation is single sided / one way (so the opposite relation isn't present), false otherwise</returns>
		protected override bool CheckOneWayRelations(string propertyName)
		{
			int numberOfOneWayRelations = 0;
			switch(propertyName)
			{
				case null:
					return ((numberOfOneWayRelations > 0) || base.CheckOneWayRelations(null));
				default:
					return base.CheckOneWayRelations(propertyName);
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "AccessPoints":
					this.AccessPoints.Add((AccessPointEntity)relatedEntity);
					break;
				case "Agreements":
					this.Agreements.Add((AgreementEntity)relatedEntity);
					break;
				case "CalorificValues":
					this.CalorificValues.Add((CalorificValueEntity)relatedEntity);
					break;
				case "GasAccounts":
					this.GasAccounts.Add((GasAccountEntity)relatedEntity);
					break;
				default:
					break;
			}
		}

		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "AccessPoints":
					this.PerformRelatedEntityRemoval(this.AccessPoints, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Agreements":
					this.PerformRelatedEntityRemoval(this.Agreements, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "CalorificValues":
					this.PerformRelatedEntityRemoval(this.CalorificValues, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "GasAccounts":
					this.PerformRelatedEntityRemoval(this.GasAccounts, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity2 objects, referenced by this entity</returns>
		protected override List<IEntity2> GetDependingRelatedEntities()
		{
			List<IEntity2> toReturn = new List<IEntity2>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These
		/// entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity2 objects, referenced by this entity</returns>
		protected override List<IEntity2> GetDependentRelatedEntities()
		{
			List<IEntity2> toReturn = new List<IEntity2>();
			return toReturn;
		}
		
		/// <summary>Gets a list of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection2 objects, referenced by this entity</returns>
		protected override List<IEntityCollection2> GetMemberEntityCollections()
		{
			List<IEntityCollection2> toReturn = new List<IEntityCollection2>();
			toReturn.Add(this.AccessPoints);
			toReturn.Add(this.Agreements);
			toReturn.Add(this.CalorificValues);
			toReturn.Add(this.GasAccounts);
			return toReturn;
		}

		/// <summary>ISerializable member. Does custom serialization so event handlers do not get serialized. Serializes members of this entity class and uses the base class' implementation to serialize the rest.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (SerializationHelper.Optimization != SerializationOptimization.Fast) 
			{
				info.AddValue("_accessPoints", ((_accessPoints!=null) && (_accessPoints.Count>0) && !this.MarkedForDeletion)?_accessPoints:null);
				info.AddValue("_agreements", ((_agreements!=null) && (_agreements.Count>0) && !this.MarkedForDeletion)?_agreements:null);
				info.AddValue("_calorificValues", ((_calorificValues!=null) && (_calorificValues.Count>0) && !this.MarkedForDeletion)?_calorificValues:null);
				info.AddValue("_gasAccounts", ((_gasAccounts!=null) && (_gasAccounts.Count>0) && !this.MarkedForDeletion)?_gasAccounts:null);
			}
			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			base.GetObjectData(info, context);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new GasStorageRelations().GetAllRelations();
		}

		/// <summary> Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'AccessPoint' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoAccessPoints()
		{
			IRelationPredicateBucket bucket = new RelationPredicateBucket();
			bucket.PredicateExpression.Add(new FieldCompareValuePredicate(AccessPointFields.GasStorageId, null, ComparisonOperator.Equal, this.Id));
			return bucket;
		}

		/// <summary> Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Agreement' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoAgreements()
		{
			IRelationPredicateBucket bucket = new RelationPredicateBucket();
			bucket.PredicateExpression.Add(new FieldCompareValuePredicate(AgreementFields.GasStorageId, null, ComparisonOperator.Equal, this.Id));
			return bucket;
		}

		/// <summary> Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'CalorificValue' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoCalorificValues()
		{
			IRelationPredicateBucket bucket = new RelationPredicateBucket();
			bucket.PredicateExpression.Add(new FieldCompareValuePredicate(CalorificValueFields.GasStorageId, null, ComparisonOperator.Equal, this.Id));
			return bucket;
		}

		/// <summary> Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'GasAccount' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoGasAccounts()
		{
			IRelationPredicateBucket bucket = new RelationPredicateBucket();
			bucket.PredicateExpression.Add(new FieldCompareValuePredicate(GasAccountFields.GasStorageId, null, ComparisonOperator.Equal, this.Id));
			return bucket;
		}
		

		/// <summary>Creates a new instance of the factory related to this entity</summary>
		protected override IEntityFactory2 CreateEntityFactory()
		{
			return EntityFactoryCache2.GetEntityFactory(typeof(GasStorageEntityFactory));
		}

		/// <summary>Adds the member collections to the collections queue (base first)</summary>
		/// <param name="collectionsQueue">The collections queue.</param>
		protected override void AddToMemberEntityCollectionsQueue(Queue<IEntityCollection2> collectionsQueue) 
		{
			base.AddToMemberEntityCollectionsQueue(collectionsQueue);
			collectionsQueue.Enqueue(this._accessPoints);
			collectionsQueue.Enqueue(this._agreements);
			collectionsQueue.Enqueue(this._calorificValues);
			collectionsQueue.Enqueue(this._gasAccounts);
		}
		
		/// <summary>Gets the member collections queue from the queue (base first)</summary>
		/// <param name="collectionsQueue">The collections queue.</param>
		protected override void GetFromMemberEntityCollectionsQueue(Queue<IEntityCollection2> collectionsQueue)
		{
			base.GetFromMemberEntityCollectionsQueue(collectionsQueue);
			this._accessPoints = (EntityCollection<AccessPointEntity>) collectionsQueue.Dequeue();
			this._agreements = (EntityCollection<AgreementEntity>) collectionsQueue.Dequeue();
			this._calorificValues = (EntityCollection<CalorificValueEntity>) collectionsQueue.Dequeue();
			this._gasAccounts = (EntityCollection<GasAccountEntity>) collectionsQueue.Dequeue();

		}
		
		/// <summary>Determines whether the entity has populated member collections</summary>
		/// <returns>true if the entity has populated member collections.</returns>
		protected override bool HasPopulatedMemberEntityCollections()
		{
			bool toReturn = false;
			toReturn |=(this._accessPoints != null);
			toReturn |=(this._agreements != null);
			toReturn |=(this._calorificValues != null);
			toReturn |=(this._gasAccounts != null);
			return toReturn ? true : base.HasPopulatedMemberEntityCollections();
		}
		
		/// <summary>Creates the member entity collections queue.</summary>
		/// <param name="collectionsQueue">The collections queue.</param>
		/// <param name="requiredQueue">The required queue.</param>
		protected override void CreateMemberEntityCollectionsQueue(Queue<IEntityCollection2> collectionsQueue, Queue<bool> requiredQueue) 
		{
			base.CreateMemberEntityCollectionsQueue(collectionsQueue, requiredQueue);
			collectionsQueue.Enqueue(requiredQueue.Dequeue() ? new EntityCollection<AccessPointEntity>(EntityFactoryCache2.GetEntityFactory(typeof(AccessPointEntityFactory))) : null);
			collectionsQueue.Enqueue(requiredQueue.Dequeue() ? new EntityCollection<AgreementEntity>(EntityFactoryCache2.GetEntityFactory(typeof(AgreementEntityFactory))) : null);
			collectionsQueue.Enqueue(requiredQueue.Dequeue() ? new EntityCollection<CalorificValueEntity>(EntityFactoryCache2.GetEntityFactory(typeof(CalorificValueEntityFactory))) : null);
			collectionsQueue.Enqueue(requiredQueue.Dequeue() ? new EntityCollection<GasAccountEntity>(EntityFactoryCache2.GetEntityFactory(typeof(GasAccountEntityFactory))) : null);
		}

		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("AccessPoints", _accessPoints);
			toReturn.Add("Agreements", _agreements);
			toReturn.Add("CalorificValues", _calorificValues);
			toReturn.Add("GasAccounts", _gasAccounts);
			return toReturn;
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			OnInitClassMembersComplete();
		}


		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Id", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
		}
		#endregion

		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this GasStorageEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END
			

			OnInitialized();

		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static GasStorageRelations Relations
		{
			get	{ return new GasStorageRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'AccessPoint' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathAccessPoints
		{
			get	{ return new PrefetchPathElement2( new EntityCollection<AccessPointEntity>(EntityFactoryCache2.GetEntityFactory(typeof(AccessPointEntityFactory))), (IEntityRelation)GetRelationsForField("AccessPoints")[0], (int)Orms.DataAccessLLBLGenPro.EntityType.GasStorageEntity, (int)Orms.DataAccessLLBLGenPro.EntityType.AccessPointEntity, 0, null, null, null, null, "AccessPoints", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany);	}
		}

		/// <summary> Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Agreement' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathAgreements
		{
			get	{ return new PrefetchPathElement2( new EntityCollection<AgreementEntity>(EntityFactoryCache2.GetEntityFactory(typeof(AgreementEntityFactory))), (IEntityRelation)GetRelationsForField("Agreements")[0], (int)Orms.DataAccessLLBLGenPro.EntityType.GasStorageEntity, (int)Orms.DataAccessLLBLGenPro.EntityType.AgreementEntity, 0, null, null, null, null, "Agreements", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany);	}
		}

		/// <summary> Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'CalorificValue' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathCalorificValues
		{
			get	{ return new PrefetchPathElement2( new EntityCollection<CalorificValueEntity>(EntityFactoryCache2.GetEntityFactory(typeof(CalorificValueEntityFactory))), (IEntityRelation)GetRelationsForField("CalorificValues")[0], (int)Orms.DataAccessLLBLGenPro.EntityType.GasStorageEntity, (int)Orms.DataAccessLLBLGenPro.EntityType.CalorificValueEntity, 0, null, null, null, null, "CalorificValues", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany);	}
		}

		/// <summary> Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'GasAccount' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathGasAccounts
		{
			get	{ return new PrefetchPathElement2( new EntityCollection<GasAccountEntity>(EntityFactoryCache2.GetEntityFactory(typeof(GasAccountEntityFactory))), (IEntityRelation)GetRelationsForField("GasAccounts")[0], (int)Orms.DataAccessLLBLGenPro.EntityType.GasStorageEntity, (int)Orms.DataAccessLLBLGenPro.EntityType.GasAccountEntity, 0, null, null, null, null, "GasAccounts", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany);	}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The Id property of the Entity GasStorage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "GasStorages"."Id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 Id
		{
			get { return (System.Int32)GetValue((int)GasStorageFieldIndex.Id, true); }
			set	{ SetValue((int)GasStorageFieldIndex.Id, value); }
		}

		/// <summary> The Name property of the Entity GasStorage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "GasStorages"."Name"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)GasStorageFieldIndex.Name, true); }
			set	{ SetValue((int)GasStorageFieldIndex.Name, value); }
		}

		/// <summary> Gets the EntityCollection with the related entities of type 'AccessPointEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(AccessPointEntity))]
		public virtual EntityCollection<AccessPointEntity> AccessPoints
		{
			get { return GetOrCreateEntityCollection<AccessPointEntity, AccessPointEntityFactory>("GasStorage", true, false, ref _accessPoints);	}
		}

		/// <summary> Gets the EntityCollection with the related entities of type 'AgreementEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(AgreementEntity))]
		public virtual EntityCollection<AgreementEntity> Agreements
		{
			get { return GetOrCreateEntityCollection<AgreementEntity, AgreementEntityFactory>("GasStorage", true, false, ref _agreements);	}
		}

		/// <summary> Gets the EntityCollection with the related entities of type 'CalorificValueEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(CalorificValueEntity))]
		public virtual EntityCollection<CalorificValueEntity> CalorificValues
		{
			get { return GetOrCreateEntityCollection<CalorificValueEntity, CalorificValueEntityFactory>("GasStorage", true, false, ref _calorificValues);	}
		}

		/// <summary> Gets the EntityCollection with the related entities of type 'GasAccountEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(GasAccountEntity))]
		public virtual EntityCollection<GasAccountEntity> GasAccounts
		{
			get { return GetOrCreateEntityCollection<GasAccountEntity, GasAccountEntityFactory>("GasStorage", true, false, ref _gasAccounts);	}
		}
	
		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}
		
		/// <summary>Returns the Orms.DataAccessLLBLGenPro.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Orms.DataAccessLLBLGenPro.EntityType.GasStorageEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		
		#endregion

		#region Included code

		#endregion
	}
}
