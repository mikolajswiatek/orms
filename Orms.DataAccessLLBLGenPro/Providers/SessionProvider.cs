﻿using System.Collections.Generic;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.DataAccessLLBLGenPro.DatabaseSpecific;
using Orms.DataAccessLLBLGenPro.Repositories;
using Orms.Models;
using Orms.Providers;

namespace Orms.DataAccessLLBLGenPro.Providers
{
    public class SessionProvider : ISessionProvider
    {
        private readonly ISessionRepository<DataAccessAdapter> _repository;
        private readonly DataAccessAdapter _adapter;

        public SessionProvider(DataAccessAdapter adapter)
        {
            _repository = new SessionRepository();
            _adapter = adapter;
        }

        public IEnumerable<Session> GetAll()
        {
            return _repository.GetAll(_adapter);
        }

        public Session Get(int id)
        {
            return _repository.Get(_adapter, id);
        }

        public void Update(Session s)
        {
            _repository.Update(_adapter, s);
        }

        public void Add(Session s)
        {
            _repository.Add(_adapter, s);
        }

        public Session GetBy(string guid)
        {
            return _repository.GetBy(_adapter, guid);
        }

        public void Delete(Session s)
        {
            _repository.Delete(_adapter, s);
        }
    }
}