﻿using System;
using System.Collections.Generic;
using Orms.DataAccessHelper;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.DataAccessLLBLGenPro.DatabaseSpecific;
using Orms.DataAccessLLBLGenPro.Repositories;
using Orms.Models;
using Orms.Providers;

namespace Orms.DataAccessLLBLGenPro.Providers
{
    public class ContractorContactProvider : IContractorContactProvider
    {
        private readonly IContractorContactRepository<DataAccessAdapter> _repository;
        private readonly DataAccessAdapter _adapter;

        public ContractorContactProvider(DataAccessAdapter adapter)
        {
            _repository = new ContractorContactRepository();
            _adapter = adapter;
        }

        public IEnumerable<ContractorContact> GetAll()
        {
            return _repository.GetAll(_adapter);
        }

        public ContractorContact Get(int id)
        {
            return _repository.Get(_adapter, id);
        }

        public void Update(ContractorContact contact)
        {
            _repository.Update(_adapter, contact);
        }

        public void Add(ContractorContact contact)
        {
            _repository.Add(_adapter, contact);
        }

        public IEnumerable<ContractorContact> GetByContractor(Contractor contractor)
        {
            return _repository.GetByContractor(_adapter, contractor);
        }
    }
}