﻿using System;
using System.Collections.Generic;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.DataAccessLLBLGenPro.DatabaseSpecific;
using Orms.DataAccessLLBLGenPro.Repositories;
using Orms.Models;
using Orms.Providers;

namespace Orms.DataAccessLLBLGenPro.Providers
{
    public class NominationProvider : INominationProvider
    {
        private readonly INominationRepository<DataAccessAdapter> _repository;
        private readonly DataAccessAdapter _adapter;

        public NominationProvider(DataAccessAdapter adapter)
        {
            _repository = new NominationRepository();
            _adapter = adapter;
        }

        public IEnumerable<Nomination> GetAll()
        {
            return _repository.GetAll(_adapter);
        }

        public Nomination Get(int id)
        {
            return _repository.Get(_adapter, id);
        }

        public void Update(Nomination nomination)
        {
            _repository.Update(_adapter, nomination);
        }

        public void Add(Nomination nomination)
        {
            _repository.Add(_adapter, nomination);
        }

        public IEnumerable<Nomination> GetBy(Contractor contractor)
        {
            return _repository.GetBy(_adapter, contractor);
        }

        public IEnumerable<Nomination> GetBy(GasStorage gasStorage)
        {
            return _repository.GetBy(_adapter, gasStorage);
        }

        public IEnumerable<Nomination> GetBy(User user)
        {
            return _repository.GetBy(_adapter, user);
        }

        public IEnumerable<Nomination> GetBy(DateTime gasDay)
        {
            return _repository.GetBy(_adapter, gasDay);
        }

        public Nomination GetLast(Contractor contractor)
        {
            return _repository.GetLast(_adapter, contractor);
        }

        public Nomination GetLast(GasStorage gasStorage)
        {
            return _repository.GetLast(_adapter, gasStorage);
        }

        public Nomination GetLast(User user)
        {
            return _repository.GetLast(_adapter, user);
        }

        public Nomination GetLast(Contractor contractor, DateTime gasDay)
        {
            return _repository.GetLast(_adapter, contractor, gasDay);
        }

        public IEnumerable<Nomination> GetForGasDay(Contractor contractor, DateTime gasDay)
        {
            return _repository.GetForGasDay(_adapter, contractor, gasDay);
        }

        public IEnumerable<Nomination> GetForGasDay(GasStorage gasStorage, DateTime gasDay)
        {
            return _repository.GetForGasDay(_adapter, gasStorage, gasDay);
        }

        public IEnumerable<Nomination> GetForGasDay(Contractor contractor, GasStorage gasStorage, DateTime gasDay)
        {
            return _repository.GetForGasDay(_adapter, contractor, gasStorage, gasDay);
        }
    }
}