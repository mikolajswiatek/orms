﻿using System;
using System.Collections.Generic;
using Orms.DataAccessHelper;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.DataAccessLLBLGenPro.DatabaseSpecific;
using Orms.DataAccessLLBLGenPro.Repositories;
using Orms.Models;
using Orms.Providers;

namespace Orms.DataAccessLLBLGenPro.Providers
{
    public class ContractorProvider : IContractorProvider
    {
        private readonly IContractorRepository<DataAccessAdapter> _repository;
        private readonly DataAccessAdapter _adapter;

        public ContractorProvider(DataAccessAdapter adapter)
        {
            _repository = new ContractorRepository();
            _adapter = adapter;
        }

        public IEnumerable<Contractor> GetAll()
        {
            return _repository.GetAll(_adapter);
        }

        public Contractor Get(int id)
        {
            return _repository.Get(_adapter, id);
        }

        public void Update(Contractor contractor)
        {
            _repository.Update(_adapter, contractor);
        }

        public void Add(Contractor contractor)
        {
            _repository.Add(_adapter, contractor);
        }
    }
}