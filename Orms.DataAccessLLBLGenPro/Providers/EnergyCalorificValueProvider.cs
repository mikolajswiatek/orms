﻿using System.Collections.Generic;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.DataAccessLLBLGenPro.DatabaseSpecific;
using Orms.DataAccessLLBLGenPro.Repositories;
using Orms.Models;
using Orms.Providers;

namespace Orms.DataAccessLLBLGenPro.Providers
{
    public class EnergyCalorificValueProvider : IEnergyCalorificValueProvider
    {
        private readonly IEnergyCalorificValueRepository<DataAccessAdapter> _repository;
        private readonly DataAccessAdapter _adapter;

        public EnergyCalorificValueProvider(DataAccessAdapter adapter)
        {
            _repository = new EnergyCalorificValueRepository();
            _adapter = adapter;
        }

        public IEnumerable<EnergyCalorificValue> GetAll()
        {
            return _repository.GetAll(_adapter);
        }

        public EnergyCalorificValue Get(int id)
        {
            return _repository.Get(_adapter, id);
        }

        public void Update(EnergyCalorificValue ecv)
        {
            _repository.Update(_adapter, ecv);
        }

        public void Add(EnergyCalorificValue ecv)
        {
            _repository.Add(_adapter, ecv);
        }

        public void Add(IEnumerable<EnergyCalorificValue> ecvs)
        {
            _repository.Add(_adapter, ecvs);
        }

        public IEnumerable<EnergyCalorificValue> GetAll(CalorificValue cv)
        {
            return _repository.GetAll(_adapter, cv);
        }
    }
}