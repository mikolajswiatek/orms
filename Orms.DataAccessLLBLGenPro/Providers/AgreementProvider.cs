﻿using System;
using System.Collections.Generic;
using Orms.DataAccessHelper;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.DataAccessLLBLGenPro.DatabaseSpecific;
using Orms.DataAccessLLBLGenPro.Repositories;
using Orms.Models;
using Orms.Providers;

namespace Orms.DataAccessLLBLGenPro.Providers
{
    public class AgreementProvider : IAgreementProvider
    {
        private readonly IAgreementRepository<DataAccessAdapter> _repository;
        private readonly DataAccessAdapter _adapter;

        public AgreementProvider(DataAccessAdapter adapter)
        {
            _repository = new AgreementRepository();
            _adapter = adapter;
        }

        public IEnumerable<Agreement> GetAll()
        {
            return _repository.GetAll(_adapter);
        }

        public Agreement Get(int id)
        {
            return _repository.Get(_adapter, id);
        }

        public void Update(Agreement agreement)
        {
            _repository.Update(_adapter, agreement);
        }

        public void Add(Agreement agreement)
        {
            _repository.Add(_adapter, agreement);
        }

        public IEnumerable<Agreement> GetBy(Contractor contractor)
        {
            return _repository.GetBy(_adapter, contractor);
        }

        public IEnumerable<Agreement> GetBy(GasStorage gasStorage)
        {
            return _repository.GetBy(_adapter, gasStorage);
        }
    }
}