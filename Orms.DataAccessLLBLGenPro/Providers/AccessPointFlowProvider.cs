﻿using System.Collections.Generic;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.DataAccessLLBLGenPro.DatabaseSpecific;
using Orms.DataAccessLLBLGenPro.Repositories;
using Orms.Models;
using Orms.Providers;

namespace Orms.DataAccessLLBLGenPro.Providers
{
    public class AccessPointFlowProvider : IAccessPointFlowProvider
    {
        private readonly IAccessPointFlowRepository<DataAccessAdapter> _repository;
        private readonly DataAccessAdapter _adapter;

        public AccessPointFlowProvider(DataAccessAdapter adapter)
        {
            _repository = new AccessPointFlowRepository();
            _adapter = adapter;
        }

        public IEnumerable<AccessPointFlow> GetAll()
        {
            return _repository.GetAll(_adapter);
        }

        public AccessPointFlow Get(int id)
        {
            return _repository.Get(_adapter, id);
        }

        public void Update(AccessPointFlow accessPointFlow)
        {
            _repository.Update(_adapter, accessPointFlow);
        }

        public void Add(AccessPointFlow accessPointFlow)
        {
            _repository.Add(_adapter, accessPointFlow);
        }
    }
}