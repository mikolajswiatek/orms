﻿using System.Collections.Generic;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.DataAccessLLBLGenPro.DatabaseSpecific;
using Orms.DataAccessLLBLGenPro.Repositories;
using Orms.Models;
using Orms.Providers;

namespace Orms.DataAccessLLBLGenPro.Providers
{
    public class UserProvider : IUserProvider
    {
        private readonly IUserRepository<DataAccessAdapter> _repository;
        private readonly DataAccessAdapter _adapter;

        public UserProvider(DataAccessAdapter adapter)
        {
            _repository = new UserRepository();
            _adapter = adapter;
        }

        public IEnumerable<User> GetAll()
        {
            return _repository.GetAll(_adapter);
        }

        public User Get(int id)
        {
            return _repository.Get(_adapter, id);
        }

        public void Update(User user)
        {
            _repository.Update(_adapter, user);
        }

        public void Add(User user)
        {
            _repository.Add(_adapter, user);
        }

        public IEnumerable<User> GetBy(Contractor contractor)
        {
            return _repository.GetBy(_adapter, contractor);
        }

        public IEnumerable<User> GetBy(string nameOrEmail)
        {
            return _repository.GetBy(_adapter, nameOrEmail);
        }
    }
}