﻿using System;
using System.Collections.Generic;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.DataAccessLLBLGenPro.DatabaseSpecific;
using Orms.DataAccessLLBLGenPro.Repositories;
using Orms.Models;
using Orms.Providers;

namespace Orms.DataAccessLLBLGenPro.Providers
{
    public class GasAccountProvider : IGasAccountProvider
    {
        private readonly IGasAccountRepository<DataAccessAdapter> _repository;
        private readonly DataAccessAdapter _adapter;

        public GasAccountProvider(DataAccessAdapter adapter)
        {
            _repository = new GasAccountRepository();
            _adapter = adapter;
        }

        public IEnumerable<GasAccount> GetAll()
        {
            return _repository.GetAll(_adapter);
        }

        public GasAccount Get(int id)
        {
            return _repository.Get(_adapter, id);
        }

        public void Update(GasAccount gasAccount)
        {
            _repository.Update(_adapter, gasAccount);
        }

        public void Add(GasAccount gasAccount)
        {
            _repository.Add(_adapter, gasAccount);
        }

        public void Add(IEnumerable<GasAccount> gasAccounts)
        {
            _repository.Add(_adapter, gasAccounts);
        }

        public void Update(IEnumerable<GasAccount> gasAccounts)
        {
            _repository.Update(_adapter, gasAccounts);
        }

        public IEnumerable<GasAccount> Get(DateTime gasDay)
        {
            return _repository.Get(_adapter, gasDay);
        }

        public IEnumerable<GasAccount> Get(GasStorage gasStorage, DateTime gasDay)
        {
            return _repository.Get(_adapter, gasStorage, gasDay);
        }
    }
}