﻿using System;
using System.Collections.Generic;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.DataAccessLLBLGenPro.DatabaseSpecific;
using Orms.DataAccessLLBLGenPro.Repositories;
using Orms.Models;
using Orms.Providers;

namespace Orms.DataAccessLLBLGenPro.Providers
{
    public class CalorificValueProvider : ICalorificValueProvider
    {
        private readonly ICalorificValueRepository<DataAccessAdapter> _repository;
        private readonly DataAccessAdapter _adapter;

        public CalorificValueProvider(DataAccessAdapter adapter)
        {
            _repository = new CalorificValueRepository();
            _adapter = adapter;
        }

        public IEnumerable<CalorificValue> GetAll()
        {
            return _repository.GetAll(_adapter);
        }

        public CalorificValue Get(int id)
        {
            return _repository.Get(_adapter, id);
        }

        public void Update(CalorificValue cv)
        {
            _repository.Update(_adapter, cv);
        }

        public void Add(CalorificValue cv)
        {
            _repository.Add(_adapter, cv);
        }

        public CalorificValue GetLastBy(GasStorage gasStorage)
        {
            return _repository.GetLastBy(_adapter, gasStorage);
        }

        public CalorificValue GetForGasDay(GasStorage gasStorage, DateTime gasDay)
        {
            return _repository.GetForGasDay(_adapter, gasStorage, gasDay);
        }
    }
}