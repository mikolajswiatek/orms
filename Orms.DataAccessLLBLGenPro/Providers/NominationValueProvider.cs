﻿using System.Collections.Generic;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.DataAccessLLBLGenPro.DatabaseSpecific;
using Orms.DataAccessLLBLGenPro.Repositories;
using Orms.Models;
using Orms.Providers;

namespace Orms.DataAccessLLBLGenPro.Providers
{
    public class NominationValueProvider : INominationValueProvider
    {
        private readonly INominationValueRepository<DataAccessAdapter> _repository;
        private readonly DataAccessAdapter _adapter;

        public NominationValueProvider(DataAccessAdapter adapter)
        {
            _repository = new NominationValueRepository();
            _adapter = adapter;
        }

        public IEnumerable<NominationValue> GetAll()
        {
            return _repository.GetAll(_adapter);
        }

        public NominationValue Get(int id)
        {
            return _repository.Get(_adapter, id);
        }

        public void Update(NominationValue value)
        {
            _repository.Update(_adapter, value);
        }

        public void Add(NominationValue value)
        {
            _repository.Add(_adapter, value);
        }

        public void Add(IEnumerable<NominationValue> values)
        {
            _repository.Add(_adapter, values);
        }

        public IEnumerable<NominationValue> GetBy(Nomination nomination)
        {
            return _repository.GetBy(_adapter, nomination);
        }
    }
}