﻿using System.Collections.Generic;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.DataAccessLLBLGenPro.DatabaseSpecific;
using Orms.DataAccessLLBLGenPro.Repositories;
using Orms.Models;
using Orms.Providers;

namespace Orms.DataAccessLLBLGenPro.Providers
{
    public class AccessPointProvider : IAccessPointProvider
    {
        private readonly IAccessPointRepository<DataAccessAdapter> _repository;
        private readonly DataAccessAdapter _adapter;

        public AccessPointProvider(DataAccessAdapter adapter)
        {
            _repository = new AccessPointRepository();
            _adapter = adapter;
        }

        public IEnumerable<AccessPoint> GetAll()
        {
            return _repository.GetAll(_adapter);
        }

        public AccessPoint Get(int id)
        {
            return _repository.Get(_adapter, id);
        }

        public void Update(AccessPoint accessPoint)
        {
            _repository.Update(_adapter, accessPoint);
        }

        public void Add(AccessPoint accessPoint)
        {
            _repository.Add(_adapter, accessPoint);
        }

        public IEnumerable<AccessPoint> GetBy(GasStorage gasStorage)
        {
            return _repository.GetBy(_adapter, gasStorage);
        }
    }
}