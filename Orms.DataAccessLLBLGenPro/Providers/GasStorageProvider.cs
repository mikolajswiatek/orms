﻿using System.Collections.Generic;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.DataAccessLLBLGenPro.DatabaseSpecific;
using Orms.DataAccessLLBLGenPro.Repositories;
using Orms.Models;
using Orms.Providers;

namespace Orms.DataAccessLLBLGenPro.Providers
{
    public class GasStorageProvider : IGasStorageProvider
    {
        private readonly IGasStorageRepository<DataAccessAdapter> _repository;
        private readonly DataAccessAdapter _adapter;

        public GasStorageProvider(DataAccessAdapter adapter)
        {
            _repository = new GasStorageRepository();
            _adapter = adapter;
        }

        public IEnumerable<GasStorage> GetAll()
        {
            return _repository.GetAll(_adapter);
        }

        public GasStorage Get(int id)
        {
            return _repository.Get(_adapter, id);
        }

        public void Update(GasStorage gasStorage)
        {
            _repository.Update(_adapter, gasStorage);
        }

        public void Add(GasStorage gasStorage)
        {
            _repository.Add(_adapter, gasStorage);
        }
    }
}