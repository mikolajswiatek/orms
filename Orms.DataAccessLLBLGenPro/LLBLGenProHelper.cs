﻿using System;
using Orms.DataAccessHelper;
using Orms.DataAccessLLBLGenPro.DatabaseSpecific;

namespace Orms.DataAccessLLBLGenPro
{
    public class LlblGenProHelper : SessionWorker<DataAccessAdapter>
    {
        private DataAccessAdapter instance;

        public LlblGenProHelper(string connectionString) : base(connectionString)
        {
        }

        public override void OpenSession()
        {
            isOpen = true;
        }

        public override void CloseSession()
        {
            if (instance != null)
            {
                instance.CloseConnection();
                instance.Dispose();
                instance = null;
            }

            isOpen = false;
        }

        public override DataAccessAdapter Read()
        {
            isOpen = true;
            return new DataAccessAdapter(_connectionString);
        }

        public override DataAccessAdapter ReadAndWrite()
        {
            isOpen = true;
            return new DataAccessAdapter(_connectionString);
        }

        public override void SaveChanges()
        {
            throw new System.NotImplementedException();
        }
    }
}