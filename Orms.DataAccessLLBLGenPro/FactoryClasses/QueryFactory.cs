﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
////////////////////////////////////////////////////////////// 
using System;
using System.Linq;
using Orms.DataAccessLLBLGenPro.EntityClasses;
using Orms.DataAccessLLBLGenPro.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using SD.LLBLGen.Pro.QuerySpec;

namespace Orms.DataAccessLLBLGenPro.FactoryClasses
{
	/// <summary>Factory class to produce DynamicQuery instances and EntityQuery instances</summary>
	public partial class QueryFactory
	{
		private int _aliasCounter = 0;

		/// <summary>Creates a new DynamicQuery instance with no alias set.</summary>
		/// <returns>Ready to use DynamicQuery instance</returns>
		public DynamicQuery Create()
		{
			return Create(string.Empty);
		}

		/// <summary>Creates a new DynamicQuery instance with the alias specified as the alias set.</summary>
		/// <param name="alias">The alias.</param>
		/// <returns>Ready to use DynamicQuery instance</returns>
		public DynamicQuery Create(string alias)
		{
			return new DynamicQuery(new ElementCreator(), alias, this.GetNextAliasCounterValue());
		}

		/// <summary>Creates a new DynamicQuery which wraps the specified TableValuedFunction call</summary>
		/// <param name="toWrap">The table valued function call to wrap.</param>
		/// <returns>toWrap wrapped in a DynamicQuery.</returns>
		public DynamicQuery Create(TableValuedFunctionCall toWrap)
		{
			return this.Create().From(new TvfCallWrapper(toWrap)).Select(toWrap.GetFieldsAsArray().Select(f => this.Field(toWrap.Alias, f.Alias)).ToArray());
		}

		/// <summary>Creates a new EntityQuery for the entity of the type specified with no alias set.</summary>
		/// <typeparam name="TEntity">The type of the entity to produce the query for.</typeparam>
		/// <returns>ready to use EntityQuery instance</returns>
		public EntityQuery<TEntity> Create<TEntity>()
			where TEntity : IEntityCore
		{
			return Create<TEntity>(string.Empty);
		}

		/// <summary>Creates a new EntityQuery for the entity of the type specified with the alias specified as the alias set.</summary>
		/// <typeparam name="TEntity">The type of the entity to produce the query for.</typeparam>
		/// <param name="alias">The alias.</param>
		/// <returns>ready to use EntityQuery instance</returns>
		public EntityQuery<TEntity> Create<TEntity>(string alias)
			where TEntity : IEntityCore
		{
			return new EntityQuery<TEntity>(new ElementCreator(), alias, this.GetNextAliasCounterValue());
		}
				
		/// <summary>Creates a new field object with the name specified and of resulttype 'object'. Used for referring to aliased fields in another projection.</summary>
		/// <param name="fieldName">Name of the field.</param>
		/// <returns>Ready to use field object</returns>
		public EntityField2 Field(string fieldName)
		{
			return Field<object>(string.Empty, fieldName);
		}

		/// <summary>Creates a new field object with the name specified and of resulttype 'object'. Used for referring to aliased fields in another projection.</summary>
		/// <param name="targetAlias">The alias of the table/query to target.</param>
		/// <param name="fieldName">Name of the field.</param>
		/// <returns>Ready to use field object</returns>
		public EntityField2 Field(string targetAlias, string fieldName)
		{
			return Field<object>(targetAlias, fieldName);
		}

		/// <summary>Creates a new field object with the name specified and of resulttype 'TValue'. Used for referring to aliased fields in another projection.</summary>
		/// <typeparam name="TValue">The type of the value represented by the field.</typeparam>
		/// <param name="fieldName">Name of the field.</param>
		/// <returns>Ready to use field object</returns>
		public EntityField2 Field<TValue>(string fieldName)
		{
			return Field<TValue>(string.Empty, fieldName);
		}

		/// <summary>Creates a new field object with the name specified and of resulttype 'TValue'. Used for referring to aliased fields in another projection.</summary>
		/// <typeparam name="TValue">The type of the value.</typeparam>
		/// <param name="targetAlias">The alias of the table/query to target.</param>
		/// <param name="fieldName">Name of the field.</param>
		/// <returns>Ready to use field object</returns>
		public EntityField2 Field<TValue>(string targetAlias, string fieldName)
		{
			return new EntityField2(fieldName, targetAlias, typeof(TValue));
		}
		
		/// <summary>Gets the next alias counter value to produce artifical aliases with</summary>
		private int GetNextAliasCounterValue()
		{
			_aliasCounter++;
			return _aliasCounter;
		}
		

		/// <summary>Creates and returns a new EntityQuery for the AccessPoint entity</summary>
		public EntityQuery<AccessPointEntity> AccessPoint
		{
			get { return Create<AccessPointEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the AccessPointFlow entity</summary>
		public EntityQuery<AccessPointFlowEntity> AccessPointFlow
		{
			get { return Create<AccessPointFlowEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Agreement entity</summary>
		public EntityQuery<AgreementEntity> Agreement
		{
			get { return Create<AgreementEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CalorificValue entity</summary>
		public EntityQuery<CalorificValueEntity> CalorificValue
		{
			get { return Create<CalorificValueEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CalorificValueValue entity</summary>
		public EntityQuery<CalorificValueValueEntity> CalorificValueValue
		{
			get { return Create<CalorificValueValueEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Contractor entity</summary>
		public EntityQuery<ContractorEntity> Contractor
		{
			get { return Create<ContractorEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ContractorContact entity</summary>
		public EntityQuery<ContractorContactEntity> ContractorContact
		{
			get { return Create<ContractorContactEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the GasAccount entity</summary>
		public EntityQuery<GasAccountEntity> GasAccount
		{
			get { return Create<GasAccountEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the GasStorage entity</summary>
		public EntityQuery<GasStorageEntity> GasStorage
		{
			get { return Create<GasStorageEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Nomination entity</summary>
		public EntityQuery<NominationEntity> Nomination
		{
			get { return Create<NominationEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the NominationValue entity</summary>
		public EntityQuery<NominationValueEntity> NominationValue
		{
			get { return Create<NominationValueEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Session entity</summary>
		public EntityQuery<SessionEntity> Session
		{
			get { return Create<SessionEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the User entity</summary>
		public EntityQuery<UserEntity> User
		{
			get { return Create<UserEntity>(); }
		}


 
	}
}