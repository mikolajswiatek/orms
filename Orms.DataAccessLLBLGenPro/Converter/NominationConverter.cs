﻿using System;
using System.Linq;
using Orms.DataAccessLLBLGenPro.EntityClasses;
using Orms.Domains.Enums;
using Orms.Models;

namespace Orms.DataAccessLLBLGenPro.Converter
{
    public static partial class Converter
    {
        public static NominationEntity ToEntity(this Nomination nomination)
        {
            return new NominationEntity(nomination.Id)
            {
                Id = nomination.Id,
                Name = nomination.Name,
                AccessPointFlowId = nomination.AccessPointFlow.Id,
                Channel = nomination.Channel.ToString(),
                ContractorId = nomination.Contractor.Id,
                GasDay = nomination.GasDay,
                MajorVersion = nomination.MajorVersion,
                Version = nomination.Version,
                ParentNominationId = nomination.ParentNomination?.Id,
                UserId = nomination.Creator.Id,
                IsLast = nomination.IsLast,
                Comment = nomination.Comment,
                CalorificValueId = nomination.CalorificValue.Id,
                IsAccepted = nomination.IsAccepted,
                CreateDatetime = nomination.CreateDateTime
            };
        }

        public static Nomination ToModel(this NominationEntity nomination)
        {
            if (nomination == null) return null;

            return new Nomination()
            {
                Id = nomination.Id,
                Name = nomination.Name,
                AccessPointFlow = nomination.AccessPointFlow.ToModel(),
                Channel = (Channel)Enum.Parse(typeof(Channel), nomination.Channel),
                Contractor = nomination.Contractor.ToModel(),
                GasDay = nomination.GasDay,
                MajorVersion = nomination.MajorVersion,
                Version = nomination.Version,
                ParentNomination = nomination.Nomination.ToModel(),
                Creator = nomination.User.ToModel(),
                IsLast = nomination.IsLast,
                IsAccepted = nomination.IsAccepted,
                Comment = nomination.Comment,
                CalorificValue = nomination.CalorificValue.ToModel(),
                CreateDateTime = nomination.CreateDatetime,
                Values = nomination.NominationValues.Select(nv => nv.ToModel())
            };
        }
    }
}