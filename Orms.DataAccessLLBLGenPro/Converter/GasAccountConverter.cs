﻿using System;
using System.Linq;
using Orms.DataAccessLLBLGenPro.EntityClasses;
using Orms.Domains.Enums;
using Orms.Domains.Units;
using Orms.Models;

namespace Orms.DataAccessLLBLGenPro.Converter
{
    public static partial class Converter
    {
        public static GasAccountEntity ToEntity(this GasAccount gasAccount)
        {
            return new GasAccountEntity()
            {
                Id = gasAccount.Id,
                GasDay = gasAccount.GasDay,
                EnergyUnit = gasAccount.Energy.Unit.ToString(),
                EnergyValue = gasAccount.Energy.Value,
                VolumeUnit = gasAccount.Volume.Unit.ToString(),
                VolumeValue = gasAccount.Volume.Value,
                GasStorageId = gasAccount.GasStorage.Id,
                HourIndex = gasAccount.HourIndex
            };
        }

        public static GasAccount ToModel(this GasAccountEntity gasAccount)
        {
            return new GasAccount()
            {
                Id = gasAccount.Id,
                GasDay = gasAccount.GasDay,
                Energy = new UnitValue((decimal)gasAccount.EnergyValue, (Unit)Enum.Parse(typeof(Unit), (string)gasAccount.EnergyUnit, true)),
                Volume = new UnitValue((decimal)gasAccount.VolumeValue, (Unit)Enum.Parse(typeof(Unit), (string)gasAccount.VolumeUnit, true)),
                GasStorage = gasAccount.GasStorage.ToModel(),
                HourIndex = gasAccount.HourIndex
            };
        }
    }
}