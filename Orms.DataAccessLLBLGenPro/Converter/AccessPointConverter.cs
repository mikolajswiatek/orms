﻿using System.Linq;
using Orms.DataAccessLLBLGenPro.EntityClasses;
using Orms.Models;

namespace Orms.DataAccessLLBLGenPro.Converter
{
    public static partial class Converter
    {
        public static AccessPointEntity ToEntity(this AccessPoint accessPoint)
        {
            return new AccessPointEntity()
            {
                Id = accessPoint.Id,
                Code = accessPoint.Code,
                GasStorageId = accessPoint.GasStorage.Id
            };
        }

        public static AccessPoint ToModel(this AccessPointEntity accessPoint)
        {
            if (accessPoint == null) return null;

            return new AccessPoint()
            {
                Id = accessPoint.Id,
                Code = accessPoint.Code,
                GasStorage = accessPoint.GasStorage.ToModel(),
                AccessPointFlows = accessPoint.AccessPointFlows.Select(apf => apf.ToModel())
            };
        }
    }
}