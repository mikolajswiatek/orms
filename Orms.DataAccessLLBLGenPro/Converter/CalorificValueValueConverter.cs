﻿using System;
using Orms.DataAccessLLBLGenPro.EntityClasses;
using Orms.Domains.Enums;
using Orms.Domains.Units;
using Orms.Models;

namespace Orms.DataAccessLLBLGenPro.Converter
{
    public static partial class Converter
    {
        public static CalorificValueValueEntity ToEntity(this EnergyCalorificValue ecv)
        {
            return new CalorificValueValueEntity()
            {
                Id = ecv.Id,
                CalorificValue = ecv.CalorificValue.ToEntity(),
                HourIndex = ecv.HourIndex,
                EnergyValue = ecv.Energy.Value,
                EnergyUnit = ecv.Energy.Unit.ToString(),
                CalorificValueId = ecv.CalorificValue.Id
            };
        }

        public static EnergyCalorificValue ToModel(this CalorificValueValueEntity ecv)
        {
            return new EnergyCalorificValue()
            {
                Id = ecv.Id,
                CalorificValue = ecv.CalorificValue.ToModel(),
                Energy = new UnitValue((decimal)ecv.EnergyValue, (Unit)Enum.Parse(typeof(Unit), (string)ecv.EnergyUnit, true)),
                HourIndex = ecv.HourIndex
            };
        }
    }
}