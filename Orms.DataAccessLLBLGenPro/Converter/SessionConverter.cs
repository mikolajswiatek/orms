﻿using Orms.DataAccessLLBLGenPro.EntityClasses;
using Orms.Models;

namespace Orms.DataAccessLLBLGenPro.Converter
{
    public static partial class Converter
    {
        public static SessionEntity ToEntity(this Session session)
        {
            return new SessionEntity()
            {
                Id = session.Id,
                Guid = session.Guid,
                UserId = session.User != null ? session.User.Id : 0,
                Ip = session.Ip,
                Browser = session.Browser,
                LastActivityDatetime = session.LastActivityDatetime,
                CreateDatetime = session.CreateDatetime,
            };
        }

        public static Session ToModel(this SessionEntity session)
        {
            return new Session()
            {
                Id = session.Id,
                Guid = session.Guid,
                User = session.User.ToModel(),
                Ip = session.Ip,
                Browser = session.Browser,
                LastActivityDatetime = session.LastActivityDatetime,
                CreateDatetime = session.CreateDatetime,
            };
        }
    }
}