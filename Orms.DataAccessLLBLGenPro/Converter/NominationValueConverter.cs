﻿using System;
using System.Linq;
using Orms.DataAccessLLBLGenPro.EntityClasses;
using Orms.Domains.Enums;
using Orms.Domains.Units;
using Orms.Models;

namespace Orms.DataAccessLLBLGenPro.Converter
{
    public static partial class Converter
    {
        public static NominationValueEntity ToEntity(this NominationValue value)
        {
            return new NominationValueEntity()
            {
                Id = value.Id,
                EnergyUnit = value.Energy.Unit.ToString(),
                EnergyValue = value.Energy.Value,
                VolumeUnit = value.Volume.Unit.ToString(),
                VolumeValue = value.Volume.Value,
                HourIndex = value.HourIndex,
                NominationId = value.Nomination.Id
            };
        }

        public static NominationValue ToModel(this NominationValueEntity value)
        {
            return new NominationValue()
            {
                Id = value.Id,
                Nomination = value.Nomination.ToModel(),
                Energy = new UnitValue((decimal)value.EnergyValue, (Unit)Enum.Parse(typeof(Unit), (string)value.EnergyUnit, true)),
                Volume = new UnitValue((decimal)value.VolumeValue, (Unit)Enum.Parse(typeof(Unit), (string)value.VolumeUnit, true)),
                HourIndex = value.HourIndex
            };
        }
    }
}