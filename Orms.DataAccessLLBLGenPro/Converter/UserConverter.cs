﻿using System.Linq;
using Orms.DataAccessLLBLGenPro.EntityClasses;
using Orms.Models;

namespace Orms.DataAccessLLBLGenPro.Converter
{
    public static partial class Converter
    {
        public static UserEntity ToEntity(this User user)
        {
            return new UserEntity()
            {
                Id = user.Id,
                Name = user.Name,
                ContractorId = user.Contractor.Id,
                CreateDatetime = user.CreateDateTime,
                Email = user.Email,
                IsActive = user.IsActive,
                LastActivityDatetime = user.LastActivityDateTime,
                Password = user.Password,
            };
        }

        public static User ToModel(this UserEntity user)
        {
            if (user == null) return null;

            return new User()
            {
                Id = user.Id,
                Name = user.Name,
                Contractor = user.Contractor.ToModel(),
                CreateDateTime = user.CreateDatetime,
                Email = user.Email,
                IsActive = user.IsActive,
                LastActivityDateTime = user.LastActivityDatetime,
                Password = user.Password,
            };
        }
    }
}