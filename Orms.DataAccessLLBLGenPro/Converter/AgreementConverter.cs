﻿using Orms.DataAccessLLBLGenPro.EntityClasses;
using Orms.Models;

namespace Orms.DataAccessLLBLGenPro.Converter
{
    public static partial class Converter
    {
        public static AgreementEntity ToEntity(this Agreement agreement)
        {
            return new AgreementEntity()
            {
                Id = agreement.Id,
                Contractor = agreement.Contractor.ToEntity(),
                GasStorage = agreement.GasStorage.ToEntity(),
                ContractorId = agreement.Contractor.Id,
                CreateDatetime = agreement.CreateDate,
                EndDatetime = agreement.End,
                GasStorageId = agreement.GasStorage.Id,
                StartDatetime = agreement.Start
            };
        }

        public static Agreement ToModel(this AgreementEntity agreement)
        {
            return new Agreement()
            {
                Id = agreement.Id,
                CreateDate = agreement.CreateDatetime,
                End = agreement.EndDatetime,
                GasStorage = agreement.GasStorage.ToModel(),
                Start = agreement.StartDatetime,
                Contractor = agreement.Contractor.ToModel()
            };
        }
    }
}