﻿using System;
using Orms.DataAccessLLBLGenPro.EntityClasses;
using Orms.Domains.Enums;
using Orms.Models;

namespace Orms.DataAccessLLBLGenPro.Converter
{
    public static partial class Converter
    {
        public static AccessPointFlowEntity ToEntity(this AccessPointFlow accessPointFlow)
        {
            return new AccessPointFlowEntity()
            {
                Id = accessPointFlow.Id,
                Direction = accessPointFlow.Direction.ToString(),
                AccessPointId = accessPointFlow.AccessPoint.Id
            };
        }

        public static AccessPointFlow ToModel(this AccessPointFlowEntity accessPoint)
        {
            if (accessPoint == null) return null;

            return new AccessPointFlow()
            {
                Id = accessPoint.Id,
                Direction = (DirectionType)Enum.Parse(typeof(DirectionType), accessPoint.Direction),
                AccessPoint = accessPoint.AccessPoint.ToModel()
            };
        }
    }
}