﻿using System.Linq;
using Orms.DataAccessLLBLGenPro.EntityClasses;
using Orms.Models;

namespace Orms.DataAccessLLBLGenPro.Converter
{
    public static partial class Converter
    {
        public static ContractorEntity ToEntity(this Contractor contractor)
        {
            return new ContractorEntity()
            {
                Id = contractor.Id,
                Name = contractor.Name,
                Adress = contractor.Adress,
                Code = contractor.Code
            };
        }

        public static Contractor ToModel(this ContractorEntity contractor)
        {
            if (contractor == null) return null;

            return new Contractor()
            {
                Id = contractor.Id,
                Name = contractor.Name,
                Adress = contractor.Adress,
                Code = contractor.Code
            };
        }
    }
}