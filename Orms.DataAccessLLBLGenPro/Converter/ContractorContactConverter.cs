﻿using System.Linq;
using Orms.DataAccessLLBLGenPro.EntityClasses;
using Orms.Models;

namespace Orms.DataAccessLLBLGenPro.Converter
{
    public static partial class Converter
    {
        public static ContractorContactEntity ToEntity(this ContractorContact contact)
        {
            return new ContractorContactEntity()
            {
                Id = contact.Id,
                Name = contact.Name,
                Adress = contact.Adress,
                Email = contact.Email,
                ContractorId = contact.Contractor.Id,
                FirstName = contact.FirstName,
                LastName = contact.LastName,
                Phone = contact.Phone
            };
        }

        public static ContractorContact ToModel(this ContractorContactEntity contact)
        {
            return new ContractorContact()
            {
                Id = contact.Id,
                Name = contact.Name,
                Adress = contact.Adress,
                Email = contact.Email,
                Contractor = contact.Contractor.ToModel(),
                FirstName = contact.FirstName,
                LastName = contact.LastName,
                Phone = contact.Phone
            };
        }
    }
}