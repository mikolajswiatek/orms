﻿using System.Linq;
using Orms.DataAccessLLBLGenPro.EntityClasses;
using Orms.Models;

namespace Orms.DataAccessLLBLGenPro.Converter
{
    public static partial class Converter
    {
        public static GasStorageEntity ToEntity(this GasStorage gasStorage)
        {
            return new GasStorageEntity()
            {
                Id = gasStorage.Id,
                Name = gasStorage.Name
            };
        }

        public static GasStorage ToModel(this GasStorageEntity gasStorage)
        {
            if (gasStorage == null) return null;

            return new GasStorage()
            {
                Id = gasStorage.Id,
                Name = gasStorage.Name,
                AccessPoints = gasStorage.AccessPoints.Select(ap => ap.ToModel())
            };
        }
    }
}