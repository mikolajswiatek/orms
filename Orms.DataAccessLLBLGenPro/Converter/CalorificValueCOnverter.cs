﻿using System.Linq;
using Orms.DataAccessLLBLGenPro.EntityClasses;
using Orms.Models;

namespace Orms.DataAccessLLBLGenPro.Converter
{
    public static partial class Converter
    {
        public static CalorificValueEntity ToEntity(this CalorificValue cv)
        {
            return new CalorificValueEntity()
            {
                Id = cv.Id,
                //GasStorage = cv.GasStorage.ToEntity(),
                GasDay = cv.GasDay,
                GasStorageId = cv.GasStorage.Id,
            };
        }

        public static CalorificValue ToModel(this CalorificValueEntity cv)
        {
            if (cv == null) return null;

            return new CalorificValue()
            {
                Id = cv.Id,
                GasStorage = cv.GasStorage.ToModel(),
                GasDay = cv.GasDay,
                Values = cv.CalorificValueValues.Select(v => v.ToModel())
            };
        }
    }
}