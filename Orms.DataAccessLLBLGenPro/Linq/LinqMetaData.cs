﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
//////////////////////////////////////////////////////////////
using System;
using System.Linq;
using System.Collections.Generic;
using SD.LLBLGen.Pro.LinqSupportClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

using Orms.DataAccessLLBLGenPro;
using Orms.DataAccessLLBLGenPro.EntityClasses;
using Orms.DataAccessLLBLGenPro.FactoryClasses;
using Orms.DataAccessLLBLGenPro.HelperClasses;
using Orms.DataAccessLLBLGenPro.RelationClasses;

namespace Orms.DataAccessLLBLGenPro.Linq
{
	/// <summary>Meta-data class for the construction of Linq queries which are to be executed using LLBLGen Pro code.</summary>
	public partial class LinqMetaData: ILinqMetaData
	{
		#region Class Member Declarations
		private IDataAccessAdapter _adapterToUse;
		private FunctionMappingStore _customFunctionMappings;
		private Context _contextToUse;
		#endregion
		
		/// <summary>CTor. Using this ctor will leave the IDataAccessAdapter object to use empty. To be able to execute the query, an IDataAccessAdapter instance
		/// is required, and has to be set on the LLBLGenProProvider2 object in the query to execute. </summary>
		public LinqMetaData() : this(null, null)
		{
		}
		
		/// <summary>CTor which accepts an IDataAccessAdapter implementing object, which will be used to execute queries created with this metadata class.</summary>
		/// <param name="adapterToUse">the IDataAccessAdapter to use in queries created with this meta data</param>
		/// <remarks> Be aware that the IDataAccessAdapter object set via this property is kept alive by the LLBLGenProQuery objects created with this meta data
		/// till they go out of scope.</remarks>
		public LinqMetaData(IDataAccessAdapter adapterToUse) : this (adapterToUse, null)
		{
		}

		/// <summary>CTor which accepts an IDataAccessAdapter implementing object, which will be used to execute queries created with this metadata class.</summary>
		/// <param name="adapterToUse">the IDataAccessAdapter to use in queries created with this meta data</param>
		/// <param name="customFunctionMappings">The custom function mappings to use. These take higher precedence than the ones in the DQE to use.</param>
		/// <remarks> Be aware that the IDataAccessAdapter object set via this property is kept alive by the LLBLGenProQuery objects created with this meta data
		/// till they go out of scope.</remarks>
		public LinqMetaData(IDataAccessAdapter adapterToUse, FunctionMappingStore customFunctionMappings)
		{
			_adapterToUse = adapterToUse;
			_customFunctionMappings = customFunctionMappings;
		}
	
		/// <summary>returns the datasource to use in a Linq query for the entity type specified</summary>
		/// <param name="typeOfEntity">the type of the entity to get the datasource for</param>
		/// <returns>the requested datasource</returns>
		public IDataSource GetQueryableForEntity(int typeOfEntity)
		{
			IDataSource toReturn = null;
			switch((Orms.DataAccessLLBLGenPro.EntityType)typeOfEntity)
			{
				case Orms.DataAccessLLBLGenPro.EntityType.AccessPointEntity:
					toReturn = this.AccessPoint;
					break;
				case Orms.DataAccessLLBLGenPro.EntityType.AccessPointFlowEntity:
					toReturn = this.AccessPointFlow;
					break;
				case Orms.DataAccessLLBLGenPro.EntityType.AgreementEntity:
					toReturn = this.Agreement;
					break;
				case Orms.DataAccessLLBLGenPro.EntityType.CalorificValueEntity:
					toReturn = this.CalorificValue;
					break;
				case Orms.DataAccessLLBLGenPro.EntityType.CalorificValueValueEntity:
					toReturn = this.CalorificValueValue;
					break;
				case Orms.DataAccessLLBLGenPro.EntityType.ContractorEntity:
					toReturn = this.Contractor;
					break;
				case Orms.DataAccessLLBLGenPro.EntityType.ContractorContactEntity:
					toReturn = this.ContractorContact;
					break;
				case Orms.DataAccessLLBLGenPro.EntityType.GasAccountEntity:
					toReturn = this.GasAccount;
					break;
				case Orms.DataAccessLLBLGenPro.EntityType.GasStorageEntity:
					toReturn = this.GasStorage;
					break;
				case Orms.DataAccessLLBLGenPro.EntityType.NominationEntity:
					toReturn = this.Nomination;
					break;
				case Orms.DataAccessLLBLGenPro.EntityType.NominationValueEntity:
					toReturn = this.NominationValue;
					break;
				case Orms.DataAccessLLBLGenPro.EntityType.SessionEntity:
					toReturn = this.Session;
					break;
				case Orms.DataAccessLLBLGenPro.EntityType.UserEntity:
					toReturn = this.User;
					break;
				default:
					toReturn = null;
					break;
			}
			return toReturn;
		}

		/// <summary>returns the datasource to use in a Linq query for the entity type specified</summary>
		/// <typeparam name="TEntity">the type of the entity to get the datasource for</typeparam>
		/// <returns>the requested datasource</returns>
		public DataSource2<TEntity> GetQueryableForEntity<TEntity>()
				where TEntity : class
		{
			return new DataSource2<TEntity>(_adapterToUse, new ElementCreator(), _customFunctionMappings, _contextToUse);
		}

		/// <summary>returns the datasource to use in a Linq query when targeting AccessPointEntity instances in the database.</summary>
		public DataSource2<AccessPointEntity> AccessPoint
		{
			get { return new DataSource2<AccessPointEntity>(_adapterToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting AccessPointFlowEntity instances in the database.</summary>
		public DataSource2<AccessPointFlowEntity> AccessPointFlow
		{
			get { return new DataSource2<AccessPointFlowEntity>(_adapterToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting AgreementEntity instances in the database.</summary>
		public DataSource2<AgreementEntity> Agreement
		{
			get { return new DataSource2<AgreementEntity>(_adapterToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting CalorificValueEntity instances in the database.</summary>
		public DataSource2<CalorificValueEntity> CalorificValue
		{
			get { return new DataSource2<CalorificValueEntity>(_adapterToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting CalorificValueValueEntity instances in the database.</summary>
		public DataSource2<CalorificValueValueEntity> CalorificValueValue
		{
			get { return new DataSource2<CalorificValueValueEntity>(_adapterToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting ContractorEntity instances in the database.</summary>
		public DataSource2<ContractorEntity> Contractor
		{
			get { return new DataSource2<ContractorEntity>(_adapterToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting ContractorContactEntity instances in the database.</summary>
		public DataSource2<ContractorContactEntity> ContractorContact
		{
			get { return new DataSource2<ContractorContactEntity>(_adapterToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting GasAccountEntity instances in the database.</summary>
		public DataSource2<GasAccountEntity> GasAccount
		{
			get { return new DataSource2<GasAccountEntity>(_adapterToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting GasStorageEntity instances in the database.</summary>
		public DataSource2<GasStorageEntity> GasStorage
		{
			get { return new DataSource2<GasStorageEntity>(_adapterToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting NominationEntity instances in the database.</summary>
		public DataSource2<NominationEntity> Nomination
		{
			get { return new DataSource2<NominationEntity>(_adapterToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting NominationValueEntity instances in the database.</summary>
		public DataSource2<NominationValueEntity> NominationValue
		{
			get { return new DataSource2<NominationValueEntity>(_adapterToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting SessionEntity instances in the database.</summary>
		public DataSource2<SessionEntity> Session
		{
			get { return new DataSource2<SessionEntity>(_adapterToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting UserEntity instances in the database.</summary>
		public DataSource2<UserEntity> User
		{
			get { return new DataSource2<UserEntity>(_adapterToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		

		#region Class Property Declarations
		/// <summary> Gets / sets the IDataAccessAdapter to use for the queries created with this meta data object.</summary>
		/// <remarks> Be aware that the IDataAccessAdapter object set via this property is kept alive by the LLBLGenProQuery objects created with this meta data
		/// till they go out of scope.</remarks>
		public IDataAccessAdapter AdapterToUse
		{
			get { return _adapterToUse;}
			set { _adapterToUse = value;}
		}

		/// <summary>Gets or sets the custom function mappings to use. These take higher precedence than the ones in the DQE to use</summary>
		public FunctionMappingStore CustomFunctionMappings
		{
			get { return _customFunctionMappings; }
			set { _customFunctionMappings = value; }
		}
		
		/// <summary>Gets or sets the Context instance to use for entity fetches.</summary>
		public Context ContextToUse
		{
			get { return _contextToUse;}
			set { _contextToUse = value;}
		}
		#endregion
	}
}