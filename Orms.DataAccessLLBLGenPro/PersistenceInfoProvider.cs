﻿///////////////////////////////////////////////////////////////
// This is generated code.
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.2
// Code is generated on:
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Orms.DataAccessLLBLGenPro.DatabaseSpecific
{
	/// <summary>Singleton implementation of the PersistenceInfoProvider. This class is the singleton wrapper through which the actual instance is retrieved.</summary>
	/// <remarks>It uses a single instance of an internal class. The access isn't marked with locks as the PersistenceInfoProviderBase class is threadsafe.</remarks>
	internal static class PersistenceInfoProviderSingleton
	{
		#region Class Member Declarations
		private static readonly IPersistenceInfoProvider _providerInstance = new PersistenceInfoProviderCore();
		#endregion

		/// <summary>Dummy static constructor to make sure threadsafe initialization is performed.</summary>
		static PersistenceInfoProviderSingleton()
		{
		}

		/// <summary>Gets the singleton instance of the PersistenceInfoProviderCore</summary>
		/// <returns>Instance of the PersistenceInfoProvider.</returns>
		public static IPersistenceInfoProvider GetInstance()
		{
			return _providerInstance;
		}
	}

	/// <summary>Actual implementation of the PersistenceInfoProvider. Used by singleton wrapper.</summary>
	internal class PersistenceInfoProviderCore : PersistenceInfoProviderBase
	{
		/// <summary>Initializes a new instance of the <see cref="PersistenceInfoProviderCore"/> class.</summary>
		internal PersistenceInfoProviderCore()
		{
			Init();
		}

		/// <summary>Method which initializes the internal datastores with the structure of hierarchical types.</summary>
		private void Init()
		{
			this.InitClass();
			InitAccessPointEntityMappings();
			InitAccessPointFlowEntityMappings();
			InitAgreementEntityMappings();
			InitCalorificValueEntityMappings();
			InitCalorificValueValueEntityMappings();
			InitContractorEntityMappings();
			InitContractorContactEntityMappings();
			InitGasAccountEntityMappings();
			InitGasStorageEntityMappings();
			InitNominationEntityMappings();
			InitNominationValueEntityMappings();
			InitSessionEntityMappings();
			InitUserEntityMappings();
		}

		/// <summary>Inits AccessPointEntity's mappings</summary>
		private void InitAccessPointEntityMappings()
		{
			this.AddElementMapping("AccessPointEntity", @"aspnet-Orms-20170502014625", @"dbo", "AccessPoints", 3, 0);
			this.AddElementFieldMapping("AccessPointEntity", "Code", "Code", false, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 0);
			this.AddElementFieldMapping("AccessPointEntity", "GasStorageId", "GasStorageId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("AccessPointEntity", "Id", "Id", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 2);
		}

		/// <summary>Inits AccessPointFlowEntity's mappings</summary>
		private void InitAccessPointFlowEntityMappings()
		{
			this.AddElementMapping("AccessPointFlowEntity", @"aspnet-Orms-20170502014625", @"dbo", "AccessPointFlows", 3, 0);
			this.AddElementFieldMapping("AccessPointFlowEntity", "AccessPointId", "AccessPointId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("AccessPointFlowEntity", "Direction", "Direction", false, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("AccessPointFlowEntity", "Id", "Id", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 2);
		}

		/// <summary>Inits AgreementEntity's mappings</summary>
		private void InitAgreementEntityMappings()
		{
			this.AddElementMapping("AgreementEntity", @"aspnet-Orms-20170502014625", @"dbo", "Agreements", 6, 0);
			this.AddElementFieldMapping("AgreementEntity", "ContractorId", "ContractorId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("AgreementEntity", "CreateDatetime", "CreateDatetime", false, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 1);
			this.AddElementFieldMapping("AgreementEntity", "EndDatetime", "EndDatetime", false, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 2);
			this.AddElementFieldMapping("AgreementEntity", "GasStorageId", "GasStorageId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("AgreementEntity", "Id", "Id", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("AgreementEntity", "StartDatetime", "StartDatetime", false, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
		}

		/// <summary>Inits CalorificValueEntity's mappings</summary>
		private void InitCalorificValueEntityMappings()
		{
			this.AddElementMapping("CalorificValueEntity", @"aspnet-Orms-20170502014625", @"dbo", "CalorificValues", 3, 0);
			this.AddElementFieldMapping("CalorificValueEntity", "GasDay", "GasDay", false, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 0);
			this.AddElementFieldMapping("CalorificValueEntity", "GasStorageId", "GasStorageId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("CalorificValueEntity", "Id", "Id", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 2);
		}

		/// <summary>Inits CalorificValueValueEntity's mappings</summary>
		private void InitCalorificValueValueEntityMappings()
		{
			this.AddElementMapping("CalorificValueValueEntity", @"aspnet-Orms-20170502014625", @"dbo", "CalorificValueValues", 5, 0);
			this.AddElementFieldMapping("CalorificValueValueEntity", "CalorificValueId", "CalorificValueId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("CalorificValueValueEntity", "EnergyUnit", "EnergyUnit", false, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("CalorificValueValueEntity", "EnergyValue", "EnergyValue", false, "Decimal", 0, 18, 0, false, "", null, typeof(System.Decimal), 2);
			this.AddElementFieldMapping("CalorificValueValueEntity", "HourIndex", "HourIndex", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("CalorificValueValueEntity", "Id", "Id", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 4);
		}

		/// <summary>Inits ContractorEntity's mappings</summary>
		private void InitContractorEntityMappings()
		{
			this.AddElementMapping("ContractorEntity", @"aspnet-Orms-20170502014625", @"dbo", "Contractors", 4, 0);
			this.AddElementFieldMapping("ContractorEntity", "Adress", "Adress", false, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 0);
			this.AddElementFieldMapping("ContractorEntity", "Code", "Code", false, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("ContractorEntity", "Id", "Id", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("ContractorEntity", "Name", "Name", false, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 3);
		}

		/// <summary>Inits ContractorContactEntity's mappings</summary>
		private void InitContractorContactEntityMappings()
		{
			this.AddElementMapping("ContractorContactEntity", @"aspnet-Orms-20170502014625", @"dbo", "ContractorContacts", 8, 0);
			this.AddElementFieldMapping("ContractorContactEntity", "Adress", "Adress", false, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 0);
			this.AddElementFieldMapping("ContractorContactEntity", "ContractorId", "ContractorId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("ContractorContactEntity", "Email", "Email", false, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("ContractorContactEntity", "FirstName", "FirstName", false, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("ContractorContactEntity", "Id", "Id", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("ContractorContactEntity", "LastName", "LastName", false, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("ContractorContactEntity", "Name", "Name", false, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("ContractorContactEntity", "Phone", "Phone", false, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 7);
		}

		/// <summary>Inits GasAccountEntity's mappings</summary>
		private void InitGasAccountEntityMappings()
		{
			this.AddElementMapping("GasAccountEntity", @"aspnet-Orms-20170502014625", @"dbo", "GasAccounts", 8, 0);
			this.AddElementFieldMapping("GasAccountEntity", "EnergyUnit", "EnergyUnit", false, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 0);
			this.AddElementFieldMapping("GasAccountEntity", "EnergyValue", "EnergyValue", false, "Decimal", 0, 18, 0, false, "", null, typeof(System.Decimal), 1);
			this.AddElementFieldMapping("GasAccountEntity", "GasDay", "GasDay", false, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 2);
			this.AddElementFieldMapping("GasAccountEntity", "GasStorageId", "GasStorageId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("GasAccountEntity", "HourIndex", "HourIndex", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("GasAccountEntity", "Id", "Id", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("GasAccountEntity", "VolumeUnit", "VolumeUnit", false, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("GasAccountEntity", "VolumeValue", "VolumeValue", false, "Decimal", 0, 18, 0, false, "", null, typeof(System.Decimal), 7);
		}

		/// <summary>Inits GasStorageEntity's mappings</summary>
		private void InitGasStorageEntityMappings()
		{
			this.AddElementMapping("GasStorageEntity", @"aspnet-Orms-20170502014625", @"dbo", "GasStorages", 2, 0);
			this.AddElementFieldMapping("GasStorageEntity", "Id", "Id", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("GasStorageEntity", "Name", "Name", false, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 1);
		}

		/// <summary>Inits NominationEntity's mappings</summary>
		private void InitNominationEntityMappings()
		{
			this.AddElementMapping("NominationEntity", @"aspnet-Orms-20170502014625", @"dbo", "Nominations", 15, 0);
			this.AddElementFieldMapping("NominationEntity", "AccessPointFlowId", "AccessPointFlowId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("NominationEntity", "CalorificValueId", "CalorificValueId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("NominationEntity", "Channel", "Channel", false, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("NominationEntity", "Comment", "Comment", false, "NVarChar", 1, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("NominationEntity", "ContractorId", "ContractorId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("NominationEntity", "CreateDatetime", "CreateDatetime", false, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("NominationEntity", "GasDay", "GasDay", false, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("NominationEntity", "Id", "Id", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("NominationEntity", "IsAccepted", "IsAccepted", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 8);
			this.AddElementFieldMapping("NominationEntity", "IsLast", "IsLast", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 9);
			this.AddElementFieldMapping("NominationEntity", "MajorVersion", "MajorVersion", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 10);
			this.AddElementFieldMapping("NominationEntity", "Name", "Name", false, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 11);
			this.AddElementFieldMapping("NominationEntity", "ParentNominationId", "ParentNominationId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 12);
			this.AddElementFieldMapping("NominationEntity", "UserId", "UserId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 13);
			this.AddElementFieldMapping("NominationEntity", "Version", "Version", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 14);
		}

		/// <summary>Inits NominationValueEntity's mappings</summary>
		private void InitNominationValueEntityMappings()
		{
			this.AddElementMapping("NominationValueEntity", @"aspnet-Orms-20170502014625", @"dbo", "NominationValues", 7, 0);
			this.AddElementFieldMapping("NominationValueEntity", "EnergyUnit", "EnergyUnit", false, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 0);
			this.AddElementFieldMapping("NominationValueEntity", "EnergyValue", "EnergyValue", false, "Decimal", 0, 18, 0, false, "", null, typeof(System.Decimal), 1);
			this.AddElementFieldMapping("NominationValueEntity", "HourIndex", "HourIndex", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("NominationValueEntity", "Id", "Id", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("NominationValueEntity", "NominationId", "NominationId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("NominationValueEntity", "VolumeUnit", "VolumeUnit", false, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("NominationValueEntity", "VolumeValue", "VolumeValue", false, "Decimal", 0, 18, 0, false, "", null, typeof(System.Decimal), 6);
		}

		/// <summary>Inits SessionEntity's mappings</summary>
		private void InitSessionEntityMappings()
		{
			this.AddElementMapping("SessionEntity", @"aspnet-Orms-20170502014625", @"dbo", "Sessions", 7, 0);
			this.AddElementFieldMapping("SessionEntity", "Browser", "Browser", false, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 0);
			this.AddElementFieldMapping("SessionEntity", "CreateDatetime", "CreateDatetime", false, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 1);
			this.AddElementFieldMapping("SessionEntity", "Guid", "Guid", false, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("SessionEntity", "Id", "Id", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("SessionEntity", "Ip", "Ip", false, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("SessionEntity", "LastActivityDatetime", "LastActivityDatetime", false, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("SessionEntity", "UserId", "UserId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
		}

		/// <summary>Inits UserEntity's mappings</summary>
		private void InitUserEntityMappings()
		{
			this.AddElementMapping("UserEntity", @"aspnet-Orms-20170502014625", @"dbo", "Users", 8, 0);
			this.AddElementFieldMapping("UserEntity", "ContractorId", "ContractorId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("UserEntity", "CreateDatetime", "CreateDatetime", false, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 1);
			this.AddElementFieldMapping("UserEntity", "Email", "Email", false, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("UserEntity", "Id", "Id", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("UserEntity", "IsActive", "IsActive", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 4);
			this.AddElementFieldMapping("UserEntity", "LastActivityDatetime", "LastActivityDatetime", false, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("UserEntity", "Name", "Name", false, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("UserEntity", "Password", "Password", false, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 7);
		}

	}
}
