﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Orms.DataAccessLLBLGenPro.FactoryClasses;
using Orms.DataAccessLLBLGenPro;

namespace Orms.DataAccessLLBLGenPro.HelperClasses
{
	/// <summary>Field Creation Class for entity AccessPointEntity</summary>
	public partial class AccessPointFields
	{
		/// <summary>Creates a new AccessPointEntity.Code field instance</summary>
		public static EntityField2 Code
		{
			get { return (EntityField2)EntityFieldFactory.Create(AccessPointFieldIndex.Code);}
		}
		/// <summary>Creates a new AccessPointEntity.GasStorageId field instance</summary>
		public static EntityField2 GasStorageId
		{
			get { return (EntityField2)EntityFieldFactory.Create(AccessPointFieldIndex.GasStorageId);}
		}
		/// <summary>Creates a new AccessPointEntity.Id field instance</summary>
		public static EntityField2 Id
		{
			get { return (EntityField2)EntityFieldFactory.Create(AccessPointFieldIndex.Id);}
		}
	}

	/// <summary>Field Creation Class for entity AccessPointFlowEntity</summary>
	public partial class AccessPointFlowFields
	{
		/// <summary>Creates a new AccessPointFlowEntity.AccessPointId field instance</summary>
		public static EntityField2 AccessPointId
		{
			get { return (EntityField2)EntityFieldFactory.Create(AccessPointFlowFieldIndex.AccessPointId);}
		}
        /// <summary>Creates a new AccessPointFlowEntity.Direction field instance</summary>
        public static EntityField2 Direction
        {
			get { return (EntityField2)EntityFieldFactory.Create(AccessPointFlowFieldIndex.Direction);}
		}
		/// <summary>Creates a new AccessPointFlowEntity.Id field instance</summary>
		public static EntityField2 Id
		{
			get { return (EntityField2)EntityFieldFactory.Create(AccessPointFlowFieldIndex.Id);}
		}
	}

	/// <summary>Field Creation Class for entity AgreementEntity</summary>
	public partial class AgreementFields
	{
		/// <summary>Creates a new AgreementEntity.ContractorId field instance</summary>
		public static EntityField2 ContractorId
		{
			get { return (EntityField2)EntityFieldFactory.Create(AgreementFieldIndex.ContractorId);}
		}
		/// <summary>Creates a new AgreementEntity.CreateDatetime field instance</summary>
		public static EntityField2 CreateDatetime
		{
			get { return (EntityField2)EntityFieldFactory.Create(AgreementFieldIndex.CreateDatetime);}
		}
		/// <summary>Creates a new AgreementEntity.EndDatetime field instance</summary>
		public static EntityField2 EndDatetime
		{
			get { return (EntityField2)EntityFieldFactory.Create(AgreementFieldIndex.EndDatetime);}
		}
		/// <summary>Creates a new AgreementEntity.GasStorageId field instance</summary>
		public static EntityField2 GasStorageId
		{
			get { return (EntityField2)EntityFieldFactory.Create(AgreementFieldIndex.GasStorageId);}
		}
		/// <summary>Creates a new AgreementEntity.Id field instance</summary>
		public static EntityField2 Id
		{
			get { return (EntityField2)EntityFieldFactory.Create(AgreementFieldIndex.Id);}
		}
		/// <summary>Creates a new AgreementEntity.StartDatetime field instance</summary>
		public static EntityField2 StartDatetime
		{
			get { return (EntityField2)EntityFieldFactory.Create(AgreementFieldIndex.StartDatetime);}
		}
	}

	/// <summary>Field Creation Class for entity CalorificValueEntity</summary>
	public partial class CalorificValueFields
	{
		/// <summary>Creates a new CalorificValueEntity.GasDay field instance</summary>
		public static EntityField2 GasDay
		{
			get { return (EntityField2)EntityFieldFactory.Create(CalorificValueFieldIndex.GasDay);}
		}
		/// <summary>Creates a new CalorificValueEntity.GasStorageId field instance</summary>
		public static EntityField2 GasStorageId
		{
			get { return (EntityField2)EntityFieldFactory.Create(CalorificValueFieldIndex.GasStorageId);}
		}
		/// <summary>Creates a new CalorificValueEntity.Id field instance</summary>
		public static EntityField2 Id
		{
			get { return (EntityField2)EntityFieldFactory.Create(CalorificValueFieldIndex.Id);}
		}
	}

	/// <summary>Field Creation Class for entity CalorificValueValueEntity</summary>
	public partial class CalorificValueValueFields
	{
		/// <summary>Creates a new CalorificValueValueEntity.CalorificValueId field instance</summary>
		public static EntityField2 CalorificValueId
		{
			get { return (EntityField2)EntityFieldFactory.Create(CalorificValueValueFieldIndex.CalorificValueId);}
		}
		/// <summary>Creates a new CalorificValueValueEntity.EnergyUnit field instance</summary>
		public static EntityField2 EnergyUnit
		{
			get { return (EntityField2)EntityFieldFactory.Create(CalorificValueValueFieldIndex.EnergyUnit);}
		}
		/// <summary>Creates a new CalorificValueValueEntity.EnergyValue field instance</summary>
		public static EntityField2 EnergyValue
		{
			get { return (EntityField2)EntityFieldFactory.Create(CalorificValueValueFieldIndex.EnergyValue);}
		}
		/// <summary>Creates a new CalorificValueValueEntity.HourIndex field instance</summary>
		public static EntityField2 HourIndex
		{
			get { return (EntityField2)EntityFieldFactory.Create(CalorificValueValueFieldIndex.HourIndex);}
		}
		/// <summary>Creates a new CalorificValueValueEntity.Id field instance</summary>
		public static EntityField2 Id
		{
			get { return (EntityField2)EntityFieldFactory.Create(CalorificValueValueFieldIndex.Id);}
		}
	}

	/// <summary>Field Creation Class for entity ContractorEntity</summary>
	public partial class ContractorFields
	{
		/// <summary>Creates a new ContractorEntity.Adress field instance</summary>
		public static EntityField2 Adress
		{
			get { return (EntityField2)EntityFieldFactory.Create(ContractorFieldIndex.Adress);}
		}
		/// <summary>Creates a new ContractorEntity.Code field instance</summary>
		public static EntityField2 Code
		{
			get { return (EntityField2)EntityFieldFactory.Create(ContractorFieldIndex.Code);}
		}
		/// <summary>Creates a new ContractorEntity.Id field instance</summary>
		public static EntityField2 Id
		{
			get { return (EntityField2)EntityFieldFactory.Create(ContractorFieldIndex.Id);}
		}
		/// <summary>Creates a new ContractorEntity.Name field instance</summary>
		public static EntityField2 Name
		{
			get { return (EntityField2)EntityFieldFactory.Create(ContractorFieldIndex.Name);}
		}
	}

	/// <summary>Field Creation Class for entity ContractorContactEntity</summary>
	public partial class ContractorContactFields
	{
		/// <summary>Creates a new ContractorContactEntity.Adress field instance</summary>
		public static EntityField2 Adress
		{
			get { return (EntityField2)EntityFieldFactory.Create(ContractorContactFieldIndex.Adress);}
		}
		/// <summary>Creates a new ContractorContactEntity.ContractorId field instance</summary>
		public static EntityField2 ContractorId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ContractorContactFieldIndex.ContractorId);}
		}
		/// <summary>Creates a new ContractorContactEntity.Email field instance</summary>
		public static EntityField2 Email
		{
			get { return (EntityField2)EntityFieldFactory.Create(ContractorContactFieldIndex.Email);}
		}
		/// <summary>Creates a new ContractorContactEntity.FirstName field instance</summary>
		public static EntityField2 FirstName
		{
			get { return (EntityField2)EntityFieldFactory.Create(ContractorContactFieldIndex.FirstName);}
		}
		/// <summary>Creates a new ContractorContactEntity.Id field instance</summary>
		public static EntityField2 Id
		{
			get { return (EntityField2)EntityFieldFactory.Create(ContractorContactFieldIndex.Id);}
		}
		/// <summary>Creates a new ContractorContactEntity.LastName field instance</summary>
		public static EntityField2 LastName
		{
			get { return (EntityField2)EntityFieldFactory.Create(ContractorContactFieldIndex.LastName);}
		}
		/// <summary>Creates a new ContractorContactEntity.Name field instance</summary>
		public static EntityField2 Name
		{
			get { return (EntityField2)EntityFieldFactory.Create(ContractorContactFieldIndex.Name);}
		}
		/// <summary>Creates a new ContractorContactEntity.Phone field instance</summary>
		public static EntityField2 Phone
		{
			get { return (EntityField2)EntityFieldFactory.Create(ContractorContactFieldIndex.Phone);}
		}
	}

	/// <summary>Field Creation Class for entity GasAccountEntity</summary>
	public partial class GasAccountFields
	{
		/// <summary>Creates a new GasAccountEntity.EnergyUnit field instance</summary>
		public static EntityField2 EnergyUnit
		{
			get { return (EntityField2)EntityFieldFactory.Create(GasAccountFieldIndex.EnergyUnit);}
		}
		/// <summary>Creates a new GasAccountEntity.EnergyValue field instance</summary>
		public static EntityField2 EnergyValue
		{
			get { return (EntityField2)EntityFieldFactory.Create(GasAccountFieldIndex.EnergyValue);}
		}
		/// <summary>Creates a new GasAccountEntity.GasDay field instance</summary>
		public static EntityField2 GasDay
		{
			get { return (EntityField2)EntityFieldFactory.Create(GasAccountFieldIndex.GasDay);}
		}
		/// <summary>Creates a new GasAccountEntity.GasStorageId field instance</summary>
		public static EntityField2 GasStorageId
		{
			get { return (EntityField2)EntityFieldFactory.Create(GasAccountFieldIndex.GasStorageId);}
		}
		/// <summary>Creates a new GasAccountEntity.HourIndex field instance</summary>
		public static EntityField2 HourIndex
		{
			get { return (EntityField2)EntityFieldFactory.Create(GasAccountFieldIndex.HourIndex);}
		}
		/// <summary>Creates a new GasAccountEntity.Id field instance</summary>
		public static EntityField2 Id
		{
			get { return (EntityField2)EntityFieldFactory.Create(GasAccountFieldIndex.Id);}
		}
		/// <summary>Creates a new GasAccountEntity.VolumeUnit field instance</summary>
		public static EntityField2 VolumeUnit
		{
			get { return (EntityField2)EntityFieldFactory.Create(GasAccountFieldIndex.VolumeUnit);}
		}
		/// <summary>Creates a new GasAccountEntity.VolumeValue field instance</summary>
		public static EntityField2 VolumeValue
		{
			get { return (EntityField2)EntityFieldFactory.Create(GasAccountFieldIndex.VolumeValue);}
		}
	}

	/// <summary>Field Creation Class for entity GasStorageEntity</summary>
	public partial class GasStorageFields
	{
		/// <summary>Creates a new GasStorageEntity.Id field instance</summary>
		public static EntityField2 Id
		{
			get { return (EntityField2)EntityFieldFactory.Create(GasStorageFieldIndex.Id);}
		}
		/// <summary>Creates a new GasStorageEntity.Name field instance</summary>
		public static EntityField2 Name
		{
			get { return (EntityField2)EntityFieldFactory.Create(GasStorageFieldIndex.Name);}
		}
	}

	/// <summary>Field Creation Class for entity NominationEntity</summary>
	public partial class NominationFields
	{
		/// <summary>Creates a new NominationEntity.AccessPointFlowId field instance</summary>
		public static EntityField2 AccessPointFlowId
		{
			get { return (EntityField2)EntityFieldFactory.Create(NominationFieldIndex.AccessPointFlowId);}
		}
		/// <summary>Creates a new NominationEntity.CalorificValueId field instance</summary>
		public static EntityField2 CalorificValueId
		{
			get { return (EntityField2)EntityFieldFactory.Create(NominationFieldIndex.CalorificValueId);}
		}
		/// <summary>Creates a new NominationEntity.Channel field instance</summary>
		public static EntityField2 Channel
		{
			get { return (EntityField2)EntityFieldFactory.Create(NominationFieldIndex.Channel);}
		}
		/// <summary>Creates a new NominationEntity.Comment field instance</summary>
		public static EntityField2 Comment
		{
			get { return (EntityField2)EntityFieldFactory.Create(NominationFieldIndex.Comment);}
		}
		/// <summary>Creates a new NominationEntity.ContractorId field instance</summary>
		public static EntityField2 ContractorId
		{
			get { return (EntityField2)EntityFieldFactory.Create(NominationFieldIndex.ContractorId);}
		}
		/// <summary>Creates a new NominationEntity.CreateDatetime field instance</summary>
		public static EntityField2 CreateDatetime
		{
			get { return (EntityField2)EntityFieldFactory.Create(NominationFieldIndex.CreateDatetime);}
		}
		/// <summary>Creates a new NominationEntity.GasDay field instance</summary>
		public static EntityField2 GasDay
		{
			get { return (EntityField2)EntityFieldFactory.Create(NominationFieldIndex.GasDay);}
		}
		/// <summary>Creates a new NominationEntity.Id field instance</summary>
		public static EntityField2 Id
		{
			get { return (EntityField2)EntityFieldFactory.Create(NominationFieldIndex.Id);}
		}
		/// <summary>Creates a new NominationEntity.IsAccepted field instance</summary>
		public static EntityField2 IsAccepted
		{
			get { return (EntityField2)EntityFieldFactory.Create(NominationFieldIndex.IsAccepted);}
		}
		/// <summary>Creates a new NominationEntity.IsLast field instance</summary>
		public static EntityField2 IsLast
		{
			get { return (EntityField2)EntityFieldFactory.Create(NominationFieldIndex.IsLast);}
		}
		/// <summary>Creates a new NominationEntity.MajorVersion field instance</summary>
		public static EntityField2 MajorVersion
		{
			get { return (EntityField2)EntityFieldFactory.Create(NominationFieldIndex.MajorVersion);}
		}
		/// <summary>Creates a new NominationEntity.Name field instance</summary>
		public static EntityField2 Name
		{
			get { return (EntityField2)EntityFieldFactory.Create(NominationFieldIndex.Name);}
		}
		/// <summary>Creates a new NominationEntity.ParentNominationId field instance</summary>
		public static EntityField2 ParentNominationId
		{
			get { return (EntityField2)EntityFieldFactory.Create(NominationFieldIndex.ParentNominationId);}
		}
		/// <summary>Creates a new NominationEntity.UserId field instance</summary>
		public static EntityField2 UserId
		{
			get { return (EntityField2)EntityFieldFactory.Create(NominationFieldIndex.UserId);}
		}
		/// <summary>Creates a new NominationEntity.Version field instance</summary>
		public static EntityField2 Version
		{
			get { return (EntityField2)EntityFieldFactory.Create(NominationFieldIndex.Version);}
		}
	}

	/// <summary>Field Creation Class for entity NominationValueEntity</summary>
	public partial class NominationValueFields
	{
		/// <summary>Creates a new NominationValueEntity.EnergyUnit field instance</summary>
		public static EntityField2 EnergyUnit
		{
			get { return (EntityField2)EntityFieldFactory.Create(NominationValueFieldIndex.EnergyUnit);}
		}
		/// <summary>Creates a new NominationValueEntity.EnergyValue field instance</summary>
		public static EntityField2 EnergyValue
		{
			get { return (EntityField2)EntityFieldFactory.Create(NominationValueFieldIndex.EnergyValue);}
		}
		/// <summary>Creates a new NominationValueEntity.HourIndex field instance</summary>
		public static EntityField2 HourIndex
		{
			get { return (EntityField2)EntityFieldFactory.Create(NominationValueFieldIndex.HourIndex);}
		}
		/// <summary>Creates a new NominationValueEntity.Id field instance</summary>
		public static EntityField2 Id
		{
			get { return (EntityField2)EntityFieldFactory.Create(NominationValueFieldIndex.Id);}
		}
		/// <summary>Creates a new NominationValueEntity.NominationId field instance</summary>
		public static EntityField2 NominationId
		{
			get { return (EntityField2)EntityFieldFactory.Create(NominationValueFieldIndex.NominationId);}
		}
		/// <summary>Creates a new NominationValueEntity.VolumeUnit field instance</summary>
		public static EntityField2 VolumeUnit
		{
			get { return (EntityField2)EntityFieldFactory.Create(NominationValueFieldIndex.VolumeUnit);}
		}
		/// <summary>Creates a new NominationValueEntity.VolumeValue field instance</summary>
		public static EntityField2 VolumeValue
		{
			get { return (EntityField2)EntityFieldFactory.Create(NominationValueFieldIndex.VolumeValue);}
		}
	}

	/// <summary>Field Creation Class for entity SessionEntity</summary>
	public partial class SessionFields
	{
		/// <summary>Creates a new SessionEntity.Browser field instance</summary>
		public static EntityField2 Browser
		{
			get { return (EntityField2)EntityFieldFactory.Create(SessionFieldIndex.Browser);}
		}
		/// <summary>Creates a new SessionEntity.CreateDatetime field instance</summary>
		public static EntityField2 CreateDatetime
		{
			get { return (EntityField2)EntityFieldFactory.Create(SessionFieldIndex.CreateDatetime);}
		}
		/// <summary>Creates a new SessionEntity.Guid field instance</summary>
		public static EntityField2 Guid
		{
			get { return (EntityField2)EntityFieldFactory.Create(SessionFieldIndex.Guid);}
		}
		/// <summary>Creates a new SessionEntity.Id field instance</summary>
		public static EntityField2 Id
		{
			get { return (EntityField2)EntityFieldFactory.Create(SessionFieldIndex.Id);}
		}
		/// <summary>Creates a new SessionEntity.Ip field instance</summary>
		public static EntityField2 Ip
		{
			get { return (EntityField2)EntityFieldFactory.Create(SessionFieldIndex.Ip);}
		}
		/// <summary>Creates a new SessionEntity.LastActivityDatetime field instance</summary>
		public static EntityField2 LastActivityDatetime
		{
			get { return (EntityField2)EntityFieldFactory.Create(SessionFieldIndex.LastActivityDatetime);}
		}
		/// <summary>Creates a new SessionEntity.UserId field instance</summary>
		public static EntityField2 UserId
		{
			get { return (EntityField2)EntityFieldFactory.Create(SessionFieldIndex.UserId);}
		}
	}

	/// <summary>Field Creation Class for entity UserEntity</summary>
	public partial class UserFields
	{
		/// <summary>Creates a new UserEntity.ContractorId field instance</summary>
		public static EntityField2 ContractorId
		{
			get { return (EntityField2)EntityFieldFactory.Create(UserFieldIndex.ContractorId);}
		}
		/// <summary>Creates a new UserEntity.CreateDatetime field instance</summary>
		public static EntityField2 CreateDatetime
		{
			get { return (EntityField2)EntityFieldFactory.Create(UserFieldIndex.CreateDatetime);}
		}
		/// <summary>Creates a new UserEntity.Email field instance</summary>
		public static EntityField2 Email
		{
			get { return (EntityField2)EntityFieldFactory.Create(UserFieldIndex.Email);}
		}
		/// <summary>Creates a new UserEntity.Id field instance</summary>
		public static EntityField2 Id
		{
			get { return (EntityField2)EntityFieldFactory.Create(UserFieldIndex.Id);}
		}
		/// <summary>Creates a new UserEntity.IsActive field instance</summary>
		public static EntityField2 IsActive
		{
			get { return (EntityField2)EntityFieldFactory.Create(UserFieldIndex.IsActive);}
		}
		/// <summary>Creates a new UserEntity.LastActivityDatetime field instance</summary>
		public static EntityField2 LastActivityDatetime
		{
			get { return (EntityField2)EntityFieldFactory.Create(UserFieldIndex.LastActivityDatetime);}
		}
		/// <summary>Creates a new UserEntity.Name field instance</summary>
		public static EntityField2 Name
		{
			get { return (EntityField2)EntityFieldFactory.Create(UserFieldIndex.Name);}
		}
		/// <summary>Creates a new UserEntity.Password field instance</summary>
		public static EntityField2 Password
		{
			get { return (EntityField2)EntityFieldFactory.Create(UserFieldIndex.Password);}
		}
	}
	

}