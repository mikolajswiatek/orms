﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Orms.DataAccessLLBLGenPro.HelperClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	
	
	/// <summary>Singleton implementation of the FieldInfoProvider. This class is the singleton wrapper through which the actual instance is retrieved.</summary>
	/// <remarks>It uses a single instance of an internal class. The access isn't marked with locks as the FieldInfoProviderBase class is threadsafe.</remarks>
	internal static class FieldInfoProviderSingleton
	{
		#region Class Member Declarations
		private static readonly IFieldInfoProvider _providerInstance = new FieldInfoProviderCore();
		#endregion

		/// <summary>Dummy static constructor to make sure threadsafe initialization is performed.</summary>
		static FieldInfoProviderSingleton()
		{
		}

		/// <summary>Gets the singleton instance of the FieldInfoProviderCore</summary>
		/// <returns>Instance of the FieldInfoProvider.</returns>
		public static IFieldInfoProvider GetInstance()
		{
			return _providerInstance;
		}
	}

	/// <summary>Actual implementation of the FieldInfoProvider. Used by singleton wrapper.</summary>
	internal class FieldInfoProviderCore : FieldInfoProviderBase
	{
		/// <summary>Initializes a new instance of the <see cref="FieldInfoProviderCore"/> class.</summary>
		internal FieldInfoProviderCore()
		{
			Init();
		}

		/// <summary>Method which initializes the internal datastores.</summary>
		private void Init()
		{
			this.InitClass();
			InitAccessPointEntityInfos();
			InitAccessPointFlowEntityInfos();
			InitAgreementEntityInfos();
			InitCalorificValueEntityInfos();
			InitCalorificValueValueEntityInfos();
			InitContractorEntityInfos();
			InitContractorContactEntityInfos();
			InitGasAccountEntityInfos();
			InitGasStorageEntityInfos();
			InitNominationEntityInfos();
			InitNominationValueEntityInfos();
			InitSessionEntityInfos();
			InitUserEntityInfos();

			this.ConstructElementFieldStructures(InheritanceInfoProviderSingleton.GetInstance());
		}

		/// <summary>Inits AccessPointEntity's FieldInfo objects</summary>
		private void InitAccessPointEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(AccessPointFieldIndex), "AccessPointEntity");
			this.AddElementFieldInfo("AccessPointEntity", "Code", typeof(System.String), false, false, false, false,  (int)AccessPointFieldIndex.Code, 50, 0, 0);
			this.AddElementFieldInfo("AccessPointEntity", "GasStorageId", typeof(System.Int32), false, true, false, false,  (int)AccessPointFieldIndex.GasStorageId, 0, 0, 10);
			this.AddElementFieldInfo("AccessPointEntity", "Id", typeof(System.Int32), true, false, true, false,  (int)AccessPointFieldIndex.Id, 0, 0, 10);
		}
		/// <summary>Inits AccessPointFlowEntity's FieldInfo objects</summary>
		private void InitAccessPointFlowEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(AccessPointFlowFieldIndex), "AccessPointFlowEntity");
			this.AddElementFieldInfo("AccessPointFlowEntity", "AccessPointId", typeof(System.Int32), false, true, false, false,  (int)AccessPointFlowFieldIndex.AccessPointId, 0, 0, 10);
			this.AddElementFieldInfo("AccessPointFlowEntity", "Direction", typeof(System.String), false, false, false, false,  (int)AccessPointFlowFieldIndex.Direction, 50, 0, 0);
			this.AddElementFieldInfo("AccessPointFlowEntity", "Id", typeof(System.Int32), true, false, true, false,  (int)AccessPointFlowFieldIndex.Id, 0, 0, 10);
		}
		/// <summary>Inits AgreementEntity's FieldInfo objects</summary>
		private void InitAgreementEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(AgreementFieldIndex), "AgreementEntity");
			this.AddElementFieldInfo("AgreementEntity", "ContractorId", typeof(System.Int32), false, true, false, false,  (int)AgreementFieldIndex.ContractorId, 0, 0, 10);
			this.AddElementFieldInfo("AgreementEntity", "CreateDatetime", typeof(System.DateTime), false, false, false, false,  (int)AgreementFieldIndex.CreateDatetime, 0, 0, 0);
			this.AddElementFieldInfo("AgreementEntity", "EndDatetime", typeof(System.DateTime), false, false, false, false,  (int)AgreementFieldIndex.EndDatetime, 0, 0, 0);
			this.AddElementFieldInfo("AgreementEntity", "GasStorageId", typeof(System.Int32), false, true, false, false,  (int)AgreementFieldIndex.GasStorageId, 0, 0, 10);
			this.AddElementFieldInfo("AgreementEntity", "Id", typeof(System.Int32), true, false, true, false,  (int)AgreementFieldIndex.Id, 0, 0, 10);
			this.AddElementFieldInfo("AgreementEntity", "StartDatetime", typeof(System.DateTime), false, false, false, false,  (int)AgreementFieldIndex.StartDatetime, 0, 0, 0);
		}
		/// <summary>Inits CalorificValueEntity's FieldInfo objects</summary>
		private void InitCalorificValueEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(CalorificValueFieldIndex), "CalorificValueEntity");
			this.AddElementFieldInfo("CalorificValueEntity", "GasDay", typeof(System.DateTime), false, false, false, false,  (int)CalorificValueFieldIndex.GasDay, 0, 0, 0);
			this.AddElementFieldInfo("CalorificValueEntity", "GasStorageId", typeof(System.Int32), false, true, false, false,  (int)CalorificValueFieldIndex.GasStorageId, 0, 0, 10);
			this.AddElementFieldInfo("CalorificValueEntity", "Id", typeof(System.Int32), true, false, true, false,  (int)CalorificValueFieldIndex.Id, 0, 0, 10);
		}
		/// <summary>Inits CalorificValueValueEntity's FieldInfo objects</summary>
		private void InitCalorificValueValueEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(CalorificValueValueFieldIndex), "CalorificValueValueEntity");
			this.AddElementFieldInfo("CalorificValueValueEntity", "CalorificValueId", typeof(System.Int32), false, true, false, false,  (int)CalorificValueValueFieldIndex.CalorificValueId, 0, 0, 10);
			this.AddElementFieldInfo("CalorificValueValueEntity", "EnergyUnit", typeof(System.String), false, false, false, false,  (int)CalorificValueValueFieldIndex.EnergyUnit, 50, 0, 0);
			this.AddElementFieldInfo("CalorificValueValueEntity", "EnergyValue", typeof(System.Decimal), false, false, false, false,  (int)CalorificValueValueFieldIndex.EnergyValue, 0, 0, 18);
			this.AddElementFieldInfo("CalorificValueValueEntity", "HourIndex", typeof(System.Int32), false, false, false, false,  (int)CalorificValueValueFieldIndex.HourIndex, 0, 0, 10);
			this.AddElementFieldInfo("CalorificValueValueEntity", "Id", typeof(System.Int32), true, false, true, false,  (int)CalorificValueValueFieldIndex.Id, 0, 0, 10);
		}
		/// <summary>Inits ContractorEntity's FieldInfo objects</summary>
		private void InitContractorEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(ContractorFieldIndex), "ContractorEntity");
			this.AddElementFieldInfo("ContractorEntity", "Adress", typeof(System.String), false, false, false, false,  (int)ContractorFieldIndex.Adress, 255, 0, 0);
			this.AddElementFieldInfo("ContractorEntity", "Code", typeof(System.String), false, false, false, false,  (int)ContractorFieldIndex.Code, 255, 0, 0);
			this.AddElementFieldInfo("ContractorEntity", "Id", typeof(System.Int32), true, false, true, false,  (int)ContractorFieldIndex.Id, 0, 0, 10);
			this.AddElementFieldInfo("ContractorEntity", "Name", typeof(System.String), false, false, false, false,  (int)ContractorFieldIndex.Name, 255, 0, 0);
		}
		/// <summary>Inits ContractorContactEntity's FieldInfo objects</summary>
		private void InitContractorContactEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(ContractorContactFieldIndex), "ContractorContactEntity");
			this.AddElementFieldInfo("ContractorContactEntity", "Adress", typeof(System.String), false, false, false, false,  (int)ContractorContactFieldIndex.Adress, 255, 0, 0);
			this.AddElementFieldInfo("ContractorContactEntity", "ContractorId", typeof(System.Int32), false, true, false, false,  (int)ContractorContactFieldIndex.ContractorId, 0, 0, 10);
			this.AddElementFieldInfo("ContractorContactEntity", "Email", typeof(System.String), false, false, false, false,  (int)ContractorContactFieldIndex.Email, 255, 0, 0);
			this.AddElementFieldInfo("ContractorContactEntity", "FirstName", typeof(System.String), false, false, false, false,  (int)ContractorContactFieldIndex.FirstName, 50, 0, 0);
			this.AddElementFieldInfo("ContractorContactEntity", "Id", typeof(System.Int32), true, false, true, false,  (int)ContractorContactFieldIndex.Id, 0, 0, 10);
			this.AddElementFieldInfo("ContractorContactEntity", "LastName", typeof(System.String), false, false, false, false,  (int)ContractorContactFieldIndex.LastName, 50, 0, 0);
			this.AddElementFieldInfo("ContractorContactEntity", "Name", typeof(System.String), false, false, false, false,  (int)ContractorContactFieldIndex.Name, 50, 0, 0);
			this.AddElementFieldInfo("ContractorContactEntity", "Phone", typeof(System.String), false, false, false, false,  (int)ContractorContactFieldIndex.Phone, 50, 0, 0);
		}
		/// <summary>Inits GasAccountEntity's FieldInfo objects</summary>
		private void InitGasAccountEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(GasAccountFieldIndex), "GasAccountEntity");
			this.AddElementFieldInfo("GasAccountEntity", "EnergyUnit", typeof(System.String), false, false, false, false,  (int)GasAccountFieldIndex.EnergyUnit, 50, 0, 0);
			this.AddElementFieldInfo("GasAccountEntity", "EnergyValue", typeof(System.Decimal), false, false, false, false,  (int)GasAccountFieldIndex.EnergyValue, 0, 0, 18);
			this.AddElementFieldInfo("GasAccountEntity", "GasDay", typeof(System.DateTime), false, false, false, false,  (int)GasAccountFieldIndex.GasDay, 0, 0, 0);
			this.AddElementFieldInfo("GasAccountEntity", "GasStorageId", typeof(System.Int32), false, true, false, false,  (int)GasAccountFieldIndex.GasStorageId, 0, 0, 10);
			this.AddElementFieldInfo("GasAccountEntity", "HourIndex", typeof(System.Int32), false, false, false, false,  (int)GasAccountFieldIndex.HourIndex, 0, 0, 10);
			this.AddElementFieldInfo("GasAccountEntity", "Id", typeof(System.Int32), true, false, true, false,  (int)GasAccountFieldIndex.Id, 0, 0, 10);
			this.AddElementFieldInfo("GasAccountEntity", "VolumeUnit", typeof(System.String), false, false, false, false,  (int)GasAccountFieldIndex.VolumeUnit, 50, 0, 0);
			this.AddElementFieldInfo("GasAccountEntity", "VolumeValue", typeof(System.Decimal), false, false, false, false,  (int)GasAccountFieldIndex.VolumeValue, 0, 0, 18);
		}
		/// <summary>Inits GasStorageEntity's FieldInfo objects</summary>
		private void InitGasStorageEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(GasStorageFieldIndex), "GasStorageEntity");
			this.AddElementFieldInfo("GasStorageEntity", "Id", typeof(System.Int32), true, false, true, false,  (int)GasStorageFieldIndex.Id, 0, 0, 10);
			this.AddElementFieldInfo("GasStorageEntity", "Name", typeof(System.String), false, false, false, false,  (int)GasStorageFieldIndex.Name, 50, 0, 0);
		}
		/// <summary>Inits NominationEntity's FieldInfo objects</summary>
		private void InitNominationEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(NominationFieldIndex), "NominationEntity");
			this.AddElementFieldInfo("NominationEntity", "AccessPointFlowId", typeof(System.Int32), false, true, false, false,  (int)NominationFieldIndex.AccessPointFlowId, 0, 0, 10);
			this.AddElementFieldInfo("NominationEntity", "CalorificValueId", typeof(System.Int32), false, true, false, false,  (int)NominationFieldIndex.CalorificValueId, 0, 0, 10);
			this.AddElementFieldInfo("NominationEntity", "Channel", typeof(System.String), false, false, false, false,  (int)NominationFieldIndex.Channel, 50, 0, 0);
			this.AddElementFieldInfo("NominationEntity", "Comment", typeof(System.String), false, false, false, false,  (int)NominationFieldIndex.Comment, 1, 0, 0);
			this.AddElementFieldInfo("NominationEntity", "ContractorId", typeof(System.Int32), false, true, false, false,  (int)NominationFieldIndex.ContractorId, 0, 0, 10);
			this.AddElementFieldInfo("NominationEntity", "CreateDatetime", typeof(System.DateTime), false, false, false, false,  (int)NominationFieldIndex.CreateDatetime, 0, 0, 0);
			this.AddElementFieldInfo("NominationEntity", "GasDay", typeof(System.DateTime), false, false, false, false,  (int)NominationFieldIndex.GasDay, 0, 0, 0);
			this.AddElementFieldInfo("NominationEntity", "Id", typeof(System.Int32), true, false, true, false,  (int)NominationFieldIndex.Id, 0, 0, 10);
			this.AddElementFieldInfo("NominationEntity", "IsAccepted", typeof(System.Boolean), false, false, false, false,  (int)NominationFieldIndex.IsAccepted, 0, 0, 0);
			this.AddElementFieldInfo("NominationEntity", "IsLast", typeof(System.Boolean), false, false, false, false,  (int)NominationFieldIndex.IsLast, 0, 0, 0);
			this.AddElementFieldInfo("NominationEntity", "MajorVersion", typeof(System.Int32), false, false, false, false,  (int)NominationFieldIndex.MajorVersion, 0, 0, 10);
			this.AddElementFieldInfo("NominationEntity", "Name", typeof(System.String), false, false, false, false,  (int)NominationFieldIndex.Name, 50, 0, 0);
			this.AddElementFieldInfo("NominationEntity", "ParentNominationId", typeof(Nullable<System.Int32>), false, true, false, true,  (int)NominationFieldIndex.ParentNominationId, 0, 0, 10);
			this.AddElementFieldInfo("NominationEntity", "UserId", typeof(System.Int32), false, true, false, false,  (int)NominationFieldIndex.UserId, 0, 0, 10);
			this.AddElementFieldInfo("NominationEntity", "Version", typeof(System.Int32), false, false, false, false,  (int)NominationFieldIndex.Version, 0, 0, 10);
		}
		/// <summary>Inits NominationValueEntity's FieldInfo objects</summary>
		private void InitNominationValueEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(NominationValueFieldIndex), "NominationValueEntity");
			this.AddElementFieldInfo("NominationValueEntity", "EnergyUnit", typeof(System.String), false, false, false, false,  (int)NominationValueFieldIndex.EnergyUnit, 50, 0, 0);
			this.AddElementFieldInfo("NominationValueEntity", "EnergyValue", typeof(System.Decimal), false, false, false, false,  (int)NominationValueFieldIndex.EnergyValue, 0, 0, 18);
			this.AddElementFieldInfo("NominationValueEntity", "HourIndex", typeof(System.Int32), false, false, false, false,  (int)NominationValueFieldIndex.HourIndex, 0, 0, 10);
			this.AddElementFieldInfo("NominationValueEntity", "Id", typeof(System.Int32), true, false, true, false,  (int)NominationValueFieldIndex.Id, 0, 0, 10);
			this.AddElementFieldInfo("NominationValueEntity", "NominationId", typeof(System.Int32), false, true, false, false,  (int)NominationValueFieldIndex.NominationId, 0, 0, 10);
			this.AddElementFieldInfo("NominationValueEntity", "VolumeUnit", typeof(System.String), false, false, false, false,  (int)NominationValueFieldIndex.VolumeUnit, 50, 0, 0);
			this.AddElementFieldInfo("NominationValueEntity", "VolumeValue", typeof(System.Decimal), false, false, false, false,  (int)NominationValueFieldIndex.VolumeValue, 0, 0, 18);
		}
		/// <summary>Inits SessionEntity's FieldInfo objects</summary>
		private void InitSessionEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(SessionFieldIndex), "SessionEntity");
			this.AddElementFieldInfo("SessionEntity", "Browser", typeof(System.String), false, false, false, false,  (int)SessionFieldIndex.Browser, 50, 0, 0);
			this.AddElementFieldInfo("SessionEntity", "CreateDatetime", typeof(System.DateTime), false, false, false, false,  (int)SessionFieldIndex.CreateDatetime, 0, 0, 0);
			this.AddElementFieldInfo("SessionEntity", "Guid", typeof(System.String), false, false, false, false,  (int)SessionFieldIndex.Guid, 2147483647, 0, 0);
			this.AddElementFieldInfo("SessionEntity", "Id", typeof(System.Int32), true, false, true, false,  (int)SessionFieldIndex.Id, 0, 0, 10);
			this.AddElementFieldInfo("SessionEntity", "Ip", typeof(System.String), false, false, false, false,  (int)SessionFieldIndex.Ip, 50, 0, 0);
			this.AddElementFieldInfo("SessionEntity", "LastActivityDatetime", typeof(System.DateTime), false, false, false, false,  (int)SessionFieldIndex.LastActivityDatetime, 0, 0, 0);
			this.AddElementFieldInfo("SessionEntity", "UserId", typeof(System.Int32), false, true, false, false,  (int)SessionFieldIndex.UserId, 0, 0, 10);
		}
		/// <summary>Inits UserEntity's FieldInfo objects</summary>
		private void InitUserEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(UserFieldIndex), "UserEntity");
			this.AddElementFieldInfo("UserEntity", "ContractorId", typeof(System.Int32), false, true, false, false,  (int)UserFieldIndex.ContractorId, 0, 0, 10);
			this.AddElementFieldInfo("UserEntity", "CreateDatetime", typeof(System.DateTime), false, false, false, false,  (int)UserFieldIndex.CreateDatetime, 0, 0, 0);
			this.AddElementFieldInfo("UserEntity", "Email", typeof(System.String), false, false, false, false,  (int)UserFieldIndex.Email, 50, 0, 0);
			this.AddElementFieldInfo("UserEntity", "Id", typeof(System.Int32), true, false, true, false,  (int)UserFieldIndex.Id, 0, 0, 10);
			this.AddElementFieldInfo("UserEntity", "IsActive", typeof(System.Boolean), false, false, false, false,  (int)UserFieldIndex.IsActive, 0, 0, 0);
			this.AddElementFieldInfo("UserEntity", "LastActivityDatetime", typeof(System.DateTime), false, false, false, false,  (int)UserFieldIndex.LastActivityDatetime, 0, 0, 0);
			this.AddElementFieldInfo("UserEntity", "Name", typeof(System.String), false, false, false, false,  (int)UserFieldIndex.Name, 50, 0, 0);
			this.AddElementFieldInfo("UserEntity", "Password", typeof(System.String), false, false, false, false,  (int)UserFieldIndex.Password, 2147483647, 0, 0);
		}
		
	}
}




