﻿///////////////////////////////////////////////////////////////
// This is generated code.
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.2
// Code is generated on:
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version:
//////////////////////////////////////////////////////////////
using System;

namespace Orms.DataAccessLLBLGenPro
{
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: AccessPoint.</summary>
	public enum AccessPointFieldIndex
	{
		///<summary>Code. </summary>
		Code,
		///<summary>GasStorageId. </summary>
		GasStorageId,
		///<summary>Id. </summary>
		Id,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: AccessPointFlow.</summary>
	public enum AccessPointFlowFieldIndex
	{
		///<summary>AccessPointId. </summary>
		AccessPointId,
        ///<summary>Direction. </summary>
        Direction,
		///<summary>Id. </summary>
		Id,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Agreement.</summary>
	public enum AgreementFieldIndex
	{
		///<summary>ContractorId. </summary>
		ContractorId,
		///<summary>CreateDatetime. </summary>
		CreateDatetime,
		///<summary>EndDatetime. </summary>
		EndDatetime,
		///<summary>GasStorageId. </summary>
		GasStorageId,
		///<summary>Id. </summary>
		Id,
		///<summary>StartDatetime. </summary>
		StartDatetime,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CalorificValue.</summary>
	public enum CalorificValueFieldIndex
	{
		///<summary>GasDay. </summary>
		GasDay,
		///<summary>GasStorageId. </summary>
		GasStorageId,
		///<summary>Id. </summary>
		Id,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CalorificValueValue.</summary>
	public enum CalorificValueValueFieldIndex
	{
		///<summary>CalorificValueId. </summary>
		CalorificValueId,
		///<summary>EnergyUnit. </summary>
		EnergyUnit,
		///<summary>EnergyValue. </summary>
		EnergyValue,
		///<summary>HourIndex. </summary>
		HourIndex,
		///<summary>Id. </summary>
		Id,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Contractor.</summary>
	public enum ContractorFieldIndex
	{
		///<summary>Adress. </summary>
		Adress,
		///<summary>Code. </summary>
		Code,
		///<summary>Id. </summary>
		Id,
		///<summary>Name. </summary>
		Name,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ContractorContact.</summary>
	public enum ContractorContactFieldIndex
	{
		///<summary>Adress. </summary>
		Adress,
		///<summary>ContractorId. </summary>
		ContractorId,
		///<summary>Email. </summary>
		Email,
		///<summary>FirstName. </summary>
		FirstName,
		///<summary>Id. </summary>
		Id,
		///<summary>LastName. </summary>
		LastName,
		///<summary>Name. </summary>
		Name,
		///<summary>Phone. </summary>
		Phone,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: GasAccount.</summary>
	public enum GasAccountFieldIndex
	{
		///<summary>EnergyUnit. </summary>
		EnergyUnit,
		///<summary>EnergyValue. </summary>
		EnergyValue,
		///<summary>GasDay. </summary>
		GasDay,
		///<summary>GasStorageId. </summary>
		GasStorageId,
		///<summary>HourIndex. </summary>
		HourIndex,
		///<summary>Id. </summary>
		Id,
		///<summary>VolumeUnit. </summary>
		VolumeUnit,
		///<summary>VolumeValue. </summary>
		VolumeValue,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: GasStorage.</summary>
	public enum GasStorageFieldIndex
	{
		///<summary>Id. </summary>
		Id,
		///<summary>Name. </summary>
		Name,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Nomination.</summary>
	public enum NominationFieldIndex
	{
		///<summary>AccessPointFlowId. </summary>
		AccessPointFlowId,
		///<summary>CalorificValueId. </summary>
		CalorificValueId,
		///<summary>Channel. </summary>
		Channel,
		///<summary>Comment. </summary>
		Comment,
		///<summary>ContractorId. </summary>
		ContractorId,
		///<summary>CreateDatetime. </summary>
		CreateDatetime,
		///<summary>GasDay. </summary>
		GasDay,
		///<summary>Id. </summary>
		Id,
		///<summary>IsAccepted. </summary>
		IsAccepted,
		///<summary>IsLast. </summary>
		IsLast,
		///<summary>MajorVersion. </summary>
		MajorVersion,
		///<summary>Name. </summary>
		Name,
		///<summary>ParentNominationId. </summary>
		ParentNominationId,
		///<summary>UserId. </summary>
		UserId,
		///<summary>Version. </summary>
		Version,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: NominationValue.</summary>
	public enum NominationValueFieldIndex
	{
		///<summary>EnergyUnit. </summary>
		EnergyUnit,
		///<summary>EnergyValue. </summary>
		EnergyValue,
		///<summary>HourIndex. </summary>
		HourIndex,
		///<summary>Id. </summary>
		Id,
		///<summary>NominationId. </summary>
		NominationId,
		///<summary>VolumeUnit. </summary>
		VolumeUnit,
		///<summary>VolumeValue. </summary>
		VolumeValue,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Session.</summary>
	public enum SessionFieldIndex
	{
		///<summary>Browser. </summary>
		Browser,
		///<summary>CreateDatetime. </summary>
		CreateDatetime,
		///<summary>Guid. </summary>
		Guid,
		///<summary>Id. </summary>
		Id,
		///<summary>Ip. </summary>
		Ip,
		///<summary>LastActivityDatetime. </summary>
		LastActivityDatetime,
		///<summary>UserId. </summary>
		UserId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: User.</summary>
	public enum UserFieldIndex
	{
		///<summary>ContractorId. </summary>
		ContractorId,
		///<summary>CreateDatetime. </summary>
		CreateDatetime,
		///<summary>Email. </summary>
		Email,
		///<summary>Id. </summary>
		Id,
		///<summary>IsActive. </summary>
		IsActive,
		///<summary>LastActivityDatetime. </summary>
		LastActivityDatetime,
		///<summary>Name. </summary>
		Name,
		///<summary>Password. </summary>
		Password,
		/// <summary></summary>
		AmountOfFields
	}



	/// <summary>Enum definition for all the entity types defined in this namespace. Used by the entityfields factory.</summary>
	public enum EntityType
	{
		///<summary>AccessPoint</summary>
		AccessPointEntity,
		///<summary>AccessPointFlow</summary>
		AccessPointFlowEntity,
		///<summary>Agreement</summary>
		AgreementEntity,
		///<summary>CalorificValue</summary>
		CalorificValueEntity,
		///<summary>CalorificValueValue</summary>
		CalorificValueValueEntity,
		///<summary>Contractor</summary>
		ContractorEntity,
		///<summary>ContractorContact</summary>
		ContractorContactEntity,
		///<summary>GasAccount</summary>
		GasAccountEntity,
		///<summary>GasStorage</summary>
		GasStorageEntity,
		///<summary>Nomination</summary>
		NominationEntity,
		///<summary>NominationValue</summary>
		NominationValueEntity,
		///<summary>Session</summary>
		SessionEntity,
		///<summary>User</summary>
		UserEntity
	}


	#region Custom ConstantsEnums Code

	// __LLBLGENPRO_USER_CODE_REGION_START CustomUserConstants
	// __LLBLGENPRO_USER_CODE_REGION_END

	#endregion

	#region Included code

	#endregion
}

