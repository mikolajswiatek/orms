﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Orms.DataAccessLLBLGenPro;
using Orms.DataAccessLLBLGenPro.FactoryClasses;
using Orms.DataAccessLLBLGenPro.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Orms.DataAccessLLBLGenPro.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: GasStorage. </summary>
	public partial class GasStorageRelations
	{
		/// <summary>CTor</summary>
		public GasStorageRelations()
		{
		}

		/// <summary>Gets all relations of the GasStorageEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AccessPointEntityUsingGasStorageId);
			toReturn.Add(this.AgreementEntityUsingGasStorageId);
			toReturn.Add(this.CalorificValueEntityUsingGasStorageId);
			toReturn.Add(this.GasAccountEntityUsingGasStorageId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between GasStorageEntity and AccessPointEntity over the 1:n relation they have, using the relation between the fields:
		/// GasStorage.Id - AccessPoint.GasStorageId
		/// </summary>
		public virtual IEntityRelation AccessPointEntityUsingGasStorageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AccessPoints" , true);
				relation.AddEntityFieldPair(GasStorageFields.Id, AccessPointFields.GasStorageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GasStorageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AccessPointEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between GasStorageEntity and AgreementEntity over the 1:n relation they have, using the relation between the fields:
		/// GasStorage.Id - Agreement.GasStorageId
		/// </summary>
		public virtual IEntityRelation AgreementEntityUsingGasStorageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Agreements" , true);
				relation.AddEntityFieldPair(GasStorageFields.Id, AgreementFields.GasStorageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GasStorageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AgreementEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between GasStorageEntity and CalorificValueEntity over the 1:n relation they have, using the relation between the fields:
		/// GasStorage.Id - CalorificValue.GasStorageId
		/// </summary>
		public virtual IEntityRelation CalorificValueEntityUsingGasStorageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CalorificValues" , true);
				relation.AddEntityFieldPair(GasStorageFields.Id, CalorificValueFields.GasStorageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GasStorageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CalorificValueEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between GasStorageEntity and GasAccountEntity over the 1:n relation they have, using the relation between the fields:
		/// GasStorage.Id - GasAccount.GasStorageId
		/// </summary>
		public virtual IEntityRelation GasAccountEntityUsingGasStorageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "GasAccounts" , true);
				relation.AddEntityFieldPair(GasStorageFields.Id, GasAccountFields.GasStorageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GasStorageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GasAccountEntity", false);
				return relation;
			}
		}


		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticGasStorageRelations
	{
		internal static readonly IEntityRelation AccessPointEntityUsingGasStorageIdStatic = new GasStorageRelations().AccessPointEntityUsingGasStorageId;
		internal static readonly IEntityRelation AgreementEntityUsingGasStorageIdStatic = new GasStorageRelations().AgreementEntityUsingGasStorageId;
		internal static readonly IEntityRelation CalorificValueEntityUsingGasStorageIdStatic = new GasStorageRelations().CalorificValueEntityUsingGasStorageId;
		internal static readonly IEntityRelation GasAccountEntityUsingGasStorageIdStatic = new GasStorageRelations().GasAccountEntityUsingGasStorageId;

		/// <summary>CTor</summary>
		static StaticGasStorageRelations()
		{
		}
	}
}
