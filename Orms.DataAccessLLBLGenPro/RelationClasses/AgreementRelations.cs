﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Orms.DataAccessLLBLGenPro;
using Orms.DataAccessLLBLGenPro.FactoryClasses;
using Orms.DataAccessLLBLGenPro.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Orms.DataAccessLLBLGenPro.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Agreement. </summary>
	public partial class AgreementRelations
	{
		/// <summary>CTor</summary>
		public AgreementRelations()
		{
		}

		/// <summary>Gets all relations of the AgreementEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ContractorEntityUsingContractorId);
			toReturn.Add(this.GasStorageEntityUsingGasStorageId);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between AgreementEntity and ContractorEntity over the m:1 relation they have, using the relation between the fields:
		/// Agreement.ContractorId - Contractor.Id
		/// </summary>
		public virtual IEntityRelation ContractorEntityUsingContractorId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Contractor", false);
				relation.AddEntityFieldPair(ContractorFields.Id, AgreementFields.ContractorId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractorEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AgreementEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between AgreementEntity and GasStorageEntity over the m:1 relation they have, using the relation between the fields:
		/// Agreement.GasStorageId - GasStorage.Id
		/// </summary>
		public virtual IEntityRelation GasStorageEntityUsingGasStorageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "GasStorage", false);
				relation.AddEntityFieldPair(GasStorageFields.Id, AgreementFields.GasStorageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GasStorageEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AgreementEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticAgreementRelations
	{
		internal static readonly IEntityRelation ContractorEntityUsingContractorIdStatic = new AgreementRelations().ContractorEntityUsingContractorId;
		internal static readonly IEntityRelation GasStorageEntityUsingGasStorageIdStatic = new AgreementRelations().GasStorageEntityUsingGasStorageId;

		/// <summary>CTor</summary>
		static StaticAgreementRelations()
		{
		}
	}
}
