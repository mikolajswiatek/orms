﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Orms.DataAccessLLBLGenPro;
using Orms.DataAccessLLBLGenPro.FactoryClasses;
using Orms.DataAccessLLBLGenPro.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Orms.DataAccessLLBLGenPro.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: AccessPointFlow. </summary>
	public partial class AccessPointFlowRelations
	{
		/// <summary>CTor</summary>
		public AccessPointFlowRelations()
		{
		}

		/// <summary>Gets all relations of the AccessPointFlowEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.NominationEntityUsingAccessPointFlowId);
			toReturn.Add(this.AccessPointEntityUsingAccessPointId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between AccessPointFlowEntity and NominationEntity over the 1:n relation they have, using the relation between the fields:
		/// AccessPointFlow.Id - Nomination.AccessPointFlowId
		/// </summary>
		public virtual IEntityRelation NominationEntityUsingAccessPointFlowId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Nominations" , true);
				relation.AddEntityFieldPair(AccessPointFlowFields.Id, NominationFields.AccessPointFlowId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AccessPointFlowEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("NominationEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between AccessPointFlowEntity and AccessPointEntity over the m:1 relation they have, using the relation between the fields:
		/// AccessPointFlow.AccessPointId - AccessPoint.Id
		/// </summary>
		public virtual IEntityRelation AccessPointEntityUsingAccessPointId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "AccessPoint", false);
				relation.AddEntityFieldPair(AccessPointFields.Id, AccessPointFlowFields.AccessPointId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AccessPointEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AccessPointFlowEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticAccessPointFlowRelations
	{
		internal static readonly IEntityRelation NominationEntityUsingAccessPointFlowIdStatic = new AccessPointFlowRelations().NominationEntityUsingAccessPointFlowId;
		internal static readonly IEntityRelation AccessPointEntityUsingAccessPointIdStatic = new AccessPointFlowRelations().AccessPointEntityUsingAccessPointId;

		/// <summary>CTor</summary>
		static StaticAccessPointFlowRelations()
		{
		}
	}
}
