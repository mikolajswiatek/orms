﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Orms.DataAccessLLBLGenPro;
using Orms.DataAccessLLBLGenPro.FactoryClasses;
using Orms.DataAccessLLBLGenPro.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Orms.DataAccessLLBLGenPro.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Contractor. </summary>
	public partial class ContractorRelations
	{
		/// <summary>CTor</summary>
		public ContractorRelations()
		{
		}

		/// <summary>Gets all relations of the ContractorEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AgreementEntityUsingContractorId);
			toReturn.Add(this.ContractorContactEntityUsingContractorId);
			toReturn.Add(this.NominationEntityUsingContractorId);
			toReturn.Add(this.UserEntityUsingContractorId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between ContractorEntity and AgreementEntity over the 1:n relation they have, using the relation between the fields:
		/// Contractor.Id - Agreement.ContractorId
		/// </summary>
		public virtual IEntityRelation AgreementEntityUsingContractorId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Agreements" , true);
				relation.AddEntityFieldPair(ContractorFields.Id, AgreementFields.ContractorId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractorEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AgreementEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ContractorEntity and ContractorContactEntity over the 1:n relation they have, using the relation between the fields:
		/// Contractor.Id - ContractorContact.ContractorId
		/// </summary>
		public virtual IEntityRelation ContractorContactEntityUsingContractorId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ContractorContacts" , true);
				relation.AddEntityFieldPair(ContractorFields.Id, ContractorContactFields.ContractorId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractorEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractorContactEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ContractorEntity and NominationEntity over the 1:n relation they have, using the relation between the fields:
		/// Contractor.Id - Nomination.ContractorId
		/// </summary>
		public virtual IEntityRelation NominationEntityUsingContractorId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Nominations" , true);
				relation.AddEntityFieldPair(ContractorFields.Id, NominationFields.ContractorId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractorEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("NominationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ContractorEntity and UserEntity over the 1:n relation they have, using the relation between the fields:
		/// Contractor.Id - User.ContractorId
		/// </summary>
		public virtual IEntityRelation UserEntityUsingContractorId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Users" , true);
				relation.AddEntityFieldPair(ContractorFields.Id, UserFields.ContractorId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractorEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserEntity", false);
				return relation;
			}
		}


		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticContractorRelations
	{
		internal static readonly IEntityRelation AgreementEntityUsingContractorIdStatic = new ContractorRelations().AgreementEntityUsingContractorId;
		internal static readonly IEntityRelation ContractorContactEntityUsingContractorIdStatic = new ContractorRelations().ContractorContactEntityUsingContractorId;
		internal static readonly IEntityRelation NominationEntityUsingContractorIdStatic = new ContractorRelations().NominationEntityUsingContractorId;
		internal static readonly IEntityRelation UserEntityUsingContractorIdStatic = new ContractorRelations().UserEntityUsingContractorId;

		/// <summary>CTor</summary>
		static StaticContractorRelations()
		{
		}
	}
}
