﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Orms.DataAccessLLBLGenPro;
using Orms.DataAccessLLBLGenPro.FactoryClasses;
using Orms.DataAccessLLBLGenPro.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Orms.DataAccessLLBLGenPro.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: AccessPoint. </summary>
	public partial class AccessPointRelations
	{
		/// <summary>CTor</summary>
		public AccessPointRelations()
		{
		}

		/// <summary>Gets all relations of the AccessPointEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AccessPointFlowEntityUsingAccessPointId);
			toReturn.Add(this.GasStorageEntityUsingGasStorageId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between AccessPointEntity and AccessPointFlowEntity over the 1:n relation they have, using the relation between the fields:
		/// AccessPoint.Id - AccessPointFlow.AccessPointId
		/// </summary>
		public virtual IEntityRelation AccessPointFlowEntityUsingAccessPointId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AccessPointFlows" , true);
				relation.AddEntityFieldPair(AccessPointFields.Id, AccessPointFlowFields.AccessPointId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AccessPointEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AccessPointFlowEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between AccessPointEntity and GasStorageEntity over the m:1 relation they have, using the relation between the fields:
		/// AccessPoint.GasStorageId - GasStorage.Id
		/// </summary>
		public virtual IEntityRelation GasStorageEntityUsingGasStorageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "GasStorage", false);
				relation.AddEntityFieldPair(GasStorageFields.Id, AccessPointFields.GasStorageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GasStorageEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AccessPointEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticAccessPointRelations
	{
		internal static readonly IEntityRelation AccessPointFlowEntityUsingAccessPointIdStatic = new AccessPointRelations().AccessPointFlowEntityUsingAccessPointId;
		internal static readonly IEntityRelation GasStorageEntityUsingGasStorageIdStatic = new AccessPointRelations().GasStorageEntityUsingGasStorageId;

		/// <summary>CTor</summary>
		static StaticAccessPointRelations()
		{
		}
	}
}
