﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Orms.DataAccessLLBLGenPro;
using Orms.DataAccessLLBLGenPro.FactoryClasses;
using Orms.DataAccessLLBLGenPro.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Orms.DataAccessLLBLGenPro.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: CalorificValue. </summary>
	public partial class CalorificValueRelations
	{
		/// <summary>CTor</summary>
		public CalorificValueRelations()
		{
		}

		/// <summary>Gets all relations of the CalorificValueEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CalorificValueValueEntityUsingCalorificValueId);
			toReturn.Add(this.NominationEntityUsingCalorificValueId);
			toReturn.Add(this.GasStorageEntityUsingGasStorageId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between CalorificValueEntity and CalorificValueValueEntity over the 1:n relation they have, using the relation between the fields:
		/// CalorificValue.Id - CalorificValueValue.CalorificValueId
		/// </summary>
		public virtual IEntityRelation CalorificValueValueEntityUsingCalorificValueId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CalorificValueValues" , true);
				relation.AddEntityFieldPair(CalorificValueFields.Id, CalorificValueValueFields.CalorificValueId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CalorificValueEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CalorificValueValueEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CalorificValueEntity and NominationEntity over the 1:n relation they have, using the relation between the fields:
		/// CalorificValue.Id - Nomination.CalorificValueId
		/// </summary>
		public virtual IEntityRelation NominationEntityUsingCalorificValueId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Nominations" , true);
				relation.AddEntityFieldPair(CalorificValueFields.Id, NominationFields.CalorificValueId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CalorificValueEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("NominationEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between CalorificValueEntity and GasStorageEntity over the m:1 relation they have, using the relation between the fields:
		/// CalorificValue.GasStorageId - GasStorage.Id
		/// </summary>
		public virtual IEntityRelation GasStorageEntityUsingGasStorageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "GasStorage", false);
				relation.AddEntityFieldPair(GasStorageFields.Id, CalorificValueFields.GasStorageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GasStorageEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CalorificValueEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticCalorificValueRelations
	{
		internal static readonly IEntityRelation CalorificValueValueEntityUsingCalorificValueIdStatic = new CalorificValueRelations().CalorificValueValueEntityUsingCalorificValueId;
		internal static readonly IEntityRelation NominationEntityUsingCalorificValueIdStatic = new CalorificValueRelations().NominationEntityUsingCalorificValueId;
		internal static readonly IEntityRelation GasStorageEntityUsingGasStorageIdStatic = new CalorificValueRelations().GasStorageEntityUsingGasStorageId;

		/// <summary>CTor</summary>
		static StaticCalorificValueRelations()
		{
		}
	}
}
