﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Orms.DataAccessLLBLGenPro;
using Orms.DataAccessLLBLGenPro.FactoryClasses;
using Orms.DataAccessLLBLGenPro.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Orms.DataAccessLLBLGenPro.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: CalorificValueValue. </summary>
	public partial class CalorificValueValueRelations
	{
		/// <summary>CTor</summary>
		public CalorificValueValueRelations()
		{
		}

		/// <summary>Gets all relations of the CalorificValueValueEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CalorificValueEntityUsingCalorificValueId);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between CalorificValueValueEntity and CalorificValueEntity over the m:1 relation they have, using the relation between the fields:
		/// CalorificValueValue.CalorificValueId - CalorificValue.Id
		/// </summary>
		public virtual IEntityRelation CalorificValueEntityUsingCalorificValueId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CalorificValue", false);
				relation.AddEntityFieldPair(CalorificValueFields.Id, CalorificValueValueFields.CalorificValueId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CalorificValueEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CalorificValueValueEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticCalorificValueValueRelations
	{
		internal static readonly IEntityRelation CalorificValueEntityUsingCalorificValueIdStatic = new CalorificValueValueRelations().CalorificValueEntityUsingCalorificValueId;

		/// <summary>CTor</summary>
		static StaticCalorificValueValueRelations()
		{
		}
	}
}
