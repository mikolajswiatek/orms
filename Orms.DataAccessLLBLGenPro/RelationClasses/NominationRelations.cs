﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Orms.DataAccessLLBLGenPro;
using Orms.DataAccessLLBLGenPro.FactoryClasses;
using Orms.DataAccessLLBLGenPro.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Orms.DataAccessLLBLGenPro.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Nomination. </summary>
	public partial class NominationRelations
	{
		/// <summary>CTor</summary>
		public NominationRelations()
		{
		}

		/// <summary>Gets all relations of the NominationEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.NominationEntityUsingParentNominationId);
			toReturn.Add(this.NominationValueEntityUsingNominationId);
			toReturn.Add(this.AccessPointFlowEntityUsingAccessPointFlowId);
			toReturn.Add(this.CalorificValueEntityUsingCalorificValueId);
			toReturn.Add(this.ContractorEntityUsingContractorId);
			toReturn.Add(this.NominationEntityUsingIdParentNominationId);
			toReturn.Add(this.UserEntityUsingUserId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between NominationEntity and NominationEntity over the 1:n relation they have, using the relation between the fields:
		/// Nomination.Id - Nomination.ParentNominationId
		/// </summary>
		public virtual IEntityRelation NominationEntityUsingParentNominationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Nominations" , true);
				relation.AddEntityFieldPair(NominationFields.Id, NominationFields.ParentNominationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("NominationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("NominationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between NominationEntity and NominationValueEntity over the 1:n relation they have, using the relation between the fields:
		/// Nomination.Id - NominationValue.NominationId
		/// </summary>
		public virtual IEntityRelation NominationValueEntityUsingNominationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "NominationValues" , true);
				relation.AddEntityFieldPair(NominationFields.Id, NominationValueFields.NominationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("NominationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("NominationValueEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between NominationEntity and AccessPointFlowEntity over the m:1 relation they have, using the relation between the fields:
		/// Nomination.AccessPointFlowId - AccessPointFlow.Id
		/// </summary>
		public virtual IEntityRelation AccessPointFlowEntityUsingAccessPointFlowId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "AccessPointFlow", false);
				relation.AddEntityFieldPair(AccessPointFlowFields.Id, NominationFields.AccessPointFlowId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AccessPointFlowEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("NominationEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between NominationEntity and CalorificValueEntity over the m:1 relation they have, using the relation between the fields:
		/// Nomination.CalorificValueId - CalorificValue.Id
		/// </summary>
		public virtual IEntityRelation CalorificValueEntityUsingCalorificValueId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CalorificValue", false);
				relation.AddEntityFieldPair(CalorificValueFields.Id, NominationFields.CalorificValueId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CalorificValueEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("NominationEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between NominationEntity and ContractorEntity over the m:1 relation they have, using the relation between the fields:
		/// Nomination.ContractorId - Contractor.Id
		/// </summary>
		public virtual IEntityRelation ContractorEntityUsingContractorId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Contractor", false);
				relation.AddEntityFieldPair(ContractorFields.Id, NominationFields.ContractorId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractorEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("NominationEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between NominationEntity and NominationEntity over the m:1 relation they have, using the relation between the fields:
		/// Nomination.ParentNominationId - Nomination.Id
		/// </summary>
		public virtual IEntityRelation NominationEntityUsingIdParentNominationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Nomination", false);
				relation.AddEntityFieldPair(NominationFields.Id, NominationFields.ParentNominationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("NominationEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("NominationEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between NominationEntity and UserEntity over the m:1 relation they have, using the relation between the fields:
		/// Nomination.UserId - User.Id
		/// </summary>
		public virtual IEntityRelation UserEntityUsingUserId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "User", false);
				relation.AddEntityFieldPair(UserFields.Id, NominationFields.UserId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("NominationEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticNominationRelations
	{
		internal static readonly IEntityRelation NominationEntityUsingParentNominationIdStatic = new NominationRelations().NominationEntityUsingParentNominationId;
		internal static readonly IEntityRelation NominationValueEntityUsingNominationIdStatic = new NominationRelations().NominationValueEntityUsingNominationId;
		internal static readonly IEntityRelation AccessPointFlowEntityUsingAccessPointFlowIdStatic = new NominationRelations().AccessPointFlowEntityUsingAccessPointFlowId;
		internal static readonly IEntityRelation CalorificValueEntityUsingCalorificValueIdStatic = new NominationRelations().CalorificValueEntityUsingCalorificValueId;
		internal static readonly IEntityRelation ContractorEntityUsingContractorIdStatic = new NominationRelations().ContractorEntityUsingContractorId;
		internal static readonly IEntityRelation NominationEntityUsingIdParentNominationIdStatic = new NominationRelations().NominationEntityUsingIdParentNominationId;
		internal static readonly IEntityRelation UserEntityUsingUserIdStatic = new NominationRelations().UserEntityUsingUserId;

		/// <summary>CTor</summary>
		static StaticNominationRelations()
		{
		}
	}
}
