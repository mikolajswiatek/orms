﻿using System.Collections.Generic;
using System.Linq;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.DataAccessLLBLGenPro.Converter;
using Orms.DataAccessLLBLGenPro.DatabaseSpecific;
using Orms.DataAccessLLBLGenPro.EntityClasses;
using Orms.DataAccessLLBLGenPro.Linq;
using Orms.Models;

namespace Orms.DataAccessLLBLGenPro.Repositories
{
    public class GasStorageRepository : IGasStorageRepository<DataAccessAdapter>
    {
        public void Add(DataAccessAdapter adapter, GasStorage gasStorage)
        {
            adapter.SaveEntity(gasStorage.ToEntity());
        }

        public void Delete(DataAccessAdapter adapter, GasStorage gasStorage)
        {
            adapter.DeleteEntity(gasStorage.ToEntity());
        }

        public GasStorage Get(DataAccessAdapter adapter, int id)
        {
            var metaData = new LinqMetaData(adapter);
            var results = metaData.GasStorage
                .Join(metaData.AccessPoint, g => g.Id, ap => ap.GasStorageId, (g, ap) => new {g, ap})
                .Where(@t => @t.g.Id == id)
                .Select(@t => new {Gs = @t.g, Aps = @t.ap});

            var gs = results.Select(r => r.Gs);
            var aps = results.Select(r => r.Aps);

            return Convert(gs, aps).FirstOrDefault();
        }

        public IEnumerable<GasStorage> GetAll(DataAccessAdapter adapter)
        {
            var metaData = new LinqMetaData(adapter);
            var gasStorages = metaData.GasStorage.ToList();

            return gasStorages.Select(gs => gs.ToModel());
        }

        public void Update(DataAccessAdapter adapter, GasStorage gasStorage)
        {
            var entity = gasStorage.ToEntity();
            adapter.FetchEntity(entity);
            adapter.SaveEntity(entity);
        }

        private static IEnumerable<GasStorage> Convert(IQueryable<GasStorageEntity> gasStorages, IQueryable<AccessPointEntity> aps)
        {
            var gs = new List<GasStorage>();

            gasStorages = gasStorages.Distinct();

            var accessPointsByGasStorage = aps.Distinct().Select(a => new {id = a.GasStorageId, ap = a}).ToList();

            foreach (var g in gasStorages)
            {
                var gEntity = g.ToModel();
                var accessPoints = new List<AccessPoint>();

                foreach (var item in accessPointsByGasStorage.Where(a => a.id == gEntity.Id))
                {
                    var ap = item.ap.ToModel();
                    ap.GasStorage = gEntity;
                    accessPoints.Add(ap);
                }

                gEntity.AccessPoints = accessPoints;
                gs.Add(gEntity);
            }

            return gs;
        }
    }
}
