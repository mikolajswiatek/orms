﻿using System.Collections.Generic;
using System.Linq;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.DataAccessLLBLGenPro.Converter;
using Orms.DataAccessLLBLGenPro.DatabaseSpecific;
using Orms.DataAccessLLBLGenPro.Linq;
using Orms.Models;

namespace Orms.DataAccessLLBLGenPro.Repositories
{
    public class EnergyCalorificValueRepository : IEnergyCalorificValueRepository<DataAccessAdapter>
    {
        public IEnumerable<EnergyCalorificValue> GetAll(DataAccessAdapter adapter)
        {
            var metaData = new LinqMetaData(adapter);
            var ecvs = metaData.CalorificValueValue.Select(e => e.ToModel());

            return ecvs;
        }

        public EnergyCalorificValue Get(DataAccessAdapter adapter, int id)
        {
            var metaData = new LinqMetaData(adapter);
            var ecv = metaData.CalorificValueValue
                .FirstOrDefault(e => e.Id == id)
                ?.ToModel();

            return ecv;
        }

        public void Update(DataAccessAdapter adapter, EnergyCalorificValue ecv)
        {
            var entity = ecv.ToEntity();
            adapter.FetchEntity(entity);
            adapter.SaveEntity(entity);
        }

        public void Add(DataAccessAdapter adapter, EnergyCalorificValue ecv)
        {
            Add(adapter, new List<EnergyCalorificValue>() { ecv });
        }

        public void Add(DataAccessAdapter adapter, IEnumerable<EnergyCalorificValue> ecvs)
        {
            foreach (var ecv in ecvs)
            {
                adapter.SaveEntity(ecv.ToEntity());
            }
        }

        public IEnumerable<EnergyCalorificValue> GetAll(DataAccessAdapter adapter, CalorificValue cv)
        {
            var metaData = new LinqMetaData(adapter);
            var cvs = metaData.CalorificValueValue
                .Where(e => e.CalorificValueId == cv.Id)
                .Select(e => e.ToModel());

            return cvs;
        }
    }
}