﻿using System.Collections.Generic;
using System.Linq;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.DataAccessLLBLGenPro.Converter;
using Orms.DataAccessLLBLGenPro.DatabaseSpecific;
using Orms.DataAccessLLBLGenPro.Linq;
using Orms.Models;

namespace Orms.DataAccessLLBLGenPro.Repositories
{
    public class ContractorContactRepository : IContractorContactRepository<DataAccessAdapter>
    {
        public void Add(DataAccessAdapter adapter, ContractorContact contractorContact)
        {
            adapter.SaveEntity(contractorContact.ToEntity());
        }

        public void Delete(DataAccessAdapter adapter, ContractorContact contractorContact)
        {
            adapter.DeleteEntity(contractorContact.ToEntity());
        }

        public ContractorContact Get(DataAccessAdapter adapter, int id)
        {
            var metaData = new LinqMetaData(adapter);
            var contact = metaData.ContractorContact
                .FirstOrDefault(cc => cc.ContractorId == id)
                ?.ToModel();

            return contact;
        }

        public IEnumerable<ContractorContact> GetAll(DataAccessAdapter adapter)
        {
            var metaData = new LinqMetaData(adapter);
            var contacts = metaData.ContractorContact.Select(cc => cc.ToModel());

            return contacts;
        }

        public void Update(DataAccessAdapter adapter, ContractorContact contractorContact)
        {
            var entity = contractorContact.ToEntity();
            adapter.FetchEntity(entity);
            adapter.SaveEntity(entity);
        }

        public IEnumerable<ContractorContact> GetByContractor(DataAccessAdapter adapter, Contractor contractor)
        {
            var metaData = new LinqMetaData(adapter);
            var contacts = metaData.ContractorContact
                .Where(cc => cc.ContractorId == contractor.Id)
                .ToList();

            return contacts.Select(cc => cc.ToModel());
        }
    }
}
