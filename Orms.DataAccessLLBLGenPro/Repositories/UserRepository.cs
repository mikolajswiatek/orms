﻿using System.Collections.Generic;
using System.Linq;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.DataAccessLLBLGenPro.Converter;
using Orms.DataAccessLLBLGenPro.DatabaseSpecific;
using Orms.DataAccessLLBLGenPro.EntityClasses;
using Orms.DataAccessLLBLGenPro.HelperClasses;
using Orms.DataAccessLLBLGenPro.Linq;
using Orms.Models;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Orms.DataAccessLLBLGenPro.Repositories
{
    public class UserRepository : IUserRepository<DataAccessAdapter>
    {
        public void Add(DataAccessAdapter adapter, User user)
        {
            adapter.SaveEntity(user.ToEntity());
        }

        public User Get(DataAccessAdapter adapter, int id)
        {
            var prefetchPath = new PrefetchPath2(EntityType.UserEntity);
            var users = new EntityCollection<UserEntity>();
            prefetchPath.Add(UserEntity.PrefetchPathContractor);
            RelationPredicateBucket filter = new RelationPredicateBucket(UserFields.Id == id);
            adapter.FetchEntityCollection(users, filter, prefetchPath);

            return users.FirstOrDefault()?.ToModel();
        }

        public IEnumerable<User> GetAll(DataAccessAdapter adapter)
        {
            var prefetchPath = new PrefetchPath2(EntityType.UserEntity);
            var users = new EntityCollection<UserEntity>();
            prefetchPath.Add(UserEntity.PrefetchPathContractor);
            RelationPredicateBucket filter = new RelationPredicateBucket();
            adapter.FetchEntityCollection(users, filter,prefetchPath);

            return users.ToList().Select(u => u.ToModel());
        }

        public void Update(DataAccessAdapter adapter, User user)
        {
            var prefetchPath = new PrefetchPath2(EntityType.UserEntity);
            var users = new EntityCollection<UserEntity>();
            prefetchPath.Add(UserEntity.PrefetchPathContractor);
            RelationPredicateBucket filter = new RelationPredicateBucket(UserFields.Id == user.Id);
            adapter.FetchEntityCollection(users, filter, prefetchPath);

            var u = users.FirstOrDefault();

            if (u != null)
            {
                //adapter.FetchEntity(u);
                u.LastActivityDatetime = user.LastActivityDateTime;
                u.IsActive = user.IsActive;
                u.Password = user.Password;
                u.Email = user.Email;
                adapter.SaveEntity(u);
            }
        }

        public IEnumerable<User> GetBy(DataAccessAdapter adapter, Contractor contractor)
        {
            var metaData = new LinqMetaData(adapter);
            var users = metaData.User
                .Where(u => u.ContractorId == contractor.Id)
                .ToList();

            return users.Select(u => u.ToModel()); ;
        }

        public IEnumerable<User> GetBy(DataAccessAdapter adapter, string nameOrEmail)
        {
            var metaData = new LinqMetaData(adapter);
            var users = metaData.User
                .Where(u => u.Name == nameOrEmail || u.Email == nameOrEmail)
                .ToList();

            return users.Select(u => u.ToModel()); ;
        }
    }
}
