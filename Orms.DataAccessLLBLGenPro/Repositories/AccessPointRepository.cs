﻿using System.Collections.Generic;
using System.Linq;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.DataAccessLLBLGenPro.Converter;
using Orms.DataAccessLLBLGenPro.DatabaseSpecific;
using Orms.DataAccessLLBLGenPro.EntityClasses;
using Orms.DataAccessLLBLGenPro.HelperClasses;
using Orms.DataAccessLLBLGenPro.Linq;
using Orms.Models;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Orms.DataAccessLLBLGenPro.Repositories
{
    public class AccessPointRepository : IAccessPointRepository<DataAccessAdapter>
    {
        public void Add(DataAccessAdapter adapter, AccessPoint ap)
        {
            var apEntity = ap.ToEntity();

            var prefetchPath = new PrefetchPath2(EntityType.AccessPointEntity);
            var gasStorages = new EntityCollection<GasStorageEntity>();
            prefetchPath.Add(AccessPointEntity.PrefetchPathAccessPointFlows);
            RelationPredicateBucket filter = new RelationPredicateBucket(GasStorageFields.Id == apEntity.GasStorageId);
            adapter.FetchEntityCollection(gasStorages, filter, prefetchPath);
            //gasStorage.FirstOrDefault().AccessPoints.Add(apEntity);

            apEntity.GasStorage = gasStorages.First();
            apEntity.GasStorage.AccessPoints.Add(apEntity);

            adapter.SaveEntity(apEntity);
        }

        public void Delete(DataAccessAdapter adapter, AccessPoint ap)
        {
            adapter.DeleteEntity(ap.ToEntity());
        }

        public AccessPoint Get(DataAccessAdapter adapter, int id)
        {
            var prefetchPath = new PrefetchPath2(EntityType.AccessPointEntity);
            var accessPoints = new EntityCollection<AccessPointEntity>();
            prefetchPath.Add(AccessPointEntity.PrefetchPathAccessPointFlows);
            prefetchPath.Add(AccessPointEntity.PrefetchPathGasStorage);
            RelationPredicateBucket filter = new RelationPredicateBucket(AccessPointFields.Id == id);
            adapter.FetchEntityCollection(accessPoints, filter, prefetchPath);

            var accessPoint = accessPoints.FirstOrDefault()?.ToModel();

            return accessPoint;
        }

        public IEnumerable<AccessPoint> GetAll(DataAccessAdapter adapter)
        {
            var metaData = new LinqMetaData(adapter);
            var accessPoints = metaData.AccessPoint.Select(ap => ap.ToModel());

            return accessPoints;
        }

        public void Update(DataAccessAdapter adapter, AccessPoint ap)
        {
            var entity = ap.ToEntity();
            adapter.FetchEntity(entity);
            adapter.SaveEntity(entity);
        }

        public IEnumerable<AccessPoint> GetBy(DataAccessAdapter adapter, GasStorage gasStorage)
        {
            //var aps = adapter.FetchQuery<GasStorageEntity>(new EntityQuery<GasAccountEntity>().);

            /*
            var results = metaData.GasStorage
                .Join(metaData.AccessPoint, g => g.Id, ap => ap.GasStorageId, (g, ap) => new { g, ap })
                .Join(metaData.AccessPointFlow, x => x.ap.Id, apf => apf.AccessPointId, (x, apf) => new { x, apf })
                .Where(@t => @t.x.ap.GasStorageId == gasStorage.Id)
                .Select(@t => new { Gs = @t.x.g, Aps = @t.x.ap, Apfs = @t.apf });
            */
            /*
            var results = metaData.AccessPoint
                .Where(@t => @t.GasStorageId == gasStorage.Id)
                .OrderBy(@t => @t.Id);

            //var gs = results.Select(r => r.Gs.ToModel()).Distinct().ToList();
            var aps = results.ToList();
            //var apfs = results.Select(r => r.Apfs).Distinct().ToList();

            var apIds = aps.Select(ap => ap.Id);

            var results2 = metaData.AccessPointFlow
                .Where(@t => apIds.Any(id => id == @t.Id));


            var apfs = results2.ToList();

            return Convert(new List<GasStorage>() { gasStorage }, aps, apfs);
            */

            var prefetchPath = new PrefetchPath2(EntityType.AccessPointEntity);
            var accessPoints = new EntityCollection<AccessPointEntity>();
            prefetchPath.Add(AccessPointEntity.PrefetchPathAccessPointFlows);
            prefetchPath.Add(AccessPointEntity.PrefetchPathGasStorage);
            RelationPredicateBucket filter = new RelationPredicateBucket(AccessPointFields.GasStorageId == gasStorage.Id);
            adapter.FetchEntityCollection(accessPoints, filter, prefetchPath);

            return accessPoints.ToList().Select(ap => ap.ToModel());
        }

    }
}
