﻿using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.DataAccessLLBLGenPro.Converter;
using Orms.DataAccessLLBLGenPro.DatabaseSpecific;
using Orms.DataAccessLLBLGenPro.Linq;
using Orms.Models;
using System.Collections.Generic;
using System.Linq;

namespace Orms.DataAccessLLBLGenPro.Repositories
{
    public class ContractorRepository : IContractorRepository<DataAccessAdapter>
    {
        public void Add(DataAccessAdapter adapter, Contractor contractor)
        {
            adapter.SaveEntity(contractor.ToEntity());
        }

        public void Delete(DataAccessAdapter adapter, Contractor contractor)
        {
            adapter.DeleteEntity(contractor.ToEntity());
        }

        public Contractor Get(DataAccessAdapter adapter, int id)
        {
            var metaData = new LinqMetaData(adapter);
            var contractor = metaData.Contractor.FirstOrDefault(c => c.Id == id);

            return contractor.ToModel();
        }

        public IEnumerable<Contractor> GetAll(DataAccessAdapter adapter)
        {
            var metaData = new LinqMetaData(adapter);
            var contractors = metaData.Contractor.ToList().Select(c => c.ToModel());

            return contractors;
        }

        public void Update(DataAccessAdapter adapter, Contractor contractor)
        {
            var entity = contractor.ToEntity();
            adapter.FetchEntity(entity);
            adapter.SaveEntity(entity);
        }
    }
}