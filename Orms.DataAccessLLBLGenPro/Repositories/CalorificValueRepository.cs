﻿using System;
using System.Collections.Generic;
using System.Linq;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.DataAccessLLBLGenPro.Converter;
using Orms.DataAccessLLBLGenPro.DatabaseSpecific;
using Orms.DataAccessLLBLGenPro.EntityClasses;
using Orms.DataAccessLLBLGenPro.HelperClasses;
using Orms.DataAccessLLBLGenPro.Linq;
using Orms.Models;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Orms.DataAccessLLBLGenPro.Repositories
{
    public class CalorificValueRepository : ICalorificValueRepository<DataAccessAdapter>
    {
        public IEnumerable<CalorificValue> GetAll(DataAccessAdapter adapter)
        {
            var metaData = new LinqMetaData(adapter);
            var cvs = metaData.CalorificValue.Select(a => a.ToModel());

            return cvs;
        }

        public CalorificValue Get(DataAccessAdapter adapter, int id)
        {
            var metaData = new LinqMetaData(adapter);
            var cvs = metaData.CalorificValue
                .FirstOrDefault(c => c.Id == id)
                ?.ToModel();

            return cvs;
        }

        public void Update(DataAccessAdapter adapter, CalorificValue cv)
        {
            var entity = cv.ToEntity();
            adapter.FetchEntity(entity);
            adapter.SaveEntity(entity);
        }

        public void Add(DataAccessAdapter adapter, CalorificValue cv)
        {
            var calorificValue = cv.ToEntity();
            calorificValue.CalorificValueValues.AddRange(cv.Values.Select(ecv => ecv.ToEntity()));

            adapter.SaveEntity(calorificValue);
        }

        public IEnumerable<CalorificValue> GetBy(DataAccessAdapter adapter, GasStorage gasStorage)
        {
            var metaData = new LinqMetaData(adapter);
            var cvs = metaData.CalorificValue
                .Where(c => c.GasStorageId == gasStorage.Id)
                .Select(c => c.ToModel());

            return cvs;
        }

        public CalorificValue GetLastBy(DataAccessAdapter adapter, GasStorage gasStorage)
        {
            var metaData = new LinqMetaData(adapter);
            var cvs = metaData.CalorificValue
                .Where(c => c.GasStorageId == gasStorage.Id)
                .OrderByDescending(c => c.Id)
                .FirstOrDefault()
                ?.ToModel();

            return cvs;
        }

        public CalorificValue GetForGasDay(DataAccessAdapter adapter, GasStorage gasStorage, DateTime gasDay)
        {
            var prefetchPath = new PrefetchPath2(EntityType.CalorificValueEntity);
            var cvs = new EntityCollection<CalorificValueEntity>();
            prefetchPath.Add(CalorificValueEntity.PrefetchPathGasStorage);
            prefetchPath.Add(CalorificValueEntity.PrefetchPathCalorificValueValues);
            var sorter = new SortExpression(CalorificValueFields.Id | SortOperator.Descending);
            var filter = new RelationPredicateBucket();
            filter.PredicateExpression.Add(CalorificValueFields.GasDay == gasDay);
            filter.PredicateExpression.Add(CalorificValueFields.GasStorageId == gasStorage.Id);
            adapter.FetchEntityCollection(cvs, filter, 1, sorter, prefetchPath);

            return cvs.FirstOrDefault()?.ToModel();
        }
    }
}