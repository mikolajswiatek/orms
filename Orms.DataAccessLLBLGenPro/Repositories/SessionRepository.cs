﻿using System;
using System.Collections.Generic;
using System.Linq;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.DataAccessLLBLGenPro.Converter;
using Orms.DataAccessLLBLGenPro.DatabaseSpecific;
using Orms.DataAccessLLBLGenPro.EntityClasses;
using Orms.DataAccessLLBLGenPro.HelperClasses;
using Orms.DataAccessLLBLGenPro.Linq;
using Orms.Models;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Orms.DataAccessLLBLGenPro.Repositories
{
    public class SessionRepository : ISessionRepository<DataAccessAdapter>
    {
        public void Add(DataAccessAdapter adapter, Session session)
        {
            adapter.SaveEntity(session.ToEntity());
        }

        public Session Get(DataAccessAdapter adapter, int id)
        {
            var metaData = new LinqMetaData(adapter);
            var session = metaData.Session
                .FirstOrDefault(s => s.Id == id)
                ?.ToModel();

            return session;
        }

        public IEnumerable<Session> GetAll(DataAccessAdapter adapter)
        {
            var metaData = new LinqMetaData(adapter);
            var sessions = metaData.Session
                .ToList()
                .Select(s => s.ToModel());

            return sessions;
        }

        public void Update(DataAccessAdapter adapter, Session session)
        {
            var prefetchPath = new PrefetchPath2(EntityType.SessionEntity);
            var sessions = new EntityCollection<SessionEntity>();
            prefetchPath.Add(SessionEntity.PrefetchPathUser);
            RelationPredicateBucket filter = new RelationPredicateBucket(SessionFields.Guid == session.Guid);
            adapter.FetchEntityCollection(sessions, filter, prefetchPath);

            var metaData = new LinqMetaData(adapter);
            var contractor = metaData.Contractor
                .FirstOrDefault(c => c.Id == session.User.Contractor.Id);

            var s = sessions.FirstOrDefault();
            s.User.Contractor = contractor;
            s.User.LastActivityDatetime = session.User.LastActivityDateTime;
            s.LastActivityDatetime = session.LastActivityDatetime;

            adapter.SaveEntity(s);
        }

        public Session GetBy(DataAccessAdapter adapter, string guid)
        {
            var prefetchPath = new PrefetchPath2(EntityType.SessionEntity);
            var sessions = new EntityCollection<SessionEntity>();
            prefetchPath.Add(SessionEntity.PrefetchPathUser);
            RelationPredicateBucket filter = new RelationPredicateBucket(SessionFields.Guid == guid);
            adapter.FetchEntityCollection(sessions, filter, prefetchPath);

            var session = sessions.FirstOrDefault();

            var metaData = new LinqMetaData(adapter);
            var contractor = metaData.Contractor
                .FirstOrDefault(c => c.Id == session.User.ContractorId);

            session.User.Contractor = contractor;

            return session?.ToModel();
        }

        public void Delete(DataAccessAdapter adapter, Session session)
        {
            var metaData = new LinqMetaData(adapter);
            var queryResult = metaData.Session
                .FirstOrDefault(s => s.Guid == session.Guid);

            adapter.DeleteEntity(queryResult);
        }
    }
}
