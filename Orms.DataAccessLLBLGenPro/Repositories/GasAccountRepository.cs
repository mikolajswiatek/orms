﻿using System;
using System.Collections.Generic;
using System.Linq;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.DataAccessLLBLGenPro.Converter;
using Orms.DataAccessLLBLGenPro.DatabaseSpecific;
using Orms.DataAccessLLBLGenPro.EntityClasses;
using Orms.DataAccessLLBLGenPro.HelperClasses;
using Orms.DataAccessLLBLGenPro.Linq;
using Orms.Models;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Orms.DataAccessLLBLGenPro.Repositories
{
    public class GasAccountRepository : IGasAccountRepository<DataAccessAdapter>
    {
        public void Add(DataAccessAdapter adapter, GasAccount gasAccount)
        {
            var ga = gasAccount.ToEntity();
            adapter.SaveEntity(ga);
        }

        public void Delete(DataAccessAdapter adapter, GasAccount gasAccount)
        {
            adapter.DeleteEntity(gasAccount.ToEntity());
        }

        public GasAccount Get(DataAccessAdapter adapter, int id)
        {
            var metaData = new LinqMetaData(adapter);
            var gasAcount = metaData.GasAccount
                .FirstOrDefault(ga => ga.Id == id)
                ?.ToModel();

            return gasAcount;
        }

        public IEnumerable<GasAccount> GetAll(DataAccessAdapter adapter)
        {
            var metaData = new LinqMetaData(adapter);
            var gasAcounts = metaData.GasAccount
                .Select(ga => ga.ToModel());

            return gasAcounts;
        }

        public void Update(DataAccessAdapter adapter, GasAccount gasAccount)
        {
            var entity = gasAccount.ToEntity();
            adapter.SaveEntity(entity);
        }

        public void Add(DataAccessAdapter adapter, IEnumerable<GasAccount> gasAccounts)
        {
            foreach (var gasAccount in gasAccounts)
            {
                Add(adapter, gasAccount);
            }
        }

        public void Update(DataAccessAdapter adapter, IEnumerable<GasAccount> gasAccounts)
        {
            var gasDay = gasAccounts.First().GasDay;
            var gasStorageId = gasAccounts.First().GasStorage.Id;

            var prefetchPath = new PrefetchPath2(EntityType.GasAccountEntity);
            var gasAccountEntities = new EntityCollection<GasAccountEntity>();
            prefetchPath.Add(GasAccountEntity.PrefetchPathGasStorage);
            var filter = new RelationPredicateBucket();
            filter.PredicateExpression.Add(GasAccountFields.GasDay == gasDay);
            filter.PredicateExpression.Add(GasAccountFields.GasStorageId == gasStorageId);
            adapter.FetchEntityCollection(gasAccountEntities, filter, prefetchPath);

            var gaes = gasAccountEntities.ToList();
            gasAccountEntities.Dispose();

            foreach (var gasAccount in gasAccounts)
            {
                var gasAccountEntity = gaes.First(ga => ga.Id == gasAccount.Id);
                gasAccountEntity.EnergyValue = gasAccount.Energy.Value;
                gasAccountEntity.VolumeValue = gasAccount.Volume.Value;

                adapter.SaveEntity(gasAccountEntity);
            }
        }

        public IEnumerable<GasAccount> Get(DataAccessAdapter adapter, GasStorage gasStorage, DateTime gasDay)
        {
            var prefetchPath = new PrefetchPath2(EntityType.GasAccountEntity);
            var gasAccounts = new EntityCollection<GasAccountEntity>();
            prefetchPath.Add(GasAccountEntity.PrefetchPathGasStorage);
            var filter = new RelationPredicateBucket();
            filter.PredicateExpression.Add(GasAccountFields.GasDay == gasDay);
            filter.PredicateExpression.Add(GasAccountFields.GasStorageId == gasStorage.Id);
            adapter.FetchEntityCollection(gasAccounts, filter, prefetchPath);

            return gasAccounts.ToList().Select(ga => ga.ToModel());
        }

        public IEnumerable<GasAccount> Get(DataAccessAdapter adapter, DateTime gasDay)
        {
            var metaData = new LinqMetaData(adapter);
            var gasAcounts = metaData.GasAccount
                .Where(ga => ga.GasDay == gasDay)
                .Select(ga => ga.ToModel());

            return gasAcounts;
        }
    }
}
