﻿using System.Collections.Generic;
using System.Linq;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.DataAccessLLBLGenPro.Converter;
using Orms.DataAccessLLBLGenPro.DatabaseSpecific;
using Orms.DataAccessLLBLGenPro.EntityClasses;
using Orms.DataAccessLLBLGenPro.HelperClasses;
using Orms.DataAccessLLBLGenPro.Linq;
using Orms.Models;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Orms.DataAccessLLBLGenPro.Repositories
{
    public class AgreementRepository : IAgreementRepository<DataAccessAdapter>
    {
        public void Add(DataAccessAdapter adapter, Agreement agreement)
        {
            var agreementEntity = agreement.ToEntity();
            var metaData = new LinqMetaData(adapter);

            var contractorEntity = metaData.Contractor
                .Where(a => a.Id == agreement.Contractor.Id)
                .FirstOrDefault();

            var gasStorageEntity = metaData.GasStorage
                .Where(a => a.Id == agreement.GasStorage.Id)
                .FirstOrDefault();

            agreementEntity.GasStorage = gasStorageEntity;
            agreementEntity.Contractor = contractorEntity;

            adapter.SaveEntity(agreementEntity);
        }

        public Agreement Get(DataAccessAdapter adapter, int id)
        {
            var prefetchPath = new PrefetchPath2(EntityType.AgreementEntity);
            var agreements = new EntityCollection<AgreementEntity>();
            prefetchPath.Add(AgreementEntity.PrefetchPathContractor);
            prefetchPath.Add(AgreementEntity.PrefetchPathGasStorage);
            RelationPredicateBucket filter = new RelationPredicateBucket(AgreementFields.Id == id);
            adapter.FetchEntityCollection(agreements, filter, prefetchPath);

            return agreements.FirstOrDefault()?.ToModel();
        }

        public IEnumerable<Agreement> GetAll(DataAccessAdapter adapter)
        {
            var prefetchPath = new PrefetchPath2(EntityType.AgreementEntity);
            var agreements = new EntityCollection<AgreementEntity>();
            prefetchPath.Add(AgreementEntity.PrefetchPathContractor);
            prefetchPath.Add(AgreementEntity.PrefetchPathGasStorage);
            RelationPredicateBucket filter = new RelationPredicateBucket();
            adapter.FetchEntityCollection(agreements, filter, prefetchPath);

            return agreements.Select(a => a.ToModel());
        }

        public void Update(DataAccessAdapter adapter, Agreement agreement)
        {
            var entity = agreement.ToEntity();
            adapter.FetchEntity(entity);
            adapter.SaveEntity(entity);
        }

        public IEnumerable<Agreement> GetBy(DataAccessAdapter adapter, GasStorage gasStorage)
        {
            var metaData = new LinqMetaData(adapter);
            var agreements = metaData.Agreement
                .Where(a => a.GasStorageId == gasStorage.Id)
                .ToList();

            return agreements.Select(a => a.ToModel());
        }

        public IEnumerable<Agreement> GetBy(DataAccessAdapter adapter, Contractor contractor)
        {
            var prefetchPath = new PrefetchPath2(EntityType.AgreementEntity);
            var agreements = new EntityCollection<AgreementEntity>();
            prefetchPath.Add(AgreementEntity.PrefetchPathContractor);
            prefetchPath.Add(AgreementEntity.PrefetchPathGasStorage);
            RelationPredicateBucket filter = new RelationPredicateBucket(AgreementFields.ContractorId == contractor.Id);
            adapter.FetchEntityCollection(agreements, filter, prefetchPath);

            return agreements.Select(a => a.ToModel());
        }
    }
}
