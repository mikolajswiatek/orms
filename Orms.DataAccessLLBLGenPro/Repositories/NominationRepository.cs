﻿using System;
using System.Collections.Generic;
using System.Linq;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.DataAccessLLBLGenPro.Converter;
using Orms.DataAccessLLBLGenPro.DatabaseSpecific;
using Orms.DataAccessLLBLGenPro.EntityClasses;
using Orms.DataAccessLLBLGenPro.HelperClasses;
using Orms.DataAccessLLBLGenPro.Linq;
using Orms.Models;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Orms.DataAccessLLBLGenPro.Repositories
{
    public class NominationRepository : INominationRepository<DataAccessAdapter>
    {
        public void Add(DataAccessAdapter adapter, Nomination nomination)
        {
            var entity = nomination.ToEntity();
            entity.NominationValues.AddRange(nomination.Values.Select(nv => nv.ToEntity()));
            adapter.SaveEntity(entity);
        }

        public void Delete(DataAccessAdapter adapter, Nomination nomination)
        {
            adapter.SaveEntity(nomination.ToEntity());
        }

        public Nomination Get(DataAccessAdapter adapter, int id)
        {
            var prefetchPath = new PrefetchPath2(EntityType.NominationEntity);
            var nominations = new EntityCollection<NominationEntity>();
            prefetchPath.Add(NominationEntity.PrefetchPathUser)
                .SubPath.Add(UserEntity.PrefetchPathContractor);
            prefetchPath.Add(NominationEntity.PrefetchPathAccessPointFlow)
                .SubPath.Add(AccessPointFlowEntity.PrefetchPathAccessPoint)
                .SubPath.Add(AccessPointEntity.PrefetchPathGasStorage);
            prefetchPath.Add(NominationEntity.PrefetchPathCalorificValue);
            prefetchPath.Add(NominationEntity.PrefetchPathContractor);
            prefetchPath.Add(NominationEntity.PrefetchPathNominationValues);
            prefetchPath.Add(NominationEntity.PrefetchPathNomination);
            var filter = new RelationPredicateBucket();
            filter.PredicateExpression.Add(NominationFields.Id == id);
            adapter.FetchEntityCollection(nominations, filter, 1, null, prefetchPath);

            var nomination = nominations.FirstOrDefault()?.ToModel();

            return nomination;
        }

        public IEnumerable<Nomination> GetAll(DataAccessAdapter adapter)
        {
            var metaData = new LinqMetaData(adapter);
            var nominations = metaData.Nomination
                .Select(n => n.ToModel());

            return nominations;
        }

        public void Update(DataAccessAdapter adapter, Nomination nomination)
        {
            var prefetchPath = new PrefetchPath2(EntityType.NominationEntity);
            var nominations = new EntityCollection<NominationEntity>();
            prefetchPath.Add(NominationEntity.PrefetchPathUser)
                .SubPath.Add(UserEntity.PrefetchPathContractor);
            prefetchPath.Add(NominationEntity.PrefetchPathAccessPointFlow)
                .SubPath.Add(AccessPointFlowEntity.PrefetchPathAccessPoint)
                .SubPath.Add(AccessPointEntity.PrefetchPathGasStorage);
            prefetchPath.Add(NominationEntity.PrefetchPathCalorificValue);
            prefetchPath.Add(NominationEntity.PrefetchPathContractor);
            prefetchPath.Add(NominationEntity.PrefetchPathNominationValues);
            prefetchPath.Add(NominationEntity.PrefetchPathNomination);
            var filter = new RelationPredicateBucket();
            filter.PredicateExpression.Add(NominationFields.Id == nomination.Id);
            adapter.FetchEntityCollection(nominations, filter, 1, null, prefetchPath);

            var entity = nominations.FirstOrDefault();
            entity.IsAccepted = nomination.IsAccepted;
            entity.IsLast = nomination.IsLast;
            adapter.SaveEntity(entity);
        }

        public IEnumerable<Nomination> GetBy(DataAccessAdapter adapter, Contractor contractor)
        {
            var metaData = new LinqMetaData(adapter);
            var nominations = metaData.Nomination
                .Where(n => n.ContractorId == contractor.Id)
                .Select(n => n.ToModel());

            return nominations;
        }

        public IEnumerable<Nomination> GetBy(DataAccessAdapter adapter, GasStorage gasStorage)
        {
            var apfIds = new List<int>();

            foreach (var accessPoint in gasStorage.AccessPoints)
            {
                foreach (var accessPointFlow in accessPoint.AccessPointFlows)
                {
                    apfIds.Add(accessPointFlow.Id);
                }
            }

            var metaData = new LinqMetaData(adapter);
            var nominations = metaData.Nomination
                .Where(n => apfIds.Contains(n.AccessPointFlowId))
                .Select(n => n.ToModel());

            return nominations;
        }

        public IEnumerable<Nomination> GetBy(DataAccessAdapter adapter, User user)
        {
            var metaData = new LinqMetaData(adapter);
            var nominations = metaData.Nomination
                .Where(n => n.UserId == user.Id)
                .Select(n => n.ToModel());

            return nominations;
        }

        public IEnumerable<Nomination> GetBy(DataAccessAdapter adapter, DateTime gasDay)
        {
            var prefetchPath = new PrefetchPath2(EntityType.NominationEntity);
            var nominations = new EntityCollection<NominationEntity>();
            prefetchPath.Add(NominationEntity.PrefetchPathUser)
                .SubPath.Add(UserEntity.PrefetchPathContractor);
            prefetchPath.Add(NominationEntity.PrefetchPathAccessPointFlow)
                .SubPath.Add(AccessPointFlowEntity.PrefetchPathAccessPoint)
                    .SubPath.Add(AccessPointEntity.PrefetchPathGasStorage);
            prefetchPath.Add(NominationEntity.PrefetchPathCalorificValue);
            prefetchPath.Add(NominationEntity.PrefetchPathContractor);
            prefetchPath.Add(NominationEntity.PrefetchPathNominationValues);
            prefetchPath.Add(NominationEntity.PrefetchPathNomination);
            var filter = new RelationPredicateBucket();
            filter.PredicateExpression.Add(NominationFields.GasDay == gasDay);
            adapter.FetchEntityCollection(nominations, filter, prefetchPath);

            var ns = nominations.ToList();
            nominations.Dispose();

            var results = ns.Select(n => n.ToModel());

            return results;
        }

        public Nomination GetLast(DataAccessAdapter adapter, Contractor contractor)
        {
            var metaData = new LinqMetaData(adapter);
            var nominations = metaData.Nomination
                .Where(n => n.ContractorId == contractor.Id && n.IsAccepted)
                .OrderByDescending(n => n.Id)
                .FirstOrDefault()
                ?.ToModel();

            return nominations;
        }

        public Nomination GetLast(DataAccessAdapter adapter, GasStorage gasStorage)
        {
            var apfIds = new List<int>();

            foreach (var accessPoint in gasStorage.AccessPoints)
            {
                foreach (var accessPointFlow in accessPoint.AccessPointFlows)
                {
                    apfIds.Add(accessPointFlow.Id);
                }
            }

            var metaData = new LinqMetaData(adapter);
            var nominations = metaData.Nomination
                .Where(n => apfIds.Contains(n.AccessPointFlowId))
                .OrderByDescending(n => n.Id)
                .FirstOrDefault()
                ?.ToModel();

            return nominations;
        }

        public Nomination GetLast(DataAccessAdapter adapter, User user)
        {
            var metaData = new LinqMetaData(adapter);
            var nominations = metaData.Nomination
                .Where(n => n.UserId == user.Id && n.IsAccepted)
                .OrderByDescending(n => n.Id)
                .FirstOrDefault()
                ?.ToModel();

            return nominations;
        }

        public Nomination GetLast(DataAccessAdapter adapter, Contractor contractor, DateTime gasDay)
        {
            var metaData = new LinqMetaData(adapter);
            var nominations = metaData.Nomination
                .Where(n => n.ContractorId == contractor.Id && n.IsAccepted && n.GasDay == gasDay)
                .OrderByDescending(n => n.Id)
                .FirstOrDefault()
                ?.ToModel();

            return nominations;
        }

        public IEnumerable<Nomination> GetForGasDay(DataAccessAdapter adapter, Contractor contractor, DateTime gasDay)
        {
            var metaData = new LinqMetaData(adapter);
            var nominations = metaData.Nomination
                .Where(n => n.ContractorId == contractor.Id && n.IsAccepted && n.GasDay == gasDay)
                .Select(n => n.ToModel());

            return nominations;
        }

        public IEnumerable<Nomination> GetForGasDay(DataAccessAdapter adapter, GasStorage gasStorage, DateTime gasDay)
        {
            var apfIds = new List<int>();

            foreach (var accessPoint in gasStorage.AccessPoints)
            {
                foreach (var accessPointFlow in accessPoint.AccessPointFlows)
                {
                    apfIds.Add(accessPointFlow.Id);
                }
            }

            var metaData = new LinqMetaData(adapter);
            var nominations = metaData.Nomination
                .Where(n => apfIds.Contains(n.AccessPointFlowId) && n.IsAccepted && n.GasDay == gasDay)
                .Select(n => n.ToModel());

            return nominations;
        }

        public IEnumerable<Nomination> GetForGasDay(DataAccessAdapter adapter, Contractor contractor, GasStorage gasStorage, DateTime gasDay)
        {
            var prefetchPath = new PrefetchPath2(EntityType.NominationEntity);
            var nominations = new EntityCollection<NominationEntity>();
            prefetchPath.Add(NominationEntity.PrefetchPathUser)
                .SubPath.Add(UserEntity.PrefetchPathContractor);
            prefetchPath.Add(NominationEntity.PrefetchPathAccessPointFlow)
                .SubPath.Add(AccessPointFlowEntity.PrefetchPathAccessPoint)
                .SubPath.Add(AccessPointEntity.PrefetchPathGasStorage);
            prefetchPath.Add(NominationEntity.PrefetchPathCalorificValue);
            prefetchPath.Add(NominationEntity.PrefetchPathContractor);
            prefetchPath.Add(NominationEntity.PrefetchPathNominationValues);
            prefetchPath.Add(NominationEntity.PrefetchPathNomination);
            var filter = new RelationPredicateBucket();
            filter.PredicateExpression.Add(NominationFields.ContractorId == contractor.Id);

            foreach (var accessPoint in gasStorage.AccessPoints)
            {
                foreach (var accessPointFlow in accessPoint.AccessPointFlows)
                {
                    filter.PredicateExpression.Add(NominationFields.AccessPointFlowId == accessPointFlow.Id);
                }
            }

            filter.PredicateExpression.Add(NominationFields.GasDay == gasDay);
            adapter.FetchEntityCollection(nominations, filter, prefetchPath);

            var ns = nominations.ToList();
            nominations.Dispose();

            var results = ns.Select(n => n.ToModel());

            return results;
        }
    }
}
