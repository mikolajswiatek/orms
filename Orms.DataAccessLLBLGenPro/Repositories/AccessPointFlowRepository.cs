﻿using System.Collections.Generic;
using System.Linq;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.DataAccessLLBLGenPro.Converter;
using Orms.DataAccessLLBLGenPro.DatabaseSpecific;
using Orms.DataAccessLLBLGenPro.Linq;
using Orms.Models;

namespace Orms.DataAccessLLBLGenPro.Repositories
{
    public class AccessPointFlowRepository : IAccessPointFlowRepository<DataAccessAdapter>
    {
        public IEnumerable<AccessPointFlow> GetAll(DataAccessAdapter adapter)
        {
            var metaData = new LinqMetaData(adapter);
            var apfs = metaData.AccessPointFlow.Select(apf => apf.ToModel());

            return apfs;
        }

        public AccessPointFlow Get(DataAccessAdapter adapter, int id)
        {
            var metaData = new LinqMetaData(adapter);
            var accessPointFlow = metaData.AccessPointFlow
                .FirstOrDefault(apf => apf.Id == id)
                ?.ToModel();

            return accessPointFlow;
        }

        public void Update(DataAccessAdapter adapter, AccessPointFlow apf)
        {
            var entity = apf.ToEntity();
            adapter.FetchEntity(entity);
            adapter.SaveEntity(entity);
        }

        public void Add(DataAccessAdapter adapter, AccessPointFlow apf)
        {
            adapter.SaveEntity(apf.ToEntity());
        }

        public void Delete(DataAccessAdapter adapter, AccessPointFlow apf)
        {
            adapter.DeleteEntity(apf.ToEntity());
        }
    }
}
