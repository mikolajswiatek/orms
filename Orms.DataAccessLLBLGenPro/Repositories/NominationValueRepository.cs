﻿using System;
using System.Collections.Generic;
using System.Linq;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.DataAccessLLBLGenPro.Converter;
using Orms.DataAccessLLBLGenPro.DatabaseSpecific;
using Orms.DataAccessLLBLGenPro.Linq;
using Orms.Models;

namespace Orms.DataAccessLLBLGenPro.Repositories
{
    public class NominationValueRepository : INominationValueRepository<DataAccessAdapter>
    {
        public void Add(DataAccessAdapter adapter, NominationValue nominationValue)
        {
            adapter.SaveEntity(nominationValue.ToEntity());
        }

        public void Delete(DataAccessAdapter adapter, NominationValue nominationValue)
        {
            adapter.DeleteEntity(nominationValue.ToEntity());
        }

        public NominationValue Get(DataAccessAdapter adapter, int id)
        {
            var metaData = new LinqMetaData(adapter);
            var nominationValue = metaData.NominationValue
                .FirstOrDefault(nv => nv.Id == id)
                ?.ToModel();

            return nominationValue;
        }

        public IEnumerable<NominationValue> GetAll(DataAccessAdapter adapter)
        {
            var metaData = new LinqMetaData(adapter);
            var nominationValues = metaData.NominationValue
                .Select(nv => nv.ToModel());

            return nominationValues;
        }

        public void Update(DataAccessAdapter adapter, NominationValue nominationValue)
        {
            var entity = nominationValue.ToEntity();
            adapter.FetchEntity(entity);
            adapter.SaveEntity(entity);
        }

        public void Add(DataAccessAdapter adapter, IEnumerable<NominationValue> values)
        {
            foreach (var value in values)
            {
                adapter.SaveEntity(value.ToEntity());
            }
        }

        public IEnumerable<NominationValue> GetBy(DataAccessAdapter adapter, Nomination nomination)
        {
            var metaData = new LinqMetaData(adapter);
            var nominationValues = metaData.NominationValue
                .Where(nv => nv.NominationId == nomination.Id)
                .Select(nv => nv.ToModel());

            return nominationValues;
        }
    }
}
