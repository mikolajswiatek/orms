﻿using System;
using Orms.Domains.Units;

namespace Orms.Models
{
    public class GasAccount : Entity
    {
        public virtual UnitValue Energy { get; set; }

        public virtual UnitValue Volume { get; set; }

        public virtual DateTime GasDay { get; set; }

        public virtual int HourIndex { get; set; }

        public virtual GasStorage GasStorage { get; set; }

        public virtual int? GasStorageId { get; set; }
    }
}