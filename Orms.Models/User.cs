﻿using System;

namespace Orms.Models
{
    public class User : Entity
    {
        public virtual string Name
        {
            get;
            set;
        }

        public virtual string Password
        {
            get;
            set;
        }

        public virtual string Email
        {
            get;
            set;
        }

        public virtual bool IsActive
        {
            get;
            set;
        }

        public virtual DateTime CreateDateTime
        {
            get;
            set;
        }

        public virtual DateTime LastActivityDateTime
        {
            get;
            set;
        }

        public virtual Contractor Contractor
        {
            get;
            set;
        }

        public virtual int? ContractorId
        {
            get;
            set;
        }
    }
}
