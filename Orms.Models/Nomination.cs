﻿using Orms.Domains.Enums;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Orms.Models.Extensions;

namespace Orms.Models
{
    public class Nomination : Entity
    {
        public virtual string Name { get; set; }

        public virtual DateTime GasDay { get; set; }

        public virtual Contractor Contractor { get; set; }

        public virtual int? ContractorId { get; set; }

        public virtual AccessPointFlow AccessPointFlow { get; set; }

        public virtual int? AccessPointFlowId { get; set; }

        public virtual DateTime CreateDateTime { get; set; }

        public virtual bool IsLast { get; set; }

        public virtual bool IsAccepted { get; set; }

        public virtual Nomination ParentNomination { get; set; }

        public virtual int? ParentNominationId { get; set; }

        public virtual IEnumerable<NominationValue> Values { get; set; }

        public virtual ICollection<NominationValue> CollectionOfNominationValues { get; set; }

        public virtual User Creator { get; set; }

        public virtual int? UserId { get; set; }

        public virtual Channel Channel { get; set; }

        public virtual string ChannelString
        {
            get { return Channel.ToString(); }
            set { Channel = EnumHelper.ParseEnum<Channel>(value); }
        }

        public virtual int MajorVersion { get; set; }

        public virtual int Version { get; set; }

        public virtual string FullVersion
        {
            get { return $"{MajorVersion}.{Version}"; }
        }

        public virtual string Comment { get; set; }

        public virtual CalorificValue CalorificValue { get; set; }

        public virtual int? CalorificValueId { get; set; }

        public Nomination()
        {
            this.MajorVersion = 1;
            this.Version = 0;
        }
    }
}