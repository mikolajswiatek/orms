﻿using System;
using System.Collections.Generic;

namespace Orms.Models
{
    public class AccessPoint : Entity, ICloneable
    {
        public virtual string Code { get; set; }

        public virtual IEnumerable<AccessPointFlow> AccessPointFlows { get; set; }

        public virtual IList<AccessPointFlow> CollectionOfAccessPointFlows
        {
            get;
            set;
        }

        public virtual GasStorage GasStorage { get; set; }

        public virtual int? GasStorageId { get; set; }

        public virtual object Clone()
        {
            var entity = base.MemberwiseClone() as AccessPoint;

            return entity;
        }

        public AccessPoint()
        {
            AccessPointFlows = new List<AccessPointFlow>();
            CollectionOfAccessPointFlows = new List<AccessPointFlow>();
        }
    }
}