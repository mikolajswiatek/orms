﻿using System;
using Orms.Domains.Enums;
using Orms.Models.Extensions;

namespace Orms.Models
{
    public class Agreement : Entity
    {
        public virtual DateTime CreateDate { get; set; }

        public virtual DateTime Start { get; set; }

        public virtual GasDay GasDayStart => new GasDay(Start);

        public virtual DateTime End { get; set; }

        public virtual GasDay GasDayEnd => new GasDay(End);

        public virtual Contractor Contractor { get; set; }

        public virtual int? ContractorId { get; set; }

        public virtual GasStorage GasStorage { get; set; }

        public virtual int? GasStorageId { get; set; }

        public virtual AgreementStatus Status
        {
            get { return End.Status(); }
        }
    }
}