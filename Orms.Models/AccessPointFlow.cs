﻿using System;
using Orms.Domains.Enums;
using Orms.Models.Extensions;

namespace Orms.Models
{
    public class AccessPointFlow : Entity, ICloneable
    {
        public virtual DirectionType Direction { get; set; }

        public virtual string DirectionString
        {
            get { return Direction.ToString(); }
            set { Direction = EnumHelper.ParseEnum<DirectionType>(value);  }
        }

        public virtual AccessPoint AccessPoint { get; set; }

        public virtual int? AccessPointId { get; set; }

        public virtual object Clone()
        {
            var entity = base.MemberwiseClone() as AccessPointFlow;

            return entity;
        }
    }
}