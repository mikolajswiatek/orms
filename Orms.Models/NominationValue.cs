﻿using Orms.Domains.Units;

namespace Orms.Models
{
    public class NominationValue : Entity
    {
        public virtual UnitValue Energy { get; set; }

        public virtual UnitValue Volume { get; set; }

        public virtual int HourIndex { get; set; }

        public virtual Nomination Nomination { get; set; }

        public virtual int? NominationId { get; set; }
    }
}
