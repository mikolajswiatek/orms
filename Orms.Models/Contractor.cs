﻿namespace Orms.Models
{
    public class Contractor : Entity
    {
        public virtual string Name
        {
            get;
            set;
        }

        public virtual string Code
        {
            get;
            set;
        }

        public virtual string Adress
        {
            get;
            set;
        }
    }
}