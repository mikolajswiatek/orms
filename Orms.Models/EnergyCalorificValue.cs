﻿using Orms.Domains.Units;

namespace Orms.Models
{
    public class EnergyCalorificValue : Entity
    {
        public virtual UnitValue Energy { get; set; }

        public virtual int HourIndex { get; set; }

        public virtual CalorificValue CalorificValue { get; set; }

        public virtual int? CalorificValueId { get; set; }
    }
}