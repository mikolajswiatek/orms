﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Orms.Models
{
    public class GasStorage : Entity, ICloneable
    {
        public virtual string Name { get; set; }

        public virtual IEnumerable<AccessPoint> AccessPoints { get; set; }

        public virtual ICollection<AccessPoint> CollectionOfAccessPoints { get; set; }

        public GasStorage()
        {
            AccessPoints = new List<AccessPoint>();
        }

        public virtual object Clone()
        {
            var entity = base.MemberwiseClone() as GasStorage;

            return entity;
        }
    }
}