﻿namespace Orms.Models
{
    public class ContractorContact : Entity
    {
        public virtual string Name { get; set; }

        public virtual string FirstName { get; set; }

        public virtual string LastName { get; set; }

        public virtual string Adress { get; set; }

        public virtual string Email { get; set; }

        public virtual string Phone { get; set; }

        public virtual Contractor Contractor { get; set; }

        public virtual int? ContractorId { get; set; }
    }
}