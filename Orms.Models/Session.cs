﻿using System;

namespace Orms.Models
{
    public class Session : Entity
    {
        public virtual string Guid { get; set; }

        public virtual string Ip { get; set; }

        public virtual string Browser { get; set; }

        public virtual DateTime CreateDatetime { get; set; }

        public virtual DateTime LastActivityDatetime { get; set; }

        public virtual User User { get; set; }

        public virtual int? UserId { get; set; }
    }
}
