﻿using System;
using System.Collections.Generic;

namespace Orms.Models
{
    public class CalorificValue : Entity
    {
        public virtual DateTime GasDay { get; set; }

        public virtual GasStorage GasStorage { get; set; }

        public virtual int? GasStorageId { get; set; }

        public virtual IEnumerable<EnergyCalorificValue> Values { get; set; }

        public virtual ICollection<EnergyCalorificValue> CollectionOfValues { get; set; }

        public CalorificValue()
        {
            Values = new List<EnergyCalorificValue>();
            CollectionOfValues = new List<EnergyCalorificValue>();
        }
    }
}