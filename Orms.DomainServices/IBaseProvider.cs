﻿using System.Collections.Generic;

namespace Orms.Providers
{
    public interface IBaseProvider<M>
    {
        IEnumerable<M> GetAll();
        M Get(int id);
        void Update(M m);
        void Add(M m);
    }
}