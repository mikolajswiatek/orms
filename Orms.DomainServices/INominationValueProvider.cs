﻿using System.Collections.Generic;
using Orms.Models;

namespace Orms.Providers
{
    public interface INominationValueProvider : IBaseProvider<NominationValue>, IListBaseProvider<NominationValue>
    {
        IEnumerable<NominationValue> GetBy(Nomination nomination);
    }
}