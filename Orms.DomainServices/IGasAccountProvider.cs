﻿using Orms.Models;
using System;
using System.Collections.Generic;

namespace Orms.Providers
{
    public interface IGasAccountProvider : IBaseProvider<GasAccount>, IListBaseProvider<GasAccount>
    {
        void Update(IEnumerable<GasAccount> gasAccounts);

        IEnumerable<GasAccount> Get(DateTime gasDay);

        IEnumerable<GasAccount> Get(GasStorage gasStorage, DateTime gasDay);
    }
}