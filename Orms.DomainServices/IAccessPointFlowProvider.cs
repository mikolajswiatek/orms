﻿using Orms.Models;

namespace Orms.Providers
{
    public interface IAccessPointFlowProvider : IBaseProvider<AccessPointFlow>
    {

    }
}