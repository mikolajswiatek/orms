﻿using System.Collections.Generic;
using Orms.Models;

namespace Orms.Providers
{
    public interface IAgreementProvider : IBaseProvider<Agreement>
    {
        IEnumerable<Agreement> GetBy(Contractor contractor);
        IEnumerable<Agreement> GetBy(GasStorage gasStorage);
    }
}