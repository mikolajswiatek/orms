﻿using Orms.Models;

namespace Orms.Providers
{
    public interface IContractorProvider : IBaseProvider<Contractor>
    {
    }
}