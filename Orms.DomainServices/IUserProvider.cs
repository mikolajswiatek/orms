﻿using System.Collections.Generic;
using Orms.Models;

namespace Orms.Providers
{
    public interface IUserProvider : IBaseProvider<User>
    {
        IEnumerable<User> GetBy(Contractor contractor);

        IEnumerable<User> GetBy(string nameOrEmail);
    }
}