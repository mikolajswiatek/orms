﻿using System;
using Orms.Models;

namespace Orms.Providers
{
    public interface ICalorificValueProvider : IBaseProvider<CalorificValue>
    {
        CalorificValue GetLastBy(GasStorage gasStorage);

        CalorificValue GetForGasDay(GasStorage gasStorage, DateTime gasDay);
    }
}