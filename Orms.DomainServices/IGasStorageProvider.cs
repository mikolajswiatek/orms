﻿using Orms.Models;

namespace Orms.Providers
{
    public interface IGasStorageProvider : IBaseProvider<GasStorage>
    {

    }
}