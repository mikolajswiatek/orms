﻿using System;
using System.Collections.Generic;
using Orms.Models;

namespace Orms.Providers
{
    public interface INominationProvider : IBaseProvider<Nomination>
    {
        IEnumerable<Nomination> GetBy(Contractor contractor);

        IEnumerable<Nomination> GetBy(GasStorage gasStorage);

        IEnumerable<Nomination> GetBy(User user);

        IEnumerable<Nomination> GetBy(DateTime gasDay);

        Nomination GetLast(Contractor contractor);

        Nomination GetLast(GasStorage gasStorage);

        Nomination GetLast(User user);

        Nomination GetLast(Contractor contractor, DateTime gasDay);

        IEnumerable<Nomination> GetForGasDay(Contractor contractor, DateTime gasDay);

        IEnumerable<Nomination> GetForGasDay(GasStorage gasStorage, DateTime gasDay);

        IEnumerable<Nomination> GetForGasDay(Contractor contractor, GasStorage gasStorage, DateTime gasDay);
    }
}