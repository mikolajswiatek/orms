﻿using System.Collections.Generic;
using Orms.Models;

namespace Orms.Providers
{
    public interface IContractorContactProvider : IBaseProvider<ContractorContact>
    {
        IEnumerable<ContractorContact> GetByContractor(Contractor contractor);
    }
}