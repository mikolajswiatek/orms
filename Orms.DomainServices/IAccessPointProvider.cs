﻿using System.Collections.Generic;
using Orms.Models;

namespace Orms.Providers
{
    public interface IAccessPointProvider : IBaseProvider<AccessPoint>
    {
        IEnumerable<AccessPoint> GetBy(GasStorage gasStorage);
    }
}