﻿using System.Collections.Generic;
using Orms.Models;

namespace Orms.Providers
{
    public interface IEnergyCalorificValueProvider :
        IBaseProvider<EnergyCalorificValue>,
        IListBaseProvider<EnergyCalorificValue>
    {
        IEnumerable<EnergyCalorificValue> GetAll(CalorificValue cv);
    }
}