﻿using Orms.Models;

namespace Orms.Providers
{
    public interface ISessionProvider : IBaseProvider<Session>
    {
        Session GetBy(string guid);

        void Delete(Session s);
    }
}