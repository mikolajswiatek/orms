﻿using System.Collections.Generic;

namespace Orms.Providers
{
    public interface IListBaseProvider<M>
    {
        void Add(IEnumerable<M> m);
    }
}