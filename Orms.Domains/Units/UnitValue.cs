﻿using Orms.Domains.Enums;
using Orms.Models.Extensions;

namespace Orms.Domains.Units
{
    public class UnitValue
    {
        public virtual decimal Value { get; set; }

        public virtual Unit Unit { get; set; }

        public virtual string UnitString
        {
            get { return Unit.ToString(); }
            set { Unit = EnumHelper.ParseEnum<Unit>(value);  }
        }

        public UnitValue()
        {
        }

        public UnitValue(decimal value, Unit unit)
        {
            Value = value;
            Unit = unit;
        }

        public UnitValue(decimal value, string unit)
        {
            Value = value;
            UnitString = unit;
        }
    }
}