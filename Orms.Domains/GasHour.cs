﻿using System;

namespace Orms.Models
{
    public class GasHour
    {
        public DateTime First { get; set; }

        public DateTime Last { get; set; }

        public GasHour(DateTime first, DateTime last)
        {
            First = first;
            Last = last;
        }
    }
}