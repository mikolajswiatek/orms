﻿namespace Orms.Domains.Enums
{
    public enum DirectionType
    {
        Injection,
        Withdrawal
    }
}
