﻿namespace Orms.Domains.Enums
{
    public enum AgreementStatus
    {
        Expired,
        Current
    }
}