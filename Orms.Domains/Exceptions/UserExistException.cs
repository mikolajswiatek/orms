﻿using System;

namespace Orms.Domains.Exceptions
{
    public class UserExistException : Exception
    {
        public UserExistException(string userName, string userEmail) : base(SetMessage(userName, userEmail))
        {
        }

        private static string SetMessage(string userName, string userEmail)
        {
            return $"Username '{userName}' or email '{userEmail}' is exist.";
        }
    }
}