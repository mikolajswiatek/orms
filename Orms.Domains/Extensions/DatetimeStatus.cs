﻿using System;
using Orms.Domains.Enums;

namespace Orms.Models.Extensions
{
    public static class DatetimeStatus
    {
        public static AgreementStatus Status(this DateTime dateTime)
        {
            return dateTime > DateTime.Now ?
                AgreementStatus.Current :
                AgreementStatus.Expired;
        }
    }
}