﻿using System;
using System.Collections.Generic;

namespace Orms.Models
{
    public class GasDay
    {
        public int HourIndexs { get; set; }

        public DateTime First { get; set; }

        public DateTime Last { get; set; }

        public GasDay(DateTime dateTime)
        {
            First = new DateTime(
                dateTime.Year,
                dateTime.Month,
                dateTime.Day,
                6,
                0,
                0);

            Last = new DateTime(
                dateTime.Year,
                dateTime.Month,
                dateTime.Day + 1,
                5,
                0,
                0);

            HourIndexs = GetHourIndex();
        }

        private int GetHourIndex()
        {
            if (First.Month == 3 || First.Month == 10)
            {
                if (First.DayOfWeek == DayOfWeek.Sunday)
                {
                    if (First.Day <= 31 && First.Day > 24)
                    {
                        return First.Month == 3 ? 23 : 25;
                    }
                }

            }

            return 24;
        }

        public static GasDay Today => new GasDay(DateTime.Now);

        public static GasDay Tomorrow => new GasDay(new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day + 1));

        public IEnumerable<GasHour> Hours
        {
            get
            {
                var gasHours = new List<GasHour>();

                if (HourIndexs == 24)
                {
                    for (double hourIndex = 0; hourIndex < 24; hourIndex++)
                    {
                        gasHours.Add(new GasHour(First.AddHours(hourIndex), First.AddHours(hourIndex + 1)));
                    }
                }

                if (HourIndexs == 23)
                {
                    for (double hourIndex = 0; hourIndex < 24; hourIndex++)
                    {
                        if (First.AddHours(hourIndex).Hour != 2 &&
                            First.AddHours(hourIndex + 1).Hour != 3)
                        {
                            gasHours.Add(new GasHour(First.AddHours(hourIndex), First.AddHours(hourIndex + 1)));
                        }
                    }
                }

                if (HourIndexs == 25)
                {
                    for (double hourIndex = 0; hourIndex < 25; hourIndex++)
                    {
                        if (hourIndex == 25d)
                        {
                            gasHours.Add(new GasHour(
                                new DateTime(First.Year, First.Month, First.Day, 2, 0, 0),
                                new DateTime(First.Year, First.Month, First.Day, 3, 0, 0)));
                        }
                    }
                }

                return gasHours;
            }
        }
    }
}
