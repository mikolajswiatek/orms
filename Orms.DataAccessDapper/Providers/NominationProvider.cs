﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Orms.DataAccessDapper.Repositories;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;
using Orms.Providers;

namespace Orms.DataAccessDapper.Providers
{
    public class NominationProvider : INominationProvider
    {
        private readonly INominationRepository<SqlConnection> _repository;
        private readonly SqlConnection _connection;

        public NominationProvider(SqlConnection connection)
        {
            _repository = new NominationRepository();
            _connection = connection;
        }

        public IEnumerable<Nomination> GetAll()
        {
            return _repository.GetAll(_connection);
        }

        public Nomination Get(int id)
        {
            return _repository.Get(_connection, id);
        }

        public void Update(Nomination nomination)
        {
            _repository.Update(_connection, nomination);
        }

        public void Add(Nomination nomination)
        {
            _repository.Add(_connection, nomination);
        }

        public IEnumerable<Nomination> GetBy(Contractor contractor)
        {
            return _repository.GetBy(_connection, contractor);
        }

        public IEnumerable<Nomination> GetBy(GasStorage gasStorage)
        {
            return _repository.GetBy(_connection, gasStorage);
        }

        public IEnumerable<Nomination> GetBy(User user)
        {
            return _repository.GetBy(_connection, user);
        }

        public IEnumerable<Nomination> GetBy(DateTime gasDay)
        {
            return _repository.GetBy(_connection, gasDay);
        }

        public Nomination GetLast(Contractor contractor)
        {
            return _repository.GetLast(_connection, contractor);
        }

        public Nomination GetLast(GasStorage gasStorage)
        {
            return _repository.GetLast(_connection, gasStorage);
        }

        public Nomination GetLast(User user)
        {
            return _repository.GetLast(_connection, user);
        }

        public Nomination GetLast(Contractor contractor, DateTime gasDay)
        {
            return _repository.GetLast(_connection, contractor, gasDay);
        }

        public IEnumerable<Nomination> GetForGasDay(Contractor contractor, DateTime gasDay)
        {
            return _repository.GetForGasDay(_connection, contractor, gasDay);
        }

        public IEnumerable<Nomination> GetForGasDay(GasStorage gasStorage, DateTime gasDay)
        {
            return _repository.GetForGasDay(_connection, gasStorage, gasDay);
        }

        public IEnumerable<Nomination> GetForGasDay(Contractor contractor, GasStorage gasStorage, DateTime gasDay)
        {
            return _repository.GetForGasDay(_connection, contractor, gasStorage, gasDay);
        }
    }
}