﻿using System.Collections.Generic;
using System.Data.SqlClient;
using Orms.DataAccessDapper.Repositories;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;
using Orms.Providers;

namespace Orms.DataAccessDapper.Providers
{
    public class UserProvider : IUserProvider
    {
        private readonly IUserRepository<SqlConnection> _repository;
        private readonly SqlConnection _connection;

        public UserProvider(SqlConnection connection)
        {
            _repository = new UserRepository();
            _connection = connection;
        }

        public IEnumerable<User> GetAll()
        {
            return _repository.GetAll(_connection);
        }

        public User Get(int id)
        {
            return _repository.Get(_connection, id);
        }

        public void Update(User user)
        {
            _repository.Update(_connection, user);
        }

        public void Add(User user)
        {
            _repository.Add(_connection, user);
        }

        public IEnumerable<User> GetBy(Contractor contractor)
        {
            return _repository.GetBy(_connection, contractor);
        }

        public IEnumerable<User> GetBy(string nameOrEmail)
        {
            return _repository.GetBy(_connection, nameOrEmail);
        }
    }
}