﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Orms.DataAccessDapper.Repositories;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;
using Orms.Providers;
using GasAccount = Orms.Models.GasAccount;

namespace Orms.DataAccessDapper.Providers
{
    public class GasAccountProvider : IGasAccountProvider
    {
        private readonly IGasAccountRepository<SqlConnection> _repository;
        private readonly SqlConnection _connection;

        public GasAccountProvider(SqlConnection connection)
        {
            _repository = new GasAccountRepository();
            _connection = connection;
        }

        public IEnumerable<GasAccount> GetAll()
        {
            return _repository.GetAll(_connection);
        }

        public GasAccount Get(int id)
        {
            return _repository.Get(_connection, id);
        }

        public void Update(GasAccount gasAccount)
        {
            _repository.Update(_connection, gasAccount);
        }

        public void Add(GasAccount gasAccount)
        {
            _repository.Add(_connection, gasAccount);
        }

        public void Add(IEnumerable<GasAccount> gasAccounts)
        {
            _repository.Add(_connection, gasAccounts);
        }

        public void Update(IEnumerable<GasAccount> gasAccounts)
        {
            _repository.Update(_connection, gasAccounts);
        }

        public IEnumerable<GasAccount> Get(DateTime gasDay)
        {
            return _repository.Get(_connection, gasDay);
        }

        public IEnumerable<GasAccount> Get(GasStorage gasStorage, DateTime gasDay)
        {
            return _repository.Get(_connection, gasStorage, gasDay);
        }
    }
}