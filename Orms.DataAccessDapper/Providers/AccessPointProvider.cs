﻿using System.Collections.Generic;
using System.Data.SqlClient;
using Orms.DataAccessDapper.Repositories;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;
using Orms.Providers;

namespace Orms.DataAccessDapper.Providers
{
    public class AccessPointProvider : IAccessPointProvider
    {
        private readonly IAccessPointRepository<SqlConnection> _repository;
        private readonly SqlConnection _connection;

        public AccessPointProvider(SqlConnection connection)
        {
            _repository = new AccessPointRepository();
            _connection = connection;
        }

        public IEnumerable<AccessPoint> GetAll()
        {
            return _repository.GetAll(_connection);
        }

        public AccessPoint Get(int id)
        {
            return _repository.Get(_connection, id);
        }

        public void Update(AccessPoint accessPoint)
        {
            _repository.Update(_connection, accessPoint);
        }

        public void Add(AccessPoint accessPoint)
        {
            _repository.Add(_connection, accessPoint);
        }

        public IEnumerable<AccessPoint> GetBy(GasStorage gasStorage)
        {
            return _repository.GetBy(_connection, gasStorage);
        }
    }
}