﻿using System.Collections.Generic;
using System.Data.SqlClient;
using Orms.DataAccessDapper.Repositories;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;
using Orms.Providers;
using NominationValue = Orms.Models.NominationValue;

namespace Orms.DataAccessDapper.Providers
{
    public class NominationValueProvider : INominationValueProvider
    {
        private readonly INominationValueRepository<SqlConnection> _repository;
        private readonly SqlConnection _connection;

        public NominationValueProvider(SqlConnection connection)
        {
            _repository = new NominationValueRepository();
            _connection = connection;
        }

        public IEnumerable<NominationValue> GetAll()
        {
            return _repository.GetAll(_connection);
        }

        public NominationValue Get(int id)
        {
            return _repository.Get(_connection, id);
        }

        public void Update(NominationValue value)
        {
            _repository.Update(_connection, value);
        }

        public void Add(NominationValue value)
        {
            _repository.Add(_connection, value);
        }

        public void Add(IEnumerable<NominationValue> values)
        {
            _repository.Add(_connection, values);
        }

        public IEnumerable<NominationValue> GetBy(Nomination nomination)
        {
            return _repository.GetBy(_connection, nomination);
        }
    }
}