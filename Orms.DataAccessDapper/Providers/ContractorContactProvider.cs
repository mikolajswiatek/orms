﻿using System.Collections.Generic;
using System.Data.SqlClient;
using Orms.DataAccessDapper.Repositories;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;
using Orms.Providers;

namespace Orms.DataAccessDapper.Providers
{
    public class ContractorContactProvider : IContractorContactProvider
    {
        private readonly IContractorContactRepository<SqlConnection> _repository;
        private readonly SqlConnection _connection;

        public ContractorContactProvider(SqlConnection connection)
        {
            _repository = new ContractorContactRepository();
            _connection = connection;
        }

        public IEnumerable<ContractorContact> GetAll()
        {
            return _repository.GetAll(_connection);
        }

        public ContractorContact Get(int id)
        {
            return _repository.Get(_connection, id);

        }

        public void Update(ContractorContact contact)
        {
            _repository.Update(_connection, contact);

        }

        public void Add(ContractorContact contact)
        {
            _repository.Add(_connection, contact);

        }

        public IEnumerable<ContractorContact> GetByContractor(Contractor contractor)
        {
            return _repository.GetByContractor(_connection, contractor);

        }
    }
}