﻿using System.Collections.Generic;
using System.Data.SqlClient;
using Orms.DataAccessDapper.Repositories;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;
using Orms.Providers;

namespace Orms.DataAccessDapper.Providers
{
    public class AgreementProvider : IAgreementProvider
    {
        private readonly IAgreementRepository<SqlConnection> _repository;
        private readonly SqlConnection _connection;

        public AgreementProvider(SqlConnection connection)
        {
            _repository = new AgreementRepository();
            _connection = connection;
        }

        public IEnumerable<Agreement> GetAll()
        {
            return _repository.GetAll(_connection);
        }

        public Agreement Get(int id)
        {
            return _repository.Get(_connection, id);
        }

        public void Update(Agreement agreement)
        {
            _repository.Update(_connection, agreement);
        }

        public void Add(Agreement agreement)
        {
            _repository.Add(_connection, agreement);
        }

        public IEnumerable<Agreement> GetBy(Contractor contractor)
        {
            return _repository.GetBy(_connection, contractor);
        }

        public IEnumerable<Agreement> GetBy(GasStorage gasStorage)
        {
            return _repository.GetBy(_connection, gasStorage);
        }
    }
}