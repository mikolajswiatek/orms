﻿using System.Collections.Generic;
using System.Data.SqlClient;
using Orms.DataAccessDapper.Repositories;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;
using Orms.Providers;

namespace Orms.DataAccessDapper.Providers
{
    public class SessionProvider : ISessionProvider
    {
        private readonly ISessionRepository<SqlConnection> _repository;
        private readonly SqlConnection _connection;

        public SessionProvider(SqlConnection connection)
        {
            _repository = new SessionRepository();
            _connection = connection;
        }

        public IEnumerable<Session> GetAll()
        {
            return _repository.GetAll(_connection);
        }

        public Session Get(int id)
        {
            return _repository.Get(_connection, id);
        }

        public void Update(Session session)
        {
            _repository.Update(_connection, session);
        }

        public void Add(Session session)
        {
            _repository.Add(_connection, session);
        }

        public Session GetBy(string guid)
        {
            return _repository.GetBy(_connection, guid);
        }

        public void Delete(Session session)
        {
            _repository.Delete(_connection, session);
        }
    }
}