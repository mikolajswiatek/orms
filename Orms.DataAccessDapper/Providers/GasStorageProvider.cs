﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Orms.DataAccessDapper.Repositories;
using Orms.DataAccessHelper;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;
using Orms.Providers;

namespace Orms.DataAccessDapper.Providers
{
    public class GasStorageProvider : IGasStorageProvider
    {
        private readonly IGasStorageRepository<SqlConnection> _repository;
        private readonly SqlConnection _connection;

        public GasStorageProvider(SqlConnection connection)
        {
            _repository = new GasStorageRepository();
            _connection = connection;
        }

        public IEnumerable<GasStorage> GetAll()
        {
            return _repository.GetAll(_connection);
        }

        public GasStorage Get(int id)
        {
            return _repository.Get(_connection, id);
        }

        public void Update(GasStorage gasStorage)
        {
            _repository.Update(_connection, gasStorage);
        }

        public void Add(GasStorage gasStorage)
        {
            _repository.Add(_connection, gasStorage);
        }
    }
}