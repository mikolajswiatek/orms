﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Orms.DataAccessDapper.Repositories;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;
using Orms.Providers;

namespace Orms.DataAccessDapper.Providers
{
    public class AccessPointFlowProvider : IAccessPointFlowProvider
    {
        private readonly IAccessPointFlowRepository<SqlConnection> _repository;
        private readonly SqlConnection _connection;

        public AccessPointFlowProvider(SqlConnection connection)
        {
            _repository = new AccessPointFlowRepository();
            _connection = connection;
        }

        public IEnumerable<AccessPointFlow> GetAll()
        {
            return _repository.GetAll(_connection);
        }

        public AccessPointFlow Get(int id)
        {
            return _repository.Get(_connection, id);
        }

        public void Update(AccessPointFlow accessPointFlow)
        {
            _repository.Update(_connection, accessPointFlow);
        }

        public void Add(AccessPointFlow accessPointFlow)
        {
            _repository.Add(_connection, accessPointFlow);
        }
    }
}