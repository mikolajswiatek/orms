﻿using System.Collections.Generic;
using System.Data.SqlClient;
using Orms.DataAccessDapper.Repositories;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;
using Orms.Providers;

namespace Orms.DataAccessDapper.Providers
{
    public class ContractorProvider : IContractorProvider
    {
        private readonly IContractorRepository<SqlConnection> _repository;
        private readonly SqlConnection _connection;

        public ContractorProvider(SqlConnection connection)
        {
            _repository = new ContractorRepository();
            _connection = connection;
        }

        public IEnumerable<Contractor> GetAll()
        {
            return _repository.GetAll(_connection);

        }

        public Contractor Get(int id)
        {
            return _repository.Get(_connection, id);
        }

        public void Update(Contractor contractor)
        {
            _repository.Update(_connection, contractor);
        }

        public void Add(Contractor contractor)
        {
            _repository.Add(_connection, contractor);
        }
    }
}