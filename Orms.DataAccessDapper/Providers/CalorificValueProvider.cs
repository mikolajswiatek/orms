﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Orms.DataAccessDapper.Repositories;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;
using Orms.Providers;

namespace Orms.DataAccessDapper.Providers
{
    public class CalorificValueProvider : ICalorificValueProvider
    {
        private readonly ICalorificValueRepository<SqlConnection> _repository;
        private readonly SqlConnection _connection;

        public CalorificValueProvider(SqlConnection connection)
        {
            _repository = new CalorificValueRepository();
            _connection = connection;
        }

        public IEnumerable<CalorificValue> GetAll()
        {
            return _repository.GetAll(_connection);
        }

        public CalorificValue Get(int id)
        {
            return _repository.Get(_connection, id);
        }

        public void Update(CalorificValue cv)
        {
            _repository.Update(_connection, cv);
        }

        public void Add(CalorificValue cv)
        {
            _repository.Add(_connection, cv);
        }

        public CalorificValue GetLastBy(GasStorage gasStorage)
        {
            return _repository.GetLastBy(_connection, gasStorage);
        }

        public CalorificValue GetForGasDay(GasStorage gasStorage, DateTime gasDay)
        {
            return _repository.GetForGasDay(_connection, gasStorage, gasDay);
        }
    }
}