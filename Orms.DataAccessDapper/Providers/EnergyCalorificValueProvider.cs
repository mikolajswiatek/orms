﻿using System.Collections.Generic;
using System.Data.SqlClient;
using Orms.DataAccessDapper.Repositories;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;
using Orms.Providers;
using EnergyCalorificValue = Orms.Models.EnergyCalorificValue;

namespace Orms.DataAccessDapper.Providers
{
    public class EnergyCalorificValueProvider : IEnergyCalorificValueProvider
    {
        private readonly IEnergyCalorificValueRepository<SqlConnection> _repository;
        private readonly SqlConnection _connection;

        public EnergyCalorificValueProvider(SqlConnection connection)
        {
            _repository = new EnergyCalorificValueRepository();
            _connection = connection;
        }

        public IEnumerable<EnergyCalorificValue> GetAll()
        {
            return _repository.GetAll(_connection);

        }

        public EnergyCalorificValue Get(int id)
        {
            return _repository.Get(_connection, id);

        }

        public void Update(EnergyCalorificValue ecv)
        {
            _repository.Update(_connection, ecv);
        }

        public void Add(EnergyCalorificValue ecv)
        {
            _repository.Add(_connection, ecv);
        }

        public void Add(IEnumerable<EnergyCalorificValue> ecvs)
        {
            _repository.Add(_connection, ecvs);
        }

        public IEnumerable<EnergyCalorificValue> GetAll(CalorificValue cv)
        {
            return _repository.GetAll(_connection, cv);
        }
    }
}