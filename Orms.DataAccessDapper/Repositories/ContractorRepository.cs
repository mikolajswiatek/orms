﻿using System.Collections.Generic;
using System.Data.SqlClient;
using Dapper;
using Orms.DataAccessDapper.Helpers;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;

namespace Orms.DataAccessDapper.Repositories
{
    public class ContractorRepository : IContractorRepository<SqlConnection>
    {
        private readonly string TableName = DapperRepositoryHelper.ContractorTableName;
        private readonly string[] ColumnName = DapperRepositoryHelper.ContractorColumns;

        public IEnumerable<Contractor> GetAll(SqlConnection connection)
        {
            var query = $"SELECT * FROM {TableName}";

            var contractors = connection.Query<Contractor>(query);

            return contractors;
        }

        public Contractor Get(SqlConnection connection, int id)
        {
            var query = $"SELECT * FROM {TableName} WHERE {ColumnName[0]} = {id}";

            var contractor = connection.QueryFirst<Contractor>(query);

            return contractor;
        }

        public void Update(SqlConnection connection, Contractor contractor)
        {
            var query =
                $"UPDATE {TableName} SET " +
                $"{ColumnName[1]} = '{contractor.Name}', " +
                $"{ColumnName[2]} = '{contractor.Code}'," +
                $"{ColumnName[3]} = '{contractor.Adress}'" +
                $" WHERE {ColumnName[0]} = {contractor.Id}";

            connection.Execute(query);
        }

        public void Add(SqlConnection connection, Contractor contractor)
        {
            var query =
                $"INSERT {TableName} ({ColumnName[1]}, {ColumnName[2]}, {ColumnName[3]}) " +
                $"VALUES ('{contractor.Name}', '{contractor.Code}', '{contractor.Adress}')";

            connection.Execute(query);
        }
    }
}