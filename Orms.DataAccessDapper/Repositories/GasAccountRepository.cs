﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using Dapper;
using Orms.DataAccessDapper.Extensions;
using Orms.DataAccessDapper.Helpers;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Domains.Units;
using Orms.Models;
using GasAccount = Orms.Models.GasAccount;
using GA = Orms.DataAccessDapper.Models.GasAccount;

namespace Orms.DataAccessDapper.Repositories
{
    public class GasAccountRepository : IGasAccountRepository<SqlConnection>
    {
        private readonly string TableName = DapperRepositoryHelper.GasAccountTableName;
        private readonly string[] ColumnName = DapperRepositoryHelper.GasAccountColumns;

        public IEnumerable<GasAccount> GetAll(SqlConnection connection)
        {
            var query = $"SELECT * FROM {TableName}";

            var gasAccounts = connection.Query<GA>(query).Select(ga => new GasAccount()
            {
                Id = ga.Id,
                Energy = new UnitValue(ga.EnergyValue, ga.EnergyUnit),
                Volume = new UnitValue(ga.VolumeValue, ga.VolumeUnit),
                HourIndex = ga.HourIndex
            });

            return gasAccounts;
        }

        public GasAccount Get(SqlConnection connection, int id)
        {
            var gasAccount = new GasAccount();

            var query = $"SELECT * FROM {TableName}";

            var ga = connection.Query(query).FirstOrDefault();

            if (ga != null)
            {
                gasAccount = new GasAccount()
                {
                    Id = ga.Id,
                    Energy = new UnitValue(ga.EnergyValue, ga.EnergyUnit),
                    Volume = new UnitValue(ga.VolumeValue, ga.VolumeUnit),
                    HourIndex = ga.HourIndex
                };
            }

            return gasAccount;
        }

        public IEnumerable<GasAccount> Get(SqlConnection connection, DateTime gasDay)
        {
            var query = $"SELECT ga.*, g.* " +
                        $"FROM {TableName} " +
                        $"INNER JOIN {DapperRepositoryHelper.GasStorageTableName} AS g ON " +
                        $"ga.{ColumnName[7]} = g.{DapperRepositoryHelper.GasStorageColumns[0]}; ";

            var gasAccounts = connection.Query<GA, GasStorage, GasAccount>(query, (ga, g) =>
            {
                GasAccount gasAccount = null;

                if (ga == null) return gasAccount;

                gasAccount = new GasAccount()
                {
                    Id = ga.Id,
                    Energy = new UnitValue(ga.EnergyValue, ga.EnergyUnit),
                    Volume = new UnitValue(ga.VolumeValue, ga.VolumeUnit),
                    HourIndex = ga.HourIndex
                };

                if (g != null)
                {
                    gasAccount.GasStorage = g;
                }

                return gasAccount;
            });

            return gasAccounts;
        }

        public void Add(SqlConnection connection, IEnumerable<GasAccount> gasAccounts)
        {
            var nfi = GetNumberFormat();
            var query =
                $"INSERT INTO {TableName} ({ColumnName[1]}, {ColumnName[2]}, {ColumnName[3]}, {ColumnName[4]}, {ColumnName[5]}, {ColumnName[6]}, {ColumnName[7]}) " +
                $"VALUES ";

            for (var index = 0; index < gasAccounts.Count(); index++)
            {
                var gasAccount = gasAccounts.ElementAt(index);

                query += $"({gasAccount.Energy.Value.ToString(nfi)}, '{gasAccount.Energy.Unit}', {gasAccount.Volume.Value.ToString(nfi)}, '{gasAccount.Volume.Unit}', '{gasAccount.GasDay.ToQuery()}', {gasAccount.HourIndex}, {gasAccount.GasStorage.Id}) ";

                if (index + 1 != gasAccounts.Count())
                {
                    query += ", ";
                }
                else
                {
                    query += "; ";
                }
            }

            connection.Execute(query);
        }

        public void Update(SqlConnection connection, IEnumerable<GasAccount> gasAccounts)
        {
            var query = string.Empty;

            var nfi = GetNumberFormat();

            foreach (var gasAccount in gasAccounts)
            {
                query +=
                    $"UPDATE {TableName} SET " +
                    $"{ColumnName[1]} = {gasAccount.Energy.Value.ToString(nfi)}, " +
                    $"{ColumnName[2]} = '{gasAccount.Energy.Unit}', " +
                    $"{ColumnName[3]} = {gasAccount.Volume.Value.ToString(nfi)}, " +
                    $"{ColumnName[4]} = '{gasAccount.Volume.Unit}', " +
                    $"{ColumnName[5]} = '{gasAccount.GasDay.ToQuery()}', " +
                    $"{ColumnName[6]} = {gasAccount.HourIndex}, " +
                    $"{ColumnName[7]} = {gasAccount.GasStorage.Id} " +
                    $"WHERE {ColumnName[0]} = {gasAccount.Id}; ";
            }

            connection.Execute(query);
        }

        private static NumberFormatInfo GetNumberFormat()
        {
            var nfi = new NumberFormatInfo();
            nfi.NumberDecimalSeparator = ".";
            nfi.NumberGroupSeparator = ",";
            return nfi;
        }

        public IEnumerable<GasAccount> Get(SqlConnection connection, GasStorage gasStorage, DateTime gasDay)
        {
            var query = $"SELECT ga.*, g.* " +
                        $"FROM {TableName} ga " +
                        $"INNER JOIN {DapperRepositoryHelper.GasStorageTableName} AS g ON " +
                        $"ga.{ColumnName[7]} = g.{DapperRepositoryHelper.GasStorageColumns[0]} " +
                        $"WHERE ga.{ColumnName[5]} = '{gasDay.ToQuery()}' AND ga.{ColumnName[7]} = {gasStorage.Id}";

            var gasAccounts = connection.Query<GA, GasStorage, GasAccount>(query, (ga, g) =>
            {
                GasAccount gasAccount = null;

                if (ga == null) return gasAccount;

                gasAccount = new GasAccount()
                {
                    Id = ga.Id,
                    Energy = new UnitValue(ga.EnergyValue, ga.EnergyUnit),
                    Volume = new UnitValue(ga.VolumeValue, ga.VolumeUnit),
                    HourIndex = ga.HourIndex,
                    GasDay = ga.GasDay
                };

                if (g != null)
                {
                    gasAccount.GasStorage = g;
                }

                return gasAccount;
            });

            return gasAccounts;
        }

        public void Update(SqlConnection connection, GasAccount gasAccount)
        {
            var nfi = GetNumberFormat();

            var query =
                $"UPDATE {TableName} SET " +
                $"{ColumnName[1]} = {gasAccount.Energy.Value.ToString(nfi)}, " +
                $"{ColumnName[2]} = '{gasAccount.Energy.Unit}', " +
                $"{ColumnName[3]} = {gasAccount.Volume.Value.ToString(nfi)}, " +
                $"{ColumnName[4]} = '{gasAccount.Volume.Unit}', " +
                $"{ColumnName[5]} = '{gasAccount.GasDay.ToQuery()}' , " +
                $"{ColumnName[6]} = {gasAccount.HourIndex}, " +
                $"{ColumnName[7]} = {gasAccount.GasStorage.Id}" +
                $" WHERE {ColumnName[0]} = {gasAccount.Id}";

            connection.Execute(query);
        }

        public void Add(SqlConnection connection, GasAccount gasAccount)
        {
            var query =
                $"INSERT {TableName} ({ColumnName[1]}, {ColumnName[2]}, {ColumnName[3]}, {ColumnName[4]}, {ColumnName[5]}, {ColumnName[6]}, {ColumnName[7]}) " +
                $"VALUES ({gasAccount.Energy.Value}, '{gasAccount.Energy.Unit}', {gasAccount.Volume.Value}, '{gasAccount.Volume.Unit}', '{gasAccount.GasDay.ToQuery()};, {gasAccount.HourIndex}, {gasAccount.GasStorage.Id})";

            connection.Execute(query);
        }
    }
}