﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using Orms.DataAccessDapper.Extensions;
using Orms.DataAccessDapper.Helpers;
using Orms.DataAccessHelper.Exceptions;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;

namespace Orms.DataAccessDapper.Repositories
{
    public class UserRepository : IUserRepository<SqlConnection>
    {
        private static readonly string TableName = DapperRepositoryHelper.UserTableName;
        private static readonly string[] ColumnName = DapperRepositoryHelper.UserColumns;

        public IEnumerable<User> GetAll(SqlConnection connection)
        {
            var query = $"SELECT u.*, c.* " +
                        $"FROM {TableName} u " +
                        $"INNER JOIN {DapperRepositoryHelper.ContractorTableName} c ON " +
                        $"u.{ColumnName[7]} = c.{DapperRepositoryHelper.GasStorageColumns[0]} ";

            var users = connection.Query<User, Contractor, User>(query, (u, c) =>
            {
                var user = u;
                user.Contractor = c;

                return user;
            });

            return users;
        }

        public User Get(SqlConnection connection, int id)
        {
            var query = $"SELECT u.*, c.* " +
                        $"FROM {TableName} u " +
                        $"INNER JOIN {DapperRepositoryHelper.ContractorTableName} c ON " +
                        $"u.{ColumnName[7]} = c.{DapperRepositoryHelper.GasStorageColumns[0]} " +
                        $"WHERE u.{ColumnName[0]} = {id}";

            var users = connection.Query<User, Contractor, User>(query, (u, c) =>
            {
                var user = u;
                user.Contractor = c;

                return user;
            });

            return users.FirstOrDefault();
        }

        public void Update(SqlConnection connection, User user)
        {
            var query =
                $"UPDATE {TableName} SET " +
                $"{ColumnName[1]} = '{user.Name}', " +
                $"{ColumnName[2]} = '{user.Password}', " +
                $"{ColumnName[3]} = '{user.Email}', " +
                $"{ColumnName[4]} = '{user.IsActive}', " +
                $"{ColumnName[5]} = '" + user.CreateDateTime.ToQuery() + "', " +
                $"{ColumnName[6]} = '" + user.LastActivityDateTime.ToQuery() + "', " +
                $"{ColumnName[7]} = {user.Contractor.Id} " +
                $"WHERE {ColumnName[0]} = {user.Id}";

            connection.Execute(query);
        }

        public void Add(SqlConnection connection, User user)
        {
            var query =
                $"INSERT {TableName} ({ColumnName[1]}, {ColumnName[2]}, {ColumnName[3]}, {ColumnName[4]}, {ColumnName[5]}, {ColumnName[6]}, {ColumnName[7]}) " +
                $"VALUES ('{user.Name}', '{user.Password}', '{user.Email}', '{user.IsActive}', '" +
                    user.CreateDateTime.ToQuery() + "', '" +
                    user.LastActivityDateTime.ToQuery() + $"', {user.Contractor.Id})";

            connection.Execute(query);
        }

        public void Delete(SqlConnection connection, User user)
        {
            throw new DatabaseOperationException("Operation is disabled");
        }

        public IEnumerable<User> GetBy(SqlConnection connection, Contractor contractor)
        {
            var query = $"SELECT * FROM {TableName} WHERE {ColumnName[7]} = {contractor.Id}";

            var users = connection.Query<User>(query);

            return users;
        }

        public IEnumerable<User> GetBy(SqlConnection connection, string nameOrEmail)
        {
            var query = $"SELECT u.*, c.* " +
                        $"FROM {TableName} u " +
                        $"INNER JOIN {DapperRepositoryHelper.ContractorTableName} c ON " +
                        $"u.{ColumnName[7]} = c.{DapperRepositoryHelper.GasStorageColumns[0]} " +
                        $"WHERE u.{ColumnName[1]} = '{nameOrEmail}' OR u.{ColumnName[3]} = '{nameOrEmail}'";

            var users = connection.Query<User, Contractor, User>(query, (u, c) =>
            {
                var user = u;
                user.Contractor = c;

                return user;
            });

            return users;
        }
    }
}