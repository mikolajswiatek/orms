﻿using System.Collections.Generic;
using Orms.DataAccessHelper.Repositories.Interfaces;
using System.Data.SqlClient;
using Dapper;
using Orms.DataAccessDapper.Helpers;
using Orms.Models;

namespace Orms.DataAccessDapper.Repositories
{
    public class ContractorContactRepository : IContractorContactRepository<SqlConnection>
    {
        private readonly string TableName = DapperRepositoryHelper.ContractorContactTableName;
        private readonly string[] ColumnName = DapperRepositoryHelper.ContractorContactColumns;

        public IEnumerable<ContractorContact> GetAll(SqlConnection connection)
        {
            var query = $"SELECT * FROM {TableName}";

            var contacts = connection.Query<ContractorContact>(query);

            return contacts;
        }

        public ContractorContact Get(SqlConnection connection, int id)
        {
            var query = $"SELECT * FROM {TableName} WHERE {ColumnName[0]} = {id}";

            var contact = connection.QueryFirst<ContractorContact>(query);

            return contact;
        }

        public void Update(SqlConnection connection, ContractorContact contact)
        {
            var query =
                $"INSERT {TableName} SET " +
                $"{ColumnName[1]} = '{contact.Name}', " +
                $"{ColumnName[2]} = '{contact.FirstName}', " +
                $"{ColumnName[3]} = '{contact.LastName}'," +
                $"{ColumnName[4]} = '{contact.Adress}'," +
                $"{ColumnName[5]} = '{contact.Email}'," +
                $"{ColumnName[6]} = '{contact.Phone}'," +
                $"{ColumnName[7]} = {contact.Contractor.Id}" +
                $" WHERE {ColumnName[0]} = {contact.Id}";

            connection.Execute(query);
        }

        public void Add(SqlConnection connection, ContractorContact contact)
        {
            var query =
                $"INSERT {TableName} ({ColumnName[1]}, {ColumnName[2]}, {ColumnName[3]}, {ColumnName[4]}, {ColumnName[5]}, {ColumnName[6]}, {ColumnName[7]}) " +
                $"VALUES ('{contact.Name}', '{contact.FirstName}', '{contact.LastName}', '{contact.Adress}', '{contact.Email}', '{contact.Phone}', {contact.Contractor.Id})";

            connection.Execute(query);
        }

        public IEnumerable<ContractorContact> GetByContractor(SqlConnection connection, Contractor contractor)
        {
            var query = $"SELECT * FROM {TableName} WHERE {ColumnName[7]} = {contractor.Id}";

            var contact = connection.Query<ContractorContact>(query);

            return contact;
        }
    }
}