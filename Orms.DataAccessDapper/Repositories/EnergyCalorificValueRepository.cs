﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using Orms.DataAccessDapper.Helpers;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Domains.Enums;
using Orms.Domains.Units;
using Orms.Models;
using EnergyCalorificValue = Orms.Models.EnergyCalorificValue;

namespace Orms.DataAccessDapper.Repositories
{
    public class EnergyCalorificValueRepository : IEnergyCalorificValueRepository<SqlConnection>
    {
        private readonly string TableName = DapperRepositoryHelper.CalorificValueValueTableName;
        private readonly string[] ColumnName = DapperRepositoryHelper.CalorificValueValueColumns;

        public IEnumerable<EnergyCalorificValue> GetAll(SqlConnection connection)
        {
            var query = $"SELECT * FROM {TableName}";

            var values = connection.Query(query).Select(n => new EnergyCalorificValue()
            {
                Id = (int)n.ID,
                Energy = new UnitValue((decimal)n.EnergyValue, (Unit)Enum.Parse(typeof(Unit), (string)n.EnergyUnit, true)),
                HourIndex = (int)n.HourIndex,
                CalorificValue = null
            });

            return values;
        }

        public EnergyCalorificValue Get(SqlConnection connection, int id)
        {
            var value = new EnergyCalorificValue();

            var query = $"SELECT * FROM {TableName} WHERE {ColumnName[0]} = {id}";

            var n = connection.Query(query).FirstOrDefault();

            if (n != null)
            {
                value = new EnergyCalorificValue()
                {
                    Id = (int)n.ID,
                    Energy = new UnitValue((decimal)n.EnergyValue, (Unit)Enum.Parse(typeof(Unit), (string)n.EnergyUnit, true)),
                    HourIndex = (int)n.HourIndex,
                    CalorificValue = null
                };
            }

            return value;
        }

        public void Update(SqlConnection connection, EnergyCalorificValue energyCalorificValue)
        {
            var query =
                $"UPDATE {TableName} SET " +
                $"{ColumnName[1]} = '{energyCalorificValue.Energy.Value}', " +
                $"{ColumnName[2]} = '{energyCalorificValue.Energy.Unit}', " +
                $"{ColumnName[3]} = '{energyCalorificValue.HourIndex}', " +
                $"{ColumnName[4]} = '{energyCalorificValue.CalorificValue.Id}'" +
                $" WHERE {ColumnName[0]} = {energyCalorificValue.Id}";

            connection.Execute(query);
        }

        public void Add(SqlConnection connection, EnergyCalorificValue energyCalorificValue)
        {
            Add(connection, new List<EnergyCalorificValue>() { energyCalorificValue });
        }

        public void Add(SqlConnection connection, IEnumerable<EnergyCalorificValue> values)
        {
            var query = values.Aggregate(string.Empty, (current, value) => current + ($"INSERT {TableName} ({ColumnName[1]}, {ColumnName[2]}, {ColumnName[3]}, {ColumnName[4]})" + $"VALUES ({value.Energy.Value}, '{value.Energy.Unit}', {value.HourIndex}, {value.CalorificValue.Id}); "));

            connection.Execute(query);
        }

        public IEnumerable<EnergyCalorificValue> GetAll(SqlConnection connection, CalorificValue cv)
        {
            var query = $"SELECT * FROM {TableName} WHERE {ColumnName[4]} = '{cv.Id}'";

            var values = connection.Query(query).Select(n => new EnergyCalorificValue()
            {
                Id = (int)n.ID,
                Energy = new UnitValue((decimal)n.EnergyValue, (Unit)Enum.Parse(typeof(Unit), (string)n.EnergyUnit, true)),
                HourIndex = (int)n.HourIndex,
                CalorificValue = null
            });

            return values;
        }
    }
}