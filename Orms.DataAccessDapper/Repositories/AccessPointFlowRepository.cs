﻿using System.Collections.Generic;
using System.Data.SqlClient;
using Dapper;
using Orms.DataAccessDapper.Helpers;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;

namespace Orms.DataAccessDapper.Repositories
{
    public class AccessPointFlowRepository : IAccessPointFlowRepository<SqlConnection>
    {
        private readonly string TableName = DapperRepositoryHelper.AccessPointFlowTableName;
        private readonly string[] ColumnNames = DapperRepositoryHelper.AccessPointFlowColumns;

        public void Add(SqlConnection connection, AccessPointFlow apf)
        {
            var query =
                $"INSERT INTO {TableName} ({ColumnNames[1]}, {ColumnNames[2]}) " +
                $"VALUES ('{apf.Direction}', {apf.AccessPoint.Id})";

            connection.Execute(query);
        }

        public AccessPointFlow Get(SqlConnection connection, int id)
        {
            var query = $"SELECT * FROM {TableName} WHERE {ColumnNames[0]} = {id}";

            var apf = connection.QueryFirst<AccessPointFlow>(query);

            return apf;
        }

        public IEnumerable<AccessPointFlow> GetAll(SqlConnection connection)
        {
            var query = $"SELECT * FROM {TableName}";

            var apfs = connection.Query<AccessPointFlow>(query);

            return apfs;
        }

        public void Update(SqlConnection connection, AccessPointFlow apf)
        {
            var query =
                $"UPDATE {TableName} SET " +
                $"{ColumnNames[1]} = '{apf.Direction}', " +
                $"{ColumnNames[2]} = {apf.AccessPoint.Id}" +
                $" WHERE {ColumnNames[0]} = {apf.Id}";

            connection.Execute(query);
        }
    }
}