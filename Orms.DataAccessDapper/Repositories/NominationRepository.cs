﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using Dapper;
using Orms.DataAccessDapper.Extensions;
using Orms.DataAccessDapper.Helpers;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Domains.Units;
using Orms.Models;
using NV = Orms.DataAccessDapper.Models.NominationValue;

namespace Orms.DataAccessDapper.Repositories
{
    public class NominationRepository : INominationRepository<SqlConnection>
    {
        private static readonly string TableName = DapperRepositoryHelper.NominationTableName;
        private static readonly string[] ColumnName = DapperRepositoryHelper.NominationColumns;

        public IEnumerable<Nomination> GetAll(SqlConnection connection)
        {
            var query = $"SELECT * FROM {TableName}";

            var nominations = connection.Query<Nomination>(query);

            return nominations;
        }

        public Nomination Get(SqlConnection connection, int id)
        {
            var query = $"SELECT n.*, cv.*, u.*, c.*, apf.*, ap.*, g.* " +
                        $"FROM {DapperRepositoryHelper.NominationTableName} n " +
                        $"INNER JOIN {DapperRepositoryHelper.CalorificValueTableName} cv ON n.{DapperRepositoryHelper.NominationColumns[13]} = cv.{DapperRepositoryHelper.CalorificValueColumns[0]} " +
                        $"INNER JOIN {DapperRepositoryHelper.UserTableName} u ON n.{DapperRepositoryHelper.NominationColumns[9]} = u.{DapperRepositoryHelper.UserColumns[0]} " +
                        $"INNER JOIN {DapperRepositoryHelper.ContractorTableName} c ON n.{DapperRepositoryHelper.NominationColumns[3]} = c.{DapperRepositoryHelper.ContractorColumns[0]} " +
                        $"INNER JOIN {DapperRepositoryHelper.AccessPointFlowTableName} apf ON n.{DapperRepositoryHelper.NominationColumns[4]} = apf.{DapperRepositoryHelper.AccessPointFlowColumns[0]} " +
                        $"INNER JOIN {DapperRepositoryHelper.AccessPointTableName} ap ON apf.{DapperRepositoryHelper.AccessPointFlowColumns[2]} = ap.{DapperRepositoryHelper.AccessPointColumns[0]} " +
                        $"INNER JOIN {DapperRepositoryHelper.GasStorageTableName} g ON ap.{DapperRepositoryHelper.AccessPointColumns[2]} = g.{DapperRepositoryHelper.GasStorageColumns[0]} " +
                        $"WHERE n.{ColumnName[0]} = {id}";

            var nominations = connection.Query<Nomination, CalorificValue, User, Contractor, AccessPointFlow, AccessPoint, GasStorage, Nomination>(query,
                (n, cv, u, c, apf, ap, g) =>
                {
                    Nomination nom = null;

                    if (n != null)
                    {
                        nom = n;

                        if (cv != null)
                        {
                            n.CalorificValue = cv;
                        }

                        if (u != null)
                        {
                            n.Creator = u;
                        }

                        if (c != null)
                        {
                            n.Contractor = c;
                        }

                        if (apf != null)
                        {
                            n.AccessPointFlow = apf;

                            if (ap != null)
                            {
                                n.AccessPointFlow.AccessPoint = ap;

                                if (g != null)
                                {
                                    n.AccessPointFlow.AccessPoint.GasStorage = g;
                                }
                            }
                        }
                    }

                    return nom;
                });

            var nomination = nominations.FirstOrDefault();

            if (nomination != null)
            {
                query = $"SELECT * FROM {DapperRepositoryHelper.NominationValueTableName} " +
                        $"WHERE {DapperRepositoryHelper.NominationValueColumns[6]} = {nomination.Id} " +
                        $"ORDER BY {DapperRepositoryHelper.NominationValueColumns[5]} ASC;";

                var nvs = connection.Query<NV>(query).Select(nv =>
                {
                    var nominationValue = new NominationValue()
                    {
                        Nomination = nomination,
                        Id = nv.Id,
                        HourIndex = nv.HourIndex,
                        NominationId = nv.Id,
                        Energy = new UnitValue(nv.EnergyValue, nv.EnergyUnit),
                        Volume = new UnitValue(nv.VolumeValue, nv.VolumeUnit)
                    };

                    return nominationValue;
                });

                nomination.Values = nvs;
            }

            return nomination;
        }

        public void Update(SqlConnection connection, Nomination nomination)
        {
            var parentId = nomination.ParentNomination == null ? "NULL" : nomination.ParentNomination.Id.ToString();

            var query =
                $"UPDATE {TableName} SET " +
                $"{ColumnName[1]} = '{nomination.Name}', " +
                $"{ColumnName[2]} = '{nomination.GasDay.ToQuery()}'," +
                $"{ColumnName[3]} = {nomination.Contractor.Id}, " +
                $"{ColumnName[4]} = {nomination.AccessPointFlow.Id}," +
                $"{ColumnName[5]} = '{nomination.CreateDateTime.ToQuery()}', " +
                $"{ColumnName[6]} = '{nomination.IsLast}', " +
                $"{ColumnName[7]} = '{nomination.IsAccepted}', " +
                $"{ColumnName[8]} = {parentId}, " +
                $"{ColumnName[9]} = {nomination.Creator.Id}, " +
                $"{ColumnName[10]} = '{nomination.Channel}', " +
                $"{ColumnName[11]} = {nomination.MajorVersion}, " +
                $"{ColumnName[12]} = {nomination.Version}, " +
                $"{ColumnName[13]} = {nomination.CalorificValue.Id}, " +
                $"{ColumnName[14]} = '{nomination.Comment}' " +
                $" WHERE {ColumnName[0]} = {nomination.Id}";

            connection.Execute(query);
        }

        public void Add(SqlConnection connection, Nomination nomination)
        {
            var parentId = nomination.ParentNomination == null ? "NULL" : nomination.ParentNomination.Id.ToString();

            var query =
                $"INSERT {TableName} ({ColumnName[1]}, {ColumnName[2]}, {ColumnName[3]}, {ColumnName[4]}, {ColumnName[5]}, {ColumnName[6]}, {ColumnName[7]}, {ColumnName[8]}, {ColumnName[9]}, {ColumnName[10]}, {ColumnName[11]}, {ColumnName[12]}, {ColumnName[13]}, {ColumnName[14]}) " +
                $"VALUES ('{nomination.Name}', '{nomination.GasDay.ToQuery()}', {nomination.Contractor.Id}, {nomination.AccessPointFlow.Id}, '{nomination.CreateDateTime.ToQuery()}', '{nomination.IsLast}', '{nomination.IsAccepted}', {parentId}, {nomination.Creator.Id}, '{nomination.Channel}', {nomination.MajorVersion}, {nomination.Version}, {nomination.CalorificValue.Id}, '{nomination.Comment}'); " +
                "SELECT CAST(SCOPE_IDENTITY() as int) 'Id'";

            var nId = connection.QueryFirst<int>(query);

            var nfi = new NumberFormatInfo();
            nfi.NumberDecimalSeparator = ".";
            nfi.NumberGroupSeparator = ",";

            query =
                $"INSERT INTO {DapperRepositoryHelper.NominationValueTableName} ({DapperRepositoryHelper.NominationValueColumns[1]}, {DapperRepositoryHelper.NominationValueColumns[2]}, {DapperRepositoryHelper.NominationValueColumns[3]}, {DapperRepositoryHelper.NominationValueColumns[4]}, {DapperRepositoryHelper.NominationValueColumns[5]}, {DapperRepositoryHelper.NominationValueColumns[6]}) " +
                $"VALUES ";

            for (var index = 0; index < nomination.Values.Count(); index++)
            {
                var value = nomination.Values.ElementAt(index);

                query += $"(CAST({value.Energy.Value.ToString(nfi)} AS Decimal(19, 5)), '{value.Energy.Unit}', CAST({value.Volume.Value.ToString(nfi)} AS Decimal(19, 5)), '{value.Volume.Unit}', {value.HourIndex}, {nId}) ";

                if (index + 1 != nomination.Values.Count())
                {
                    query += ", ";
                }
                else
                {
                    query += "; ";
                }
            }

            connection.Execute(query);
        }

        public IEnumerable<Nomination> GetBy(SqlConnection connection, Contractor contractor)
        {
            var query = $"SELECT * FROM {TableName} WHERE {ColumnName[3]} = {contractor.Id}";

            var nominations = connection.Query<Nomination>(query);

            return nominations;
        }

        public IEnumerable<Nomination> GetBy(SqlConnection connection, GasStorage gasStorage)
        {
            var apfIds = GetAccessPointFlowId(gasStorage);

            var query = $"SELECT * FROM {TableName} WHERE {apfIds}";

            var nominations = connection.Query<Nomination>(query);

            return nominations;
        }

        public IEnumerable<Nomination> GetBy(SqlConnection connection, User user)
        {
            var query = $"SELECT * FROM {TableName} WHERE {ColumnName[9]} = {user.Id}";

            var nominations = connection.Query<Nomination>(query);

            return nominations;
        }

        public IEnumerable<Nomination> GetBy(SqlConnection connection, DateTime gasDay)
        {
            var query = $"SELECT n.*, cv.*, u.*, c.*, apf.*, ap.*, g.* " +
                        $"FROM {DapperRepositoryHelper.NominationTableName} n " +
                        $"INNER JOIN {DapperRepositoryHelper.CalorificValueTableName} cv ON n.{DapperRepositoryHelper.NominationColumns[13]} = cv.{DapperRepositoryHelper.CalorificValueColumns[0]} " +
                        $"INNER JOIN {DapperRepositoryHelper.UserTableName} u ON n.{DapperRepositoryHelper.NominationColumns[9]} = u.{DapperRepositoryHelper.UserColumns[0]} " +
                        $"INNER JOIN {DapperRepositoryHelper.ContractorTableName} c ON n.{DapperRepositoryHelper.NominationColumns[3]} = c.{DapperRepositoryHelper.ContractorColumns[0]} " +
                        $"INNER JOIN {DapperRepositoryHelper.AccessPointFlowTableName} apf ON n.{DapperRepositoryHelper.NominationColumns[4]} = apf.{DapperRepositoryHelper.AccessPointFlowColumns[0]} " +
                        $"INNER JOIN {DapperRepositoryHelper.AccessPointTableName} ap ON apf.{DapperRepositoryHelper.AccessPointFlowColumns[2]} = ap.{DapperRepositoryHelper.AccessPointColumns[0]} " +
                        $"INNER JOIN {DapperRepositoryHelper.GasStorageTableName} g ON ap.{DapperRepositoryHelper.AccessPointColumns[2]} = g.{DapperRepositoryHelper.GasStorageColumns[0]} " +
                        $"WHERE n.{ColumnName[2]} = '{gasDay.ToQuery()}'";

            var nominations = connection.Query<Nomination, CalorificValue, User, Contractor, AccessPointFlow, AccessPoint, GasStorage, Nomination>(query,
                (n, cv, u, c, apf, ap, g) =>
                {
                    Nomination nomination = null;

                    if (n != null)
                    {
                        nomination = n;

                        if (cv != null)
                        {
                            n.CalorificValue = cv;
                        }

                        if (u != null)
                        {
                            n.Creator = u;
                        }

                        if (c != null)
                        {
                            n.Contractor = c;
                        }

                        if (apf != null)
                        {
                            n.AccessPointFlow = apf;

                            if (ap != null)
                            {
                                n.AccessPointFlow.AccessPoint = ap;

                                if (g != null)
                                {
                                    n.AccessPointFlow.AccessPoint.GasStorage = g;
                                }
                            }
                        }
                    }

                    return nomination;
                });

            return nominations;
        }

        public Nomination GetLast(SqlConnection connection, Contractor contractor)
        {
            var query = $"SELECT * FROM {TableName} WHERE {ColumnName[3]} = {contractor.Id} ORDER BY {ColumnName[0]} DESC;";

            var nomination = connection.QueryFirstOrDefault<Nomination>(query);

            return nomination;
        }

        public Nomination GetLast(SqlConnection connection, GasStorage gasStorage)
        {
            var apfIds = GetAccessPointFlowId(gasStorage);

            var query = $"SELECT * FROM {TableName} WHERE {apfIds} ORDER BY {ColumnName[0]} DESC;";

            var nomination = connection.QueryFirstOrDefault<Nomination>(query);

            return nomination;
        }

        private static string GetAccessPointFlowId(GasStorage gasStorage)
        {
            var apfIds = string.Empty;
            var count = 0;

            foreach (var accessPoint in gasStorage.AccessPoints)
            {
                foreach (var accessPointFlow in accessPoint.AccessPointFlows)
                {
                    apfIds += $"{ColumnName[4]} = {accessPointFlow.Id}";

                    if (count > 0)
                    {
                        apfIds += " OR ";
                    }
                    else
                    {
                        count++;
                    }
                }
            }

            return apfIds;
        }

        public Nomination GetLast(SqlConnection connection, User user)
        {
            var query = $"SELECT * FROM {TableName} WHERE {ColumnName[9]} = {user.Id} ORDER BY {ColumnName[0]} DESC;";

            var nomination = connection.QueryFirstOrDefault<Nomination>(query);

            return nomination;
        }

        public Nomination GetLast(SqlConnection connection, Contractor contractor, DateTime gasDay)
        {
            var query = $"SELECT * FROM {TableName} WHERE {ColumnName[3]} = {contractor.Id} AND {ColumnName[2]} = {gasDay} ORDER BY {ColumnName[0]} DESC;";

            var nomination = connection.QueryFirstOrDefault<Nomination>(query);

            return nomination;
        }

        public IEnumerable<Nomination> GetForGasDay(SqlConnection connection, Contractor contractor, DateTime gasDay)
        {
            var query = $"SELECT * FROM {TableName} WHERE {ColumnName[3]} = {contractor.Id} AND {ColumnName[2]} = {gasDay}";

            var nominations = connection.Query<Nomination>(query);

            return nominations;
        }

        public IEnumerable<Nomination> GetForGasDay(SqlConnection connection, GasStorage gasStorage, DateTime gasDay)
        {
            var query = $"SELECT n.*, cv.*, u.*, c.*, apf.*, ap.*, g.* " +
                        $"FROM {DapperRepositoryHelper.NominationTableName} n " +
                        $"INNER JOIN {DapperRepositoryHelper.CalorificValueTableName} cv ON n.{DapperRepositoryHelper.NominationColumns[13]} = cv.{DapperRepositoryHelper.CalorificValueColumns[0]} " +
                        $"INNER JOIN {DapperRepositoryHelper.UserTableName} u ON n.{DapperRepositoryHelper.NominationColumns[9]} = u.{DapperRepositoryHelper.UserColumns[0]} " +
                        $"INNER JOIN {DapperRepositoryHelper.ContractorTableName} c ON n.{DapperRepositoryHelper.NominationColumns[3]} = c.{DapperRepositoryHelper.ContractorColumns[0]} " +
                        $"INNER JOIN {DapperRepositoryHelper.AccessPointFlowTableName} apf ON n.{DapperRepositoryHelper.NominationColumns[4]} = apf.{DapperRepositoryHelper.AccessPointFlowColumns[0]} " +
                        $"INNER JOIN {DapperRepositoryHelper.AccessPointTableName} ap ON apf.{DapperRepositoryHelper.AccessPointFlowColumns[2]} = ap.{DapperRepositoryHelper.AccessPointColumns[0]} " +
                        $"INNER JOIN {DapperRepositoryHelper.GasStorageTableName} g ON ap.{DapperRepositoryHelper.AccessPointColumns[2]} = g.{DapperRepositoryHelper.GasStorageColumns[0]} " +
                        $"WHERE ap.{DapperRepositoryHelper.AccessPointColumns[2]} = {gasStorage.Id} AND n.{ColumnName[2]} = '{gasDay.ToQuery()}'";

            var nIds = new List<int>();

            var nominations = connection.Query<Nomination, CalorificValue, User, Contractor, AccessPointFlow, AccessPoint, GasStorage, Nomination>(query,
                (n, cv, u, c, apf, ap, g) =>
                {
                    Nomination nom = null;

                    if (n != null)
                    {
                        nom = n;

                        nIds.Add(nom.Id);

                        if (cv != null)
                        {
                            n.CalorificValue = cv;
                        }

                        if (u != null)
                        {
                            n.Creator = u;
                        }

                        if (c != null)
                        {
                            n.Contractor = c;
                        }

                        if (apf != null)
                        {
                            n.AccessPointFlow = apf;

                            if (ap != null)
                            {
                                n.AccessPointFlow.AccessPoint = ap;

                                if (g != null)
                                {
                                    n.AccessPointFlow.AccessPoint.GasStorage = g;
                                }
                            }
                        }
                    }

                    return nom;
                });

            if (nominations.Count() > 0)
            {
                var stringNominationIds = string.Empty;

                foreach (var id in nIds)
                {
                    if (!string.IsNullOrEmpty(stringNominationIds))
                    {
                        stringNominationIds += "OR ";
                    }

                    stringNominationIds += $"{DapperRepositoryHelper.NominationValueColumns[6]} = {id} ";
                }

                query = $"SELECT * FROM {DapperRepositoryHelper.NominationValueTableName} " +
                        $"WHERE {stringNominationIds}; ";
                var nvs = connection.Query<NV>(query);

                foreach (var nv in nvs)
                {
                    var nomination = nominations.FirstOrDefault(n => n.Id == nv.NominationId);

                    var nominationValue = new NominationValue()
                    {
                        Nomination = nomination,
                        Id = nv.Id,
                        HourIndex = nv.HourIndex,
                        NominationId = nv.Id,
                        Energy = new UnitValue(nv.EnergyValue, nv.EnergyUnit),
                        Volume = new UnitValue(nv.VolumeValue, nv.VolumeUnit)
                    };

                    if (nomination.CollectionOfNominationValues == null)
                    {
                        nomination.CollectionOfNominationValues = new List<NominationValue>();
                    }

                    nomination.CollectionOfNominationValues.Add(nominationValue);
                };

                foreach (var nomination in nominations)
                {
                    nomination.Values = nomination.CollectionOfNominationValues.ToList();
                }
            }

            return nominations;
        }

        public IEnumerable<Nomination> GetForGasDay(SqlConnection connection, Contractor contractor, GasStorage gasStorage, DateTime gasDay)
        {
            var query = $"SELECT n.*, cv.*, u.*, c.*, apf.*, ap.*, g.* " +
                        $"FROM {DapperRepositoryHelper.NominationTableName} n " +
                        $"INNER JOIN {DapperRepositoryHelper.CalorificValueTableName} cv ON n.{DapperRepositoryHelper.NominationColumns[13]} = cv.{DapperRepositoryHelper.CalorificValueColumns[0]} " +
                        $"INNER JOIN {DapperRepositoryHelper.UserTableName} u ON n.{DapperRepositoryHelper.NominationColumns[9]} = u.{DapperRepositoryHelper.UserColumns[0]} " +
                        $"INNER JOIN {DapperRepositoryHelper.ContractorTableName} c ON n.{DapperRepositoryHelper.NominationColumns[3]} = c.{DapperRepositoryHelper.ContractorColumns[0]} " +
                        $"INNER JOIN {DapperRepositoryHelper.AccessPointFlowTableName} apf ON n.{DapperRepositoryHelper.NominationColumns[4]} = apf.{DapperRepositoryHelper.AccessPointFlowColumns[0]} " +
                        $"INNER JOIN {DapperRepositoryHelper.AccessPointTableName} ap ON apf.{DapperRepositoryHelper.AccessPointFlowColumns[2]} = ap.{DapperRepositoryHelper.AccessPointColumns[0]} " +
                        $"INNER JOIN {DapperRepositoryHelper.GasStorageTableName} g ON ap.{DapperRepositoryHelper.AccessPointColumns[2]} = g.{DapperRepositoryHelper.GasStorageColumns[0]} " +
                        $"WHERE ap.{DapperRepositoryHelper.AccessPointColumns[2]} = {gasStorage.Id} AND n.{ColumnName[3]} = {contractor.Id} AND n.{ColumnName[2]} = '{gasDay.ToQuery()}'";

            var nIds = new List<int>();

            var nominations = connection.Query<Nomination, CalorificValue, User, Contractor, AccessPointFlow, AccessPoint, GasStorage, Nomination>(query,
                (n, cv, u, c, apf, ap, g) =>
                {
                    Nomination nom = null;

                    if (n != null)
                    {
                        nom = n;

                        nIds.Add(nom.Id);

                        if (cv != null)
                        {
                            n.CalorificValue = cv;
                        }

                        if (u != null)
                        {
                            n.Creator = u;
                        }

                        if (c != null)
                        {
                            n.Contractor = c;
                        }

                        if (apf != null)
                        {
                            n.AccessPointFlow = apf;

                            if (ap != null)
                            {
                                n.AccessPointFlow.AccessPoint = ap;

                                if (g != null)
                                {
                                    n.AccessPointFlow.AccessPoint.GasStorage = g;
                                }
                            }
                        }
                    }

                    return nom;
                });

            if (nominations.Count() > 0)
            {
                var stringNominationIds = string.Empty;

                foreach (var id in nIds)
                {
                    if (!string.IsNullOrEmpty(stringNominationIds))
                    {
                        stringNominationIds += "OR ";
                    }

                    stringNominationIds += $"{DapperRepositoryHelper.NominationValueColumns[6]} = {id} ";
                }

                query = $"SELECT * FROM {DapperRepositoryHelper.NominationValueTableName} " +
                        $"WHERE {stringNominationIds}; ";
                var nvs = connection.Query<NV>(query);

                foreach (var nv in nvs)
                {
                    var nomination = nominations.FirstOrDefault(n => n.Id == nv.NominationId);

                    var nominationValue = new NominationValue()
                    {
                        Nomination = nomination,
                        Id = nv.Id,
                        HourIndex = nv.HourIndex,
                        NominationId = nv.Id,
                        Energy = new UnitValue(nv.EnergyValue, nv.EnergyUnit),
                        Volume = new UnitValue(nv.VolumeValue, nv.VolumeUnit)
                    };

                    if (nomination.CollectionOfNominationValues == null)
                    {
                        nomination.CollectionOfNominationValues = new List<NominationValue>();
                    }

                    nomination.CollectionOfNominationValues.Add(nominationValue);
                };

                foreach (var nomination in nominations)
                {
                    nomination.Values = nomination.CollectionOfNominationValues.ToList();
                }
            }

            return nominations;
        }
    }
}