﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using Dapper;
using Orms.DataAccessDapper.Extensions;
using Orms.DataAccessDapper.Helpers;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Domains.Units;
using Orms.Models;
using ECV = Orms.DataAccessDapper.Models.EnergyCalorificValue;
using EnergyCalorificValue = Orms.Models.EnergyCalorificValue;

namespace Orms.DataAccessDapper.Repositories
{
    public class CalorificValueRepository : ICalorificValueRepository<SqlConnection>
    {
        private readonly string TableName = DapperRepositoryHelper.CalorificValueTableName;
        private readonly string[] ColumnName = DapperRepositoryHelper.CalorificValueColumns;

        public IEnumerable<CalorificValue> GetAll(SqlConnection connection)
        {
            var query = $"SELECT * FROM {TableName}";

            var cv = connection.Query<CalorificValue>(query);

            return cv;
        }

        public CalorificValue Get(SqlConnection connection, int id)
        {
            var query = $"SELECT * FROM {TableName} WHERE {ColumnName[0]} = {id}";

            var cv = connection.QueryFirst<CalorificValue>(query);

            return cv;
        }

        public void Update(SqlConnection connection, CalorificValue cv)
        {
            var query =
                $"INSERT {TableName} SET " +
                $"{ColumnName[1]} = '{cv.GasDay.ToQuery()}', " +
                $"{ColumnName[2]} = {cv.GasStorage.Id} " +
                $" WHERE {ColumnName[0]} = {cv.Id}";

            connection.Execute(query);
        }

        public void Add(SqlConnection connection, CalorificValue cv)
        {
            var query =
                $"INSERT INTO {TableName} ({ColumnName[1]}, {ColumnName[2]}) " +
                $"VALUES ('{cv.GasDay.ToQuery()}', {cv.GasStorage.Id}); " +
                "SELECT CAST(SCOPE_IDENTITY() as int) 'Id'";

            var cvId = connection.QueryFirst<int>(query);

            query =
                $"INSERT INTO {DapperRepositoryHelper.CalorificValueValueTableName} ({DapperRepositoryHelper.CalorificValueValueColumns[1]}, {DapperRepositoryHelper.CalorificValueValueColumns[2]}, {DapperRepositoryHelper.CalorificValueValueColumns[3]}, {DapperRepositoryHelper.CalorificValueValueColumns[4]}) VALUES ";

            var nfi = new NumberFormatInfo();
            nfi.NumberDecimalSeparator = ".";
            nfi.NumberGroupSeparator = ",";

            for (var index = 0; index < cv.Values.Count(); index++)
            {
                var value = cv.Values.ElementAt(index);
                query += $"(CAST({value.Energy.Value.ToString(nfi)} AS Decimal(19, 5)), '{value.Energy.Unit}', {value.HourIndex}, {cvId})";

                if (index + 1 != cv.Values.Count())
                {
                    query += ", ";
                }
                else
                {
                    query += "; ";
                }
            }

            connection.Execute(query);
        }

        public IEnumerable<CalorificValue> GetBy(SqlConnection connection, GasStorage gasStorage)
        {
            var query = $"SELECT * FROM {TableName} WHERE {ColumnName[2]} = {gasStorage.Id}";

            var calorificValues = connection.Query<CalorificValue>(query);

            return calorificValues;
        }

        public CalorificValue GetLastBy(SqlConnection connection, GasStorage gasStorage)
        {
            var query = $"SELECT * FROM {TableName} WHERE {ColumnName[2]} = {gasStorage.Id} ORDER BY {ColumnName[0]} DESC;";

            var calorificValue = connection.QueryFirst<CalorificValue>(query);

            return calorificValue;
        }

        public CalorificValue GetForGasDay(SqlConnection connection, GasStorage gasStorage, DateTime gasDay)
        {
            var query = $"SELECT TOP 25" +
                        $"cv.*, " +
                        $"ecv.*, " +
                        $"g.* " +
                        $"FROM {TableName} cv " +
                        $"LEFT OUTER JOIN {DapperRepositoryHelper.CalorificValueValueTableName} ecv ON " +
                        $"cv.{ColumnName[0]} = ecv.{DapperRepositoryHelper.CalorificValueValueColumns[4]} " +
                        $"INNER JOIN {DapperRepositoryHelper.GasStorageTableName} g ON " +
                        $"cv.{ColumnName[2]} = g.{DapperRepositoryHelper.GasStorageColumns[0]} " +
                        $"WHERE cv.{ColumnName[2]} = {gasStorage.Id} AND cv.{ColumnName[1]} = '{gasDay.ToQuery()}' " +
                        $"ORDER BY cv.{ColumnName[0]} DESC;";

            var lookup = new Dictionary<int, CalorificValue>();
            var calorificValues = connection.Query<CalorificValue, ECV, GasStorage, CalorificValue>(query, (cv, ecv, g) =>
            {
                CalorificValue calorificValue;

                if (!lookup.TryGetValue(cv.Id, out calorificValue))
                {
                    lookup.Add(cv.Id, calorificValue = cv);
                }

                if (ecv != null)
                {
                    if (null == calorificValue.CollectionOfValues)
                        calorificValue.CollectionOfValues = new List<EnergyCalorificValue>();

                    var value = new EnergyCalorificValue()
                    {
                        Id = ecv.Id,
                        CalorificValue = cv,
                        HourIndex = ecv.HourIndex,
                        Energy = new UnitValue(ecv.EnergyValue, ecv.EnergyUnit)
                    };

                    calorificValue.CollectionOfValues.Add(value);
                }

                if (g != null)
                {
                    calorificValue.GasStorage = g;
                }

                return calorificValue;
            }).Distinct();

            foreach (var cv in calorificValues.Where(cv => cv.CollectionOfValues.Any()))
            {
                cv.Values = cv.CollectionOfValues.ToList();
            }

            return calorificValues.FirstOrDefault();
        }
    }
}