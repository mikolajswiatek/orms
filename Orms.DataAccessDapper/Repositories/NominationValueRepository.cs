﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using Orms.DataAccessDapper.Helpers;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Domains.Enums;
using Orms.Domains.Units;
using Orms.Models;
using NominationValue = Orms.Models.NominationValue;

namespace Orms.DataAccessDapper.Repositories
{
    public class NominationValueRepository : INominationValueRepository<SqlConnection>
    {
        private static readonly string TableName = DapperRepositoryHelper.NominationValueTableName;
        private static readonly string[] ColumnName = DapperRepositoryHelper.NominationValueColumns;

        public IEnumerable<NominationValue> GetAll(SqlConnection connection)
        {
            var query = $"SELECT * FROM {TableName}";

            var values = connection.Query(query).Select(n => new NominationValue()
            {
                Id = (int)n.ID,
                Energy = new UnitValue((decimal)n.EnergyValue, (Unit)Enum.Parse(typeof(Unit), (string)n.EnergyUnit, true)),
                Volume = new UnitValue((decimal)n.VolumeValue, (Unit)Enum.Parse(typeof(Unit), (string)n.VolumeUnit, true)),
                HourIndex = (int)n.HourIndex,
                Nomination = null
            });

            return values;
        }

        public NominationValue Get(SqlConnection connection, int id)
        {
            var value = new NominationValue();

            var query = $"SELECT * FROM {TableName} WHERE {ColumnName[0]} = {id}";

            var n = connection.Query(query).FirstOrDefault();

            if (n != null)
            {
                value = new NominationValue()
                {
                    Id = (int)n.ID,
                    Energy = new UnitValue((decimal)n.EnergyValue,
                        (Unit)Enum.Parse(typeof(Unit), (string)n.EnergyUnit, true)),
                    Volume = new UnitValue((decimal)n.VolumeValue,
                        (Unit)Enum.Parse(typeof(Unit), (string)n.VolumeUnit, true)),
                    HourIndex = (int)n.HourIndex,
                    Nomination = null
                };
            }

            return value;
        }

        public void Update(SqlConnection connection, NominationValue value)
        {
            throw new DataException("Operation is disabled");
        }

        public void Add(SqlConnection connection, NominationValue value)
        {
            var query =
                $"INSERT {TableName} ({ColumnName[1]}, {ColumnName[2]}, {ColumnName[3]}, {ColumnName[4]}, {ColumnName[5]}, {ColumnName[6]}" +
                $"VALUES ('{value.Energy.Value}, {value.Energy.Unit}, {value.Volume.Value}, {value.Energy.Unit}, {value.HourIndex}, {value.Nomination.Id}')";

            connection.Execute(query);
        }

        public void Add(SqlConnection connection, IEnumerable<NominationValue> values)
        {
            var query = string.Empty;

            foreach (var value in values)
            {
                query += $"INSERT {TableName} ({ColumnName[1]}, {ColumnName[2]}, {ColumnName[3]}, {ColumnName[4]}, {ColumnName[5]}, {ColumnName[6]})" +
                         $"VALUES ('{value.Energy.Value}, '{value.Energy.Unit}', {value.Volume.Value}, '{value.Energy.Unit}', {value.HourIndex}, {value.Nomination.Id}'); ";
            }

            connection.Execute(query);
        }

        public IEnumerable<NominationValue> GetBy(SqlConnection connection, Nomination nomination)
        {
            var query = $"SELECT * FROM {TableName} WHERE {TableName[6]} = {nomination.Id} ORDER BY {ColumnName[5]} ASC;";

            var values = connection.Query(query).Select(n => new NominationValue()
            {
                Id = (int)n.ID,
                Energy = new UnitValue((decimal)n.EnergyValue, (Unit)Enum.Parse(typeof(Unit), (string)n.EnergyUnit, true)),
                Volume = new UnitValue((decimal)n.VolumeValue, (Unit)Enum.Parse(typeof(Unit), (string)n.VolumeUnit, true)),
                HourIndex = (int)n.HourIndex,
                Nomination = null
            });

            return values;
        }
    }
}