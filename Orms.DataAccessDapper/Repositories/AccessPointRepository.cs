﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using Orms.DataAccessDapper.Extensions;
using Orms.DataAccessDapper.Helpers;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;

namespace Orms.DataAccessDapper.Repositories
{
    public class AccessPointRepository : IAccessPointRepository<SqlConnection>
    {
        private readonly string TableName = DapperRepositoryHelper.AccessPointTableName;
        private readonly string[] ColumnName = DapperRepositoryHelper.AccessPointColumns;

        public void Add(SqlConnection connection, AccessPoint ap)
        {
            var query =
                $"INSERT INTO {TableName} ({ColumnName[1]}, {ColumnName[2]}) " +
                $"VALUES ('{ap.Code}', {ap.GasStorage.Id})";

            connection.Execute(query);
        }

        public IEnumerable<AccessPoint> GetBy(SqlConnection connection, GasStorage gasStorage)
        {
            var query = $"" +
                        $"SELECT " +
                            $"ap.*, " +
                            $"apf.* " +
                        $"FROM {TableName} ap " +
                        $"LEFT OUTER JOIN {DapperRepositoryHelper.AccessPointFlowTableName} apf ON " +
                        $"ap.{ColumnName[0]} = apf.{DapperRepositoryHelper.AccessPointFlowColumns[2]} " +
                        $"WHERE ap.{ColumnName[2]} = {gasStorage.Id}";

            var lookup = new Dictionary<int, AccessPoint>();
            var aps = connection.Query<AccessPoint, AccessPointFlow, AccessPoint>(query, (ap, apf) =>
            {
                AccessPoint accessPoint;

                if (!lookup.TryGetValue(ap.Id, out accessPoint))
                {
                    lookup.Add(ap.Id, accessPoint = ap);
                }

                if (apf != null)
                {
                    if (null == accessPoint.AccessPointFlows)
                        accessPoint.CollectionOfAccessPointFlows = new List<AccessPointFlow>();

                    apf.AccessPoint = accessPoint;
                    accessPoint.GasStorage = gasStorage;
                    accessPoint.CollectionOfAccessPointFlows.Add(apf);
                }

                return accessPoint;
            }).Distinct();

            foreach (var accessPoint in aps.Where(ap => ap.CollectionOfAccessPointFlows.Any()))
            {
                accessPoint.AccessPointFlows = accessPoint.CollectionOfAccessPointFlows.ToList();
            }

            return aps;
        }

        public AccessPoint Get(SqlConnection connection, int id)
        {
            var query = $"SELECT ap.*, g.*" +
                        $"FROM {TableName} ap " +
                        $"INNER JOIN {DapperRepositoryHelper.GasStorageTableName} g ON " +
                        $"ap.{ColumnName[2]} = g.{DapperRepositoryHelper.GasStorageColumns[0]} " +
                        $"WHERE ap.{ColumnName[0]} = {id}";

            var aps = connection.Query<AccessPoint, GasStorage, AccessPoint>(query, (ap, g) =>
            {
                var accessPoint = ap;

                if (g != null)
                {
                    accessPoint.GasStorage = g;
                }

                return accessPoint;
            });

            return aps.FirstOrDefault();
        }

        public IEnumerable<AccessPoint> GetAll(SqlConnection connection)
        {
            var query = $"SELECT * FROM {TableName}";

            var aps = connection.Query<AccessPoint>(query);

            return aps;
        }

        public void Update(SqlConnection connection, AccessPoint ap)
        {
            var query =
                $"UPDATE {TableName} SET " +
                $"{ColumnName[1]} = '{ap.Code}', " +
                $"{ColumnName[2]} = {ap.GasStorage.Id}" +
                $" WHERE {ColumnName[0]} = {ap.Id}";

            connection.Execute(query);
        }
    }
}