﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using Orms.DataAccessDapper.Extensions;
using Orms.DataAccessDapper.Helpers;
using Orms.DataAccessDapper.Models;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;

namespace Orms.DataAccessDapper.Repositories
{
    public class AgreementRepository : IAgreementRepository<SqlConnection>
    {
        private readonly string TableName = DapperRepositoryHelper.AgreementTableName;
        private readonly string[] ColumnName = DapperRepositoryHelper.AgreementgeColumns;

        public void Add(SqlConnection connection, Agreement agreement)
        {
            var query =
                $"INSERT INTO {TableName} ({ColumnName[1]}, {ColumnName[2]}, {ColumnName[3]}, {ColumnName[4]}, {ColumnName[5]}) " +
                $"VALUES ('{agreement.CreateDate.ToQuery()}', '{agreement.Start.ToQuery()}', '{agreement.End.ToQuery()}', {agreement.Contractor.Id}, {agreement.GasStorage.Id})";

            connection.Execute(query);
        }

        public IEnumerable<Agreement> GetBy(SqlConnection connection, GasStorage gasStorage)
        {
            var query = $"SELECT * FROM {TableName} WHERE {ColumnName[5]} = {gasStorage.Id}";

            var agreements = connection.Query<Agreement>(query);

            return agreements;
        }

        public IEnumerable<Agreement> GetBy(SqlConnection connection, Contractor contractor)
        {
            var query =
                $"SELECT agreemnt.{ColumnName[0]}, agreemnt.{ColumnName[1]} AS 'CreateDate', agreemnt.{ColumnName[2]} AS 'Start', agreemnt.{ColumnName[3]} AS 'End', agreemnt.{ColumnName[4]}, agreemnt.{ColumnName[5]}, " +
                $"contractor.*, gasStorage.Id, gasStorage.Name as 'GasStorageName' " +
                $"FROM {TableName} AS agreemnt " +
                $"INNER JOIN {DapperRepositoryHelper.GasStorageTableName} AS gasStorage ON " +
                $"agreemnt.{ColumnName[5]} = gasStorage.{DapperRepositoryHelper.GasStorageColumns[0]} " +
                $"INNER JOIN {DapperRepositoryHelper.ContractorTableName} AS contractor ON " +
                $"agreemnt.{ColumnName[4]} = contractor.{DapperRepositoryHelper.ContractorColumns[0]}" +
                $" WHERE agreemnt.{ColumnName[4]} = {contractor.Id}";

            var agreements = connection
                .Query<Agreement, Contractor, Gs, Agreement>(query, (a, c, g) =>
                {
                    Agreement agreement = a;
                    a.GasStorage = new GasStorage()
                    {
                        Id = g.Id,
                        Name = g.GasStorageName
                    };

                    a.Contractor = c;

                    return agreement;

                });

            return agreements;
        }
        public Agreement Get(SqlConnection connection, int id)
        {
            var query =
                $"SELECT agreemnt.{ColumnName[0]}, agreemnt.{ColumnName[1]} AS 'CreateDate', agreemnt.{ColumnName[2]} AS 'Start', agreemnt.{ColumnName[3]} AS 'End', agreemnt.{ColumnName[4]}, agreemnt.{ColumnName[5]}, " +
                $"contractor.*, gasStorage.Id, gasStorage.Name as 'GasStorageName' " +
                $"FROM {TableName} AS agreemnt " +
                $"INNER JOIN {DapperRepositoryHelper.GasStorageTableName} AS gasStorage ON " +
                $"agreemnt.{ColumnName[5]} = gasStorage.{DapperRepositoryHelper.GasStorageColumns[0]} " +
                $"INNER JOIN {DapperRepositoryHelper.ContractorTableName} AS contractor ON " +
                $"agreemnt.{ColumnName[4]} = contractor.{DapperRepositoryHelper.ContractorColumns[0]}" +
                $" WHERE agreemnt.{ColumnName[0]} = {id}";

            var agreements = connection
                .Query<Agreement, Contractor, Gs, Agreement>(query, (a, c, g) =>
                {
                    Agreement agreement = a;
                    a.GasStorage = new GasStorage()
                    {
                        Id = g.Id,
                        Name = g.GasStorageName
                    };

                    a.Contractor = c;

                    return agreement;

                });

            return agreements.FirstOrDefault();
        }

        public IEnumerable<Agreement> GetAll(SqlConnection connection)
        {
            var query = $"SELECT agreemnt.{ColumnName[0]}, agreemnt.{ColumnName[1]} AS 'CreateDate', agreemnt.{ColumnName[2]} AS 'Start', agreemnt.{ColumnName[3]} AS 'End', agreemnt.{ColumnName[4]}, agreemnt.{ColumnName[5]}, " +
                        $"contractor.*, gasStorage.Id, gasStorage.Name as 'GasStorageName' " +
                        $"FROM {TableName} AS agreemnt " +
                        $"INNER JOIN {DapperRepositoryHelper.GasStorageTableName} AS gasStorage ON " +
                        $"agreemnt.{ColumnName[5]} = gasStorage.{DapperRepositoryHelper.GasStorageColumns[0]} " +
                        $"INNER JOIN {DapperRepositoryHelper.ContractorTableName} AS contractor ON " +
                        $"agreemnt.{ColumnName[4]} = contractor.{DapperRepositoryHelper.ContractorColumns[0]} ";

            var agreements = connection
                .Query<Agreement, Contractor, Gs, Agreement>(query, (a, c, g) =>
            {
                Agreement agreement = a;
                a.GasStorage = new GasStorage()
                {
                    Id = g.Id,
                    Name = g.GasStorageName
                };

                a.Contractor = c;

                return agreement;

            });

            return agreements;
        }

        public void Update(SqlConnection connection, Agreement agreement)
        {
            var query =
                $"UPDATE {TableName} SET " +
                $"{ColumnName[1]} = '{agreement.CreateDate.ToQuery()}', " +
                $"{ColumnName[2]} = '{agreement.Start.ToQuery()}', " +
                $"{ColumnName[3]} = '{agreement.End.ToQuery()}', " +
                $"{ColumnName[4]} = {agreement.Contractor.Id}, " +
                $"{ColumnName[5]} = {agreement.GasStorage.Id} " +
                $"WHERE {ColumnName[0]} = {agreement.Id}";

            connection.Execute(query);
        }
    }
}