﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using Orms.DataAccessDapper.Extensions;
using Orms.DataAccessDapper.Helpers;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;
using SessionModel = Orms.Models.Session;

namespace Orms.DataAccessDapper.Repositories
{
    public class SessionRepository : ISessionRepository<SqlConnection>
    {
        private static readonly string TableName = DapperRepositoryHelper.SessionTableName;
        private static readonly string[] ColumnName = DapperRepositoryHelper.SessionColumns;

        public IEnumerable<Session> GetAll(SqlConnection connection)
        {
            var query = $"SELECT * FROM {TableName}";

            var sessions = connection.Query<SessionModel>(query);

            return sessions;
        }

        public SessionModel Get(SqlConnection connection, int id)
        {
            var query = $"SELECT * FROM {TableName} WHERE {ColumnName[0]} = {id}";

            var session = connection.QueryFirst<SessionModel>(query);

            return session;
        }

        public void Update(SqlConnection connection, Session session)
        {
            if (session == null) throw new ArgumentNullException(nameof(session));

            var query =
                $"UPDATE {TableName} SET " +
                $"{ColumnName[1]} = '{session.Guid}', " +
                $"{ColumnName[2]} = '{session.Ip}', " +
                $"{ColumnName[3]} = '{session.Browser}', " +
                $"{ColumnName[4]} = '" + session.CreateDatetime.ToQuery() + "', " +
                $"{ColumnName[5]} = '" + session.LastActivityDatetime.ToQuery() + "', " +
                $"{ColumnName[6]} = {session.User.Id} " +
                $"WHERE {ColumnName[0]} = {session.Id}";

            connection.Execute(query);
        }

        public void Add(SqlConnection connection, Session session)
        {
            if (session == null) throw new ArgumentNullException(nameof(session));

            var query =
                $"INSERT {TableName} ({ColumnName[1]}, {ColumnName[2]}, {ColumnName[3]}, {ColumnName[4]}, {ColumnName[5]}, {ColumnName[6]}) " +
                $"VALUES ('{session.Guid}', '{session.Ip}', '{session.Browser}', '" + session.CreateDatetime.ToQuery() + "', '" + session.LastActivityDatetime.ToQuery() + $"', {session.User.Id})";

            connection.Execute(query);
        }

        public SessionModel GetBy(SqlConnection connection, string guid)
        {
            var query = $"SELECT s.*, u.* " +
                        $"FROM {TableName} s " +
                        $"INNER JOIN {DapperRepositoryHelper.UserTableName} u ON " +
                        $"s.{ColumnName[6]} = u.{DapperRepositoryHelper.UserColumns[0]} " +
                        $"WHERE s.{ColumnName[1]} = '{guid}'";

            var session = connection.Query<SessionModel, User, SessionModel>(query, (s, u) =>
            {
                var result = s;
                result.User = u;

                return result;
            });

            return session.FirstOrDefault();
        }

        public void Delete(SqlConnection connection, Session session)
        {
            var query = $"DELETE FROM {TableName} WHERE {ColumnName[1]} = {session.Guid};";

            connection.Execute(query);
        }
    }
}