﻿using System.Collections.Generic;
using System.Data.SqlClient;
using Dapper;
using Orms.DataAccessDapper.Helpers;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;

namespace Orms.DataAccessDapper.Repositories
{
    public class GasStorageRepository : IGasStorageRepository<SqlConnection>
    {
        private static readonly string TableName = DapperRepositoryHelper.GasStorageTableName;
        private static readonly string[] ColumnName = DapperRepositoryHelper.GasStorageColumns;

        public IEnumerable<GasStorage> GetAll(SqlConnection connection)
        {
            var query = $"SELECT * FROM {TableName}";

            var gasStorages = connection.Query<GasStorage>(query);

            return gasStorages;
        }

        public GasStorage Get(SqlConnection connection, int id)
        {
            var query = $"SELECT * FROM {TableName} WHERE {ColumnName[0]} = {id}";

            var gasStorage = connection.QueryFirst<GasStorage>(query);

            return gasStorage;
        }

        public void Update(SqlConnection connection, GasStorage gasStorage)
        {
            var query =
                $"UPDATE {TableName} SET " +
                $"{ColumnName[1]} = '{gasStorage.Name}'" +
                $" WHERE {ColumnName[0]} = {gasStorage.Id}";

            connection.Execute(query);
        }

        public void Add(SqlConnection connection, GasStorage gasStorage)
        {
            var query =
                $"INSERT {TableName} ({ColumnName[1]})" +
                $"VALUES ('{gasStorage.Name}')";

            connection.Execute(query);
        }

        public void Delete(SqlConnection connection, GasStorage gasStorage)
        {
            var query = $"DELETE FROM {TableName} WHERE {ColumnName[0]} = {gasStorage.Id}";

            connection.Execute(query);
        }
    }
}