﻿using Orms.Domains.Enums;
using Orms.Models;

namespace Orms.DataAccessDapper.Models
{
    public class NominationValue : Entity
    {
        public virtual decimal EnergyValue { get; set; }

        public virtual Unit EnergyUnit { get; set; }

        public virtual decimal VolumeValue { get; set; }

        public virtual Unit VolumeUnit { get; set; }

        public virtual int HourIndex { get; set; }

        public virtual Nomination Nomination { get; set; }

        public virtual int? NominationId { get; set; }
    }
}
