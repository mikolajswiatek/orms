﻿using System;
using Orms.Domains.Enums;
using Orms.Models;

namespace Orms.DataAccessDapper.Models
{
    public class GasAccount : Entity
    {
        public virtual decimal EnergyValue { get; set; }

        public virtual Unit EnergyUnit { get; set; }

        public virtual decimal VolumeValue { get; set; }

        public virtual Unit VolumeUnit { get; set; }

        public virtual DateTime GasDay { get; set; }

        public virtual int HourIndex { get; set; }

        public virtual GasStorage GasStorage { get; set; }

        public virtual int? GasStorageId { get; set; }
    }
}