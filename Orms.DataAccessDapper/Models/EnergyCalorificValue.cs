﻿using Orms.Domains.Enums;
using Orms.Models;

namespace Orms.DataAccessDapper.Models
{
    public class EnergyCalorificValue : Entity
    {
        public virtual decimal EnergyValue { get; set; }

        public virtual Unit EnergyUnit { get; set; }

        public virtual int HourIndex { get; set; }

        public virtual CalorificValue CalorificValue { get; set; }

        public virtual int? CalorificValueId { get; set; }
    }
}