﻿using System;

namespace Orms.DataAccessDapper.Extensions
{
    public static class DateString
    {
        public static string ToQuery(this DateTime dateTime)
        {
            return dateTime.ToString("yyyy-MM-dd HH:mm:ss");
        }
    }
}