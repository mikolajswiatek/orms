﻿using System.Collections.Generic;
using System.Data.SqlClient;
using Orms.DataAccessDapper.Providers;
using Orms.DataAccessHelper;
using Orms.DataAccessHelper.Operations.Interfaces;
using Orms.Models;
using Orms.Services;
using Orms.Services.Interfaces;

namespace Orms.DataAccessDapper.Operations
{
    public class AgreementOperation : Operation<SqlConnection>, IAgreementOperation
    {
        private IAgreementService _service;
        private readonly SessionWorker<SqlConnection> _connection;

        public AgreementOperation(SessionWorker<SqlConnection> connection)
        {
            _connection = connection;
        }

        protected override void SetProviders(SqlConnection connection)
        {
            _service = new AgreementService(
                new AgreementProvider(connection));
        }

        public void Add(Agreement agreement)
        {
            using (var session = _connection.ReadAndWrite())
            {
                SetProviders(session);

                _service.Add(agreement);
            }
        }

        public IEnumerable<Agreement> GetAll(Contractor contractor)
        {
            using (var session = _connection.Read())
            {
                SetProviders(session);

                return _service.GetAll(contractor);
            }
        }

        public IEnumerable<Agreement> GetAll(GasStorage gasStorage)
        {
            using (var session = _connection.Read())
            {
                SetProviders(session);

                return _service.GetAll(gasStorage);
            }
        }

        public IEnumerable<Agreement> GetAll(Contractor contractor, GasStorage gasStorage)
        {
            using (var session = _connection.Read())
            {
                SetProviders(session);

                return _service.GetAll(contractor, gasStorage);
            }
        }

        public Agreement Get(int id)
        {
            using (var session = _connection.Read())
            {
                SetProviders(session);

                return _service.Get(id);
            }
        }

        public IEnumerable<Agreement> GetAll()
        {
            using (var session = _connection.Read())
            {
                SetProviders(session);

                return _service.GetAll();
            }
        }
    }
}