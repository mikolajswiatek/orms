﻿using System.Collections.Generic;
using System.Data.SqlClient;
using Orms.DataAccessDapper.Providers;
using Orms.DataAccessHelper;
using Orms.DataAccessHelper.Operations.Interfaces;
using Orms.Models;
using Orms.Services;
using Orms.Services.Interfaces;

namespace Orms.DataAccessDapper.Operations
{
    public class UserOperation : Operation<SqlConnection>, IUserOperation
    {
        private IUserService _service;
        private readonly SessionWorker<SqlConnection> _connection;

        public UserOperation(SessionWorker<SqlConnection> connection)
        {
            _connection = connection;
        }

        protected override void SetProviders(SqlConnection connection)
        {
            _service = new UserService(
                new UserProvider(connection),
                new SessionProvider(connection));
        }

        public User Exist(string nameOrEmail)
        {
            using (var connection = _connection.Read())
            {
                SetProviders(connection);

                return _service.Exist(nameOrEmail);
            }
        }

        public void Create(User user)
        {
            using (var connection = _connection.ReadAndWrite())
            {
                SetProviders(connection);

                _service.Create(user);
            }
        }

        public void Update(User user)
        {
            using (var connection = _connection.ReadAndWrite())
            {
                SetProviders(connection);

                _service.Update(user);
            }
        }

        public User Get(int id)
        {
            using (var connection = _connection.Read())
            {
                SetProviders(connection);

                return _service.Get(id);
            }
        }

        public IEnumerable<User> Get(Contractor contractor)
        {
            using (var connection = _connection.Read())
            {
                SetProviders(connection);

                return _service.Get(contractor);
            }
        }

        public IEnumerable<User> GetAll()
        {
            using (var connection = _connection.Read())
            {
                SetProviders(connection);

                return _service.GetAll();
            }
        }
    }
}