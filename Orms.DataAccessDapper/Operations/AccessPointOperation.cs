﻿using System.Collections.Generic;
using System.Data.SqlClient;
using Orms.DataAccessDapper.Providers;
using Orms.DataAccessHelper;
using Orms.DataAccessHelper.Operations.Interfaces;
using Orms.Models;
using Orms.Services;
using Orms.Services.Interfaces;

namespace Orms.DataAccessDapper.Operations
{
    public class AccessPointOperation : Operation<SqlConnection>, IAccessPointOperation
    {
        private IAccessPointService _service;
        private readonly SessionWorker<SqlConnection> _connection;

        public AccessPointOperation(SessionWorker<SqlConnection> connection)
        {
            _connection = connection;
        }

        protected override void SetProviders(SqlConnection connection)
        {
            _service = new AccessPointService(
                new AccessPointProvider(connection),
                new AccessPointFlowProvider(connection));
        }

        public IEnumerable<AccessPoint> GetBy(GasStorage gasStorage)
        {
            using (var connection = _connection.Read())
            {
                SetProviders(connection);

                return _service.GetBy(gasStorage);
            }
        }

        public IEnumerable<AccessPoint> GetAll()
        {
            using (var connection = _connection.Read())
            {
                SetProviders(connection);

                return _service.GetAll();
            }
        }

        public AccessPoint Get(int id)
        {
            using (var connection = _connection.Read())
            {
                SetProviders(connection);

                return _service.Get(id);
            }
        }

        public void Add(AccessPoint accessPoint)
        {
            using (var connection = _connection.ReadAndWrite())
            {
                SetProviders(connection);

                _service.Add(accessPoint);
            }
        }

        public void Add(AccessPointFlow apf, int accessPointId)
        {
            using (var connection = _connection.ReadAndWrite())
            {
                SetProviders(connection);

                _service.Add(apf, accessPointId);
            }
        }
    }
}