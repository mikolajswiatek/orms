﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Orms.DataAccessDapper.Providers;
using Orms.DataAccessHelper;
using Orms.DataAccessHelper.Operations.Interfaces;
using Orms.Models;
using Orms.Services;
using Orms.Services.Interfaces;
using NominationValue = Orms.Models.NominationValue;

namespace Orms.DataAccessDapper.Operations
{
    public class NominationOperation : Operation<SqlConnection>, INominationOperation
    {
        private INominationService _service;
        private readonly SessionWorker<SqlConnection> _connection;

        public NominationOperation(SessionWorker<SqlConnection> connection)
        {
            _connection = connection;
        }

        protected override void SetProviders(SqlConnection connection)
        {
            _service = new NominationService(
                new NominationProvider(connection),
                new NominationValueProvider(connection));
        }

        public void Add(Nomination nomination)
        {
            using (var connection = _connection.ReadAndWrite())
            {
                SetProviders(connection);

                _service.Add(nomination);
            }
        }

        public void Add(IEnumerable<NominationValue> values)
        {
            using (var connection = _connection.ReadAndWrite())
            {
                SetProviders(connection);

                _service.Add(values);
            }
        }

        public void Add(Nomination nomination, IEnumerable<NominationValue> values)
        {
            using (var connection = _connection.ReadAndWrite())
            {
                SetProviders(connection);

                _service.Add(nomination, values);
            }
        }

        public void RefreshNomiation(Nomination nomination, int parentNominationId)
        {
            using (var connection = _connection.ReadAndWrite())
            {
                SetProviders(connection);

                _service.RefreshNomiation(nomination, parentNominationId);
            }
        }

        public void AcceptedNomination(Nomination nomination)
        {
            using (var connection = _connection.ReadAndWrite())
            {
                SetProviders(connection);

                _service.AcceptedNomination(nomination);
            }
        }

        public IEnumerable<Nomination> GetBy(Contractor contractor)
        {
            using (var connection = _connection.Read())
            {
                SetProviders(connection);

                return _service.GetBy(contractor);
            }
        }

        public IEnumerable<Nomination> GetBy(GasStorage gasStorage)
        {
            using (var connection = _connection.Read())
            {
                SetProviders(connection);

                return _service.GetBy(gasStorage);
            }
        }

        public IEnumerable<Nomination> GetBy(User user)
        {
            using (var connection = _connection.Read())
            {
                SetProviders(connection);

                return _service.GetBy(user);
            }
        }

        public IEnumerable<Nomination> GetBy(DateTime gasDay)
        {
            using (var connection = _connection.Read())
            {
                SetProviders(connection);

                return _service.GetBy(gasDay);
            }
        }

        public Nomination Get(int id)
        {
            using (var connection = _connection.Read())
            {
                SetProviders(connection);

                return _service.Get(id);
            }
        }

        public Nomination GetLast(Contractor contractor)
        {
            using (var connection = _connection.Read())
            {
                SetProviders(connection);

                return _service.GetLast(contractor);
            }
        }

        public Nomination GetLast(GasStorage gasStorage)
        {
            using (var connection = _connection.Read())
            {
                SetProviders(connection);

                return _service.GetLast(gasStorage);
            }
        }

        public Nomination GetLast(User user)
        {
            using (var connection = _connection.Read())
            {
                SetProviders(connection);

                return _service.GetLast(user);
            }
        }

        public Nomination GetLast(Contractor contractor, DateTime gasDay)
        {
            using (var connection = _connection.Read())
            {
                SetProviders(connection);

                return _service.GetLast(contractor, gasDay);
            }
        }

        public IEnumerable<Nomination> GetForGasDay(Contractor contractor, DateTime gasDay)
        {
            using (var connection = _connection.Read())
            {
                SetProviders(connection);

                return _service.GetForGasDay(contractor, gasDay);
            }
        }

        public IEnumerable<Nomination> GetForGasDay(GasStorage gasStorage, DateTime gasDay)
        {
            using (var connection = _connection.Read())
            {
                SetProviders(connection);

                return _service.GetForGasDay(gasStorage, gasDay);
            }
        }

        public IEnumerable<Nomination> GetForGasDay(Contractor contractor, GasStorage gasStorage, DateTime gasDay)
        {
            using (var connection = _connection.Read())
            {
                SetProviders(connection);

                return _service.GetForGasDay(contractor, gasStorage, gasDay);
            }
        }
    }
}