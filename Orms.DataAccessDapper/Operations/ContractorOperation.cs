﻿using System.Collections.Generic;
using System.Data.SqlClient;
using Orms.DataAccessDapper.Providers;
using Orms.DataAccessHelper;
using Orms.DataAccessHelper.Operations;
using Orms.DataAccessHelper.Operations.Interfaces;
using Orms.Models;
using Orms.Services;
using Orms.Services.Interfaces;

namespace Orms.DataAccessDapper.Operations
{
    public class ContractorOperation : Operation<SqlConnection>, IContractorOperation
    {
        private IContractorService _service;
        private readonly SessionWorker<SqlConnection> _connection;

        public ContractorOperation(SessionWorker<SqlConnection> connection)
        {
            _connection = connection;
        }

        protected override void SetProviders(SqlConnection connection)
        {
            _service = new ContractorService(
                new ContractorContactProvider(connection),
                new ContractorProvider(connection));
        }

        public IEnumerable<Contractor> GetAll()
        {
            using (var connection = _connection.Read())
            {
                SetProviders(connection);

                return _service.GetAll();
            }
        }

        public IEnumerable<ContractorContact> GetAll(Contractor contractor)
        {
            using (var connection = _connection.Read())
            {
                SetProviders(connection);

                return _service.GetAll(contractor);
            }
        }

        public Contractor Get(int id)
        {
            using (var connection = _connection.Read())
            {
                SetProviders(connection);

                return _service.Get(id);
            }
        }

        public ContractorContact GetContact(int id)
        {
            using (var connection = _connection.Read())
            {
                SetProviders(connection);

                return _service.GetContact(id);
            }
        }

        public void Update(ContractorContact contact)
        {
            using (var connection = _connection.ReadAndWrite())
            {
                SetProviders(connection);

                _service.Update(contact);
            }
        }

        public void Add(Contractor contractor)
        {
            using (var connection = _connection.ReadAndWrite())
            {
                SetProviders(connection);

                _service.Add(contractor);
            }
        }

        public void Add(ContractorContact contact, int contractorId)
        {
            using (var connection = _connection.Read())
            {
                SetProviders(connection);

                _service.Add(contact, contractorId);
            }
        }
    }
}