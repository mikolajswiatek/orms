﻿using System.Data.SqlClient;
using Orms.DataAccessDapper.Providers;
using Orms.DataAccessHelper;
using Orms.DataAccessHelper.Operations.Interfaces;
using Orms.Models;
using Orms.Services;
using Orms.Services.Interfaces;

namespace Orms.DataAccessDapper.Operations
{
    public class SessionOperation : Operation<SqlConnection>, ISessionOperation
    {
        private ISessionService _service;
        private readonly SessionWorker<SqlConnection> _connection;

        public SessionOperation(SessionWorker<SqlConnection> connection)
        {
            _connection = connection;
        }

        protected override void SetProviders(SqlConnection connection)
        {
            _service = new SessionService(
                new SessionProvider(connection),
                new UserProvider(connection));
        }

        public void Add(Session session)
        {
            using (var connection = _connection.ReadAndWrite())
            {
                SetProviders(connection);

                _service.Add(session);
            }
        }

        public void Refresh(string guid)
        {
            using (var connection = _connection.ReadAndWrite())
            {
                SetProviders(connection);

                _service.Refresh(guid);
            }
        }

        public Session GetBy(string guid)
        {
            using (var connection = _connection.Read())
            {
                SetProviders(connection);

                return _service.GetBy(guid);
            }
        }

        public void Delete(Session session)
        {
            using (var connection = _connection.ReadAndWrite())
            {
                SetProviders(connection);

                _service.Delete(session);
            }
        }
    }
}