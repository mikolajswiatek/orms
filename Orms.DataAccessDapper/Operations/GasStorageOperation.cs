﻿using System.Collections.Generic;
using System.Data.SqlClient;
using Orms.DataAccessDapper.Providers;
using Orms.DataAccessHelper;
using Orms.DataAccessHelper.Operations.Interfaces;
using Orms.Models;
using Orms.Services;
using Orms.Services.Interfaces;

namespace Orms.DataAccessDapper.Operations
{
    public class GasStorageOperation : Operation<SqlConnection>, IGasStorageOperation
    {
        private IGasStorageService _service;
        private readonly SessionWorker<SqlConnection> _connection;

        public GasStorageOperation(SessionWorker<SqlConnection> connection)
        {
            _connection = connection;
        }

        protected override void SetProviders(SqlConnection connection)
        {
            _service = new GasStorageService(
                new GasStorageProvider(connection));
        }

        public void Add(GasStorage gasStorage)
        {
            using (var connection = _connection.ReadAndWrite())
            {
                SetProviders(connection);

                _service.Add(gasStorage);
            }
        }

        public GasStorage Get(int id)
        {
            using (var connection = _connection.Read())
            {
                SetProviders(connection);

                return _service.Get(id);
            }
        }

        public IEnumerable<GasStorage> GetAll()
        {
            using (var connection = _connection.Read())
            {
                SetProviders(connection);

                return _service.GetAll();
            }
        }
    }
}