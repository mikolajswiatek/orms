﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Orms.DataAccessDapper.Providers;
using Orms.DataAccessHelper;
using Orms.DataAccessHelper.Operations.Interfaces;
using Orms.Models;
using Orms.Services;
using Orms.Services.Interfaces;
using EnergyCalorificValue = Orms.Models.EnergyCalorificValue;

namespace Orms.DataAccessDapper.Operations
{
    public class CalorificValueOperation : Operation<SqlConnection>, ICalorificValueOperation
    {
        private ICalorificValueService _service;
        private readonly SessionWorker<SqlConnection> _connection;

        public CalorificValueOperation(SessionWorker<SqlConnection> connection)
        {
            _connection = connection;
        }

        protected override void SetProviders(SqlConnection connection)
        {
            _service = new CalorificValueService(new CalorificValueProvider(connection));
        }

        public void Add(CalorificValue cv, IEnumerable<EnergyCalorificValue> values)
        {
            using (var connection = _connection.ReadAndWrite())
            {
                SetProviders(connection);

                _service.Add(cv, values);
            }
        }

        public CalorificValue GetLastBy(GasStorage gasStorage)
        {
            using (var connection = _connection.Read())
            {
                SetProviders(connection);

                return _service.GetLastBy(gasStorage);
            }
        }

        public CalorificValue GetForGasDay(GasStorage gasStorage, DateTime gasDay)
        {
            using (var connection = _connection.Read())
            {
                SetProviders(connection);

                return _service.GetForGasDay(gasStorage, gasDay);
            }
        }

        public CalorificValue Get(int id)
        {
            using (var connection = _connection.Read())
            {
                SetProviders(connection);

                return _service.Get(id);
            }
        }
    }
}