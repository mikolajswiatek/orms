﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Orms.DataAccessDapper.Providers;
using Orms.DataAccessHelper;
using Orms.DataAccessHelper.Operations.Interfaces;
using Orms.Models;
using Orms.Services;
using Orms.Services.Interfaces;
using GasAccount = Orms.Models.GasAccount;

namespace Orms.DataAccessDapper.Operations
{
    public class GasAccountOperation : Operation<SqlConnection>, IGasAccountOperation
    {
        private IGasAccountService _service;
        private readonly SessionWorker<SqlConnection> _connection;

        public GasAccountOperation(SessionWorker<SqlConnection> connection)
        {
            _connection = connection;
        }

        protected override void SetProviders(SqlConnection connection)
        {
            _service = new GasAccountService(
                new GasAccountProvider(connection),
                new GasStorageProvider(connection),
                new NominationProvider(connection));
        }

        public void Update(IEnumerable<GasAccount> gasAccounts)
        {
            using (var connection = _connection.ReadAndWrite())
            {
                SetProviders(connection);

                _service.Update(gasAccounts);
            }
        }

        public IEnumerable<GasAccount> Get(DateTime gasDay)
        {
            using (var connection = _connection.Read())
            {
                SetProviders(connection);

                return _service.Get(gasDay);
            }
        }

        public IEnumerable<GasAccount> Get(GasStorage gasStorage, DateTime gasDay)
        {
            using (var connection = _connection.Read())
            {
                SetProviders(connection);

                return _service.Get(gasStorage, gasDay);
            }
        }

        public void Refresh(GasStorage gasStorage)
        {
            using (var connection = _connection.ReadAndWrite())
            {
                SetProviders(connection);

                _service.Refresh(gasStorage);
            }
        }

        public void Refresh()
        {
            using (var connection = _connection.ReadAndWrite())
            {
                SetProviders(connection);

                _service.Refresh();
            }
        }

        public void Refresh(GasStorage gasStorage, GasDay gasDay)
        {
            using (var connection = _connection.ReadAndWrite())
            {
                SetProviders(connection);

                _service.Refresh(gasStorage, gasDay);
            }
        }

        public void Refresh(GasDay gasDay)
        {
            using (var connection = _connection.ReadAndWrite())
            {
                SetProviders(connection);

                _service.Refresh(gasDay);
            }
        }
    }
}