﻿using System.Data.SqlClient;
using Orms.DataAccessHelper;

namespace Orms.DataAccessDapper
{
    public class DapperHelper : SessionWorker<SqlConnection>
    {
        private SqlConnection connection;

        public DapperHelper(string connectionString) : base(connectionString)
        {
        }

        public override void OpenSession()
        {
            connection = new SqlConnection(_connectionString);
            isOpen = true;
        }

        public override void CloseSession()
        {
            connection.Dispose();
            connection = null;
            isOpen = false;
        }

        public override SqlConnection Read()
        {
            OpenSession();
            return connection;
        }

        public override SqlConnection ReadAndWrite()
        {
            OpenSession();
            return connection;
        }

        public override void SaveChanges()
        {
            throw new System.NotImplementedException();
        }
    }
}