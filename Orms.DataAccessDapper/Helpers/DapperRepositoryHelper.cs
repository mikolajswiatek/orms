﻿namespace Orms.DataAccessDapper.Helpers
{
    public static class DapperRepositoryHelper
    {
        public static readonly string AccessPointFlowTableName = "[dbo].[AccessPointFlows]";
        public static readonly string[] AccessPointFlowColumns = { "[Id]", "[Direction]", "[AccessPointId]" };

        public static readonly string AccessPointTableName = "[dbo].[AccessPoints]";
        public static readonly string[] AccessPointColumns = { "[Id]", "[Code]", "[GasStorageId]" };

        public static readonly string AgreementTableName = "[dbo].[Agreements]";
        public static readonly string[] AgreementgeColumns =
            { "[Id]", "[CreateDatetime]", "[StartDatetime]", "[EndDatetime]", "[ContractorId]", "[GasStorageId]" };

        public static readonly string CalorificValueTableName = "[dbo].[CalorificValues]";
        public static readonly string[] CalorificValueColumns = { "[Id]", "[GasDay]", "[GasStorageId]" };

        public static readonly string ContractorContactTableName = "[dbo].[ContractorContacts]";
        public static readonly string[] ContractorContactColumns =
            { "[Id]", "[Name]", "[FirstName]", "[LastName]", "[Adress]", "[Email]", "[Phone]", "[ContractorId]" };

        public static readonly string ContractorTableName = "[dbo].[Contractors]";
        public static readonly string[] ContractorColumns = { "[Id]", "[Name]", "[Code]", "[Adress]" };

        public static readonly string CalorificValueValueTableName = "[dbo].[CalorificValueValues]";
        public static readonly string[] CalorificValueValueColumns =
            { "[Id]", "[EnergyValue]", "[EnergyUnit]", "[HourIndex]", "[CalorificValueId]" };

        public static readonly string GasAccountTableName = "[dbo].[GasAccounts]";
        public static readonly string[] GasAccountColumns =
            { "[Id]", "[EnergyValue]", "[EnergyUnit]", "[VolumeValue]", "[VolumeUnit]", "[GasDay]", "[HourIndex]", "[GasStorageId]" };

        public static readonly string GasStorageTableName = "[dbo].[GasStorages]";
        public static readonly string[] GasStorageColumns = { "[Id]", "[Name]" };

        public static readonly string NominationTableName = "[dbo].[Nominations]";
        public static readonly string[] NominationColumns =
            { "[Id]", "[Name]", "[GasDay]", "[ContractorId]", "[AccessPointFlowId]", "[CreateDatetime]", "[IsLast]", "[IsAccepted]", "[ParentNominationId]", "[UserId]", "[Channel]", "[MajorVersion]", "[Version]", "[CalorificValueId]", "[Comment]" };

        public static readonly string NominationValueTableName = "[dbo].[NominationValues]";
        public static readonly string[] NominationValueColumns =
            { "[Id]", "[EnergyValue]", "[EnergyUnit]", "[VolumeValue]", "[VolumeUnit]", "[HourIndex]", "[NominationId]" };

        public static readonly string SessionTableName = "[dbo].[Sessions]";
        public static readonly string[] SessionColumns =
            { "[Id]", "[Guid]", "[Ip]", "[Browser]", "[CreateDatetime]", "[LastActivityDatetime]", "[UserId]" };

        public static readonly string UserTableName = "[dbo].[Users]";
        public static readonly string[] UserColumns =
            { "[Id]", "[Name]", "[Password]", "[Email]", "[IsActive]", "[CreateDatetime]", "[LastActivityDatetime]", "[ContractorId]" };

    }
}