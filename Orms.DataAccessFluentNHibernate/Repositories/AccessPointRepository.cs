﻿using System.Collections.Generic;
using NHibernate;
using NHibernate.Transform;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;

namespace Orms.DataAccessFluentNHibernate.Repositories
{
    public class AccessPointRepository : IAccessPointRepository<ISession>
    {
        public void Add(ISession session, AccessPoint ap)
        {
            session.Save(ap);
        }

        public void Delete(ISession session, AccessPoint ap)
        {
            session.Delete(ap);
        }

        public AccessPoint Get(ISession session, int id)
        {
            return session.Get<AccessPoint>(id);
        }

        public IEnumerable<AccessPoint> GetAll(ISession session)
        {
            return session.CreateCriteria<AccessPoint>().List<AccessPoint>();
        }

        public void Update(ISession session, AccessPoint ap)
        {
            session.Update(ap);
        }

        public IEnumerable<AccessPoint> GetBy(ISession session, GasStorage gasStorage)
        {
            return session
                .QueryOver<AccessPoint>()
                .Where(ap => ap.GasStorage.Id == gasStorage.Id)
                .Fetch(ap => ap.AccessPointFlows).Eager
                .TransformUsing(Transformers.DistinctRootEntity)
                .List<AccessPoint>();
        }
    }
}
