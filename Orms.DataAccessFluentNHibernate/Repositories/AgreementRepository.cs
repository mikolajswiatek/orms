﻿using System.Collections.Generic;
using NHibernate;
using NHibernate.Transform;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;

namespace Orms.DataAccessFluentNHibernate.Repositories
{
    public class AgreementRepository : IAgreementRepository<ISession>
    {
        public void Add(ISession session, Agreement agreement)
        {
            session.Save(agreement);
        }

        public void Delete(ISession session, Agreement agreement)
        {
            session.Delete(agreement);
        }

        public Agreement Get(ISession session, int id)
        {
            return session.Get<Agreement>(id);
        }

        public IEnumerable<Agreement> GetAll(ISession session)
        {
            return session.CreateCriteria<Agreement>().List<Agreement>();
        }

        public void Update(ISession session, Agreement agreement)
        {
            session.Update(agreement);
        }

        public IEnumerable<Agreement> GetBy(ISession session, GasStorage gasStorage)
        {
            return session.QueryOver<Agreement>()
                .Where(a => a.GasStorage.Id == gasStorage.Id)
                .List<Agreement>();
        }

        public IEnumerable<Agreement> GetBy(ISession session, Contractor contractor)
        {
            var agreements = session.QueryOver<Agreement>()
                .Where(a => a.Contractor.Id == contractor.Id)
                .Fetch(ap => ap.GasStorage).Eager
                .TransformUsing(Transformers.DistinctRootEntity)
                .List<Agreement>();

            return agreements;
        }
    }
}
