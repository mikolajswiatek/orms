﻿using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;

namespace Orms.DataAccessFluentNHibernate.Repositories
{
    public class CalorificValueRepository : ICalorificValueRepository<ISession>
    {
        public void Add(ISession session, CalorificValue cv)
        {
            session.Save(cv);
        }

        public CalorificValue Get(ISession session, int id)
        {
            var cvs = session.Get<CalorificValue>(id);

            return cvs;
        }

        public IEnumerable<CalorificValue> GetAll(ISession session)
        {
            var cvs = session
                .CreateCriteria(typeof(CalorificValue))
                .List<CalorificValue>();

            return cvs;
        }

        public void Update(ISession session, CalorificValue cv)
        {
            session.Update(cv);
        }

        public IEnumerable<CalorificValue> GetBy(ISession session, GasStorage gasStorage)
        {
            var cvs = session
                .CreateCriteria(typeof(CalorificValue))
                .Add(Restrictions.Where<CalorificValue>(cv => cv.GasStorage.Id == gasStorage.Id))
                .List<CalorificValue>();

            return cvs;
        }

        public CalorificValue GetLastBy(ISession session, GasStorage gasStorage)
        {
            var c = session.QueryOver<CalorificValue>()
                .Where(cv => cv.GasStorage.Id == gasStorage.Id)
                .Fetch(cv => cv.Values).Eager
                .TransformUsing(Transformers.DistinctRootEntity)
                .Fetch(cv => cv.Values).Eager
                .OrderBy(cv => cv.Id)
                .Desc
                .List<CalorificValue>();

            return c.FirstOrDefault();
        }

        public CalorificValue GetForGasDay(ISession session, GasStorage gasStorage, DateTime gasDay)
        {
            var c = session
                .QueryOver<CalorificValue>()
                .Where(cv => cv.GasStorage.Id == gasStorage.Id && cv.GasDay == gasDay)
                .Fetch(cv => cv.Values).Eager
                .TransformUsing(Transformers.DistinctRootEntity)
                .OrderBy(cv => cv.Id).Desc
                .List<CalorificValue>();

            return c.FirstOrDefault();
        }
    }
}
