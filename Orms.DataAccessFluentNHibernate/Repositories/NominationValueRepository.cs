﻿using System;
using System.Collections.Generic;
using NHibernate;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;

namespace Orms.DataAccessFluentNHibernate.Repositories
{
    public class NominationValueRepository : INominationValueRepository<ISession>
    {
        public void Add(ISession session, NominationValue nominationValue)
        {
            session.Save(nominationValue);
        }

        public void Delete(ISession session, NominationValue nominationValue)
        {
            session.Delete(nominationValue);
        }

        public NominationValue Get(ISession session, int id)
        {
            var nominationValue = session.Get<NominationValue>(id);

            return nominationValue;
        }

        public IEnumerable<NominationValue> GetAll(ISession session)
        {
            var nominationValues = session
                .CreateCriteria(typeof(NominationValue))
                .List<NominationValue>();

            return nominationValues;
        }

        public void Update(ISession session, NominationValue nominationValue)
        {
            session.Update(nominationValue);
        }

        public void Add(ISession session, IEnumerable<NominationValue> values)
        {
            foreach (var value in values)
            {
                session.Save(value);
            }
        }

        public IEnumerable<NominationValue> GetBy(ISession session, Nomination nomination)
        {
            var nominationValues = session
                .QueryOver<NominationValue>()
                .Where(nv => nv.Nomination.Id == nomination.Id)
                .List();

            return nominationValues;
        }
    }
}
