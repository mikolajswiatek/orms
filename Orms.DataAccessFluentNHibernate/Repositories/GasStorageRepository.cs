﻿using System.Collections.Generic;
using NHibernate;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;

namespace Orms.DataAccessFluentNHibernate.Repositories
{
    public class GasStorageRepository : IGasStorageRepository<ISession>
    {
        public void Add(ISession session, GasStorage gasStorage)
        {
            session.Save(gasStorage);
        }

        public void Delete(ISession session, GasStorage gasStorage)
        {
            session.Delete(gasStorage);
        }

        public GasStorage Get(ISession session, int id)
        {
            var gasStorage = session.Get<GasStorage>(id);

            return gasStorage;
        }

        public IEnumerable<GasStorage> GetAll(ISession session)
        {
            var gasStorages = session
                .CreateCriteria(typeof(GasStorage))
                .List<GasStorage>();

            return gasStorages;
        }

        public void Update(ISession session, GasStorage gasStorage)
        {
            session.Update(gasStorage);
        }
    }
}
