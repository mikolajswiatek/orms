﻿using NHibernate;
using Orms.Models;

namespace Orms.DataAccessFluentNHibernate.Map
{
    public class SessionMapping : EntityMapping<Session>
    {
        public SessionMapping()
        {
            this.Table("Sessions");

            this
                .Map(c => c.Browser)
                .Column("Browser");

            this
                .Map(c => c.CreateDatetime)
                .Column("CreateDatetime");

            this
                .Map(c => c.Guid)
                .Column("Guid");

            this
                .Map(c => c.Ip)
                .Column("Ip");

            this
                .Map(c => c.LastActivityDatetime)
                .Column("LastActivityDatetime");

            this
                .References(c => c.User)
                .Column("UserId");
        }
    }
}