﻿using NHibernate;
using Orms.Models;

namespace Orms.DataAccessFluentNHibernate.Map
{
    public class UserMapping : EntityMapping<User>
    {
        public UserMapping()
        {
            this.Table("Users");

            this
                .Map(c => c.Name)
                .Column("Name");

            this
                .Map(c => c.Password)
                .Column("Password");

            this
                .Map(c => c.Email)
                .Column("Email");

            this
                .Map(c => c.IsActive)
                .Column("IsActive");

            this
                .Map(c => c.CreateDateTime)
                .Column("CreateDatetime");

            this
                .Map(c => c.LastActivityDateTime)
                .Column("LastActivityDatetime");

            this
                .References(c => c.Contractor)
                .Column("ContractorId")
                .Not.LazyLoad();
        }
    }
}