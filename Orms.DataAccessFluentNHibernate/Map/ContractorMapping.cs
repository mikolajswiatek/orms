﻿using Orms.Models;

namespace Orms.DataAccessFluentNHibernate.Map
{
    public class ContractorMapping : EntityMapping<Contractor>
    {
        public ContractorMapping()
        {
            this
                .Table("Contractors");

            this
                .Map(c => c.Adress)
                .Column("Adress")
                .CustomType<string>();

            this
                .Map(c => c.Code)
                .Column("Code")
                .CustomType<string>()
                .Unique();

            this
                .Map(c => c.Name)
                .Column("Name")
                .CustomType<string>();
        }
    }
}