﻿using FluentNHibernate.Mapping;

using Orms.Models;

namespace Orms.DataAccessFluentNHibernate.Map
{
    public class EntityMapping<T> : ClassMap<T> where T : Entity
    {
        public EntityMapping()
        {
            this
                .Id(x => x.Id)
                .Column("Id")
                .CustomType<int>()
                .GeneratedBy.Identity();
        }
    }
}