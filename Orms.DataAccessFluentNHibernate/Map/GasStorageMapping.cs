﻿using NHibernate;
using Orms.Models;

namespace Orms.DataAccessFluentNHibernate.Map
{
    public class GasStorageMapping : EntityMapping<GasStorage>
    {
        public GasStorageMapping()
        {
            this.Table("GasStorages");

            this
                .Map(x => x.Name)
                .Column("Name");

            this
                .HasMany(x => x.AccessPoints)
                .Cascade.All()
                .Inverse()
                .Table("AccessPoints")
                .KeyColumn("AccessPointId");
        }
    }
}