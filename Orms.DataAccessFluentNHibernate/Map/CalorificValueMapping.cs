﻿using System;
using NHibernate.Type;
using Orms.Models;

namespace Orms.DataAccessFluentNHibernate.Map
{
    public class CalorificValueMapping : EntityMapping<CalorificValue>
    {
        public CalorificValueMapping()
        {
            this.Table("CalorificValues");

            this
                .Map(x => x.GasDay)
                .Column("GasDay")
                .CustomType<DateTime>();

            this
                .HasMany(x => x.Values)
                .Table("CalorificValueValues")
                .KeyColumn("CalorificValueId")
                .Cascade.All();
                //.Not.LazyLoad();

            this
                .References(c => c.GasStorage)
                .Column("GasStorageId")
                .Not.LazyLoad();
        }
    }
}