﻿using FluentNHibernate.Mapping;
using NHibernate;
using Orms.Domains.Enums;
using Orms.Models;

namespace Orms.DataAccessFluentNHibernate.Map
{
    public class NominationValueMapping : EntityMapping<NominationValue>
    {
        public NominationValueMapping()
        {
            this
                .Table("NominationValues");

            this
                .Component(c => c.Energy, comp =>
                {
                    comp
                        .Map(x => x.Value)
                        .Column("EnergyValue");

                    comp
                        .Map(x => x.Unit)
                        .Column("EnergyUnit")
                        .CustomType<GenericEnumMapper<Unit>>();
                });

            this
                .Component(c => c.Volume, comp =>
                {
                    comp
                        .Map(x => x.Value)
                        .Column("VolumeValue");

                    comp
                        .Map(x => x.Unit)
                        .Column("VolumeUnit")
                        .CustomType<GenericEnumMapper<Unit>>();
                });

            this
                .Map(c => c.HourIndex)
                .Column("HourIndex")
                .CustomType<int>();

            this
                .References(c => c.Nomination)
                .Columns("NominationId")
                .Cascade.All();
        }
    }
}