﻿using System;
using FluentNHibernate.Mapping;
using Orms.Domains.Enums;
using Orms.Models;

namespace Orms.DataAccessFluentNHibernate.Map
{
    public class NominationMapping : EntityMapping<Nomination>
    {
        public NominationMapping()
        {
            this.Table("Nominations");

            this
                .Map(c => c.Name)
                .Column("Name");

            this
                .Map(c => c.Channel)
                .Column("Channel")
                .CustomType<GenericEnumMapper<Channel>>();

            this
                .Map(c => c.IsAccepted)
                .Column("IsAccepted");

            this
                .Map(c => c.IsLast)
                .Column("IsLast");

            this
                .Map(c => c.GasDay)
                .Column("GasDay")
                .CustomType<DateTime>();

            this
                .Map(c => c.CreateDateTime)
                .Column("CreateDatetime")
                .CustomType<DateTime>();

            this
                .Map(c => c.MajorVersion)
                .Column("MajorVersion");

            this
                .Map(c => c.Version)
                .Column("Version");

            this
                .Map(c => c.Comment)
                .Column("Comment");

            this
                .References(c => c.AccessPointFlow)
                .Column("AccessPointFlowId")
                .Not.LazyLoad()
                .Cascade.All();

            this
                .References(c => c.ParentNomination)
                .Column("ParentNominationId")
                .Nullable();

            this
                .References(c => c.Creator)
                .Column("UserId");

            this
                .References(c => c.Contractor)
                .Column("ContractorId");

            this
                .References(c => c.CalorificValue)
                .Column("CalorificValueId");

            this
                .HasMany(c => c.Values)
                .KeyColumn("NominationId")
                .Table("NominationValues")
                .Cascade.All()
                .Not.LazyLoad();
        }
    }
}