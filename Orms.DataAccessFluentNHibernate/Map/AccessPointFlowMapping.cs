﻿using FluentNHibernate.Mapping;
using Orms.Domains.Enums;
using Orms.Models;

namespace Orms.DataAccessFluentNHibernate.Map
{
    public class AccessPointFlowMapping : EntityMapping<AccessPointFlow>
    {
        public AccessPointFlowMapping()
        {
            this
                .Table("AccessPointFlows");

            this
                .Map(c => c.Direction)
                .Column("Direction")
                .CustomType<GenericEnumMapper<DirectionType>>();

            this
                .Map(c => c.DirectionString)
                .Column("Direction")
                .Not.Insert()
                .Not.Update();

            this
                .References(x => x.AccessPoint)
                .Column("AccessPointId");
        }
    }
}