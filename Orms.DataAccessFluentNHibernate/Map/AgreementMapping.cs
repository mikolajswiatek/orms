﻿using System;
using Orms.Models;

namespace Orms.DataAccessFluentNHibernate.Map
{
    public class AgreementMapping : EntityMapping<Agreement>
    {
        public AgreementMapping()
        {
            this
                .Table("Agreements");

            this
                .Map(c => c.CreateDate)
                .Column("CreateDatetime")
                .CustomType<DateTime>();

            this
                .Map(c => c.Start)
                .Column("StartDatetime")
                .CustomType<DateTime>();

            this
                .Map(c => c.End)
                .Column("EndDatetime")
                .CustomType<DateTime>();

            this
                .References(c => c.Contractor)
                .Column("ContractorId")
                .Not.LazyLoad();

            this
                .References(c => c.GasStorage)
                .Column("GasStorageId")
                .Not.LazyLoad();
        }
    }
}