﻿using FluentNHibernate.Mapping;
using Orms.Domains.Enums;
using Orms.Models;

namespace Orms.DataAccessFluentNHibernate.Map
{
    public class EnergyCalorificValueMapping : EntityMapping<EnergyCalorificValue>
    {
        public EnergyCalorificValueMapping()
        {
            this
                .Table("CalorificValueValues");

            this
                .Map(x => x.HourIndex)
                .Column("HourIndex")
                .CustomType<int>();

            this
                .Component(c => c.Energy, comp =>
                {
                    comp
                        .Map(x => x.Value)
                        .Column("EnergyValue");

                    comp
                        .Map(x => x.Unit)
                        .Column("EnergyUnit")
                        .CustomType<GenericEnumMapper<Unit>>();
                });

            this
                .References(c => c.CalorificValue)
                .Column("CalorificValueId")
                .Cascade.All();
        }
    }
}