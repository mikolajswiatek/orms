﻿using NHibernate;
using Orms.Models;

namespace Orms.DataAccessFluentNHibernate.Map
{
    public class ContractorContactMapping : EntityMapping<ContractorContact>
    {
        public ContractorContactMapping()
        {
            this
                .Table("ContractorContacts");

            this
                .Map(c => c.Adress)
                .Column("Adress");

            this
                .Map(c => c.Email)
                .Column("Email"); ;

            this
                .Map(c => c.Name)
                .Column("Name");

            this
                .Map(c => c.FirstName)
                .Column("FirstName");

            this
                .Map(c => c.LastName)
                .Column("LastName");

            this
                .Map(c => c.Phone)
                .Column("Phone");

            this
                .References(x => x.Contractor)
                .Column("ContractorId");
        }
    }
}