﻿using Orms.Models;

namespace Orms.DataAccessFluentNHibernate.Map
{
    public class AccessPointMapping : EntityMapping<AccessPoint>
    {
        public AccessPointMapping()
        {
            this.Table("AccessPoints");

            this
                .Map(c => c.Code)
                .Column("Code");

            this
                .References(c => c.GasStorage)
                .Column("GasStorageId");

            this
                .HasMany(x => x.AccessPointFlows)
                .KeyColumn("AccessPointId");

            this.Map(c => c.GasStorageId);
        }
    }
}