﻿using System;
using FluentNHibernate.Mapping;
using Orms.Domains.Enums;
using Orms.Models;

namespace Orms.DataAccessFluentNHibernate.Map
{
    public class GasAccountMapping : EntityMapping<GasAccount>
    {
        public GasAccountMapping()
        {
            this.Table("GasAccounts");

            this
                .Component(c => c.Energy, comp =>
            {
                comp
                .Map(x => x.Value)
                .Column("EnergyValue");

                comp
                .Map(x => x.Unit)
                .Column("EnergyUnit")
                .CustomType<GenericEnumMapper<Unit>>();
            });

            this
                .Component(c => c.Volume, comp =>
            {
                comp
                    .Map(x => x.Value)
                    .Column("VolumeValue");

                comp
                    .Map(x => x.Unit)
                    .Column("VolumeUnit")
                    .CustomType<GenericEnumMapper<Unit>>();
            });

            this
                .Map(c => c.HourIndex)
                .Column("HourIndex");

            this
                .Map(x => x.GasDay)
                .Column("GasDay")
                .CustomType<DateTime>();

            this
                .References(c => c.GasStorage)
                .Column("GasStorageId");
        }
    }
}