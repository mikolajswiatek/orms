﻿using System;
using System.Diagnostics;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Tool.hbm2ddl;
using Orms.DataAccessFluentNHibernate.Map;
using Orms.DataAccessHelper;

namespace Orms.DataAccessFluentNHibernate
{
    public class FluentNHibernateHelper : SessionWorker<ISession>
    {
        private ISessionFactory Session { get; set; }

        public FluentNHibernateHelper(string connectionString) : base(connectionString)
        {
        }

        public override void OpenSession()
        {
            Session = Fluently.Configure()
                .Database(MsSqlConfiguration.MsSql2008
                    .ConnectionString(_connectionString)
                    .FormatSql()
                    .ShowSql())
                .Mappings(m =>
                    m.FluentMappings
                        .AddFromAssemblyOf<AccessPointMapping>()
                        .AddFromAssemblyOf<AccessPointFlowMapping>()
                        .AddFromAssemblyOf<AgreementMapping>()
                        .AddFromAssemblyOf<CalorificValueMapping>()
                        .AddFromAssemblyOf<ContractorMapping>()
                        .AddFromAssemblyOf<ContractorContactMapping>()
                        .AddFromAssemblyOf<EnergyCalorificValueMapping>()
                        .AddFromAssemblyOf<GasAccountMapping>()
                        .AddFromAssemblyOf<GasStorageMapping>()
                        .AddFromAssemblyOf<NominationMapping>()
                        .AddFromAssemblyOf<NominationValueMapping>()
                        .AddFromAssemblyOf<UserMapping>()
                        .Conventions.AddFromAssemblyOf<EnumConvention>())
                .ExposeConfiguration(cfg =>
                {
                    new SchemaExport(cfg).Create(false, false);
                    cfg.SetInterceptor(new SqlStatementInterceptor());
                })
                .BuildSessionFactory();
            isOpen = true;
        }

        public override void CloseSession()
        {
            Session.Close();
            Session.Dispose();
            isOpen = false;
        }

        public override ISession Read()
        {
            var session = Session.OpenSession();
            session.FlushMode = FlushMode.Never;
            session.DefaultReadOnly = true;

            return session;
        }

        public override ISession ReadAndWrite()
        {
            var session = Session.OpenSession();
            session.FlushMode = FlushMode.Commit;
            session.DefaultReadOnly = false;

            return session;
        }

        public override void SaveChanges()
        {
            throw new NotImplementedException();
        }
    }

    public class SqlStatementInterceptor : EmptyInterceptor
    {
        public override NHibernate.SqlCommand.SqlString OnPrepareStatement(NHibernate.SqlCommand.SqlString sql)
        {
            Debug.WriteLine(sql.ToString());
            return sql;
        }
    }
}