﻿using System.Collections.Generic;
using NHibernate;
using Orms.DataAccessFluentNHibernate.Repositories;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;
using Orms.Providers;

namespace Orms.DataAccessFluentNHibernate.Providers
{
    public class NominationValueProvider : INominationValueProvider
    {
        private readonly INominationValueRepository<ISession> _repository;
        private readonly ISession _session;

        public NominationValueProvider(ISession session)
        {
            _session = session;
            _repository = new NominationValueRepository();
        }

        public void Add(NominationValue nominationValue)
        {
            _repository.Add(_session, nominationValue);
        }

        public void Add(IEnumerable<NominationValue> values)
        {
            _repository.Add(_session, values);
        }

        public IEnumerable<NominationValue> GetBy(Nomination nomination)
        {
            return _repository.GetBy(_session, nomination);
        }

        public NominationValue Get(int id)
        {
            return _repository.Get(_session, id);
        }

        public IEnumerable<NominationValue> GetAll()
        {
            return _repository.GetAll(_session);
        }

        public void Update(NominationValue nominationValue)
        {
            _repository.Update(_session, nominationValue);
        }
    }
}