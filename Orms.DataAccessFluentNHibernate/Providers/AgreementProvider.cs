﻿using System.Collections.Generic;
using NHibernate;
using Orms.DataAccessFluentNHibernate.Repositories;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;
using Orms.Providers;

namespace Orms.DataAccessFluentNHibernate.Providers
{
    public class AgreementProvider : IAgreementProvider
    {
        private readonly IAgreementRepository<ISession> _repository;
        private readonly ISession _session;

        public AgreementProvider(ISession session)
        {
            _session = session;
            _repository = new AgreementRepository();
        }

        public void Add(Agreement agreement)
        {
            _repository.Add(_session, agreement);
        }

        public Agreement Get(int id)
        {
            return _repository.Get(_session, id);
        }

        public IEnumerable<Agreement> GetAll()
        {
            return _repository.GetAll(_session);
        }

        public void Update(Agreement agreement)
        {
            _repository.Update(_session, agreement);
        }

        public IEnumerable<Agreement> GetBy(Contractor contractor)
        {
            return _repository.GetBy(_session, contractor);
        }

        public IEnumerable<Agreement> GetBy(GasStorage gasStorage)
        {
            return _repository.GetBy(_session, gasStorage);
        }
    }
}