﻿using System.Collections.Generic;
using NHibernate;
using Orms.DataAccessFluentNHibernate.Repositories;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;
using Orms.Providers;

namespace Orms.DataAccessFluentNHibernate.Providers
{
    public class ContractorContactProvider : IContractorContactProvider
    {
        private readonly IContractorContactRepository<ISession> _repository;
        private readonly ISession _session;

        public ContractorContactProvider(ISession session)
        {
            _session = session;
            _repository = new ContractorContactRepository();
        }

        public void Add(ContractorContact contractorContact)
        {
            _repository.Add(_session, contractorContact);
        }

        public IEnumerable<ContractorContact> GetByContractor(Contractor contractor)
        {
            return _repository.GetByContractor(_session, contractor);
        }

        public ContractorContact Get(int id)
        {
            return _repository.Get(_session, id);
        }

        public IEnumerable<ContractorContact> GetAll()
        {
            return _repository.GetAll(_session);
        }

        public void Update(ContractorContact contractorContact)
        {
            _repository.Update(_session, contractorContact);
        }
    }
}