﻿using System.Collections.Generic;
using NHibernate;
using Orms.DataAccessFluentNHibernate.Repositories;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;
using Orms.Providers;

namespace Orms.DataAccessFluentNHibernate.Providers
{
    public class ContractorProvider : IContractorProvider
    {
        private readonly IContractorRepository<ISession> _repository;
        private readonly ISession _session;

        public ContractorProvider(ISession session)
        {
            _session = session;
            _repository = new ContractorRepository();
        }

        public void Add(Contractor contractor)
        {
            _repository.Add(_session, contractor);
        }

        public Contractor Get(int id)
        {
            return _repository.Get(_session, id);
        }

        public IEnumerable<Contractor> GetAll()
        {
            return _repository.GetAll(_session);
        }

        public void Update(Contractor contractor)
        {
            _repository.Update(_session, contractor);
        }
    }
}