﻿using System.Collections.Generic;
using NHibernate;
using Orms.DataAccessFluentNHibernate.Repositories;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;
using Orms.Providers;

namespace Orms.DataAccessFluentNHibernate.Providers
{
    public class AccessPointFlowProvider : IAccessPointFlowProvider
    {
        private readonly IAccessPointFlowRepository<ISession> _repository;
        private readonly ISession _session;

        public AccessPointFlowProvider(ISession session)
        {
            _session = session;
            _repository = new AccessPointFlowRepository();
        }

        public void Add(AccessPointFlow apf)
        {
            _repository.Add(_session, apf);

        }

        public AccessPointFlow Get(int id)
        {
            return _repository.Get(_session, id);
        }

        public IEnumerable<AccessPointFlow> GetAll()
        {
            return _repository.GetAll(_session);
        }

        public void Update(AccessPointFlow apf)
        {
            _repository.Update(_session, apf);
        }
    }
}