﻿using System.Collections.Generic;
using NHibernate;
using Orms.DataAccessFluentNHibernate.Providers;
using Orms.DataAccessHelper;
using Orms.DataAccessHelper.Operations.Interfaces;
using Orms.Models;
using Orms.Services;
using Orms.Services.Interfaces;

namespace Orms.DataAccessFluentNHibernate.Operations
{
    public class GasStorageOperation : Operation<ISession>, IGasStorageOperation
    {
        private IGasStorageService _service;
        private readonly SessionWorker<ISession> _session;

        public GasStorageOperation(SessionWorker<ISession> session)
        {
            _session = session;
        }

        protected override void SetProviders(ISession session)
        {
            _service = new GasStorageService(
                new GasStorageProvider(session));
        }

        public void Add(GasStorage gasStorage)
        {
            using (var session = _session.ReadAndWrite())
            {
                using (var transaction = session.BeginTransaction())
                {
                    SetProviders(session);

                    _service.Add(gasStorage);
                    transaction.Commit();
                }
            }
        }

        public GasStorage Get(int id)
        {
            using (var session = _session.Read())
            {
                SetProviders(session);

                return _service.Get(id);
            }
        }

        public IEnumerable<GasStorage> GetAll()
        {
            using (var session = _session.Read())
            {
                SetProviders(session);

                return _service.GetAll();
            }
        }
    }
}