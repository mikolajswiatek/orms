﻿using System;
using System.Collections.Generic;
using NHibernate;
using Orms.DataAccessFluentNHibernate.Providers;
using Orms.DataAccessHelper;
using Orms.DataAccessHelper.Operations.Interfaces;
using Orms.Models;
using Orms.Services;
using Orms.Services.Interfaces;

namespace Orms.DataAccessFluentNHibernate.Operations
{
    public class NominationOperation : Operation<ISession>, INominationOperation
    {
        private INominationService _service;
        private readonly SessionWorker<ISession> _session;

        public NominationOperation(SessionWorker<ISession> session)
        {
            _session = session;
        }

        protected override void SetProviders(ISession session)
        {
            _service = new NominationService(
                new NominationProvider(session),
                new NominationValueProvider(session));
        }

        public void Add(Nomination nomination)
        {
            using (var session = _session.ReadAndWrite())
            {
                using (var transaction = session.BeginTransaction())
                {
                    SetProviders(session);

                    _service.Add(nomination);
                    transaction.Commit();
                }
            }
        }

        public void Add(IEnumerable<NominationValue> values)
        {
            using (var session = _session.ReadAndWrite())
            {
                using (var transaction = session.BeginTransaction())
                {
                    SetProviders(session);

                    _service.Add(values);
                    transaction.Commit();
                }
            }
        }

        public void Add(Nomination nomination, IEnumerable<NominationValue> values)
        {
            using (var session = _session.ReadAndWrite())
            {
                using (var transaction = session.BeginTransaction())
                {
                    SetProviders(session);

                    _service.Add(nomination, values);
                    transaction.Commit();
                }
            }
        }

        public void RefreshNomiation(Nomination nomination, int parentNominationId)
        {
            using (var session = _session.ReadAndWrite())
            {
                using (var transaction = session.BeginTransaction())
                {
                    SetProviders(session);

                    _service.RefreshNomiation(nomination, parentNominationId);
                    transaction.Commit();
                }
            }
        }

        public void AcceptedNomination(Nomination nomination)
        {
            using (var session = _session.ReadAndWrite())
            {
                using (var transaction = session.BeginTransaction())
                {
                    SetProviders(session);

                    _service.AcceptedNomination(nomination);
                    transaction.Commit();
                }
            }
        }

        public IEnumerable<Nomination> GetBy(Contractor contractor)
        {
            using (var session = _session.Read())
            {
                SetProviders(session);

                return _service.GetBy(contractor);
            }
        }

        public IEnumerable<Nomination> GetBy(GasStorage gasStorage)
        {
            using (var session = _session.Read())
            {
                SetProviders(session);

                return _service.GetBy(gasStorage);
            }
        }

        public IEnumerable<Nomination> GetBy(User user)
        {
            using (var session = _session.Read())
            {
                SetProviders(session);

                return _service.GetBy(user);
            }
        }

        public IEnumerable<Nomination> GetBy(DateTime gasDay)
        {
            using (var session = _session.Read())
            {
                SetProviders(session);

                return _service.GetBy(gasDay);
            }
        }

        public Nomination Get(int id)
        {
            using (var session = _session.Read())
            {
                SetProviders(session);

                return _service.Get(id);
            }
        }

        public Nomination GetLast(Contractor contractor)
        {
            using (var session = _session.Read())
            {
                SetProviders(session);

                return _service.GetLast(contractor);
            }
        }

        public Nomination GetLast(GasStorage gasStorage)
        {
            using (var session = _session.Read())
            {
                SetProviders(session);

                return _service.GetLast(gasStorage);
            }
        }

        public Nomination GetLast(User user)
        {
            using (var session = _session.Read())
            {
                SetProviders(session);

                return _service.GetLast(user);
            }
        }

        IEnumerable<Nomination> INominationOperation.GetForGasDay(Contractor contractor, DateTime gasDay)
        {
            using (var session = _session.Read())
            {
                SetProviders(session);

                return _service.GetForGasDay(contractor, gasDay);
            }
        }

        IEnumerable<Nomination> INominationOperation.GetForGasDay(GasStorage gasStorage, DateTime gasDay)
        {
            using (var session = _session.Read())
            {
                SetProviders(session);

                return _service.GetForGasDay(gasStorage, gasDay);
            }
        }

        IEnumerable<Nomination> INominationOperation.GetForGasDay(Contractor contractor, GasStorage gasStorage, DateTime gasDay)
        {
            using (var session = _session.Read())
            {
                SetProviders(session);

                return _service.GetForGasDay(contractor, gasStorage, gasDay);
            }
        }

        public Nomination GetLast(Contractor contractor, DateTime gasDay)
        {
            using (var session = _session.Read())
            {
                SetProviders(session);

                return _service.GetLast(contractor, gasDay);
            }
        }

        public IEnumerable<Nomination> GetForGasDay(Contractor contractor, DateTime gasDay)
        {
            using (var session = _session.Read())
            {
                SetProviders(session);

                return _service.GetForGasDay(contractor, gasDay);
            }
        }

        public IEnumerable<Nomination> GetForGasDay(GasStorage gasStorage, DateTime gasDay)
        {
            using (var session = _session.Read())
            {
                SetProviders(session);

                return _service.GetForGasDay(gasStorage, gasDay);
            }
        }

        public IEnumerable<Nomination> GetForGasDay(Contractor contractor, GasStorage gasStorage, DateTime gasDay)
        {
            using (var session = _session.Read())
            {
                SetProviders(session);

                return _service.GetForGasDay(contractor, gasStorage, gasDay);
            }
        }
    }
}