﻿using System.Collections.Generic;
using NHibernate;
using Orms.DataAccessFluentNHibernate.Providers;
using Orms.DataAccessHelper;
using Orms.DataAccessHelper.Operations.Interfaces;
using Orms.Models;
using Orms.Services;
using Orms.Services.Interfaces;

namespace Orms.DataAccessFluentNHibernate.Operations
{
    public class UserOperation : Operation<ISession>, IUserOperation
    {
        private IUserService _service;
        private readonly SessionWorker<ISession> _session;

        public UserOperation(SessionWorker<ISession> session)
        {
            _session = session;
        }

        protected override void SetProviders(ISession session)
        {
            _service = new UserService(
                new UserProvider(session),
                new SessionProvider(session));
        }

        public User Exist(string nameOrEmail)
        {
            using (var session = _session.Read())
            {
                SetProviders(session);

                return _service.Exist(nameOrEmail);
            }
        }

        public void Create(User user)
        {
            using (var session = _session.ReadAndWrite())
            {
                using (var transaction = session.BeginTransaction())
                {
                    SetProviders(session);

                    _service.Create(user);
                    transaction.Commit();
                }
            }
        }

        public void Update(User user)
        {
            using (var session = _session.ReadAndWrite())
            {
                using (var transaction = session.BeginTransaction())
                {
                    SetProviders(session);

                    _service.Update(user);
                    transaction.Commit();
                }
            }
        }

        public User Get(int id)
        {
            using (var session = _session.Read())
            {
                SetProviders(session);

                return _service.Get(id);
            }
        }

        public IEnumerable<User> Get(Contractor contractor)
        {
            using (var session = _session.Read())
            {
                SetProviders(session);

                return _service.Get(contractor);
            }
        }

        public IEnumerable<User> GetAll()
        {
            using (var session = _session.Read())
            {
                SetProviders(session);

                return _service.GetAll();
            }
        }
    }
}