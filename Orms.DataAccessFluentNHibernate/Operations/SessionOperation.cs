﻿using NHibernate;
using Orms.DataAccessFluentNHibernate.Providers;
using Orms.DataAccessHelper;
using Orms.DataAccessHelper.Operations.Interfaces;
using Orms.Models;
using Orms.Services;
using Orms.Services.Interfaces;

namespace Orms.DataAccessFluentNHibernate.Operations
{
    public class SessionOperation : Operation<ISession>, ISessionOperation
    {
        private ISessionService _service;
        private readonly SessionWorker<ISession> _session;

        public SessionOperation(SessionWorker<ISession> session)
        {
            _session = session;
        }

        protected override void SetProviders(ISession session)
        {
            _service = new SessionService(
                new SessionProvider(session),
                new UserProvider(session));
        }

        public void Add(Session s)
        {
            using (var session = _session.ReadAndWrite())
            {
                using (var transaction = session.BeginTransaction())
                {
                    SetProviders(session);

                    _service.Add(s);
                    transaction.Commit();
                }
            }
        }

        public void Refresh(string guid)
        {
            using (var session = _session.ReadAndWrite())
            {
                using (var transaction = session.BeginTransaction())
                {
                    SetProviders(session);

                    _service.Refresh(guid);
                    transaction.Commit();
                }
            }
        }

        public Session GetBy(string guid)
        {
            using (var session = _session.Read())
            {
                SetProviders(session);

                return _service.GetBy(guid);
            }
        }

        public void Delete(Session s)
        {
            using (var session = _session.ReadAndWrite())
            {
                using (var transaction = session.BeginTransaction())
                {
                    SetProviders(session);

                    _service.Delete(s);
                    transaction.Commit();
                }
            }
        }
    }
}