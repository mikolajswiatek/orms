﻿using NHibernate;
using NHibernate.Mapping.ByCode;
using Orms.Models;

namespace Orms.DataAccessNHibernate.Map
{
    public class EnergyCalorificValueMapping : EntityMapping<EnergyCalorificValue>
    {
        public EnergyCalorificValueMapping()
        {
            this.Table("CalorificValueValues");
            this.Schema("dbo");
            this.Lazy(true);

            this.Property(x => x.HourIndex, m =>
            {
                m.Column("HourIndex");
                m.NotNullable(true);
                m.Type(NHibernateUtil.Int32);
            });

            this.Component(c => c.Energy, UnitValueMapping.EnergyMapping());

            this.ManyToOne(c => c.CalorificValue, m =>
            {
                m.Column("CalorificValueId");
                m.NotNullable(true);
                m.Cascade(Cascade.All);
            });
        }
    }
}