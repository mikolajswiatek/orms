﻿using NHibernate;
using NHibernate.Mapping.ByCode;
using Orms.Models;

namespace Orms.DataAccessNHibernate.Map
{
    public class ContractorContactMapping : EntityMapping<ContractorContact>
    {
        public ContractorContactMapping()
        {
            this.Table("ContractorContacts");
            this.Lazy(false);

            this.Property(c => c.Adress, m =>
            {
                m.Column("Adress");
                m.NotNullable(true);
                m.Type(NHibernateUtil.String);
            });

            this.Property(c => c.Email, m =>
            {
                m.Column("Email");
                m.NotNullable(true);
                m.Type(NHibernateUtil.String);
            });


            this.Property(c => c.Name, m =>
            {
                m.Column("Name");
                m.NotNullable(true);
                m.Type(NHibernateUtil.String);
            });

            this.Property(c => c.FirstName, m =>
            {
                m.Column("FirstName");
                m.NotNullable(true);
                m.Type(NHibernateUtil.String);
            });

            this.Property(c => c.LastName, m =>
            {
                m.Column("LastName");
                m.NotNullable(true);
                m.Type(NHibernateUtil.String);
            });

            this.Property(c => c.Phone, m =>
            {
                m.Column("[Phone]");
                m.NotNullable(true);
                m.Type(NHibernateUtil.String);
            });

            this.ManyToOne(x => x.Contractor, m =>
            {
                m.Column("ContractorId");
                m.Fetch(FetchKind.Join);
            });
        }
    }
}