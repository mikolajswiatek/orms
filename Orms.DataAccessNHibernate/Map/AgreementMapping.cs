﻿using NHibernate;
using NHibernate.Mapping.ByCode;
using Orms.Models;

namespace Orms.DataAccessNHibernate.Map
{
    public class AgreementMapping : EntityMapping<Agreement>
    {
        public AgreementMapping()
        {
            this.Table("Agreements");
            this.Lazy(false);

            this.Property(c => c.CreateDate, m =>
            {
                m.Column("CreateDatetime");
                m.NotNullable(true);
                m.Type(NHibernateUtil.DateTime);

            });

            this.Property(c => c.Start, m =>
            {
                m.Column("StartDatetime");
                m.NotNullable(true);
                m.Type(NHibernateUtil.DateTime);

            });

            this.Property(c => c.End, m =>
            {
                m.Column("EndDatetime");
                m.NotNullable(true);
                m.Type(NHibernateUtil.DateTime);

            });

            this.ManyToOne(c => c.Contractor, m =>
            {
                m.Column("ContractorId");
                m.Cascade(Cascade.All);
                m.Fetch(FetchKind.Join);
            });

            this.ManyToOne(c => c.GasStorage, m =>
            {
                m.Column("GasStorageId");
                m.Cascade(Cascade.All);
                m.Fetch(FetchKind.Join);
            });
        }
    }
}