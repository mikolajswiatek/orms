﻿using NHibernate;
using NHibernate.Mapping.ByCode;
using Orms.Models;

namespace Orms.DataAccessNHibernate.Map
{
    public class CalorificValueMapping : EntityMapping<CalorificValue>
    {
        public CalorificValueMapping()
        {
            this.Table("CalorificValues");
            this.Lazy(true);

            this.Property(x => x.GasDay, m =>
            {
                m.Column("GasDay");
                m.NotNullable(true);
                m.Type(NHibernateUtil.DateTime);
            });

            this.ManyToOne(c => c.GasStorage, m =>
            {
                m.Column("GasStorageId");
                m.Class(typeof(GasStorage));
                m.Cascade(Cascade.All);
                m.Fetch(FetchKind.Join);
            });

            this.Bag(c => c.Values, m =>
            {
                m.Key(km => km.Column("CalorificValueId"));
                m.Table("CalorificValueValues");
                //m.Fetch(CollectionFetchMode.Join);
                m.Inverse(true);
                m.Cascade(Cascade.All);
            },
            action => action.OneToMany());
        }
    }
}