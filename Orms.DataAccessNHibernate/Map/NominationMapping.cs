﻿using NHibernate;
using NHibernate.Mapping.ByCode;
using Orms.Domains.Enums;
using Orms.Models;

namespace Orms.DataAccessNHibernate.Map
{
    public class NominationMapping : EntityMapping<Nomination>
    {
        public NominationMapping()
        {
            this.Table("Nominations");
            this.Lazy(true);

            this.Property(c => c.Name, m =>
            {
                m.Column("Name");
                m.NotNullable(true);
                m.Type(NHibernateUtil.String);
            });

            this.Property(c => c.Channel, m =>
            {
                m.Column("Channel");
                m.NotNullable(true);
                m.Type<NHibernate.Type.EnumStringType<Channel>>();
            });

            this.Property(c => c.IsAccepted, m =>
            {
                m.Column("IsAccepted");
                m.NotNullable(true);
                m.Type(NHibernateUtil.Boolean);
            });

            this.Property(c => c.IsLast, m =>
            {
                m.Column("IsLast");
                m.NotNullable(true);
                m.Type(NHibernateUtil.Boolean);
            });

            this.Property(c => c.GasDay, m =>
            {
                m.Column("GasDay");
                m.NotNullable(true);
                m.Type(NHibernateUtil.DateTime);
            });

            this.Property(c => c.CreateDateTime, m =>
            {
                m.Column("CreateDatetime");
                m.NotNullable(true);
                m.Type(NHibernateUtil.DateTime);
            });

            this.Property(c => c.MajorVersion, m =>
            {
                m.Column("MajorVersion");
                m.NotNullable(true);
                m.Type(NHibernateUtil.Int32);
            });

            this.Property(c => c.Version, m =>
            {
                m.Column("Version");
                m.NotNullable(true);
                m.Type(NHibernateUtil.Int32);
            });

            this.Property(c => c.Comment, m =>
            {
                m.Column("Comment");
                m.NotNullable(true);
                m.Type(NHibernateUtil.String);
            });

            this.ManyToOne(c => c.AccessPointFlow, m =>
            {
                m.Column("AccessPointFlowId");
            });

            this.ManyToOne(c => c.ParentNomination, m =>
            {
                m.Column("ParentNominationId");
                m.NotNullable(false);
            });

            this.ManyToOne(c => c.Creator, m =>
            {
                m.Column("UserId");
                m.NotNullable(true);
            });

            this.ManyToOne(c => c.Contractor, m =>
            {
                m.Column("ContractorId");
                m.NotNullable(true);
            });

            this.ManyToOne(c => c.CalorificValue, m =>
            {
                m.Column("CalorificValueId");
                m.Cascade(Cascade.All);
                m.NotNullable(true);
            });

            this.Bag(c => c.Values, m =>
            {
                m.Key(km => km.Column("NominationId"));
                m.Inverse(true);
                m.Cascade(Cascade.All);
            },
            action => action.OneToMany());
        }
    }
}
