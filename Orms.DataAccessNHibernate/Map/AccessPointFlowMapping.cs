﻿using NHibernate;
using NHibernate.Mapping.ByCode;
using Orms.Domains.Enums;
using Orms.Models;

namespace Orms.DataAccessNHibernate.Map
{
    public class AccessPointFlowMapping : EntityMapping<AccessPointFlow>
    {
        public AccessPointFlowMapping()
        {
            this.Table("AccessPointFlows");

            this.Schema("dbo");
            this.Lazy(true);

            this.Property(c => c.Direction, m =>
            {
                m.Column("Direction");
                m.NotNullable(true);
                m.Type<NHibernate.Type.EnumStringType<DirectionType>>();
            });

            this.Property(c => c.DirectionString, m =>
            {
                m.Column("Direction");
                m.NotNullable(true);
                m.Update(false);
                m.Insert(false);
            });

            ManyToOne(x => x.AccessPoint, map =>
            {
                map.Column("AccessPointId");
                map.NotNullable(true);
            });
        }
    }
}