﻿using NHibernate;
using NHibernate.Mapping.ByCode;
using Orms.Models;

namespace Orms.DataAccessNHibernate.Map
{
    public class UserMapping : EntityMapping<User>
    {
        public UserMapping()
        {
            this.Table("Users");
            this.Lazy(false);

            this.Property(c => c.Name, m =>
            {
                m.Column("Name");
                m.NotNullable(true);
                m.Type(NHibernateUtil.String);
            });

            this.Property(c => c.Password, m =>
            {
                m.Column("Password");
                m.NotNullable(true);
                m.Type(NHibernateUtil.String);
            });

            this.Property(c => c.Email, m =>
            {
                m.Column("Email");
                m.NotNullable(true);
                m.Type(NHibernateUtil.String);
            });

            this.Property(c => c.IsActive, m =>
            {
                m.Column("IsActive");
                m.NotNullable(true);
                m.Type(NHibernateUtil.Boolean);
            });

            this.Property(c => c.CreateDateTime, m =>
            {
                m.Column("CreateDatetime");
                m.NotNullable(true);
                m.Type(NHibernateUtil.DateTime);
            });

            this.Property(c => c.LastActivityDateTime, m =>
            {
                m.Column("LastActivityDatetime");
                m.NotNullable(true);
                m.Type(NHibernateUtil.DateTime);
            });

            this.ManyToOne(c => c.Contractor, m =>
            {
                m.Column("ContractorId");
                m.Fetch(FetchKind.Join);
            });
        }
    }
}