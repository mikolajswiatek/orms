﻿using NHibernate;
using NHibernate.Mapping.ByCode;
using Orms.Models;

namespace Orms.DataAccessNHibernate.Map
{
    public class ContractorMapping : EntityMapping<Contractor>
    {
        public ContractorMapping()
        {
            this.Table("Contractors");
            //this.Lazy(false);

            this.Property(c => c.Adress, m =>
            {
                m.Column("Adress");
                m.NotNullable(true);
                m.Type(NHibernateUtil.String);

            });

            this.Property(c => c.Code, m =>
            {
                m.Column("Code");
                m.NotNullable(true);
                m.Unique(true);
                m.Type(NHibernateUtil.String);
            });

            this.Property(c => c.Name, m =>
            {
                m.Column("Name");
                m.NotNullable(true);
                m.Type(NHibernateUtil.String);
            });
        }
    }
}