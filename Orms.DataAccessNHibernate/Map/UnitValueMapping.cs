﻿using System;
using NHibernate;
using NHibernate.Mapping.ByCode;
using Orms.Domains.Enums;
using Orms.Domains.Units;

namespace Orms.DataAccessNHibernate.Map
{
    public static class UnitValueMapping
    {
        public static Action<IComponentMapper<UnitValue>> EnergyMapping()
        {
            return c =>
            {
                c.Property(x => x.Value, m =>
                {
                    m.Column("EnergyValue");
                    m.NotNullable(true);
                    m.Type(NHibernateUtil.Decimal);
                });

                c.Property(x => x.Unit, m =>
                {
                    m.Column("EnergyUnit");
                    m.NotNullable(true);
                    m.Type<NHibernate.Type.EnumStringType<Unit>>();
                });
            };
        }


        public static Action<IComponentMapper<UnitValue>> VolumeMapping()
        {
            return c =>
            {
                c.Property(x => x.Value, m =>
                {
                    m.Column("VolumeValue");
                    m.NotNullable(true);
                    m.Type(NHibernateUtil.Decimal);
                });

                c.Property(x => x.Unit, m =>
                {
                    m.Column("VolumeUnit");
                    m.NotNullable(true);
                    m.Type<NHibernate.Type.EnumStringType<Unit>>();
                });
            };
        }
    }
}