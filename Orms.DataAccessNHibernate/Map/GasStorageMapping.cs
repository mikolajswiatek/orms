﻿using NHibernate;
using NHibernate.Mapping.ByCode;
using Orms.Models;

namespace Orms.DataAccessNHibernate.Map
{
    public class GasStorageMapping : EntityMapping<GasStorage>
    {
        public GasStorageMapping()
        {
            this.Table("GasStorages");
            this.Schema("dbo");
            this.Lazy(true);

            Property(x => x.Name, map => map.NotNullable(true));

            Bag(x => x.AccessPoints, colmap =>
            {
                colmap.Key(x => x.Column("GasStorageId"));
                colmap.Inverse(true);
            },
            map =>
            {
                map.OneToMany();
            });
        }
    }
}