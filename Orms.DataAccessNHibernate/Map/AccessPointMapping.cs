﻿using NHibernate;
using NHibernate.Mapping.ByCode;
using Orms.Models;

namespace Orms.DataAccessNHibernate.Map
{
    public class AccessPointMapping : EntityMapping<AccessPoint>
    {
        public AccessPointMapping()
        {
            this.Table("AccessPoints");
            this.Schema("dbo");
            this.Lazy(true);

            this.Property(c => c.Code, m =>
            {
                m.Column("Code");
                m.NotNullable(true);
                m.Type(NHibernateUtil.String);

            });

            this.ManyToOne(x => x.GasStorage, map =>
            {
                map.Column("GasStorageId");
                map.NotNullable(true);
                map.Cascade(Cascade.All);
            });

            Bag(x => x.AccessPointFlows, colmap =>
                {
                    colmap.Key(x => x.Column("AccessPointId"));
                    colmap.Inverse(true);
                },
                map =>
                {
                    map.OneToMany();
                });

        }
    }
}