﻿using NHibernate;
using NHibernate.Mapping.ByCode;
using Orms.Models;

namespace Orms.DataAccessNHibernate.Map
{
    public class NominationValueMapping : EntityMapping<NominationValue>
    {
        public NominationValueMapping()
        {
            this.Table("NominationValues");
            this.Lazy(true);

            this.Component(c => c.Energy, UnitValueMapping.EnergyMapping());

            this.Component(c => c.Volume, UnitValueMapping.VolumeMapping());

            this.Property(c => c.HourIndex, m =>
            {
                m.Column("HourIndex");
                m.NotNullable(true);
                m.Type(NHibernateUtil.Int32);
            });

            this.ManyToOne(c => c.Nomination, m =>
            {
                m.Column("NominationId");
                m.NotNullable(true);
                m.Cascade(Cascade.All);
            });
        }
    }
}