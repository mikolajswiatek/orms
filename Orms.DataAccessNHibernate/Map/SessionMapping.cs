﻿using NHibernate;
using Orms.Models;

namespace Orms.DataAccessNHibernate.Map
{
    public class SessionMapping : EntityMapping<Session>
    {
        public SessionMapping()
        {
            this.Table("Sessions");
            this.Lazy(true);

            this.Property(x => x.Guid, m =>
            {
                m.Column("Guid");
                m.NotNullable(true);
                m.Type(NHibernateUtil.String);
            });

            this.Property(x => x.Ip, m =>
            {
                m.Column("Ip");
                m.NotNullable(true);
                m.Type(NHibernateUtil.String);
            });

            this.Property(x => x.Browser, m =>
            {
                m.Column("Browser");
                m.NotNullable(true);
                m.Type(NHibernateUtil.String);
            });

            this.Property(x => x.CreateDatetime, m =>
            {
                m.Column("CreateDatetime");
                m.NotNullable(true);
                m.Type(NHibernateUtil.DateTime);
            });

            this.Property(x => x.LastActivityDatetime, m =>
            {
                m.Column("LastActivityDatetime");
                m.NotNullable(true);
                m.Type(NHibernateUtil.DateTime);
            });

            this.ManyToOne(c => c.User, m =>
            {
                m.Column("UserId");
            });
        }
    }
}