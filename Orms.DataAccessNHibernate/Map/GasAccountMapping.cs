﻿using NHibernate;
using Orms.Models;

namespace Orms.DataAccessNHibernate.Map
{
    public class GasAccountMapping : EntityMapping<GasAccount>
    {
        public GasAccountMapping()
        {
            this.Table("GasAccounts");
            this.Lazy(true);

            this.Component(c => c.Energy, UnitValueMapping.EnergyMapping());

            this.Component(c => c.Volume, UnitValueMapping.VolumeMapping());

            this.Property(c => c.HourIndex, m =>
            {
                m.Column("HourIndex");
                m.NotNullable(true);
                m.Type(NHibernateUtil.Int32);
            });

            this.Property(x => x.GasDay, m =>
            {
                m.Column("GasDay");
                m.NotNullable(true);
                m.Type(NHibernateUtil.DateTime);
            });

            this.ManyToOne(c => c.GasStorage, m =>
            {
                m.Column("GasStorageId");
                m.NotNullable(true);
            });
        }
    }
}