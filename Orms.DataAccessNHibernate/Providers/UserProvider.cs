﻿using System;
using System.Collections.Generic;
using NHibernate;
using Orms.DataAccessHelper;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.DataAccessNHibernate.Repositories;
using Orms.Models;
using Orms.Providers;

namespace Orms.DataAccessNHibernate.Providers
{
    public class UserProvider : IUserProvider
    {
        private readonly IUserRepository<ISession> _repository;
        private readonly ISession _session;

        public UserProvider(ISession session)
        {
            _session = session;
            _repository = new UserRepository();
        }

        public void Add(User user)
        {
            _repository.Add(_session, user);
        }

        public User Get(int id)
        {
            return _repository.Get(_session, id);
        }

        public IEnumerable<User> GetAll()
        {
            return _repository.GetAll(_session);
        }

        public void Update(User user)
        {
            _repository.Update(_session, user);
        }

        public IEnumerable<User> GetBy(Contractor contractor)
        {
            return _repository.GetBy(_session, contractor);
        }

        public IEnumerable<User> GetBy(string nameOrEmail)
        {
            return _repository.GetBy(_session, nameOrEmail);
        }
    }
}