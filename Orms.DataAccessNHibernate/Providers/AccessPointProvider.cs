﻿using System.Collections.Generic;
using NHibernate;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.DataAccessNHibernate.Repositories;
using Orms.Models;
using Orms.Providers;

namespace Orms.DataAccessNHibernate.Providers
{
    public class AccessPointProvider : IAccessPointProvider
    {
        private readonly IAccessPointRepository<ISession> _repository;
        private readonly ISession _session;

        public AccessPointProvider(ISession session)
        {
            _session = session;
            _repository = new AccessPointRepository();
        }

        public void Add(AccessPoint ap)
        {
            _repository.Add(_session, ap);
        }

        public IEnumerable<AccessPoint> GetBy(GasStorage gasStorage)
        {
            return _repository.GetBy(_session, gasStorage);
        }

        public AccessPoint Get(int id)
        {
            return _repository.Get(_session, id);
        }

        public IEnumerable<AccessPoint> GetAll()
        {
            return _repository.GetAll(_session);
        }

        public void Update(AccessPoint ap)
        {
            _repository.Update(_session, ap);
        }
    }
}