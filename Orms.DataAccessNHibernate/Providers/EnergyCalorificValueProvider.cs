﻿using System.Collections.Generic;
using NHibernate;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.DataAccessNHibernate.Repositories;
using Orms.Models;
using Orms.Providers;

namespace Orms.DataAccessNHibernate.Providers
{
    public class EnergyCalorificValueProvider : IEnergyCalorificValueProvider
    {
        private readonly IEnergyCalorificValueRepository<ISession> _repository;
        private readonly ISession _session;

        public EnergyCalorificValueProvider(ISession session)
        {
            _session = session;
            _repository = new EnergyCalorificValueRepository();
        }

        public IEnumerable<EnergyCalorificValue> GetAll()
        {
            return _repository.GetAll(_session);
        }

        public EnergyCalorificValue Get(int id)
        {
            return _repository.Get(_session, id);
        }

        public void Update(EnergyCalorificValue ecv)
        {
            _repository.Update(_session, ecv);
        }

        public void Add(EnergyCalorificValue ecv)
        {
            _repository.Add(_session, ecv);
        }

        public void Add(IEnumerable<EnergyCalorificValue> ecvs)
        {
            _repository.Add(_session, ecvs);
        }

        public IEnumerable<EnergyCalorificValue> GetAll(CalorificValue cv)
        {
            return _repository.GetAll(_session, cv);
        }
    }
}