﻿using System;
using System.Collections.Generic;
using NHibernate;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.DataAccessNHibernate.Repositories;
using Orms.Models;
using Orms.Providers;

namespace Orms.DataAccessNHibernate.Providers
{
    public class NominationProvider : INominationProvider
    {
        private readonly INominationRepository<ISession> _repository;
        private readonly ISession _session;

        public NominationProvider(ISession session)
        {
            _session = session;
            _repository = new NominationRepository();
        }

        public void Add(Nomination nomination)
        {
            _repository.Add(_session, nomination);
        }

        public Nomination Get(int id)
        {
            return _repository.Get(_session, id);
        }

        public IEnumerable<Nomination> GetAll()
        {
            return _repository.GetAll(_session);
        }

        public void Update(Nomination nomination)
        {
            _repository.Update(_session, nomination);
        }

        public IEnumerable<Nomination> GetBy(Contractor contractor)
        {
            return _repository.GetBy(_session, contractor);
        }

        public IEnumerable<Nomination> GetBy(GasStorage gasStorage)
        {
            return _repository.GetBy(_session, gasStorage);
        }

        public IEnumerable<Nomination> GetBy(User user)
        {
            return _repository.GetBy(_session, user);
        }

        public IEnumerable<Nomination> GetBy(DateTime gasDay)
        {
            return _repository.GetBy(_session, gasDay);
        }

        public Nomination GetLast(Contractor contractor)
        {
            return _repository.GetLast(_session, contractor);
        }

        public Nomination GetLast(GasStorage gasStorage)
        {
            return _repository.GetLast(_session, gasStorage);
        }

        public Nomination GetLast(User user)
        {
            return _repository.GetLast(_session, user);
        }

        public Nomination GetLast(Contractor contractor, DateTime gasDay)
        {
            return _repository.GetLast(_session, contractor, gasDay);
        }

        public IEnumerable<Nomination> GetForGasDay(Contractor contractor, DateTime gasDay)
        {
            return _repository.GetForGasDay(_session, contractor, gasDay);
        }

        public IEnumerable<Nomination> GetForGasDay(GasStorage gasStorage, DateTime gasDay)
        {
            return _repository.GetForGasDay(_session, gasStorage, gasDay);
        }

        public IEnumerable<Nomination> GetForGasDay(Contractor contractor, GasStorage gasStorage, DateTime gasDay)
        {
            return _repository.GetForGasDay(_session, contractor, gasStorage, gasDay);
        }
    }
}