﻿using System.Collections.Generic;
using NHibernate;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.DataAccessNHibernate.Repositories;
using Orms.Models;
using Orms.Providers;

namespace Orms.DataAccessNHibernate.Providers
{
    public class SessionProvider : ISessionProvider
    {
        private readonly ISessionRepository<ISession> _repository;
        private readonly ISession _session;

        public SessionProvider(ISession session)
        {
            _session = session;
            _repository = new SessionRepository();

        }

        public void Add(Session s)
        {
            _repository.Add(_session, s);
        }

        public Session GetBy(string guid)
        {
            return _repository.GetBy(_session, guid);
        }

        public void Delete(Session s)
        {
            _repository.Delete(_session, s);
        }

        public Session Get(int id)
        {
            return _repository.Get(_session, id);
        }

        public IEnumerable<Session> GetAll()
        {
            return _repository.GetAll(_session);
        }

        public void Update(Session s)
        {
            _repository.Update(_session, s);
        }
    }
}