﻿using System;
using System.Collections.Generic;
using NHibernate;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.DataAccessNHibernate.Repositories;
using Orms.Models;
using Orms.Providers;

namespace Orms.DataAccessNHibernate.Providers
{
    public class CalorificValueProvider : ICalorificValueProvider
    {
        private readonly ICalorificValueRepository<ISession> _repository;
        private readonly ISession _session;

        public CalorificValueProvider(ISession session)
        {
            _session = session;
            _repository = new CalorificValueRepository();
        }

        public IEnumerable<CalorificValue> GetAll()
        {
            return _repository.GetAll(_session);
        }

        public CalorificValue Get(int id)
        {
            return _repository.Get(_session, id);
        }

        public void Update(CalorificValue cv)
        {
            _repository.Update(_session, cv);
        }

        public void Add(CalorificValue cv)
        {
            _repository.Add(_session, cv);
        }

        public CalorificValue GetLastBy(GasStorage gasStorage)
        {
            return _repository.GetLastBy(_session, gasStorage);
        }

        public CalorificValue GetForGasDay(GasStorage gasStorage, DateTime gasDay)
        {
            return _repository.GetForGasDay(_session, gasStorage, gasDay);
        }
    }
}