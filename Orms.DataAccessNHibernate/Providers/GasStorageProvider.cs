﻿using System.Collections.Generic;
using NHibernate;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.DataAccessNHibernate.Repositories;
using Orms.Models;
using Orms.Providers;

namespace Orms.DataAccessNHibernate.Providers
{
    public class GasStorageProvider : IGasStorageProvider
    {
        private readonly IGasStorageRepository<ISession> _repository;
        private readonly ISession _session;

        public GasStorageProvider(ISession session)
        {
            _session = session;
            _repository = new GasStorageRepository();
        }

        public void Add(GasStorage gasStorage)
        {
            _repository.Add(_session, gasStorage);
        }

        public GasStorage Get(int id)
        {
            return _repository.Get(_session, id);
        }

        public IEnumerable<GasStorage> GetAll()
        {
            return _repository.GetAll(_session);
        }

        public void Update(GasStorage gasStorage)
        {
            _repository.Update(_session, gasStorage);
        }
    }
}