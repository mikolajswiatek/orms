﻿using NHibernate;
using Orms.Models;
using System.Collections.Generic;
using Orms.DataAccessHelper.Repositories.Interfaces;

namespace Orms.DataAccessNHibernate.Repositories
{
    public class UserRepository : IUserRepository<ISession>
    {
        public void Add(ISession session, User user)
        {
            session.Save(user);
        }

        public void Delete(ISession session, User user)
        {
            session.Delete(user);
        }

        public User Get(ISession session, int id)
        {
            var user = session.Get<User>(id);

            return user;
        }

        public IEnumerable<User> GetAll(ISession session)
        {
            var users = session
                .CreateCriteria(typeof(User))
                .List<User>();

            return users;
        }

        public void Update(ISession session, User user)
        {
            session.Update(user);
        }

        public IEnumerable<User> GetBy(ISession session, Contractor contractor)
        {
            var users = session
                .QueryOver<User>()
                .Where(u => u.Contractor.Id == contractor.Id)
                .List<User>();

            return users;
        }

        public IEnumerable<User> GetBy(ISession session, string nameOrEmail)
        {
            var users = session
                .QueryOver<User>()
                .Where(u => u.Name == nameOrEmail || u.Email == nameOrEmail)
                .List<User>();

            return users;
        }
    }
}
