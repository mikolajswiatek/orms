﻿using System;
using NHibernate;
using Orms.Models;
using System.Collections.Generic;
using NHibernate.Criterion;
using Orms.DataAccessHelper.Repositories.Interfaces;

namespace Orms.DataAccessNHibernate.Repositories
{
    public class NominationValueRepository : INominationValueRepository<ISession>
    {
        public void Add(ISession session, NominationValue nominationValue)
        {
            session.Save(nominationValue);
        }

        public void Delete(ISession session, NominationValue nominationValue)
        {
            session.Delete(nominationValue);
        }

        public NominationValue Get(ISession session, int id)
        {
            var nominationValue = session.Get<NominationValue>(id);

            return nominationValue;
        }

        public IEnumerable<NominationValue> GetAll(ISession session)
        {
            var nominationValues = session
                .CreateCriteria(typeof(NominationValue))
                .List<NominationValue>();

            return nominationValues;
        }

        public void Update(ISession session, NominationValue nominationValue)
        {
            session.Update(nominationValue);
        }

        public void Add(ISession session, IEnumerable<NominationValue> values)
        {
            foreach (var nominationValue in values)
            {
                session.Save(nominationValue);
            }
        }

        public IEnumerable<NominationValue> GetBy(ISession session, Nomination nomination)
        {
            var nominationValues = session
                .CreateCriteria(typeof(NominationValue))
                .Add(Restrictions.Where<NominationValue>(nv => nv.Nomination.Id == nomination.Id))
                .List<NominationValue>();

            return nominationValues;
        }
    }
}
