﻿using System;
using System.Collections.Generic;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;

namespace Orms.DataAccessNHibernate.Repositories
{
    public class GasAccountRepository : IGasAccountRepository<ISession>
    {
        public void Add(ISession session, GasAccount gasAccount)
        {
            session.Save(gasAccount);
        }

        public void Delete(ISession session, GasAccount gasAccount)
        {
            session.Delete(gasAccount);
        }

        public GasAccount Get(ISession session, int id)
        {
            var gasAccount = session.Get<GasAccount>(id);

            return gasAccount;
        }

        public IEnumerable<GasAccount> GetAll(ISession session)
        {
            var gasAccounts = session
                .CreateCriteria(typeof(GasAccount))
                .List<GasAccount>();

            return gasAccounts;
        }

        public void Update(ISession session, GasAccount gasAccount)
        {
            session.Update(gasAccount);
        }

        public void Add(ISession session, IEnumerable<GasAccount> gasAccounts)
        {
            foreach (var gasAccount in gasAccounts)
            {
                session.Save(gasAccount);
            }
        }

        public void Update(ISession session, IEnumerable<GasAccount> gasAccounts)
        {
            foreach (var gasAccount in gasAccounts)
            {
                session.Update(gasAccount);
            }
        }

        public IEnumerable<GasAccount> Get(ISession session, GasStorage gasStorage, DateTime gasDay)
        {
            var gasAccounts = session
                .QueryOver<GasAccount>()
                .Where(ga => ga.GasStorage.Id == gasStorage.Id && ga.GasDay == gasDay)
                .Fetch(ga => ga.GasStorage).Eager
                .TransformUsing(Transformers.DistinctRootEntity)
                .List<GasAccount>();

            return gasAccounts;
        }

        public IEnumerable<GasAccount> Get(ISession session, DateTime gasDay)
        {
            var gasAccounts = session
                .QueryOver<GasAccount>()
                .Where(ga => ga.GasDay == gasDay)
                .Fetch(ga => ga.GasStorage).Eager
                .TransformUsing(Transformers.DistinctRootEntity)
                .List<GasAccount>();

            return gasAccounts;
        }
    }
}
