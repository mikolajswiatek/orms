﻿using System;
using NHibernate;
using Orms.Models;
using System.Collections.Generic;
using NHibernate.Criterion;
using Orms.DataAccessHelper.Repositories.Interfaces;

namespace Orms.DataAccessNHibernate.Repositories
{
    public class EnergyCalorificValueRepository : IEnergyCalorificValueRepository<ISession>
    {
        public IEnumerable<EnergyCalorificValue> GetAll(ISession session)
        {
            var ecvs = session
                .CreateCriteria(typeof(EnergyCalorificValue))
                .List<EnergyCalorificValue>();

            return ecvs;
        }

        public EnergyCalorificValue Get(ISession session, int id)
        {
            var ecv = session.Get<EnergyCalorificValue>(id);

            return ecv;
        }

        public void Update(ISession session, EnergyCalorificValue ecv)
        {
            session.Update(ecv);
        }

        public void Add(ISession session, EnergyCalorificValue ecv)
        {
            Add(session, new List<EnergyCalorificValue>() { ecv });
        }

        public void Add(ISession session, IEnumerable<EnergyCalorificValue> ecvs)
        {
            try
            {

                foreach (var val in ecvs)
                {
                    session.Save(val);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public IEnumerable<EnergyCalorificValue> GetAll(ISession session, CalorificValue cv)
        {
            var ecvs = session.QueryOver<EnergyCalorificValue>()
                .Where(ecv => ecv.CalorificValue.Id == cv.Id)
                .List();

            return ecvs;
        }
    }
}
