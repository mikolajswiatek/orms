﻿using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Transform;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;

namespace Orms.DataAccessNHibernate.Repositories
{
    public class AccessPointRepository : IAccessPointRepository<ISession>
    {
        public void Add(ISession session, AccessPoint ap)
        {
            session.Save(ap);
        }

        public void Delete(ISession session, AccessPoint ap)
        {
            session.Delete(ap);
        }

        public AccessPoint Get(ISession session, int id)
        {
            return session.Get<AccessPoint>(id);
        }

        public IEnumerable<AccessPoint> GetAll(ISession session)
        {
            return session.CreateCriteria<AccessPoint>().List<AccessPoint>();
        }

        public void Update(ISession session, AccessPoint ap)
        {
            session.Update(ap);
        }

        public IEnumerable<AccessPoint> GetBy(ISession session, GasStorage gasStorage)
        {
            var aps = session
                .QueryOver<AccessPoint>()
                .Fetch(ap => ap.AccessPointFlows).Eager
                .TransformUsing(Transformers.DistinctRootEntity)
                .Where(a => a.GasStorage.Id == gasStorage.Id)
                .List<AccessPoint>()
                .ToList();

            return aps;
        }
    }
}
