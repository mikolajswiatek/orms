﻿using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Transform;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;

namespace Orms.DataAccessNHibernate.Repositories
{
    public class NominationRepository : INominationRepository<ISession>
    {
        public void Add(ISession session, Nomination nomination)
        {
            session.Save(nomination);
        }

        public void Delete(ISession session, Nomination nomination)
        {
            session.Delete(nomination);
        }

        public Nomination Get(ISession session, int id)
        {
            var nomination = session.QueryOver<Nomination>()
                .Where(n => n.Id == id)
                .Fetch(n => n.Contractor).Eager
                .Fetch(n => n.AccessPointFlow).Eager
                .Fetch(n => n.AccessPointFlow.AccessPoint).Eager
                .Fetch(n => n.AccessPointFlow.AccessPoint.GasStorage).Eager
                .Fetch(n => n.Creator).Eager
                .Fetch(n => n.Values).Eager
                .Fetch(n => n.CalorificValue).Eager
                .TransformUsing(Transformers.DistinctRootEntity)
                .SingleOrDefault<Nomination>();

            return nomination;
        }

        public IEnumerable<Nomination> GetAll(ISession session)
        {
            var nominations = session
                .QueryOver<Nomination>()
                .Fetch(n => n.Contractor).Eager
                .Fetch(n => n.AccessPointFlow).Eager
                .Fetch(n => n.AccessPointFlow.AccessPoint).Eager
                .Fetch(n => n.AccessPointFlow.AccessPoint.GasStorage).Eager
                .Fetch(n => n.Creator).Eager
                .Fetch(n => n.Values).Eager
                .Fetch(n => n.CalorificValue).Eager
                .TransformUsing(Transformers.DistinctRootEntity)
                .List<Nomination>();

            return nominations;
        }

        public void Update(ISession session, Nomination nomination)
        {
            session.Update(nomination);
        }

        public IEnumerable<Nomination> GetBy(ISession session, Contractor contractor)
        {
            var nominations = session
                .QueryOver<Nomination>()
                .Where(n => n.Creator.Id == contractor.Id)
                .List<Nomination>();

            return nominations;
        }

        public IEnumerable<Nomination> GetBy(ISession session, GasStorage gasStorage)
        {
            var nominations = session
                .QueryOver<Nomination>()
                .Where(n => n.AccessPointFlow.AccessPoint.GasStorage.Id == gasStorage.Id)
                .List<Nomination>();

            return nominations;
        }

        public IEnumerable<Nomination> GetBy(ISession session, User user)
        {
            var nominations = session
                .QueryOver<Nomination>()
                .Where(n => n.Creator.Id == user.Id)
                .List<Nomination>();

            return nominations;
        }

        public IEnumerable<Nomination> GetBy(ISession session, DateTime gasDay)
        {
            var nominations = session
                .QueryOver<Nomination>()
                .Where(n => n.GasDay == gasDay)
                .Fetch(n => n.Contractor).Eager
                .Fetch(n => n.AccessPointFlow).Eager
                .Fetch(n => n.AccessPointFlow.AccessPoint).Eager
                .Fetch(n => n.AccessPointFlow.AccessPoint.GasStorage).Eager
                .Fetch(n => n.Creator).Eager
                .Fetch(n => n.Values).Eager
                .Fetch(n => n.CalorificValue).Eager
                .TransformUsing(Transformers.DistinctRootEntity)
                .List<Nomination>();

            return nominations;
        }

        public Nomination GetLast(ISession session, Contractor contractor)
        {
            var nomination = session
                .QueryOver<Nomination>()
                .Where(n => n.Contractor.Id == contractor.Id)
                .OrderBy(n => n.Id).Desc
                .Take(1)
                .SingleOrDefault<Nomination>();

            return nomination;
        }

        public Nomination GetLast(ISession session, GasStorage gasStorage)
        {
            var nomination = session
                .QueryOver<Nomination>()
                .Where(n => n.AccessPointFlow.AccessPoint.GasStorage.Id == gasStorage.Id)
                .OrderBy(n => n.Id).Desc
                .Take(1)
                .SingleOrDefault<Nomination>();

            return nomination;
        }

        public Nomination GetLast(ISession session, User user)
        {
            var nomination = session
                .QueryOver<Nomination>()
                .Where(n => n.Creator.Id == user.Id)
                .OrderBy(n => n.Id).Desc
                .Take(1)
                .SingleOrDefault<Nomination>();

            return nomination;
        }

        public Nomination GetLast(ISession session, Contractor contractor, DateTime gasDay)
        {
            var nomination = session
                .QueryOver<Nomination>()
                .Where(n => n.Contractor.Id == contractor.Id && n.IsLast && n.GasDay == gasDay)
                .OrderBy(n => n.Id).Desc
                .Take(1)
                .SingleOrDefault<Nomination>();

            return nomination;
        }

        public IEnumerable<Nomination> GetForGasDay(ISession session, Contractor contractor, DateTime gasDay)
        {
            var nominations = session
                .QueryOver<Nomination>()
                .Where(n => n.Contractor.Id == contractor.Id && n.GasDay == gasDay)
                .OrderBy(n => n.Id).Desc
                .Take(1)
                .List<Nomination>();

            return nominations;
        }

        public IEnumerable<Nomination> GetForGasDay(ISession session, GasStorage gasStorage, DateTime gasDay)
        {
            var aps = session.QueryOver<AccessPoint>()
                .Where(ap => ap.GasStorage.Id == gasStorage.Id)
                .Fetch(ap => ap.AccessPointFlows).Eager
                .TransformUsing(Transformers.DistinctRootEntity)
                .List<AccessPoint>();

            var ids = new List<int>();

            foreach (var ap in aps)
            {
                foreach (var apf in ap.AccessPointFlows)
                {
                    ids.Add(apf.Id);
                }
            }

            var nominations = session
                .QueryOver<Nomination>()
                .Where(n => n.GasDay == gasDay && n.IsAccepted && n.IsLast)
                .OrderBy(n => n.Id).Desc
                .List<Nomination>();

            nominations = nominations.Where(n => ids.Contains(n.AccessPointFlow.Id)).ToList();

            return nominations;
        }

        public IEnumerable<Nomination> GetForGasDay(ISession session, Contractor contractor, GasStorage gasStorage, DateTime gasDay)
        {
            var aps = session.QueryOver<AccessPoint>()
                .Where(ap => ap.GasStorage.Id == gasStorage.Id)
                .Fetch(ap => ap.AccessPointFlows).Eager
                .TransformUsing(Transformers.DistinctRootEntity)
                .List<AccessPoint>();

            var ids = new List<int>();

            foreach (var ap in aps)
            {
                foreach (var apf in ap.AccessPointFlows)
                {
                    ids.Add(apf.Id);
                }
            }

            var nominations = session
                .QueryOver<Nomination>()
                .Where(n => n.Contractor.Id == contractor.Id && n.GasDay == gasDay)
                .OrderBy(n => n.Id).Desc
                .List<Nomination>();

            nominations = nominations.Where(n => ids.Contains(n.AccessPointFlow.Id)).ToList();

            return nominations;
        }
    }
}
