﻿using NHibernate;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;
using System.Collections.Generic;

namespace Orms.DataAccessNHibernate.Repositories
{
    public class ContractorRepository : IContractorRepository<ISession>
    {
        public void Add(ISession session, Contractor contractor)
        {
            session.Save(contractor);
        }

        public void Delete(ISession session, Contractor contractor)
        {
            session.Delete(contractor);
        }

        public Contractor Get(ISession session, int id)
        {
            var contractor = session.Get<Contractor>(id);

            return contractor;
        }

        public IEnumerable<Contractor> GetAll(ISession session)
        {
            var contractors = session
                .CreateCriteria(typeof(Contractor))
                .List<Contractor>();

            return contractors;
        }

        public void Update(ISession session, Contractor contractor)
        {
            session.Update(contractor);
        }
    }
}