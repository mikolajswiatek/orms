﻿using NHibernate;
using Orms.Models;
using System.Collections.Generic;
using NHibernate.Criterion;
using Orms.DataAccessHelper.Repositories.Interfaces;

namespace Orms.DataAccessNHibernate.Repositories
{
    public class ContractorContactRepository : IContractorContactRepository<ISession>
    {
        public void Add(ISession session, ContractorContact contractorContact)
        {
            session.Save(contractorContact);
        }

        public void Delete(ISession session, ContractorContact contractorContact)
        {
            session.Delete(contractorContact);
        }

        public ContractorContact Get(ISession session, int id)
        {
            return session.Get<ContractorContact>(id);
        }

        public IEnumerable<ContractorContact> GetAll(ISession session)
        {
            return session.CreateCriteria<ContractorContact>().List<ContractorContact>();
        }

        public void Update(ISession session, ContractorContact contractorContact)
        {
            session.Update(contractorContact);
        }

        public IEnumerable<ContractorContact> GetByContractor(ISession session, Contractor contractor)
        {
            var results = session
                .QueryOver<ContractorContact>()
                //.Where(cc => cc.Contractor.Id == contractor.Id)
                .List<ContractorContact>();

            return results;
        }
    }
}
