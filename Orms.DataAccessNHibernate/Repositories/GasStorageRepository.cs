﻿using NHibernate;
using Orms.Models;
using System.Collections.Generic;
using NHibernate.Transform;
using Orms.DataAccessHelper.Repositories.Interfaces;

namespace Orms.DataAccessNHibernate.Repositories
{
    public class GasStorageRepository : IGasStorageRepository<ISession>
    {
        public void Add(ISession session, GasStorage gasStorage)
        {
            session.Save(gasStorage);
        }

        public void Delete(ISession session, GasStorage gasStorage)
        {
            session.Delete(gasStorage);
        }

        public GasStorage Get(ISession session, int id)
        {
            var gasStorage = session.Get<GasStorage>(id);
            gasStorage = gasStorage.Clone() as GasStorage;

            return gasStorage;
        }

        public IEnumerable<GasStorage> GetAll(ISession session)
        {
            var gasStorages = session
                .QueryOver<GasStorage>()
                .Fetch(ap => ap.AccessPoints).Eager
                .TransformUsing(Transformers.DistinctRootEntity)
                .List<GasStorage>();

            return gasStorages;
        }

        public void Update(ISession session, GasStorage gasStorage)
        {
            session.Update(gasStorage);
        }
    }
}
