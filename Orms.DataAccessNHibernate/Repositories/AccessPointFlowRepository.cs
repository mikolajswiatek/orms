﻿using System.Collections.Generic;
using NHibernate;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;

namespace Orms.DataAccessNHibernate.Repositories
{
    public class AccessPointFlowRepository : IAccessPointFlowRepository<ISession>
    {
        public IEnumerable<AccessPointFlow> GetAll(ISession session)
        {
            return session.CreateCriteria<AccessPointFlow>().List<AccessPointFlow>();
        }

        public AccessPointFlow Get(ISession session, int id)
        {
            return session.Get<AccessPointFlow>(id);
        }

        public void Update(ISession session, AccessPointFlow apf)
        {
            session.Update(apf);
        }

        public void Add(ISession session, AccessPointFlow apf)
        {
            session.Save(apf);
        }

        public void Delete(ISession session, AccessPointFlow apf)
        {
            session.Delete(apf);
        }
    }
}
