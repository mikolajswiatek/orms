﻿using System.Collections.Generic;
using NHibernate;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;

namespace Orms.DataAccessNHibernate.Repositories
{
    public class SessionRepository : ISessionRepository<ISession>
    {
        public void Add(ISession session, Session s)
        {
            session.Save(s);
        }

        public Session Get(ISession session, int id)
        {
            var s = session.Get<Session>(id);

            return s;
        }

        public IEnumerable<Session> GetAll(ISession session)
        {
            var ss = session
                .CreateCriteria(typeof(Session))
                .List<Session>();

            return ss;
        }

        public void Update(ISession session, Session s)
        {
            session.Update(s);
        }

        public Session GetBy(ISession session, string guid)
        {
            var result = session
                .QueryOver<Orms.Models.Session>()
                .Where(s => s.Guid == guid)
                .SingleOrDefault();

            return result;
        }

        public void Delete(ISession session, Session s)
        {
            session.Delete(s);
        }
    }
}
