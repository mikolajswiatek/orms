﻿using NHibernate;
using NHibernate.Cfg;
using NHibernate.Dialect;
using NHibernate.Mapping.ByCode;
using Orms.DataAccessHelper;
using System.Reflection;

namespace Orms.DataAccessNHibernate
{
    public class NHibernateHelper : SessionWorker<ISession>
    {
        private Configuration Configuration { get; }
        private ISessionFactory Session { get; set; }

        public NHibernateHelper(string connectionString) : base(connectionString)
        {
            Configuration = new Configuration();
        }

        public override void OpenSession()
        {
            Configuration.DataBaseIntegration(c =>
            {
                c.Dialect<MsSql2012Dialect>();
                c.ConnectionString = _connectionString;
                c.KeywordsAutoImport = Hbm2DDLKeyWords.AutoQuote;
                c.LogSqlInConsole = true;
                c.LogFormattedSql = true;
               // c.SchemaAction = SchemaAutoAction.Create;
            });

            var mapper = new ModelMapper();
            mapper.AddMappings(Assembly.GetExecutingAssembly().GetExportedTypes());
            var domainMapping = mapper.CompileMappingForAllExplicitlyAddedEntities();
            Configuration.AddMapping(domainMapping);

            Session = Configuration.BuildSessionFactory();
            isOpen = true;
        }

        public override void CloseSession()
        {
            Session.Close();
            Session.Dispose();
            isOpen = false;
        }

        public override ISession Read()
        {
            var session = Session.OpenSession();
            session.FlushMode = FlushMode.Never;
            session.DefaultReadOnly = true;

            return session;
        }

        public override ISession ReadAndWrite()
        {
            var session = Session.OpenSession();
            session.FlushMode = FlushMode.Commit;
            session.DefaultReadOnly = false;

            return session;
        }

        public override void SaveChanges()
        {
            throw new System.NotImplementedException();
        }
    }
}
