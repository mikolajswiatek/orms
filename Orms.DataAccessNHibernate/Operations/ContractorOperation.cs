﻿using System.Collections.Generic;
using System.Data.SqlClient;
using NHibernate;
using Orms.DataAccessHelper;
using Orms.DataAccessHelper.Operations.Interfaces;
using Orms.DataAccessNHibernate.Providers;
using Orms.Models;
using Orms.Services;
using Orms.Services.Interfaces;

namespace Orms.DataAccessNHibernate.Operations
{
    public class ContractorOperation : Operation<ISession>, IContractorOperation
    {
        private IContractorService _service;
        private readonly SessionWorker<ISession> _session;

        public ContractorOperation(SessionWorker<ISession> session)
        {
            _session = session;
        }

        protected override void SetProviders(ISession session)
        {
            _service = new ContractorService(
                new ContractorContactProvider(session),
                new ContractorProvider(session));
        }

        public IEnumerable<Contractor> GetAll()
        {
            using (var session = _session.Read())
            {
                SetProviders(session);

                return _service.GetAll();
            }
        }

        public IEnumerable<ContractorContact> GetAll(Contractor contractor)
        {
            using (var session = _session.Read())
            {
                SetProviders(session);

                return _service.GetAll(contractor);
            }
        }

        public Contractor Get(int id)
        {
            using (var session = _session.Read())
            {
                SetProviders(session);

                return _service.Get(id);
            }
        }

        public ContractorContact GetContact(int id)
        {
            using (var session = _session.Read())
            {
                SetProviders(session);

                return _service.GetContact(id);
            }
        }

        public void Update(ContractorContact contact)
        {
            using (var session = _session.ReadAndWrite())
            {
                SetProviders(session);

                _service.Update(contact);
            }
        }

        public void Add(Contractor contractor)
        {
            using (var session = _session.ReadAndWrite())
            {
                using (var transaction = session.BeginTransaction())
                {
                    SetProviders(session);

                    _service.Add(contractor);
                    transaction.Commit();
                }
            }
        }

        public void Add(ContractorContact contact, int contractorId)
        {
            using (var session = _session.ReadAndWrite())
            {
                using (var transaction = session.BeginTransaction())
                {
                    SetProviders(session);

                    _service.Add(contact, contractorId);
                    transaction.Commit();
                }
            }
        }
    }
}