﻿using System;
using System.Collections.Generic;
using NHibernate;
using Orms.DataAccessHelper;
using Orms.DataAccessHelper.Operations.Interfaces;
using Orms.DataAccessNHibernate.Providers;
using Orms.Models;
using Orms.Services;
using Orms.Services.Interfaces;

namespace Orms.DataAccessNHibernate.Operations
{
    public class AccessPointOperation : Operation<ISession>, IAccessPointOperation
    {
        private IAccessPointService _service;
        private readonly SessionWorker<ISession> _session;

        public AccessPointOperation(SessionWorker<ISession> session)
        {
            _session = session;
        }

        protected override void SetProviders(ISession session)
        {
            _service = new AccessPointService(
                new AccessPointProvider(session),
                new AccessPointFlowProvider(session));
        }

        public IEnumerable<AccessPoint> GetBy(GasStorage gasStorage)
        {
            using (var session = _session.Read())
            {
                SetProviders(session);

                return _service.GetBy(gasStorage);
            }
        }

        public IEnumerable<AccessPoint> GetAll()
        {
            using (var session = _session.Read())
            {
                SetProviders(session);

                return _service.GetAll();
            }
        }

        public AccessPoint Get(int id)
        {
            using (var session = _session.Read())
            {
                SetProviders(session);

                return _service.Get(id);
            }
        }

        public void Add(AccessPoint accessPoint)
        {
            using (var session = _session.ReadAndWrite())
            {
                using (var transaction = session.BeginTransaction())
                {
                    SetProviders(session);

                    _service.Add(accessPoint);
                    transaction.Commit();
                }
            }
        }

        public void Add(AccessPointFlow apf, int accessPointId)
        {
            using (var session = _session.ReadAndWrite())
            {
                using (var transaction = session.BeginTransaction())
                {
                    SetProviders(session);

                    _service.Add(apf, accessPointId);
                    transaction.Commit();
                }
            }
        }
    }
}