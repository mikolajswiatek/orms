﻿using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using Orms.DataAccessHelper;
using Orms.DataAccessHelper.Operations.Interfaces;
using Orms.DataAccessNHibernate.Providers;
using Orms.Models;
using Orms.Services;
using Orms.Services.Interfaces;

namespace Orms.DataAccessNHibernate.Operations
{
    public class CalorificValueOperation : Operation<ISession>, ICalorificValueOperation
    {
        private ICalorificValueService _service;
        private readonly SessionWorker<ISession> _session;

        public CalorificValueOperation(SessionWorker<ISession> session)
        {
            _session = session;
        }

        protected override void SetProviders(ISession session)
        {
            _service = new CalorificValueService(new CalorificValueProvider(session));
        }

        public void Add(CalorificValue cv, IEnumerable<EnergyCalorificValue> values)
        {
            using (var session = _session.ReadAndWrite())
            {
                using (var transaction = session.BeginTransaction())
                {
                    SetProviders(session);

                    cv.Values = cv.Values.Select(v => new EnergyCalorificValue()
                    {
                        CalorificValue = cv,
                        HourIndex = v.HourIndex,
                        Energy = v.Energy
                    });

                    _service.Add(cv, values);
                    transaction.Commit();
                }
            }
        }

        public CalorificValue GetLastBy(GasStorage gasStorage)
        {
            using (var session = _session.Read())
            {
                SetProviders(session);

                return _service.GetLastBy(gasStorage);
            }
        }

        public CalorificValue GetForGasDay(GasStorage gasStorage, DateTime gasDay)
        {
            using (var session = _session.Read())
            {
                SetProviders(session);

                return _service.GetForGasDay(gasStorage, gasDay);
            }
        }

        public CalorificValue Get(int id)
        {
            using (var session = _session.Read())
            {
                SetProviders(session);

                return _service.Get(id);
            }
        }
    }
}