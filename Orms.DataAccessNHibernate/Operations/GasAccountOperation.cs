﻿using System;
using System.Collections.Generic;
using NHibernate;
using Orms.DataAccessHelper;
using Orms.DataAccessHelper.Operations.Interfaces;
using Orms.DataAccessNHibernate.Providers;
using Orms.Models;
using Orms.Services;
using Orms.Services.Interfaces;

namespace Orms.DataAccessNHibernate.Operations
{
    public class GasAccountOperation : Operation<ISession>, IGasAccountOperation
    {
        private IGasAccountService _service;
        private readonly SessionWorker<ISession> _session;

        public GasAccountOperation(SessionWorker<ISession> session)
        {
            _session = session;
        }

        protected override void SetProviders(ISession session)
        {
            _service = new GasAccountService(
                new GasAccountProvider(session),
                new GasStorageProvider(session),
                new NominationProvider(session));
        }

        public void Update(IEnumerable<GasAccount> gasAccounts)
        {
            using (var session = _session.ReadAndWrite())
            {
                using (var transaction = session.BeginTransaction())
                {
                    SetProviders(session);

                    _service.Update(gasAccounts);
                    transaction.Commit();
                }
            }
        }

        public IEnumerable<GasAccount> Get(DateTime gasDay)
        {
            using (var session = _session.Read())
            {
                SetProviders(session);

                return _service.Get(gasDay);
            }
        }

        public IEnumerable<GasAccount> Get(GasStorage gasStorage, DateTime gasDay)
        {
            using (var session = _session.Read())
            {
                SetProviders(session);

                return _service.Get(gasStorage, gasDay);
            }
        }

        public void Refresh(GasStorage gasStorage)
        {
            using (var session = _session.ReadAndWrite())
            {
                using (var transaction = session.BeginTransaction())
                {
                    SetProviders(session);

                    _service.Refresh(gasStorage);
                    transaction.Commit();
                }
            }
        }

        public void Refresh()
        {
            using (var session = _session.ReadAndWrite())
            {
                using (var transaction = session.BeginTransaction())
                {
                    SetProviders(session);

                    _service.Refresh();
                    transaction.Commit();
                }
            }
        }

        public void Refresh(GasStorage gasStorage, GasDay gasDay)
        {
            using (var session = _session.ReadAndWrite())
            {
                using (var transaction = session.BeginTransaction())
                {
                    SetProviders(session);

                    _service.Refresh(gasStorage, gasDay);
                    transaction.Commit();
                }
            }
        }

        public void Refresh(GasDay gasDay)
        {
            using (var session = _session.ReadAndWrite())
            {
                using (var transaction = session.BeginTransaction())
                {
                    SetProviders(session);

                    _service.Refresh(gasDay);
                    transaction.Commit();
                }
            }
        }
    }
}