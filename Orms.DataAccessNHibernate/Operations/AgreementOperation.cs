﻿using System.Collections.Generic;
using NHibernate;
using Orms.DataAccessHelper;
using Orms.DataAccessHelper.Operations.Interfaces;
using Orms.DataAccessNHibernate.Providers;
using Orms.Models;
using Orms.Services;
using Orms.Services.Interfaces;

namespace Orms.DataAccessNHibernate.Operations
{
    public class AgreementOperation : Operation<ISession>, IAgreementOperation
    {
        private IAgreementService _service;
        private readonly SessionWorker<ISession> _session;

        public AgreementOperation(SessionWorker<ISession> session)
        {
            _session = session;
        }

        protected override void SetProviders(ISession session)
        {
            _service = new AgreementService(
                new AgreementProvider(session));
        }

        public void Add(Agreement agreement)
        {
            using (var session = _session.ReadAndWrite())
            {
                using (var transaction = session.BeginTransaction())
                {
                    SetProviders(session);

                    _service.Add(agreement);
                    transaction.Commit();
                }
            }
        }

        public IEnumerable<Agreement> GetAll(Contractor contractor)
        {
            using (var session = _session.Read())
            {
                SetProviders(session);

                return _service.GetAll(contractor);
            }
        }

        public IEnumerable<Agreement> GetAll(GasStorage gasStorage)
        {
            using (var session = _session.Read())
            {
                SetProviders(session);

                return _service.GetAll(gasStorage);
            }
        }

        public IEnumerable<Agreement> GetAll(Contractor contractor, GasStorage gasStorage)
        {
            using (var session = _session.Read())
            {
                SetProviders(session);

                return _service.GetAll(contractor, gasStorage);
            }
        }

        public Agreement Get(int id)
        {
            using (var session = _session.Read())
            {
                SetProviders(session);

                return _service.Get(id);
            }
        }

        public IEnumerable<Agreement> GetAll()
        {
            using (var session = _session.Read())
            {
                SetProviders(session);

                return _service.GetAll();
            }
        }
    }
}