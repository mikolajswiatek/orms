﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using Orms.DataAccessEF.Map;
using Orms.DataAccessEF.Providers;
using Orms.Models;

namespace Orms.DataAccessEF
{
    public class GasContext : DbContext
    {
        public GasContext(string connectionString)
            : base(connectionString)
        {
            this.Database.Log = text => System.Diagnostics.Debug.WriteLine(text);
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Add<RemoveUnderscoreAndStringNamingConvention>();

            //modelBuilder.Configurations.AddFromAssembly(Assembly.GetExecutingAssembly());

            modelBuilder.Configurations
                .Add(new AccessPointFlowMapping())
                .Add(new AccessPointMapping())
                .Add(new AgreementMapping())
                .Add(new CalorificValuesMapping())
                .Add(new ContractorMapping())
                .Add(new ContractorContactMapping())
                .Add(new EnergyCalorificValueMapping())
                .Add(new GasAccountMapping())
                .Add(new GasStorageMapping())
                .Add(new NominationMapping())
                .Add(new NominationValueMapping())
                .Add(new UnitValueMapping())
                .Add(new UserMapping());
        }

        private void FixEfProviderServicesProblem()
        {
            // The Entity Framework provider type 'System.Data.Entity.SqlServer.SqlProviderServices, EntityFramework.SqlServer'
            // for the 'System.Data.SqlClient' ADO.NET provider could not be loaded.
            // Make sure the provider assembly is available to the running application.
            // See http://go.microsoft.com/fwlink/?LinkId=260882 for more information.
            var instance = System.Data.Entity.SqlServer.SqlProviderServices.Instance;
        }

        public class RemoveUnderscoreAndStringNamingConvention : IStoreModelConvention<EntityType>
        {
            public void Apply(EntityType item, DbModel model)
            {
                var entities = new [] { "EnergyCalorificValue", "GasAccount", "NominationValue" };
                if (!entities.Any(e => e == item.Name))
                {
                    return;
                }

                foreach (var property in item.Properties.Where(p => p.Name.Contains("_")))
                {
                    var underscoreIndex = property.Name.IndexOf('_');
                    property.Name = property.Name.Remove(underscoreIndex, 1);
                }

                foreach (var property in item.Properties.Where(p => p.Name.Contains("String")))
                {
                    property.Name = property.Name.Replace("String", "");
                }
            }
        }

        public DbSet<AccessPointFlow> AccessPointFlows { get; set; }
        public DbSet<AccessPoint> AccessPoints { get; set; }
        public DbSet<Agreement> Agreements { get; set; }
        public DbSet<CalorificValue> CalorificValues { get; set; }
        public DbSet<ContractorContact> ContractorContacts { get; set; }
        public DbSet<Contractor> Contractors { get; set; }
        public DbSet<EnergyCalorificValue> EnergyCalorificValues { get; set; }
        public DbSet<GasAccount> GasAccounts { get; set; }
        public DbSet<GasStorage> GasStorages { get; set; }
        public DbSet<Nomination> Nominations { get; set; }
        public DbSet<NominationValue> NominationValues { get; set; }
        public DbSet<Session> Sessions { get; set; }
        public DbSet<User> Users { get; set; }
    }
}