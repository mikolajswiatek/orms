﻿using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;

namespace Orms.DataAccessEF.Repositories
{
    public class GasStorageRepository: IGasStorageRepository<GasContext>
    {
        public void Add(GasContext context, GasStorage gasStorage)
        {
            context.GasStorages.Add(gasStorage);
        }

        public void Delete(GasContext context, GasStorage gasStorage)
        {
            context.GasStorages.Remove(gasStorage);
        }

        public GasStorage Get(GasContext context, int id)
        {
            var gasStorage = context.GasStorages.Find(id);

            return gasStorage;
        }

        public IEnumerable<GasStorage> GetAll(GasContext context)
        {
            var gasStorages = context.GasStorages.ToList();

            return gasStorages;
        }

        public void Update(GasContext context, GasStorage gasStorage)
        {
            context.GasStorages.AddOrUpdate(gasStorage);
        }
    }
}
