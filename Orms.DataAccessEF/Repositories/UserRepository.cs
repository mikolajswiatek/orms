﻿using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;

namespace Orms.DataAccessEF.Repositories
{
    public class UserRepository : IUserRepository<GasContext>
    {
        public void Add(GasContext context, User user)
        {
            context.Users.Add(user);
        }

        public void Delete(GasContext context, User user)
        {
            context.Users.Remove(user);
        }

        public User Get(GasContext context, int id)
        {
            var user = context.Users.Find(id);

            return user;
        }

        public IEnumerable<User> GetAll(GasContext context)
        {
            var users = context.Users.ToList();

            return users;
        }

        public void Update(GasContext context, User user)
        {
            context.Users.AddOrUpdate(user);
        }

        public IEnumerable<User> GetBy(GasContext context, Contractor contractor)
        {
            var users = context.Users
                .Where(u => u.Contractor.Id == contractor.Id)
                .ToList();

            return users;
        }

        public IEnumerable<User> GetBy(GasContext context, string nameOrEmail)
        {
            var users = context.Users
                .Where(u => u.Name == nameOrEmail || u.Email == nameOrEmail)
                .ToList();

            return users;
        }
    }
}
