﻿using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;

namespace Orms.DataAccessEF.Repositories
{
    public class EnergyCalorificValueRepository : IEnergyCalorificValueRepository<GasContext>
    {
        public IEnumerable<EnergyCalorificValue> GetAll(GasContext context)
        {
            return context.EnergyCalorificValues.ToList();
        }

        public EnergyCalorificValue Get(GasContext context, int id)
        {
            return context.EnergyCalorificValues.Find(id);
        }

        public void Update(GasContext context, EnergyCalorificValue ecv)
        {
            context.EnergyCalorificValues.AddOrUpdate(ecv);
        }

        public void Add(GasContext context, EnergyCalorificValue ecv)
        {
            context.EnergyCalorificValues.Add(ecv);
        }

        public void Add(GasContext context, IEnumerable<EnergyCalorificValue> ecvs)
        {
            context.EnergyCalorificValues.AddRange(ecvs);
        }

        public IEnumerable<EnergyCalorificValue> GetAll(GasContext context, CalorificValue cv)
        {
            return context.EnergyCalorificValues
                .Where(ecv => ecv.CalorificValue.Id == cv.Id)
                .OrderBy(ecv => ecv.HourIndex)
                .ToList();
        }
    }
}