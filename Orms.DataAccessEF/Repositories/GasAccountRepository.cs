﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;

namespace Orms.DataAccessEF.Repositories
{
    public class GasAccountRepository : IGasAccountRepository<GasContext>
    {
        public void Add(GasContext context, GasAccount gasAccount)
        {
            context.GasAccounts.Add(gasAccount);
        }

        public void Delete(GasContext context, GasAccount gasAccount)
        {
            context.GasAccounts.Remove(gasAccount);
        }

        public GasAccount Get(GasContext context, int id)
        {
            var gasAccount = context.GasAccounts.Find(id);

            return gasAccount;
        }

        public IEnumerable<GasAccount> GetAll(GasContext context)
        {
            var gasAccounts = context.GasAccounts.ToList();

            return gasAccounts;
        }

        public void Update(GasContext context, GasAccount gasAccount)
        {
            context.GasAccounts.AddOrUpdate(gasAccount);
        }

        public void Add(GasContext context, IEnumerable<GasAccount> gasAccounts)
        {
            context.GasAccounts.AddRange(gasAccounts);
        }

        public void Update(GasContext context, IEnumerable<GasAccount> gasAccounts)
        {
            foreach (var gasAccount in gasAccounts)
            {
                context.GasAccounts.AddOrUpdate(gasAccount);
            }
        }

        public IEnumerable<GasAccount> Get(GasContext context, GasStorage gasStorage, DateTime gasDay)
        {
            var gasAccounts = context.GasAccounts
                .Include(ga => ga.GasStorage)
                .Where(ga => ga.GasStorage.Id == gasStorage.Id && ga.GasDay == gasDay)
                .ToList();

            return gasAccounts;
        }

        public IEnumerable<GasAccount> Get(GasContext context, DateTime gasDay)
        {
            var gasAccounts = context.GasAccounts
                .Where(ga => ga.GasDay == gasDay)
                .ToList();

            return gasAccounts;
        }
    }
}
