﻿using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;

namespace Orms.DataAccessEF.Repositories
{
    public class AccessPointFlowRepository : IAccessPointFlowRepository<GasContext>
    {
        public IEnumerable<AccessPointFlow> GetAll(GasContext context)
        {
            return context.AccessPointFlows.ToList();
        }

        public AccessPointFlow Get(GasContext context, int id)
        {
            return context.AccessPointFlows.Find(id);
        }

        public void Update(GasContext context, AccessPointFlow apf)
        {
            context.AccessPointFlows.AddOrUpdate(apf);
        }

        public void Add(GasContext context, AccessPointFlow apf)
        {
            context.AccessPointFlows.Add(apf);
        }

        public void Delete(GasContext context, AccessPointFlow apf)
        {
            context.AccessPointFlows.Remove(apf);
        }
    }
}
