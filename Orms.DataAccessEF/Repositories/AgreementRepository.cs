﻿using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;

namespace Orms.DataAccessEF.Repositories
{
    public class AgreementRepository : IAgreementRepository<GasContext>
    {
        public void Add(GasContext context, Agreement agreement)
        {
            context.Agreements.Add(agreement);
        }

        public void Delete(GasContext context, Agreement agreement)
        {
            context.Agreements.Remove(agreement);
        }

        public Agreement Get(GasContext context, int id)
        {
            return context.Agreements.Find(id);
        }

        public IEnumerable<Agreement> GetAll(GasContext context)
        {
            return context.Agreements.ToList();
        }

        public void Update(GasContext context, Agreement agreement)
        {
            context.Agreements.AddOrUpdate(agreement);
        }

        public IEnumerable<Agreement> GetBy(GasContext context, GasStorage gasStorage)
        {
            return context.Agreements.Where(a => a.GasStorage.Id == gasStorage.Id).ToList();
        }

        public IEnumerable<Agreement> GetBy(GasContext context, Contractor contractor)
        {
            return context.Agreements.Where(a => a.Contractor.Id == contractor.Id).ToList();
        }
    }
}
