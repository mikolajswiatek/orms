﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;

namespace Orms.DataAccessEF.Repositories
{
    public class NominationRepository : INominationRepository<GasContext>
    {
        public void Add(GasContext context, Nomination nomination)
        {
            nomination.CollectionOfNominationValues = nomination.Values.ToList();
            context.Nominations.Add(nomination);
        }

        public void Delete(GasContext context, Nomination nomination)
        {
            context.Nominations.Remove(nomination);
        }

        public Nomination Get(GasContext context, int id)
        {
            var nomination = context.Nominations.Find(id);
            nomination.Values = nomination.CollectionOfNominationValues.ToList();

            return nomination;
        }

        public IEnumerable<Nomination> GetAll(GasContext context)
        {
            var nominations = context.Nominations.ToList();

            nominations.ForEach(_ =>
            {
                _.Values = _.CollectionOfNominationValues.ToList();
            });

            return nominations;
        }

        public void Update(GasContext context, Nomination nomination)
        {
            nomination.Values = nomination.CollectionOfNominationValues.ToList();

            context.Nominations.AddOrUpdate(nomination);
        }

        public IEnumerable<Nomination> GetBy(GasContext context, Contractor contractor)
        {
            var nominations = context.Nominations.Where(n => n.Contractor.Id == contractor.Id).ToList();

            nominations.ForEach(_ =>
            {
                _.Values = _.CollectionOfNominationValues.ToList();
            });

            return nominations;
        }

        public IEnumerable<Nomination> GetBy(GasContext context, GasStorage gasStorage)
        {
            var nominations = context.Nominations
                .Where(n => n.AccessPointFlow.AccessPoint.GasStorage.Id == gasStorage.Id)
                .ToList();

            nominations.ForEach(_ =>
            {
                _.Values = _.CollectionOfNominationValues.ToList();
            });

            return nominations;
        }

        public IEnumerable<Nomination> GetBy(GasContext context, User user)
        {
            var nominations = context.Nominations
                .Where(n => n.Creator.Id == user.Id)
                .ToList();

            nominations.ForEach(_ =>
            {
                _.Values = _.CollectionOfNominationValues.ToList();
            });

            return nominations;
        }

        public IEnumerable<Nomination> GetBy(GasContext context, DateTime gasDay)
        {
            var nominations = context.Nominations
                .Where(n => n.GasDay == gasDay)
                .ToList();

            nominations.ForEach(_ =>
            {
                _.Values = _.CollectionOfNominationValues.ToList();
            });

            return nominations;
        }

        public Nomination GetLast(GasContext context, Contractor contractor)
        {
            var nomination = context.Nominations
                .Where(n => n.Contractor.Id == contractor.Id && n.IsAccepted)
                .OrderByDescending(n => n.Id)
                .FirstOrDefault();

            nomination.Values = nomination.CollectionOfNominationValues.ToList();

            return nomination;
        }

        public Nomination GetLast(GasContext context, GasStorage gasStorage)
        {
            var nomination = context.Nominations
                .Where(n => n.AccessPointFlow.AccessPoint.GasStorage.Id == gasStorage.Id && n.IsAccepted)
                .OrderByDescending(n => n.Id)
                .FirstOrDefault();

            nomination.Values = nomination.CollectionOfNominationValues.ToList();

            return nomination;
        }

        public Nomination GetLast(GasContext context, User user)
        {
            var nomination = context.Nominations
                .Where(n => n.Creator.Id == user.Id && n.IsAccepted)
                .OrderByDescending(n => n.Id)
                .FirstOrDefault();

            nomination.Values = nomination.CollectionOfNominationValues.ToList();

            return nomination;
        }

        public Nomination GetLast(GasContext context, Contractor contractor, DateTime gasDay)
        {
            var nomination = context.Nominations
                .Where(n => n.Contractor.Id == contractor.Id && n.IsAccepted && n.GasDay == gasDay)
                .OrderByDescending(n => n.Id)
                .FirstOrDefault();

            nomination.Values = nomination.CollectionOfNominationValues.ToList();

            return nomination;
        }

        public IEnumerable<Nomination> GetForGasDay(GasContext context, Contractor contractor, DateTime gasDay)
        {
            var nominations = context.Nominations
                .Where(n => n.Contractor.Id == contractor.Id && n.IsAccepted && n.GasDay == gasDay)
                .OrderByDescending(n => n.Id).ToList();

            nominations.ForEach(_ =>
            {
                _.Values = _.CollectionOfNominationValues.ToList();
            });

            return nominations;
        }

        public IEnumerable<Nomination> GetForGasDay(GasContext context, GasStorage gasStorage, DateTime gasDay)
        {
            var nominations = context.Nominations
                .Where(n => n.AccessPointFlow.AccessPoint.GasStorage.Id == gasStorage.Id && n.IsAccepted &&
                            n.GasDay == gasDay)
                .OrderByDescending(n => n.Id).ToList();

            nominations.ForEach(_ =>
            {
                _.Values = _.CollectionOfNominationValues.ToList();
            });

            return nominations;
        }

        public IEnumerable<Nomination> GetForGasDay(GasContext context, Contractor contractor, GasStorage gasStorage, DateTime gasDay)
        {
            var nominations = context.Nominations
                .Where(n => n.Contractor.Id == contractor.Id && n.AccessPointFlow.AccessPoint.GasStorage.Id == gasStorage.Id && n.IsAccepted && n.GasDay == gasDay)
                .OrderByDescending(n => n.Id).ToList();

            nominations.ForEach(_ =>
            {
                _.Values = _.CollectionOfNominationValues.ToList();
            });

            return nominations;
        }
    }
}
