﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;

namespace Orms.DataAccessEF.Repositories
{
    public class AccessPointRepository : IAccessPointRepository<GasContext>
    {
        public void Add(GasContext context, AccessPoint ap)
        {
            context.AccessPoints.Add(ap);
        }

        public void Delete(GasContext context, AccessPoint ap)
        {
            context.AccessPoints.Remove(ap);
        }

        public AccessPoint Get(GasContext context, int id)
        {
            return context
                .AccessPoints
                .Where(ap => ap.Id == id)
                .Include(ap => ap.GasStorage)
                .Include(ap => ap.CollectionOfAccessPointFlows)
                .FirstOrDefault();
        }

        public IEnumerable<AccessPoint> GetAll(GasContext context)
        {
            return context.AccessPoints.ToList();
        }

        public void Update(GasContext context, AccessPoint ap)
        {
            context.AccessPoints.AddOrUpdate(ap);
        }

        public IEnumerable<AccessPoint> GetBy(GasContext context, GasStorage gasStorage)
        {
            var aps = context
                .AccessPoints
                .Where(c => c.GasStorage.Id == gasStorage.Id)
                .Include(ap => ap.CollectionOfAccessPointFlows)
                .ToList();

            aps.ForEach(_ =>
            {
                _.AccessPointFlows = _.CollectionOfAccessPointFlows.ToList();
            });

            return aps;
        }
    }
}
