﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;

namespace Orms.DataAccessEF.Repositories
{
    public class CalorificValueRepository : ICalorificValueRepository<GasContext>
    {
        public IEnumerable<CalorificValue> GetAll(GasContext context)
        {
            return context.CalorificValues.ToList();
        }

        public CalorificValue Get(GasContext context, int id)
        {
            return context.CalorificValues.Find(id);
        }

        public void Update(GasContext context, CalorificValue cv)
        {
            context.CalorificValues.AddOrUpdate(cv);
        }

        public void Add(GasContext context, CalorificValue cv)
        {
            context.CalorificValues.AddOrUpdate(cv);
        }

        public IEnumerable<CalorificValue> GetBy(GasContext context, GasStorage gasStorage)
        {
            return context.CalorificValues
                .Where(cv => cv.GasStorage.Id == gasStorage.Id)
                .ToList();
        }

        public CalorificValue GetLastBy(GasContext context, GasStorage gasStorage)
        {
            var c = context.CalorificValues
                .Where(cv => cv.GasStorage.Id == gasStorage.Id)
                .Include(cv => cv.CollectionOfValues)
                .OrderByDescending(cv => cv.Id)
                .Take(1)
                .FirstOrDefault();

            if (c != null)
            {
                c.Values = c.CollectionOfValues.ToList();
            }

            return c;
        }

        public CalorificValue GetForGasDay(GasContext context, GasStorage gasStorage, DateTime gasDay)
        {
            var c = context.CalorificValues
                .Where(cv => cv.GasStorage.Id == gasStorage.Id && cv.GasDay == gasDay)
                .OrderByDescending(cv => cv.Id)
                .Take(1)
                .FirstOrDefault();

            if (c != null && c.CollectionOfValues != null)
            {
                c.Values = c.CollectionOfValues.ToList();
            }

            return c;
        }
    }
}