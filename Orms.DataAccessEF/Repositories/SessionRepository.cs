﻿using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;

namespace Orms.DataAccessEF.Repositories
{
    public class SessionRepository : ISessionRepository<GasContext>
    {
        public void Add(GasContext context, Session s)
        {
            context.Sessions.Add(s);
        }

        public Session Get(GasContext context, int id)
        {
            var session = context.Sessions.Find(id);

            return session;
        }

        public IEnumerable<Session> GetAll(GasContext context)
        {
            var sessions = context.Sessions.ToList();

            return sessions;
        }

        public void Update(GasContext context, Session s)
        {
            context.Sessions.AddOrUpdate(s);
        }

        public Session GetBy(GasContext context, string guid)
        {
            var session = context.Sessions.FirstOrDefault(s => s.Guid == guid);

            return session;
        }

        public void Delete(GasContext context, Session s)
        {
            context.Sessions.Remove(s);
        }
    }
}
