﻿using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;

namespace Orms.DataAccessEF.Repositories
{
    public class ContractorContactRepository : IContractorContactRepository<GasContext>
    {
        public void Add(GasContext context, ContractorContact contractorContact)
        {
            context.ContractorContacts.Add(contractorContact);
        }

        public void Delete(GasContext context, ContractorContact contractorContact)
        {
            context.ContractorContacts.Remove(contractorContact);
        }

        public ContractorContact Get(GasContext context, int id)
        {
            return context.ContractorContacts.Find(id);
        }

        public IEnumerable<ContractorContact> GetAll(GasContext context)
        {
            return context.ContractorContacts.ToList();
        }

        public void Update(GasContext context, ContractorContact contractorContact)
        {
            context.ContractorContacts.AddOrUpdate(contractorContact);
        }

        public IEnumerable<ContractorContact> GetByContractor(GasContext context, Contractor contractor)
        {
            return context.ContractorContacts.Where(cc => cc.Contractor.Id == contractor.Id).ToList();
        }
    }
}
