﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;

namespace Orms.DataAccessEF.Repositories
{
    public class ContractorRepository : IContractorRepository<GasContext>
    {
        public void Add(GasContext context, Contractor contractor)
        {
            context.Contractors.Add(contractor);
        }

        public void Delete(GasContext context, Contractor contractor)
        {
            context.Contractors.Remove(contractor);
        }

        public Contractor Get(GasContext context, int id)
        {
            var contractor = context.Contractors.Find(id);

            return contractor;
        }

        public IEnumerable<Contractor> GetAll(GasContext context)
        {
            var contractors = context.Contractors.ToList();

            return contractors;
        }

        public void Update(GasContext context, Contractor contractor)
        {
            context.Contractors.AddOrUpdate(contractor);
        }
    }
}