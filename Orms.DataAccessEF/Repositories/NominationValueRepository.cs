﻿using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;

namespace Orms.DataAccessEF.Repositories
{
    public class NominationValueRepository : INominationValueRepository<GasContext>
    {
        public void Add(GasContext context, NominationValue nominationValue)
        {
            context.NominationValues.Add(nominationValue);
        }

        public void Delete(GasContext context, NominationValue nominationValue)
        {
            context.NominationValues.Remove(nominationValue);
        }

        public NominationValue Get(GasContext context, int id)
        {
            var nominationValue = context.NominationValues.Find(id);

            return nominationValue;
        }

        public IEnumerable<NominationValue> GetAll(GasContext context)
        {
            var nominationValues = context.NominationValues.ToList();

            return nominationValues;
        }

        public void Update(GasContext context, NominationValue nominationValue)
        {
            context.NominationValues.AddOrUpdate(nominationValue);
        }

        public void Add(GasContext context, IEnumerable<NominationValue> values)
        {
            context.NominationValues.AddRange(values);
        }

        public IEnumerable<NominationValue> GetBy(GasContext context, Nomination nomination)
        {
            var nominationValues = context.NominationValues
                .Where(nv => nv.Nomination.Id == nomination.Id)
                .ToList();

            return nominationValues;
        }
    }
}
