﻿using System.Collections.Generic;
using Orms.DataAccessEF.Repositories;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;
using Orms.Providers;

namespace Orms.DataAccessEF.Providers
{
    public class AccessPointProvider : IAccessPointProvider
    {
        private readonly IAccessPointRepository<GasContext> _repository;
        private readonly GasContext _context;

        public AccessPointProvider(GasContext context)
        {
            _context = context;
            _repository = new AccessPointRepository();
        }

        public void Add(AccessPoint ap)
        {
            _repository.Add(_context, ap);
        }

        public AccessPoint Get(int id)
        {
            return _repository.Get(_context, id);
        }

        public IEnumerable<AccessPoint> GetAll()
        {
            return _repository.GetAll(_context);
        }

        public void Update(AccessPoint ap)
        {
            _repository.Update(_context, ap);
        }

        public IEnumerable<AccessPoint> GetBy(GasStorage gasStorage)
        {
            return _repository.GetBy(_context, gasStorage);
        }
    }
}