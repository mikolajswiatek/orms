﻿using System;
using System.Collections.Generic;
using Orms.DataAccessEF.Repositories;
using Orms.DataAccessHelper;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;
using Orms.Providers;

namespace Orms.DataAccessEF.Providers
{
    public class NominationValueProvider : INominationValueProvider
    {
        private readonly INominationValueRepository<GasContext> _repository;
        private readonly GasContext _context;

        public NominationValueProvider(GasContext context)
        {
            _context = context;
            _repository = new NominationValueRepository();
        }

        public void Add(NominationValue nominationValue)
        {
            _repository.Add(_context, nominationValue);
        }

        public void Add(IEnumerable<NominationValue> values)
        {
            _repository.Add(_context, values);
        }

        public IEnumerable<NominationValue> GetBy(Nomination nomination)
        {
            return _repository.GetBy(_context, nomination);
        }

        public NominationValue Get(int id)
        {
            return _repository.Get(_context, id);
        }

        public IEnumerable<NominationValue> GetAll()
        {
            return _repository.GetAll(_context);
        }

        public void Update(NominationValue nominationValue)
        {
            _repository.Update(_context, nominationValue);
        }
    }
}