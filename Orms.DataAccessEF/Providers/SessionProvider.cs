﻿using System.Collections.Generic;
using Orms.DataAccessEF.Repositories;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;
using Orms.Providers;

namespace Orms.DataAccessEF.Providers
{
    public class SessionProvider : ISessionProvider
    {
        private readonly ISessionRepository<GasContext> _repository;
        private readonly GasContext _context;

        public SessionProvider(GasContext context)
        {
            _context = context;
            _repository = new SessionRepository();
        }

        public void Add(Session s)
        {
            _repository.Add(_context, s);

        }

        public Session GetBy(string guid)
        {
            return _repository.GetBy(_context, guid);
        }

        public void Delete(Session s)
        {
            _repository.Delete(_context, s);
        }

        public Session Get(int id)
        {
            return _repository.Get(_context, id);
        }

        public IEnumerable<Session> GetAll()
        {
            return _repository.GetAll(_context);
        }

        public void Update(Session s)
        {
            _repository.Update(_context, s);
        }
    }
}