﻿using System.Collections.Generic;
using Orms.DataAccessEF.Repositories;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;
using Orms.Providers;

namespace Orms.DataAccessEF.Providers
{
    public class EnergyCalorificValueProvider : IEnergyCalorificValueProvider
    {
        private readonly IEnergyCalorificValueRepository<GasContext> _repository;
        private readonly GasContext _context;

        public EnergyCalorificValueProvider(GasContext context)
        {
            _context = context;
            _repository = new EnergyCalorificValueRepository();
        }

        public IEnumerable<EnergyCalorificValue> GetAll()
        {
            return _repository.GetAll(_context);
        }

        public EnergyCalorificValue Get(int id)
        {
            return _repository.Get(_context, id);
        }

        public void Update(EnergyCalorificValue ecv)
        {
            _repository.Update(_context, ecv);
        }

        public void Add(EnergyCalorificValue ecv)
        {
            _repository.Add(_context, ecv);
        }

        public void Add(IEnumerable<EnergyCalorificValue> ecvs)
        {
            _repository.Add(_context, ecvs);
        }

        public IEnumerable<EnergyCalorificValue> GetAll(CalorificValue cv)
        {
            return _repository.GetAll(_context, cv);
        }
    }
}