﻿using System.Collections.Generic;
using Orms.DataAccessEF.Repositories;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;
using Orms.Providers;

namespace Orms.DataAccessEF.Providers
{
    public class AgreementProvider : IAgreementProvider
    {
        private readonly IAgreementRepository<GasContext> _repository;
        private readonly GasContext _context;

        public AgreementProvider(GasContext context)
        {
            _context = context;
            _repository = new AgreementRepository();
        }

        public void Add(Agreement agreement)
        {
            _repository.Add(_context, agreement);
        }

        public Agreement Get(int id)
        {
            return _repository.Get(_context, id);
        }

        public IEnumerable<Agreement> GetAll()
        {
            return _repository.GetAll(_context);
        }

        public void Update(Agreement agreement)
        {
            _repository.Update(_context, agreement);
        }

        public IEnumerable<Agreement> GetBy(Contractor contractor)
        {
            return _repository.GetBy(_context, contractor);
        }

        public IEnumerable<Agreement> GetBy(GasStorage gasStorage)
        {
            return _repository.GetBy(_context, gasStorage);
        }
    }
}