﻿using System;
using System.Collections.Generic;
using Orms.DataAccessEF.Repositories;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;
using Orms.Providers;

namespace Orms.DataAccessEF.Providers
{
    public class ContractorContactProvider : IContractorContactProvider
    {
        private readonly IContractorContactRepository<GasContext> _repository;
        private readonly GasContext _context;

        public ContractorContactProvider(GasContext context)
        {
            _context = context;
            _repository = new ContractorContactRepository();
        }

        public void Add(ContractorContact contractorContact)
        {
            _repository.Add(_context, contractorContact);
        }

        public IEnumerable<ContractorContact> GetByContractor(Contractor contractor)
        {
            return _repository.GetByContractor(_context, contractor);
        }

        public ContractorContact Get(int id)
        {
            return _repository.Get(_context, id);
        }

        public IEnumerable<ContractorContact> GetAll()
        {
            return _repository.GetAll(_context);
        }

        public void Update(ContractorContact contractorContact)
        {
            _repository.Update(_context, contractorContact);
        }
    }
}