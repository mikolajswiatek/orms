﻿using System;
using System.Collections.Generic;
using Orms.DataAccessEF.Repositories;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;
using Orms.Providers;

namespace Orms.DataAccessEF.Providers
{
    public class CalorificValueProvider : ICalorificValueProvider
    {
        private readonly ICalorificValueRepository<GasContext> _repository;
        private readonly GasContext _context;

        public CalorificValueProvider(GasContext context)
        {
            _context = context;
            _repository = new CalorificValueRepository();
        }

        public IEnumerable<CalorificValue> GetAll()
        {
            return _repository.GetAll(_context);
        }

        public CalorificValue Get(int id)
        {
            return _repository.Get(_context, id);
        }

        public void Update(CalorificValue cv)
        {
            _repository.Update(_context, cv);
        }

        public void Add(CalorificValue cv)
        {
            _repository.Add(_context, cv);
        }

        public CalorificValue GetLastBy(GasStorage gasStorage)
        {
            return _repository.GetLastBy(_context, gasStorage);
        }

        public CalorificValue GetForGasDay(GasStorage gasStorage, DateTime gasDay)
        {
            return _repository.GetForGasDay(_context, gasStorage, gasDay);
        }
    }
}