﻿using System.Collections.Generic;
using Orms.DataAccessEF.Repositories;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;
using Orms.Providers;

namespace Orms.DataAccessEF.Providers
{
    public class ContractorProvider : IContractorProvider
    {
        private readonly IContractorRepository<GasContext> _repository;
        private readonly GasContext _context;

        public ContractorProvider(GasContext context)
        {
            _context = context;
            _repository = new ContractorRepository();
        }

        public IEnumerable<Contractor> GetAll()
        {
            return _repository.GetAll(_context);
        }

        public Contractor Get(int id)
        {
            return _repository.Get(_context, id);
        }

        public void Update(Contractor contractor)
        {
            _repository.Update(_context, contractor);
            _context.SaveChanges();
        }

        public void Add(Contractor contractor)
        {
            _repository.Add(_context, contractor);
        }
    }
}