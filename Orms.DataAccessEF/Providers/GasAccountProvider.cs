﻿using System;
using System.Collections.Generic;
using Orms.DataAccessEF.Repositories;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;
using Orms.Providers;

namespace Orms.DataAccessEF.Providers
{
    public class GasAccountProvider : IGasAccountProvider
    {
        private readonly IGasAccountRepository<GasContext> _repository;
        private readonly GasContext _context;

        public GasAccountProvider(GasContext context)
        {
            _context = context;
            _repository = new GasAccountRepository();
        }
        public void Add(GasAccount gasAccount)
        {
            _repository.Add(_context, gasAccount);
        }

        public GasAccount Get(int id)
        {
            return _repository.Get(_context, id);
        }

        public IEnumerable<GasAccount> GetAll()
        {
            return _repository.GetAll(_context);
        }

        public void Update(GasAccount gasAccount)
        {
            _repository.Update(_context, gasAccount);
        }

        public void Add(IEnumerable<GasAccount> gasAccounts)
        {
            _repository.Add(_context, gasAccounts);
        }

        public void Update(IEnumerable<GasAccount> gasAccounts)
        {
            _repository.Update(_context, gasAccounts);
        }

        public IEnumerable<GasAccount> Get(DateTime gasDay)
        {
            return _repository.Get(_context, gasDay);
        }

        public IEnumerable<GasAccount> Get(GasStorage gasStorage, DateTime gasDay)
        {
            return _repository.Get(_context, gasStorage, gasDay);
        }
    }
}