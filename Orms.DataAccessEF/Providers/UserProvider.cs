﻿using System;
using System.Collections.Generic;
using Orms.DataAccessEF.Repositories;
using Orms.DataAccessHelper;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;
using Orms.Providers;

namespace Orms.DataAccessEF.Providers
{
    public class UserProvider : IUserProvider
    {
        private readonly IUserRepository<GasContext> _repository;
        private readonly GasContext _context;

        public UserProvider(GasContext context)
        {
            _context = context;
            _repository = new UserRepository();
        }

        public void Add(User user)
        {
            _repository.Add(_context, user);
        }

        public User Get(int id)
        {
            return _repository.Get(_context, id);
        }

        public IEnumerable<User> GetAll()
        {
            return _repository.GetAll(_context);
        }

        public void Update(User user)
        {
            _repository.Update(_context, user);
        }

        public IEnumerable<User> GetBy(Contractor contractor)
        {
            return _repository.GetBy(_context, contractor);
        }

        public IEnumerable<User> GetBy(string nameOrEmail)
        {
            return _repository.GetBy(_context, nameOrEmail);
        }
    }
}