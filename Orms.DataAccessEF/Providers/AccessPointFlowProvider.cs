﻿using System.Collections.Generic;
using Orms.DataAccessEF.Repositories;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;
using Orms.Providers;

namespace Orms.DataAccessEF.Providers
{
    public class AccessPointFlowProvider : IAccessPointFlowProvider
    {
        private readonly IAccessPointFlowRepository<GasContext> _repository;
        private readonly GasContext _context;

        public AccessPointFlowProvider(GasContext context)
        {
            _context = context;
            _repository = new AccessPointFlowRepository();
        }

        public void Add(AccessPointFlow apf)
        {
            _repository.Add(_context, apf);
        }

        public AccessPointFlow Get(int id)
        {
            return _repository.Get(_context, id);
        }

        public IEnumerable<AccessPointFlow> GetAll()
        {
            return _repository.GetAll(_context);
        }

        public void Update(AccessPointFlow apf)
        {
            _repository.Update(_context, apf);
        }
    }
}