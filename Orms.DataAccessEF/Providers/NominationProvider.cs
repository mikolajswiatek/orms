﻿using System;
using System.Collections.Generic;
using Orms.DataAccessEF.Repositories;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;
using Orms.Providers;

namespace Orms.DataAccessEF.Providers
{
    public class NominationProvider : INominationProvider
    {
        private readonly INominationRepository<GasContext> _repository;
        private readonly GasContext _context;

        public NominationProvider(GasContext context)
        {
            _context = context;
            _repository = new NominationRepository();
        }

        public void Add(Nomination nomination)
        {
            _repository.Add(_context, nomination);
        }

        public Nomination Get(int id)
        {
            return _repository.Get(_context, id);
        }

        public IEnumerable<Nomination> GetAll()
        {
            return _repository.GetAll(_context);
        }

        public void Update(Nomination nomination)
        {
            _repository.Update(_context, nomination);
        }

        public IEnumerable<Nomination> GetBy(Contractor contractor)
        {
            return _repository.GetBy(_context, contractor);
        }

        public IEnumerable<Nomination> GetBy(GasStorage gasStorage)
        {
            return _repository.GetBy(_context, gasStorage);
        }

        public IEnumerable<Nomination> GetBy(User user)
        {
            return _repository.GetBy(_context, user);
        }

        public IEnumerable<Nomination> GetBy(DateTime gasDay)
        {
            return _repository.GetBy(_context, gasDay);
        }

        public Nomination GetLast(Contractor contractor)
        {
            return _repository.GetLast(_context, contractor);
        }

        public Nomination GetLast(GasStorage gasStorage)
        {
            return _repository.GetLast(_context, gasStorage);
        }

        public Nomination GetLast(User user)
        {
            return _repository.GetLast(_context, user);
        }

        public Nomination GetLast(Contractor contractor, DateTime gasDay)
        {
            return _repository.GetLast(_context, contractor);
        }

        public IEnumerable<Nomination> GetForGasDay(Contractor contractor, DateTime gasDay)
        {
            return _repository.GetForGasDay(_context, contractor, gasDay);
        }

        public IEnumerable<Nomination> GetForGasDay(GasStorage gasStorage, DateTime gasDay)
        {
            return _repository.GetForGasDay(_context, gasStorage, gasDay);
        }

        public IEnumerable<Nomination> GetForGasDay(Contractor contractor, GasStorage gasStorage, DateTime gasDay)
        {
            return _repository.GetForGasDay(_context, contractor, gasStorage, gasDay);
        }
    }
}