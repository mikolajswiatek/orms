﻿using System.Collections.Generic;
using Orms.DataAccessEF.Repositories;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;
using Orms.Providers;

namespace Orms.DataAccessEF.Providers
{
    public class GasStorageProvider : IGasStorageProvider
    {
        private readonly IGasStorageRepository<GasContext> _repository;
        private readonly GasContext _context;

        public GasStorageProvider(GasContext context)
        {
            _context = context;
            _repository = new GasStorageRepository();
        }

        public void Add(GasStorage gasStorage)
        {
            _repository.Add(_context, gasStorage);
        }

        public GasStorage Get(int id)
        {
            return _repository.Get(_context, id);
        }

        public IEnumerable<GasStorage> GetAll()
        {
            return _repository.GetAll(_context);
        }

        public void Update(GasStorage gasStorage)
        {
            _repository.Update(_context, gasStorage);
        }
    }
}