﻿using System.Collections.Generic;
using Orms.DataAccessHelper;
using Orms.DataAccessHelper.Operations.Interfaces;
using Orms.DataAccessEF.Providers;
using Orms.Models;
using Orms.Services;
using Orms.Services.Interfaces;

namespace Orms.DataAccessEF.Operations
{
    public class AgreementOperation : Operation<GasContext>, IAgreementOperation
    {
        private IAgreementService _service;
        private readonly SessionWorker<GasContext> _context;

        public AgreementOperation(SessionWorker<GasContext> context)
        {
            _context = context;
        }

        protected override void SetProviders(GasContext context)
        {
            _service = new AgreementService(
                new AgreementProvider(context));
        }

        public void Add(Agreement agreement)
        {
            SetProviders(_context.ReadAndWrite());
            _service.Add(agreement);
            _context.SaveChanges();
        }

        public IEnumerable<Agreement> GetAll(Contractor contractor)
        {
            SetProviders(_context.Read());
            return _service.GetAll(contractor);
        }

        public IEnumerable<Agreement> GetAll(GasStorage gasStorage)
        {
            SetProviders(_context.Read());
            return _service.GetAll(gasStorage);
        }

        public IEnumerable<Agreement> GetAll(Contractor contractor, GasStorage gasStorage)
        {
            SetProviders(_context.Read());
            return _service.GetAll(contractor, gasStorage);
        }

        public Agreement Get(int id)
        {
            SetProviders(_context.Read());
            return _service.Get(id);
        }

        public IEnumerable<Agreement> GetAll()
        {
            SetProviders(_context.Read());
            return _service.GetAll();
        }
    }
}