﻿using System.Collections.Generic;
using Orms.DataAccessEF.Providers;
using Orms.DataAccessHelper;
using Orms.DataAccessHelper.Operations.Interfaces;
using Orms.Models;
using Orms.Services;
using Orms.Services.Interfaces;

namespace Orms.DataAccessEF.Operations
{
    public class GasStorageOperation : Operation<GasContext>, IGasStorageOperation
    {
        private IGasStorageService _service;
        private readonly SessionWorker<GasContext> _context;

        public GasStorageOperation(SessionWorker<GasContext> context)
        {
            _context = context;
        }

        protected override void SetProviders(GasContext _context)
        {
            _service = new GasStorageService(
                new GasStorageProvider(_context));
        }

        public void Add(GasStorage gasStorage)
        {
            SetProviders(_context.ReadAndWrite());

            _service.Add(gasStorage);
            _context.SaveChanges();
        }

        public GasStorage Get(int id)
        {
            SetProviders(_context.Read());

            return _service.Get(id);
        }

        public IEnumerable<GasStorage> GetAll()
        {
            SetProviders(_context.Read());

            return _service.GetAll();
        }
    }
}