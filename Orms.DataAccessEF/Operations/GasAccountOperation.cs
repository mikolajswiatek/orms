﻿using System;
using System.Collections.Generic;
using Orms.DataAccessEF.Providers;
using Orms.DataAccessHelper;
using Orms.DataAccessHelper.Operations.Interfaces;
using Orms.Models;
using Orms.Services;
using Orms.Services.Interfaces;

namespace Orms.DataAccessEF.Operations
{
    public class GasAccountOperation : Operation<GasContext>, IGasAccountOperation
    {
        private IGasAccountService _service;
        private readonly SessionWorker<GasContext> _context;

        public GasAccountOperation(SessionWorker<GasContext> context)
        {
            _context = context;
        }

        protected override void SetProviders(GasContext _context)
        {
            _service = new GasAccountService(
                new GasAccountProvider(_context),
                new GasStorageProvider(_context),
                new NominationProvider(_context));
        }

        public void Update(IEnumerable<GasAccount> gasAccounts)
        {
            SetProviders(_context.ReadAndWrite());

            _service.Update(gasAccounts);
            _context.SaveChanges();
        }

        public IEnumerable<GasAccount> Get(DateTime gasDay)
        {
            SetProviders(_context.Read());

            return _service.Get(gasDay);
        }

        public IEnumerable<GasAccount> Get(GasStorage gasStorage, DateTime gasDay)
        {
            SetProviders(_context.Read());

            return _service.Get(gasStorage, gasDay);
        }

        public void Refresh(GasStorage gasStorage)
        {
            SetProviders(_context.ReadAndWrite());

            _service.Refresh(gasStorage);
            _context.SaveChanges();
        }

        public void Refresh()
        {
            SetProviders(_context.ReadAndWrite());

            _service.Refresh();
            _context.SaveChanges();
        }

        public void Refresh(GasStorage gasStorage, GasDay gasDay)
        {
            SetProviders(_context.ReadAndWrite());

            _service.Refresh(gasStorage, gasDay);
            _context.SaveChanges();
        }

        public void Refresh(GasDay gasDay)
        {
            SetProviders(_context.ReadAndWrite());

            _service.Refresh(gasDay);
            _context.SaveChanges();
        }
    }
}