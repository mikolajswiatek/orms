﻿using System;
using System.Collections.Generic;
using Orms.DataAccessEF.Providers;
using Orms.DataAccessHelper;
using Orms.DataAccessHelper.Operations.Interfaces;
using Orms.Models;
using Orms.Services;
using Orms.Services.Interfaces;

namespace Orms.DataAccessEF.Operations
{
    public class NominationOperation : Operation<GasContext>, INominationOperation
    {
        private INominationService _service;
        private readonly SessionWorker<GasContext> _context;

        public NominationOperation(SessionWorker<GasContext> context)
        {
            _context = context;
        }

        protected override void SetProviders(GasContext context)
        {
            _service = new NominationService(
                new NominationProvider(context),
                new NominationValueProvider(context));
        }

        public void Add(Nomination nomination)
        {
            SetProviders(_context.ReadAndWrite());

            _service.Add(nomination);
            _context.SaveChanges();
        }

        public void Add(IEnumerable<NominationValue> values)
        {
            SetProviders(_context.ReadAndWrite());

            _service.Add(values);
            _context.SaveChanges();
        }

        public void Add(Nomination nomination, IEnumerable<NominationValue> values)
        {
            SetProviders(_context.ReadAndWrite());

            _service.Add(nomination, values);
            _context.SaveChanges();
        }

        public void RefreshNomiation(Nomination nomination, int parentNominationId)
        {
            SetProviders(_context.ReadAndWrite());

            _service.RefreshNomiation(nomination, parentNominationId);
            _context.SaveChanges();
        }

        public void AcceptedNomination(Nomination nomination)
        {
            SetProviders(_context.ReadAndWrite());

            _service.AcceptedNomination(nomination);
            _context.SaveChanges();
        }

        public IEnumerable<Nomination> GetBy(Contractor contractor)
        {
            SetProviders(_context.Read());

            return _service.GetBy(contractor);
        }

        public IEnumerable<Nomination> GetBy(GasStorage gasStorage)
        {
            SetProviders(_context.Read());

            return _service.GetBy(gasStorage);
        }

        public IEnumerable<Nomination> GetBy(User user)
        {
            SetProviders(_context.Read());

            return _service.GetBy(user);
        }

        public IEnumerable<Nomination> GetBy(DateTime gasDay)
        {
            SetProviders(_context.Read());

            return _service.GetBy(gasDay);
        }

        public Nomination Get(int id)
        {
            SetProviders(_context.Read());

            return _service.Get(id);
        }

        public Nomination GetLast(Contractor contractor)
        {
            SetProviders(_context.Read());

            return _service.GetLast(contractor);
        }

        public Nomination GetLast(GasStorage gasStorage)
        {
            SetProviders(_context.Read());

            return _service.GetLast(gasStorage);
        }

        public Nomination GetLast(User user)
        {
            SetProviders(_context.Read());

            return _service.GetLast(user);
        }

        public Nomination GetLast(Contractor contractor, DateTime gasDay)
        {
            SetProviders(_context.Read());

            return _service.GetLast(contractor, gasDay);
        }

        public IEnumerable<Nomination> GetForGasDay(Contractor contractor, DateTime gasDay)
        {
            SetProviders(_context.Read());

            return _service.GetForGasDay(contractor, gasDay);
        }

        public IEnumerable<Nomination> GetForGasDay(GasStorage gasStorage, DateTime gasDay)
        {
            SetProviders(_context.Read());

            return _service.GetForGasDay(gasStorage, gasDay);
        }

        public IEnumerable<Nomination> GetForGasDay(Contractor contractor, GasStorage gasStorage, DateTime gasDay)
        {
            SetProviders(_context.Read());

            return _service.GetForGasDay(contractor, gasStorage, gasDay);
        }
    }
}