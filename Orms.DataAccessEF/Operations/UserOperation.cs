﻿using System.Collections.Generic;
using Orms.DataAccessEF.Providers;
using Orms.DataAccessHelper;
using Orms.DataAccessHelper.Operations.Interfaces;
using Orms.Models;
using Orms.Services;
using Orms.Services.Interfaces;

namespace Orms.DataAccessEF.Operations
{
    public class UserOperation : Operation<GasContext>, IUserOperation
    {
        private IUserService _service;
        private readonly SessionWorker<GasContext> _context;

        public UserOperation(SessionWorker<GasContext> context)
        {
            _context = context;
        }

        protected override void SetProviders(GasContext context)
        {
            _service = new UserService(
                new UserProvider(context),
                new SessionProvider(context));
        }

        public User Exist(string nameOrEmail)
        {
            SetProviders(_context.Read());

            return _service.Exist(nameOrEmail);
        }

        public void Create(User user)
        {
            SetProviders(_context.ReadAndWrite());

            _service.Create(user);
            _context.SaveChanges();
        }

        public void Update(User user)
        {
            SetProviders(_context.ReadAndWrite());

            _service.Update(user);
            _context.SaveChanges();
        }

        public User Get(int id)
        {
            SetProviders(_context.Read());

            return _service.Get(id);
        }

        public IEnumerable<User> Get(Contractor contractor)
        {
            SetProviders(_context.Read());

            return _service.Get(contractor);
        }

        public IEnumerable<User> GetAll()
        {
            SetProviders(_context.Read());

            return _service.GetAll();
        }
    }
}