﻿using System.Collections.Generic;
using System.Data.SqlClient;
using Orms.DataAccessEF.Providers;
using Orms.DataAccessHelper;
using Orms.DataAccessHelper.Operations.Interfaces;
using Orms.Models;
using Orms.Services;
using Orms.Services.Interfaces;

namespace Orms.DataAccessEF.Operations
{
    public class ContractorOperation : Operation<GasContext>, IContractorOperation
    {
        private IContractorService _service;
        private readonly SessionWorker<GasContext> _context;

        public ContractorOperation(SessionWorker<GasContext> context)
        {
            _context = context;
        }

        protected override void SetProviders(GasContext _context)
        {
            _service = new ContractorService(
                new ContractorContactProvider(_context),
                new ContractorProvider(_context));
        }

        public IEnumerable<Contractor> GetAll()
        {
            SetProviders(_context.Read());

            return _service.GetAll();
        }

        public IEnumerable<ContractorContact> GetAll(Contractor contractor)
        {
            SetProviders(_context.Read());

            return _service.GetAll(contractor);
        }

        public Contractor Get(int id)
        {
            SetProviders(_context.Read());

            return _service.Get(id);
        }

        public ContractorContact GetContact(int id)
        {
            SetProviders(_context.Read());

            return _service.GetContact(id);
        }

        public void Update(ContractorContact contact)
        {
            SetProviders(_context.ReadAndWrite());

            _service.Update(contact);
            _context.SaveChanges();
        }

        public void Add(Contractor contractor)
        {
            SetProviders(_context.ReadAndWrite());

            _service.Add(contractor);
            _context.SaveChanges();
        }

        public void Add(ContractorContact contact, int contractorId)
        {
            SetProviders(_context.ReadAndWrite());

            _service.Add(contact, contractorId);
            _context.SaveChanges();
        }
    }
}