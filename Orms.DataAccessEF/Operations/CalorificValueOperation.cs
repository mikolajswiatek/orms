﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Orms.DataAccessEF.Providers;
using Orms.DataAccessHelper;
using Orms.DataAccessHelper.Operations.Interfaces;
using Orms.Models;
using Orms.Services;
using Orms.Services.Interfaces;

namespace Orms.DataAccessEF.Operations
{
    public class CalorificValueOperation : Operation<GasContext>, ICalorificValueOperation
    {
        private ICalorificValueService _service;
        private readonly SessionWorker<GasContext> _context;

        public CalorificValueOperation(SessionWorker<GasContext> context)
        {
            _context = context;
        }

        protected override void SetProviders(GasContext context)
        {
            _service = new CalorificValueService(new CalorificValueProvider(context));
        }

        public void Add(CalorificValue cv, IEnumerable<EnergyCalorificValue> values)
        {
            SetProviders(_context.ReadAndWrite());

            cv.CollectionOfValues = values.ToList();

            _service.Add(cv, values);
            _context.SaveChanges();
        }

        public CalorificValue GetLastBy(GasStorage gasStorage)
        {
            SetProviders(_context.Read());

            return _service.GetLastBy(gasStorage);
        }

        public CalorificValue GetForGasDay(GasStorage gasStorage, DateTime gasDay)
        {
            SetProviders(_context.Read());

            return _service.GetForGasDay(gasStorage, gasDay);
        }

        public CalorificValue Get(int id)
        {
            SetProviders(_context.Read());

            return _service.Get(id);
        }
    }
}