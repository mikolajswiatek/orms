﻿using System.Data.SqlClient;
using Orms.DataAccessEF.Providers;
using Orms.DataAccessHelper;
using Orms.DataAccessHelper.Operations.Interfaces;
using Orms.Models;
using Orms.Services;
using Orms.Services.Interfaces;

namespace Orms.DataAccessEF.Operations
{
    public class SessionOperation : Operation<GasContext>, ISessionOperation
    {
        private ISessionService _service;
        private readonly SessionWorker<GasContext> _context;

        public SessionOperation(SessionWorker<GasContext> context)
        {
            _context = context;
        }

        protected override void SetProviders(GasContext connection)
        {
            _service = new SessionService(
                new SessionProvider(connection),
                new UserProvider(connection));
        }

        public void Add(Session session)
        {
            SetProviders(_context.ReadAndWrite());

            _service.Add(session);
            _context.SaveChanges();
        }

        public void Refresh(string guid)
        {
            SetProviders(_context.ReadAndWrite());

            _service.Refresh(guid);
            _context.SaveChanges();
        }

        public Session GetBy(string guid)
        {
            SetProviders(_context.Read());

            return _service.GetBy(guid);
        }

        public void Delete(Session session)
        {
            SetProviders(_context.ReadAndWrite());

            _service.Delete(session);
            _context.SaveChanges();
        }
    }
}