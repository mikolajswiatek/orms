﻿using System.Collections.Generic;
using Orms.DataAccessEF.Providers;
using Orms.DataAccessHelper;
using Orms.DataAccessHelper.Operations.Interfaces;
using Orms.Models;
using Orms.Services;
using Orms.Services.Interfaces;

namespace Orms.DataAccessEF.Operations
{
    public class AccessPointOperation : Operation<GasContext>, IAccessPointOperation
    {
        private IAccessPointService _service;
        private readonly SessionWorker<GasContext> _context;

        public AccessPointOperation(SessionWorker<GasContext> context)
        {
            _context = context;
        }

        protected override void SetProviders(GasContext context)
        {
            _service = new AccessPointService(
                new AccessPointProvider(context),
                new AccessPointFlowProvider(context));
        }

        public IEnumerable<AccessPoint> GetBy(GasStorage gasStorage)
        {
            SetProviders(_context.Read());

            return _service.GetBy(gasStorage);
        }

        public IEnumerable<AccessPoint> GetAll()
        {
            SetProviders(_context.Read());

            return _service.GetAll();
        }

        public AccessPoint Get(int id)
        {
            SetProviders(_context.Read());

            return _service.Get(id);
        }

        public void Add(AccessPoint accessPoint)
        {
            SetProviders(_context.ReadAndWrite());

            _service.Add(accessPoint);
            _context.SaveChanges();
        }

        public void Add(AccessPointFlow apf, int accessPointId)
        {
            SetProviders(_context.ReadAndWrite());

            _service.Add(apf, accessPointId);
            _context.SaveChanges();
        }
    }
}