﻿using Orms.DataAccessHelper;

namespace Orms.DataAccessEF
{
    public class EnityFrameworkHelper : SessionWorker<GasContext>
    {
        private GasContext _db;

        public EnityFrameworkHelper(string connectionString) : base(connectionString)
        {
        }

        public override void OpenSession()
        {
            _db = new GasContext(_connectionString);
            isOpen = true;
        }

        public override void CloseSession()
        {
            _db.Dispose();
            isOpen = false;
        }

        public override GasContext Read()
        {
            return _db;
        }

        public override GasContext ReadAndWrite()
        {
            return _db;
        }

        public override void SaveChanges()
        {
            _db.SaveChanges();;
        }
    }
}
