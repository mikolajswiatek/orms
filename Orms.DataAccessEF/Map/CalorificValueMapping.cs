﻿using Orms.Models;

namespace Orms.DataAccessEF.Map
{
    public class CalorificValuesMapping : EntityMapping<CalorificValue>
    {
        public CalorificValuesMapping()
        {
            this
                .ToTable("CalorificValues");

            this
                .Property(c => c.GasDay)
                .HasColumnName("GasDay");

            this
                .HasRequired(c => c.GasStorage);
        }
    }
}