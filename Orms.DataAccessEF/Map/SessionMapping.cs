﻿using Orms.Models;

namespace Orms.DataAccessEF.Map
{
    public class SessionMapping : EntityMapping<Session>
    {
        public SessionMapping()
        {
            this
                .ToTable("Sessions");

            this
                .Property(c => c.Guid)
                .HasColumnName("Guid");

            this
                .Property(c => c.Ip)
                .HasColumnName("Ip");

            this
                .Property(c => c.Browser)
                .HasColumnName("Browser");

            this
                .Property(c => c.CreateDatetime)
                .HasColumnName("CreateDatetime");

            this
                .Property(c => c.LastActivityDatetime)
                .HasColumnName("LastActivityDatetime");

            this
                .HasRequired(c => c.User);
        }
    }
}