﻿using Orms.Models;

namespace Orms.DataAccessEF.Map
{
    public class ContractorContactMapping : EntityMapping<ContractorContact>
    {
        public ContractorContactMapping()
        {
            this
                .ToTable("ContractorContacts");

            this
                .Property(c => c.Adress)
                .HasColumnName("Adress");

            this
                .Property(c => c.Email)
                .HasColumnName("Email"); ;

            this
                .Property(c => c.Name)
                .HasColumnName("Name");

            this
                .Property(c => c.FirstName)
                .HasColumnName("FirstName");

            this
                .Property(c => c.LastName)
                .HasColumnName("LastName");

            this
                .Property(c => c.Phone)
                .HasColumnName("Phone");

            this
                .HasRequired(x => x.Contractor);
        }
    }
}