﻿using Orms.Models;

namespace Orms.DataAccessEF.Map
{
    public class AgreementMapping : EntityMapping<Agreement>
    {
        public AgreementMapping()
        {
            this
                .ToTable("Agreements");

            this
                .Property(c => c.CreateDate)
                .HasColumnName("CreateDatetime");

            this
                .Property(c => c.Start)
                .HasColumnName("StartDatetime");

            this
                .Property(c => c.End)
                .HasColumnName("EndDatetime");

            this
                .HasRequired(c => c.Contractor);

            this
                .HasRequired(c => c.GasStorage);
        }
    }
}