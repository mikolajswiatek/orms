﻿using Orms.Models;

namespace Orms.DataAccessEF.Map
{
    public class EnergyCalorificValueMapping : EntityMapping<EnergyCalorificValue>
    {
        public EnergyCalorificValueMapping()
        {
            this
                .ToTable("CalorificValueValues");

            this
                .Property(c => c.HourIndex)
                .HasColumnName("HourIndex");
        }
    }
}