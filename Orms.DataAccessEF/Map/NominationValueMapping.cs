﻿using Orms.Models;

namespace Orms.DataAccessEF.Map
{
    public class NominationValueMapping : EntityMapping<NominationValue>
    {
        public NominationValueMapping()
        {
            this
                .ToTable("NominationValues");

            this
                .Property(c => c.HourIndex)
                .HasColumnName("HourIndex");
        }
    }
}