﻿using Orms.Models;

namespace Orms.DataAccessEF.Map
{
    public class ContractorMapping : EntityMapping<Contractor>
    {
        public ContractorMapping()
        {
            this
                .ToTable("Contractors");

            this
                .Property(c => c.Adress)
                .HasColumnName("Adress");

            this
                .Property(c => c.Code)
                .HasColumnName("Code");

            this
                .Property(c => c.Name)
                .HasColumnName("Name");
        }
    }
}