﻿using Orms.Models;

namespace Orms.DataAccessEF.Map
{
    public class UserMapping : EntityMapping<User>
    {
        public UserMapping()
        {
            this
                .ToTable("Users");

            this
                .Property(c => c.Name)
                .HasColumnName("Name");

            this
                .Property(c => c.Password)
                .HasColumnName("Password");

            this
                .Property(c => c.Email)
                .HasColumnName("Email");

            this
                .Property(c => c.IsActive)
                .HasColumnName("IsActive");

            this
                .Property(c => c.CreateDateTime)
                .HasColumnName("CreateDatetime");

            this
                .Property(c => c.LastActivityDateTime)
                .HasColumnName("LastActivityDatetime");

            this
                .HasRequired(c => c.Contractor);
        }
    }
}