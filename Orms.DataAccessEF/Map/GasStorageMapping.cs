﻿using Orms.Models;

namespace Orms.DataAccessEF.Map
{
    public class GasStorageMapping : EntityMapping<GasStorage>
    {
        public GasStorageMapping()
        {
            this
                .ToTable("GasStorages");

            this
                .Property(c => c.Name)
                .HasColumnName("Name")
                .IsRequired();

            this
                .HasMany(c => c.CollectionOfAccessPoints);
        }
    }
}