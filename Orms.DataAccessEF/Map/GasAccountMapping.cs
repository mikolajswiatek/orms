﻿using Orms.Models;

namespace Orms.DataAccessEF.Map
{
    public class GasAccountMapping : EntityMapping<GasAccount>
    {
        public GasAccountMapping()
        {
            this.
                ToTable("GasAccounts");

            this
                .Property(c => c.HourIndex)
                .HasColumnName("HourIndex");

            this
                .Property(x => x.GasDay)
                .HasColumnName("GasDay");

            this
                .HasRequired(c => c.GasStorage);
        }
    }
}