﻿using Orms.Models;

namespace Orms.DataAccessEF.Map
{
    public class NominationMapping : EntityMapping<Nomination>
    {
        public NominationMapping()
        {
            this
                .ToTable("Nominations");

            this
                .Property(c => c.Name)
                .HasColumnName("Name");

            this
                .Property(c => c.ChannelString)
                .HasColumnName("Channel");

            this.Ignore(c => c.Channel);

            this
                .Property(c => c.IsAccepted)
                .HasColumnName("IsAccepted");

            this
                .Property(c => c.IsLast)
                .HasColumnName("IsLast");

            this
                .Property(c => c.GasDay)
                .HasColumnName("GasDay");

            this
                .Property(c => c.CreateDateTime)
                .HasColumnName("CreateDatetime");

            this
                .Property(c => c.MajorVersion)
                .HasColumnName("MajorVersion");

            this
                .Property(c => c.Version)
                .HasColumnName("Version");

            this
                .HasRequired(c => c.AccessPointFlow);

            this
                .HasOptional(c => c.ParentNomination);

            this
                .HasRequired(c => c.Creator);

            this
                .HasRequired(c => c.Contractor);

            this
                .HasRequired(c => c.CalorificValue);
        }
    }
}