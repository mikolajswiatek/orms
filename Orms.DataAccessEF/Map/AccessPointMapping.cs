﻿using Orms.Models;

namespace Orms.DataAccessEF.Map
{
    public class AccessPointMapping : EntityMapping<AccessPoint>
    {
        public AccessPointMapping()
        {
            this.
                ToTable("AccessPoints");

            this
                .Property(c => c.Code)
                .HasColumnName("Code");

            this
                .HasRequired(c => c.GasStorage);

            this
                .HasMany(c => c.CollectionOfAccessPointFlows);
        }
    }
}