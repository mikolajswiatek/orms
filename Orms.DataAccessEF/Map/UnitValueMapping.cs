﻿using System.Data.Entity.ModelConfiguration;
using Orms.Domains.Units;

namespace Orms.DataAccessEF.Map
{
    public class UnitValueMapping : EntityTypeConfiguration<UnitValue>
    {
        public UnitValueMapping()
        {
            this.
                Ignore(c => c.Unit);

            this
                .Property(c => c.Value);

            this
                .Property(c => c.UnitString);
        }
    }
}