﻿using System.Data.Entity.ModelConfiguration;
using Orms.Models;

namespace Orms.DataAccessEF.Map
{
    public class EntityMapping<T> : EntityTypeConfiguration<T> where T : Entity
    {
        public EntityMapping()
        {
            this
                .HasKey(c => c.Id);

            this
                .Property(p => p.Id)
                .HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
        }
    }
}