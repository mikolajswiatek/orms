﻿using Orms.Models;

namespace Orms.DataAccessEF.Map
{
    public class AccessPointFlowMapping : EntityMapping<AccessPointFlow>
    {
        public AccessPointFlowMapping()
        {
            this
                .ToTable("AccessPointFlows");

            this
                .Property(c => c.DirectionString)
                .HasColumnName("Direction");

            this.Ignore(c => c.Direction);

            this
                .HasRequired<AccessPoint>(c => c.AccessPoint);
        }
    }
}