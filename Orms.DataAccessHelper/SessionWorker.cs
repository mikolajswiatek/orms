﻿namespace Orms.DataAccessHelper
{
    public abstract class SessionWorker<T> where T : class
    {
        public bool isOpen { get; set; }

        protected string _connectionString { get; set; }
        protected SessionWorker<T> sessionWorker { get; set; }

        public SessionWorker(string connectionString)
        {
            _connectionString = connectionString;
        }

        public abstract void OpenSession();

        public abstract void CloseSession();

        public abstract T Read();

        public abstract T ReadAndWrite();

        public abstract void SaveChanges();
    }
}