﻿using Orms.Models;
using System;
using System.Collections.Generic;

namespace Orms.DataAccessHelper.Repositories.Interfaces
{
    public interface IGasAccountRepository<S> :
        IBaseRepository<S, GasAccount>,
        IListBaseRepository<S, GasAccount>
    {
        void Update(S session, IEnumerable<GasAccount> gasAccounts);

        IEnumerable<GasAccount> Get(S session, GasStorage gasStorage, DateTime gasDay);

        IEnumerable<GasAccount> Get(S session, DateTime gasDay);
    }
}
