﻿using Orms.Models;
using System.Collections.Generic;

namespace Orms.DataAccessHelper.Repositories.Interfaces
{
    public interface IAccessPointRepository<S> : IBaseRepository<S, AccessPoint>
    {
        IEnumerable<AccessPoint> GetBy(S session, GasStorage gasStorage);
    }
}
