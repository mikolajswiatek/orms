﻿using System.Collections.Generic;
using Orms.Models;

namespace Orms.DataAccessHelper.Repositories.Interfaces
{
    public interface IAgreementRepository<S> : IBaseRepository<S, Agreement>
    {
        IEnumerable<Agreement> GetBy(S session, GasStorage gasStorage);

        IEnumerable<Agreement> GetBy(S session, Contractor contractor);
    }
}
