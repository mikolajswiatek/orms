﻿using System.Collections.Generic;

namespace Orms.DataAccessHelper.Repositories.Interfaces
{
    public interface IListBaseRepository<S, M>
    {
        void Add(S session, IEnumerable<M> models);
    }
}