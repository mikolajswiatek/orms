﻿using Orms.Models;
using System.Collections.Generic;

namespace Orms.DataAccessHelper.Repositories.Interfaces
{
    public interface INominationValueRepository<S> :
        IBaseRepository<S, NominationValue>,
        IListBaseRepository<S, NominationValue>
    {
        IEnumerable<NominationValue> GetBy(S session, Nomination nomination);
    }
}
