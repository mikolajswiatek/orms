﻿using Orms.Models;

namespace Orms.DataAccessHelper.Repositories.Interfaces
{
    public interface IContractorRepository<S> : IBaseRepository<S, Contractor>
    {
    }
}
