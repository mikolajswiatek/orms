﻿using System;
using Orms.Models;

namespace Orms.DataAccessHelper.Repositories.Interfaces
{
    public interface ICalorificValueRepository<S> : IBaseRepository<S, CalorificValue>
    {
        CalorificValue GetLastBy(S session, GasStorage gasStorage);

        CalorificValue GetForGasDay(S session, GasStorage gasStorage, DateTime gasDay);
    }
}
