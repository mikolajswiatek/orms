﻿using System;
using System.Collections.Generic;
using Orms.Models;

namespace Orms.DataAccessHelper.Repositories.Interfaces
{
    public interface INominationRepository<S> : IBaseRepository<S, Nomination>
    {
        IEnumerable<Nomination> GetBy(S session, Contractor contractor);

        IEnumerable<Nomination> GetBy(S session, GasStorage gasStorage);

        IEnumerable<Nomination> GetBy(S session, User user);

        IEnumerable<Nomination> GetBy(S session, DateTime gasDay);

        Nomination GetLast(S session, Contractor contractor);

        Nomination GetLast(S session, GasStorage gasStorage);

        Nomination GetLast(S session, User user);

        Nomination GetLast(S session, Contractor contractor, DateTime gasDay);

        IEnumerable<Nomination> GetForGasDay(S session, Contractor contractor, DateTime gasDay);

        IEnumerable<Nomination> GetForGasDay(S session, GasStorage gasStorage, DateTime gasDay);

        IEnumerable<Nomination> GetForGasDay(S session, Contractor contractor, GasStorage gasStorage, DateTime gasDay);
    }
}
