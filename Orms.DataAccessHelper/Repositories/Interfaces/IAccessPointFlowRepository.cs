﻿using Orms.Models;

namespace Orms.DataAccessHelper.Repositories.Interfaces
{
    public interface IAccessPointFlowRepository<S> : IBaseRepository<S, AccessPointFlow>
    {
    }
}
