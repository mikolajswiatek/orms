﻿using Orms.Models;

namespace Orms.DataAccessHelper.Repositories.Interfaces
{
    public interface ISessionRepository<S> : IBaseRepository<S, Session>
    {
        Session GetBy(S s, string guid);

        void Delete(S s, Session session);
    }
}
