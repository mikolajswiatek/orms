﻿using System.Collections.Generic;
using Orms.Models;

namespace Orms.DataAccessHelper.Repositories.Interfaces
{
    public interface IContractorContactRepository<S> : IBaseRepository<S, ContractorContact>
    {
        IEnumerable<ContractorContact> GetByContractor(S session, Contractor contractor);
    }
}
