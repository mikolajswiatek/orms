﻿using System.Collections.Generic;
using Orms.Models;

namespace Orms.DataAccessHelper.Repositories.Interfaces
{
    public interface IUserRepository<S> : IBaseRepository<S, User>
    {
        IEnumerable<User> GetBy(S session, Contractor contractor);

        IEnumerable<User> GetBy(S session, string nameOrEmail);
    }
}
