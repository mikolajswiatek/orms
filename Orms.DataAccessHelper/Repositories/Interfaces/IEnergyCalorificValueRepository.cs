﻿using Orms.Models;
using System.Collections.Generic;

namespace Orms.DataAccessHelper.Repositories.Interfaces
{
    public interface IEnergyCalorificValueRepository<S> :
        IBaseRepository<S, EnergyCalorificValue>,
        IListBaseRepository<S, EnergyCalorificValue>
    {
        IEnumerable<EnergyCalorificValue> GetAll(S session, CalorificValue cv);
    }
}
