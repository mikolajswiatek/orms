﻿using Orms.Models;

namespace Orms.DataAccessHelper.Repositories.Interfaces
{
    public interface IGasStorageRepository<S> : IBaseRepository<S, GasStorage>
    {
    }
}
