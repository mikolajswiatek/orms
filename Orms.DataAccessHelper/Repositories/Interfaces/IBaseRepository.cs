﻿using System.Collections.Generic;

namespace Orms.DataAccessHelper.Repositories.Interfaces
{
    public interface IBaseRepository<S, M>
    {
        IEnumerable<M> GetAll(S session);
        M Get(S session, int id);
        void Update(S session, M model);
        void Add(S session, M model);
    }
}
