﻿using System.Collections.Generic;
using Orms.Models;

namespace Orms.DataAccessHelper.Operations.Interfaces
{
    public interface IAccessPointOperation
    {
        IEnumerable<AccessPoint> GetBy(GasStorage gasStorage);

        IEnumerable<AccessPoint> GetAll();

        AccessPoint Get(int id);

        void Add(AccessPoint accessPoint);

        void Add(AccessPointFlow apf, int accessPointId);
    }
}