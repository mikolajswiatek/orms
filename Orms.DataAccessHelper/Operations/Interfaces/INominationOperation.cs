﻿using System;
using System.Collections.Generic;
using Orms.Models;

namespace Orms.DataAccessHelper.Operations.Interfaces
{
    public interface INominationOperation
    {
        void Add(Nomination nomination);

        void Add(IEnumerable<NominationValue> values);

        void Add(Nomination nomination, IEnumerable<NominationValue> values);

        void RefreshNomiation(Nomination nomination, int parentNominationId);

        void AcceptedNomination(Nomination nomination);

        IEnumerable<Nomination> GetBy(Contractor contractor);

        IEnumerable<Nomination> GetBy(GasStorage gasStorage);

        IEnumerable<Nomination> GetBy(User user);

        IEnumerable<Nomination> GetBy(DateTime gasDay);

        Nomination Get(int id);

        Nomination GetLast(Contractor contractor);

        Nomination GetLast(GasStorage gasStorage);

        Nomination GetLast(User user);

        Nomination GetLast(Contractor contractor, DateTime gasDay);

        IEnumerable<Nomination> GetForGasDay(Contractor contractor, DateTime gasDay);

        IEnumerable<Nomination> GetForGasDay(GasStorage gasStorage, DateTime gasDay);

        IEnumerable<Nomination> GetForGasDay(Contractor contractor, GasStorage gasStorage, DateTime gasDay);
    }
}