﻿using System;
using System.Collections.Generic;
using Orms.Models;

namespace Orms.DataAccessHelper.Operations.Interfaces
{
    public interface IGasAccountOperation
    {
        void Update(IEnumerable<GasAccount> gasAccounts);

        IEnumerable<GasAccount> Get(DateTime gasDay);

        IEnumerable<GasAccount> Get(GasStorage gasStorage, DateTime gasDay);

        void Refresh(GasStorage gasStorage);

        void Refresh();

        void Refresh(GasStorage gasStorage, GasDay gasDay);

        void Refresh(GasDay gasDay);
    }
}