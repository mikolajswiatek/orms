﻿using Orms.Models;

namespace Orms.DataAccessHelper.Operations.Interfaces
{
    public interface ISessionOperation
    {
        void Add(Session session);

        void Refresh(string guid);

        Session GetBy(string guid);

        void Delete(Session session);
    }
}