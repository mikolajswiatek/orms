﻿using System.Collections.Generic;
using Orms.Models;

namespace Orms.DataAccessHelper.Operations.Interfaces
{
    public interface IGasStorageOperation
    {
        void Add(GasStorage gasStorage);

        GasStorage Get(int id);

        IEnumerable<GasStorage> GetAll();
    }
}