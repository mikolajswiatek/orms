﻿namespace Orms.DataAccessHelper.Operations.Interfaces
{
    public abstract class Operation<T> 
    {
        protected abstract void SetProviders(T t);
    }
}
