﻿using System.Collections.Generic;
using Orms.Models;

namespace Orms.DataAccessHelper.Operations.Interfaces
{
    public interface IAgreementOperation
    {
        void Add(Agreement agreement);

        Agreement Get(int id);

        IEnumerable<Agreement> GetAll();

        IEnumerable<Agreement> GetAll(Contractor contractor);

        IEnumerable<Agreement> GetAll(GasStorage gasStorage);

        IEnumerable<Agreement> GetAll(Contractor contractor, GasStorage gasStorage);
    }
}
