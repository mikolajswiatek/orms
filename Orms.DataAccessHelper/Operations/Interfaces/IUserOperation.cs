﻿using System.Collections.Generic;
using Orms.Models;

namespace Orms.DataAccessHelper.Operations.Interfaces
{
    public interface IUserOperation
    {
        User Exist(string nameOrEmail);

        void Create(User user);

        void Update(User user);

        User Get(int id);

        IEnumerable<User> Get(Contractor contractor);

        IEnumerable<User> GetAll();
    }
}