﻿using System.Collections.Generic;
using Orms.Models;

namespace Orms.DataAccessHelper.Operations.Interfaces
{
    public interface IContractorOperation
    {
        IEnumerable<Contractor> GetAll();

        IEnumerable<ContractorContact> GetAll(Contractor contractor);

        Contractor Get(int id);

        ContractorContact GetContact(int id);

        void Update(ContractorContact contact);

        void Add(Contractor contractor);

        void Add(ContractorContact contact, int contractorId);
    }
}