﻿using System.Configuration;

namespace Orms.WebApp
{
    public static class Application
    {
        public static readonly string Name = "ORMS";
        public static readonly int Version = 1;
        public static readonly string OrmName = ConfigurationManager.AppSettings["ormName"];

        private static readonly string connectionString = ConfigurationManager
            .ConnectionStrings["AppDB"].ConnectionString;

        public static string ConnectionString => connectionString;
    }
}