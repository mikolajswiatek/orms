﻿using System;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Orms.DataAccessFactory;
using Orms.DataAccessFactory.OperationFactories;
using Orms.DataAccessHelper.Operations.Interfaces;
using Orms.Models;
using Orms.WebApp.Models;

namespace Orms.WebApp.Controllers
{
    public class AgreementsController : Controller
    {
        private IAgreementOperation operation { get; }
        private IContractorOperation contractorOperation { get; }
        private IGasStorageOperation gasStorageOperation { get; }

        public AgreementsController()
        {
            var orm = OrmFactory.Get(Application.OrmName);
            operation = OperationFactory.GetAgreementOperation(orm, Application.ConnectionString);
            contractorOperation = OperationFactory.GetContractorOperation(orm, Application.ConnectionString);
            gasStorageOperation = OperationFactory.GetGasStorageOperation(orm, Application.ConnectionString);

        }

        public ActionResult Index()
        {
            var agreements = operation.GetAll();

            return View(agreements);
        }

        public ActionResult Details(int id)
        {
            var agreement = operation.Get(id);

            if (agreement == null)
            {
                return HttpNotFound();
            }

            return View(agreement);
        }

        public ActionResult Create()
        {
            var model = new AgreementCreateViewModel();
            model.Contractors = contractorOperation.GetAll().Select(c => new SelectListItem() { Text = c.Name, Value = c.Id.ToString() });
            model.GasStorages = gasStorageOperation.GetAll().Select(g => new SelectListItem() { Text = g.Name, Value = g.Id.ToString() });

            return View(model);
        }

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            if (collection == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var contractor = contractorOperation.Get(Int32.Parse(collection["SelectedContractorId"]));
            var gasStorage = gasStorageOperation.Get(Int32.Parse(collection["SelectedGasStorageId"]));

            if (contractor == null || gasStorage == null)
            {
                return HttpNotFound();
            }

            var agreement = new Agreement()
            {
                Contractor = contractor,
                CreateDate = DateTime.Now,
                GasStorage = gasStorage,
                Start = DateTime.Parse(collection["Agreement.Start"]),
                End = DateTime.Parse(collection["Agreement.End"])
            };

            operation.Add(agreement);
            return RedirectToAction("Index");
        }
    }
}
