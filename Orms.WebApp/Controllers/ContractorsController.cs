﻿using System.Web.Mvc;
using Orms.DataAccessFactory;
using Orms.DataAccessFactory.OperationFactories;
using Orms.DataAccessHelper.Operations.Interfaces;
using Orms.Models;
using Orms.WebApp.Models;

namespace Orms.WebApp.Controllers
{
    public class ContractorsController : Controller
    {
        private readonly IContractorOperation _operation;

        public ContractorsController()
        {
            var orm = OrmFactory.Get(Application.OrmName);
            _operation = OperationFactory.GetContractorOperation(orm, Application.ConnectionString);
        }

        public ActionResult Index()
        {
            var contractors = _operation.GetAll();

            return View(contractors);
        }

        public ActionResult Details(int id)
        {
            var contractor = _operation.Get(id);

            if (contractor == null)
            {
                return HttpNotFound();
            }

            var orm = OrmFactory.Get(Application.OrmName);

            var model = new ContractorDetailsViewModel()
            {
                Contractor = contractor,
                Contacts = _operation.GetAll(contractor),
                Agreements = OperationFactory.GetAgreementOperation(orm, Application.ConnectionString).GetAll(contractor),
                Users = OperationFactory.GetUserOperation(orm, Application.ConnectionString).Get(contractor),
                ContractorId = contractor.Id
            };

            return View(model);
        }

        [ValidateAntiForgeryToken]
        public ActionResult CreateContact(ContractorDetailsViewModel model)
        {
            if (ModelState.IsValid)
            {
                var contractor = _operation.Get(model.ContractorId);

                if (contractor == null)
                {
                    return HttpNotFound();
                }

                model.NewContact.Contractor = contractor;
                _operation.Add(model.NewContact, model.ContractorId);
            }

            return RedirectToAction("Details", new { id = model.ContractorId });
        }

        public ActionResult Create()
        {
           return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Name, Code, Adress")] Contractor contractor)
        {
            if (ModelState.IsValid)
            {
                _operation.Add(contractor);
                return RedirectToAction("Index");
            }

            return View(contractor);
        }
    }
}