﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Orms.DataAccessFactory;
using Orms.DataAccessFactory.OperationFactories;
using Orms.DataAccessHelper.Operations.Interfaces;
using Orms.Domains.Exceptions;
using Orms.Instractures;
using Orms.Models;
using Orms.WebApp.Models;

namespace Orms.WebApp.Controllers
{
    public class AccountController : Controller
    {
        private IUserOperation Operation;
        private ISessionOperation SessionOperation;

        public AccountController()
        {
            var orm = OrmFactory.Get(Application.OrmName);
            Operation = OperationFactory.GetUserOperation(orm, Application.ConnectionString);
            SessionOperation = OperationFactory.GetSessionOperation(orm, Application.ConnectionString);
        }

        public ActionResult Index()
        {
            RefreshSession();

            return View();
        }

        private void RefreshSession()
        {
            var sessionCookie = Request.Cookies["sessionId"];

            if (sessionCookie != null)
            {
                SessionOperation.Refresh(sessionCookie.Value);
                sessionCookie.Expires = DateTime.Now.AddDays(1);
                Response.Cookies.Add(sessionCookie);
            }
        }

        public ActionResult ForgotPassword(User user)
        {
            if (ModelState.IsValid)
            {
                var u = Operation.Exist(user.Name);

                if (u == null) return HttpNotFound();

                if (user.Email == u.Email)
                {
                    var newPassowrd = Guid.NewGuid().ToString(); ;
                    u.Password = Encryption.Hash(newPassowrd); ;
                    Operation.Update(u);

                    var model = new ForgotPasswordViewModel()
                    {
                        Name = u.Name,
                        Password = newPassowrd
                    };

                    return View(model);
                }

                throw new UserExistException(user.Name, user.Email);
            }

            return RedirectToAction("Index");
        }

        public ActionResult Login(User user)
        {
            if (ModelState.IsValid)
            {
                var u = Operation.Exist(user.Name);

                if (u == null) return HttpNotFound();

                if (Encryption.Hash(user.Password) == u.Password)
                {
                    FormsAuthentication.SetAuthCookie(user.Email, false);

                    var authTicket = new FormsAuthenticationTicket(
                        1, u.Email, DateTime.Now, DateTime.Now.AddMinutes(20), 
                        false, "user",
                        FormsAuthentication.FormsCookiePath);

                    var encryptedTicket = FormsAuthentication.Encrypt(authTicket);
                    var authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
                    HttpContext.Response.Cookies.Add(authCookie);

                    var session = new Session()
                    {
                        Guid = authCookie.Value,
                        Browser = Request.Browser.Id,
                        CreateDatetime = DateTime.Now,
                        LastActivityDatetime = DateTime.Now,
                        User = u,
                        Ip = GetIPAddress()
                    };

                    SessionOperation.Add(session);

                    return RedirectToAction("Index");
                }

                throw new UserPasswordIncorrectException();
            }

            throw new UserExistException(user.Name, string.Empty);
        }

        private string GetIPAddress()
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipAddress))
            {
                string[] addresses = ipAddress.Split(',');
                if (addresses.Length != 0)
                {
                    return addresses[0];
                }
            }

            return context.Request.ServerVariables["REMOTE_ADDR"];
        }

    }
}