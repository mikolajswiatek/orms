﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Orms.DataAccessFactory;
using Orms.DataAccessFactory.OperationFactories;
using Orms.DataAccessHelper.Operations.Interfaces;
using Orms.Domains.Enums;
using Orms.Domains.Units;
using Orms.Models;
using Orms.WebApp.Models;

namespace Orms.WebApp.Controllers
{
    public class GasAccountsController : Controller
    {
        private readonly IGasStorageOperation GasStorageOperation;
        private readonly IGasAccountOperation Operation;

        public GasAccountsController()
        {
            var orm = OrmFactory.Get(Application.OrmName);
            Operation = OperationFactory.GetGasAccountOperation(orm, Application.ConnectionString);
            GasStorageOperation = OperationFactory.GetGasStorageOperation(orm, Application.ConnectionString);
        }

        public ActionResult Index()
        {
            var gasStorages = GasStorageOperation.GetAll();

            var model = new GasAccountsViewModel()
            {
                GasDay = GasDay.Today.First,
                GasStorages = gasStorages
            };

            return View(model);
        }

        public ActionResult Details(int id, DateTime? gasDay = null)
        {
            if (gasDay == null)
            {
                gasDay = DateTime.Now;
            }

            var gasStorage = GasStorageOperation.Get(id);

            if (gasStorage == null)
            {
                return HttpNotFound();
            }

            var gd = new GasDay((DateTime)gasDay);

            var gasAccounts = Operation.Get(gasStorage, gd.First);

            if (gasAccounts.Count() == gd.Hours.Count()) return View(gasAccounts);

            var values = new List<GasAccount>();

            for (var index = 0; index < new GasDay(DateTime.Today).Hours.Count(); index++)
            {
                values.Add(new GasAccount()
                {
                    GasStorage = gasStorage,
                    HourIndex = index,
                    GasDay = gd.First,
                    Energy = new UnitValue(0, Unit.Kwh),
                    Volume = new UnitValue(0, Unit.M3)
                });
            }

            gasAccounts = values;

            return View(gasAccounts);
        }

        public ActionResult Calculate(int id, bool all, string gasDayString)
        {
            var gasDay = DateTime.Parse(gasDayString);

            if (all)
            {
                Operation.Refresh(new GasDay(gasDay));
                return RedirectToAction("Index");

            }

            var gasStorage = GasStorageOperation.Get(id);
            Operation.Refresh(gasStorage, new GasDay(gasDay));

            return RedirectToAction("Details", new {id= id, gasDay = gasDay});
        }
    }
}