﻿using System;
using System.Web.Mvc;
using Orms.DataAccessFactory;
using Orms.DataAccessFactory.OperationFactories;
using Orms.DataAccessHelper.Operations.Interfaces;
using Orms.Domains.Enums;
using Orms.Models;
using Orms.WebApp.Models;

namespace Orms.WebApp.Controllers
{
    public class GasStoragesController : Controller
    {
        private readonly IGasStorageOperation Operation;
        private readonly IAccessPointOperation AccessPointOperation;

        public GasStoragesController()
        {
            var orm = OrmFactory.Get(Application.OrmName);
            Operation = OperationFactory.GetGasStorageOperation(orm, Application.ConnectionString);
            AccessPointOperation = OperationFactory.GetAccessPointOperation(orm, Application.ConnectionString);
        }

        public ActionResult Index()
        {
            var gasStorages = Operation.GetAll();
            return View(gasStorages);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Name")] GasStorage gasStorage)
        {
            if (ModelState.IsValid)
            {
                Operation.Add(gasStorage);
                return RedirectToAction("Index");
            }

            return View(gasStorage);
        }

        public ActionResult Details(int id)
        {
            var gasStorage = Operation.Get(id);

            if (gasStorage == null)
            {
                return HttpNotFound();
            }

            gasStorage.AccessPoints = AccessPointOperation.GetBy(gasStorage);

            var model = new GasStorageDetailsViewModel()
            {
                GasStorage = gasStorage,

            };

            ViewBag.AccessPointId = new SelectList(gasStorage.AccessPoints, "SelectedAccessPointId", "Code");
            return View(model);
        }

        public ActionResult CreateAccessPoint(FormCollection collection)
        {
            var gasStorageId = Int32.Parse(collection["GasStorageId"]);

            if (ModelState.IsValid)
            {
                var gasStorage = Operation.Get(gasStorageId);

                if (gasStorage == null)
                {
                    return HttpNotFound();
                }

                var ap = new AccessPoint()
                {
                    Code = collection["NewAccessPoint.Code"],
                    GasStorage = gasStorage
                };

                AccessPointOperation.Add(ap);
                return RedirectToAction("Details", new { id = gasStorage.Id });
            }

            return RedirectToAction("Details", new { id = gasStorageId });
        }

        public ActionResult CreateAccessPointFlow(FormCollection collection)
        {
            var selectedAccessPointId = Int32.Parse(collection["SelectedAccessPointId"]);

            if (ModelState.IsValid)
            {
                var ap = AccessPointOperation.Get(selectedAccessPointId);

                if (ap == null)
                {
                    return HttpNotFound();
                }

                var apf = new AccessPointFlow()
                {
                    AccessPoint = ap,
                    Direction = collection["NewAccessPointFlow.Direction"] == "0" ?
                        DirectionType.Injection :
                        DirectionType.Withdrawal,
                };

                AccessPointOperation.Add(apf, selectedAccessPointId);

                return RedirectToAction("Details", new { id = ap.GasStorage.Id });
            }
            else
            {
                var ap = AccessPointOperation.Get(selectedAccessPointId);

                if (ap == null)
                {
                    return HttpNotFound();
                }

                return RedirectToAction("Details", new { id = ap.GasStorage.Id });
            }
        }
    }
}