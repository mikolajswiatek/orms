﻿using System;
using System.Linq;
using System.Web.Mvc;
using Orms.DataAccessFactory;
using Orms.DataAccessFactory.OperationFactories;
using Orms.DataAccessHelper.Operations.Interfaces;
using Orms.Domains.Exceptions;
using Orms.Instractures;
using Orms.Models;
using Orms.WebApp.Models;

namespace Orms.WebApp.Controllers
{
    public class UsersController : Controller
    {
        private readonly IUserOperation _operation;
        private readonly IContractorOperation _contractorOperationoperation;

        public UsersController()
        {
            var orm = OrmFactory.Get(Application.OrmName);
            _operation = OperationFactory.GetUserOperation(orm, Application.ConnectionString);
            _contractorOperationoperation = OperationFactory.GetContractorOperation(orm, Application.ConnectionString);
        }

        public ActionResult Index()
        {
            var users = _operation.GetAll();
            return View(users);
        }

        public ActionResult Create()
        {
            var model = new UserCreateViewModel()
            {
                Contractors = _contractorOperationoperation.GetAll()
                    .Select(c => new SelectListItem() { Value = c.Id.ToString(), Text = c.Name})
            };

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(UserCreateViewModel model)
        {
            if (ModelState.IsValid)
            {
                var userExist = _operation.Exist(model.User.Name);

                if (userExist == null)
                {
                    userExist = _operation.Exist(model.User.Email);

                    if (userExist == null)
                    {
                        var user = model.User;
                        user.CreateDateTime = DateTime.Now;
                        user.LastActivityDateTime = DateTime.Now;
                        user.IsActive = true;
                        user.Contractor = _contractorOperationoperation.Get(model.ContractorId);
                        user.Password = Encryption.Hash(user.Password);

                        _operation.Create(user);
                        return RedirectToAction("Index");
                    }

                    throw new UserExistException(model.User.Name, model.User.Email);
                }
            }

            throw new UserExistException(model.User.Name, string.Empty);

            return View(model);
        }

        public ActionResult Details(int id)
        {
            var user = _operation.Get(id);
            return View(user);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Details(User user)
        {
            if (ModelState.IsValid)
            {
                var existUser = _operation.Get(user.Id);
                existUser.Email = user.Email;
                existUser.IsActive = user.IsActive;

                if (user.Password != null)
                {
                    existUser.Password = Encryption.Hash(user.Password);
                }

                _operation.Update(existUser);

                return View(existUser);

            }

            return View(user);
        }
    }
}