﻿using System;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Web;
using System.Web.Mvc;
using Orms.DataAccessFactory;
using Orms.DataAccessFactory.OperationFactories;
using Orms.DataAccessHelper.Operations.Interfaces;
using Orms.Domains.Exceptions;
using Orms.Instractures;
using Orms.Models;
using Orms.WebApp.Models;
using System.Web.Mvc;
using Orms.DataAccessFactory;
using Orms.DataAccessFactory.OperationFactories;
using Orms.DataAccessHelper.Operations.Interfaces;
using Orms.Domains.Enums;
using Orms.Domains.Exceptions;
using Orms.Domains.Units;
using Orms.Models;
using Orms.Models.Extensions;
using Orms.WebApp.Models;

namespace Orms.WebApp.Controllers
{
    public class NominationsController : Controller
    {
        private readonly IGasStorageOperation GasStorageOperation;
        private readonly IAccessPointOperation AccessPointOperation;
        private readonly ICalorificValueOperation CalorificValueOperation;
        private readonly IContractorOperation ContractorOperation;
        private readonly IAgreementOperation AgreementOperation;
        private readonly INominationOperation Operation;

        public NominationsController()
        {
            var orm = OrmFactory.Get(Application.OrmName);
            Operation = OperationFactory.GetNominationOperation(orm, Application.ConnectionString);
            GasStorageOperation = OperationFactory.GetGasStorageOperation(orm, Application.ConnectionString);
            CalorificValueOperation = OperationFactory.GetCalorificValueOperation(orm, Application.ConnectionString);
            ContractorOperation = OperationFactory.GetContractorOperation(orm, Application.ConnectionString);
            AccessPointOperation = OperationFactory.GetAccessPointOperation(orm, Application.ConnectionString);
            AgreementOperation = OperationFactory.GetAgreementOperation(orm, Application.ConnectionString);
        }

        public ActionResult Index(DateTime? gasDay = null)
        {
            IEnumerable<Nomination> nominations;

            if (gasDay == null)
            {
                nominations = Operation.GetBy(GasDay.Today.First);
            }
            else
            {
                var datetime = (DateTime) gasDay;
                nominations = Operation.GetBy(new GasDay(datetime).First);
            }

            return View(new NominationListViewModel() { GasDay = gasDay ?? GasDay.Today.First, Nominations = nominations });
        }

        public ActionResult Create(DateTime? gasDay = null)
        {
            var nomination = new Nomination();

            var values = new List<NominationValue>();

            for (var index = 0; index < new GasDay(DateTime.Today).Hours.Count(); index++)
            {
                values.Add(new NominationValue()
                {
                    HourIndex = index,
                    Energy = new UnitValue(0, Unit.Kwh),
                    Volume = new UnitValue(0, Unit.M3)
                });
            }

            nomination.Values = values;
            nomination.GasDay = GasDay.Today.First;

            var model = new NominationCreateViewModel()
            {
                Nomination = nomination,
                Contractors = ContractorOperation.GetAll().Select(c => new SelectListItem() { Value = c.Id.ToString(), Text = c.Name }),
                GasStorages = GasStorageOperation.GetAll().Select(g => new SelectListItem() { Value = g.Id.ToString(), Text = g.Name }),
            };

            return View(model);
        }

        public ActionResult Get(int gasStorageId, DateTime? gasDay = null)
        {
            var day = gasDay == null ? new GasDay(DateTime.Now).First : new GasDay((DateTime)gasDay).First;

            var gasStorage = GasStorageOperation.Get(gasStorageId);
            gasStorage.AccessPoints = AccessPointOperation.GetBy(gasStorage);
            var cv = CalorificValueOperation.GetForGasDay(gasStorage, day);

            if (cv != null)
            {
                return Json(cv.Values.Select(cvv => new { HourIndex = cvv.HourIndex, Value = cvv.Energy}), JsonRequestBehavior.AllowGet);
            }

            var values = GetEmptyCV();

            return Json(values.Select(cvv => new { HourIndex = cvv.HourIndex, Value = cvv.Energy }), JsonRequestBehavior.AllowGet);
        }

        private static List<EnergyCalorificValue> GetEmptyCV(CalorificValue cv = null)
        {
            var values = new List<EnergyCalorificValue>();

            for (var index = 0; index < new GasDay(DateTime.Today).Hours.Count(); index++)
            {
                values.Add(new EnergyCalorificValue()
                {
                    HourIndex = index,
                    Energy = new UnitValue(0, Unit.Kwh),
                    CalorificValue = cv
                });
            }

            return values;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(FormCollection collection)
        {
            var gasDay = DateTime.Parse(collection["Nomination.GasDay"]);
            var gasStorage = GasStorageOperation.Get(Int32.Parse(collection["SelectedGasStorageId"]));
            var contractor = ContractorOperation.Get(Int32.Parse(collection["SelectedContractorId"]));

            var agreements = AgreementOperation.GetAll(contractor, gasStorage);

            if (!agreements.Any(a => a.Start <= gasDay && a.End >= gasDay))
            {
                throw new AgreementExistException();
            }

            var energyValue = collection.GetValues("value.Energy.Value")
                .Select(v => Decimal.Parse(v.Replace('.', ',')));

            var volumeValue = collection.GetValues("value.Volume.Value")
                .Select(v => Decimal.Parse(v.Replace('.', ',')));

            var accespoints = AccessPointOperation.GetBy(gasStorage);
            var commnet = collection["Nomination.Comment"];
            var directionType = EnumHelper.ParseEnum<DirectionType>(collection["DirectionType"]);
            var cv = CalorificValueOperation.GetForGasDay(gasStorage, gasDay);
            var name = collection["Nomination.Name"];

            if (cv == null)
            {
                cv = new CalorificValue()
                {
                    GasStorage = gasStorage,
                    GasDay = gasDay
                };

                var cvValues = GetEmptyCV(cv);

                cv.Values = cvValues;
            }

            var values = new List<NominationValue>();
            AccessPointFlow apf = new AccessPointFlow();

            foreach (var ap in accespoints)
            {
                var af = ap.AccessPointFlows.FirstOrDefault(a => a.Direction == directionType);

                if (af == null) continue;

                apf = af;

                if (apf.AccessPoint == null) apf.AccessPoint = ap;
                if (apf.AccessPoint.GasStorage.AccessPoints.Count() == 0) apf.AccessPoint.GasStorage.AccessPoints = accespoints;

                break;
            }

            if (apf.AccessPoint == null)
            {
                throw new AccessPointFlowExistException();
            }

            var nomination = new Nomination()
            {
                Channel = Channel.Application,
                GasDay = gasDay,
                Contractor = contractor,
                Name = name,
                Comment = commnet,
                AccessPointFlow = apf,
                CalorificValue = cv,
                Creator = GetCreator(),
                CreateDateTime = DateTime.Now
            };

            for (var index = 0; index < energyValue.Count(); index++)
            {
                values.Add(new NominationValue()
                {
                    HourIndex = index,
                    Energy = new UnitValue(energyValue.ElementAt(index), Unit.Kwh),
                    Volume = new UnitValue(volumeValue.ElementAt(index), Unit.M3),
                    Nomination = nomination
                });
            }

            nomination.Values = values;

            Operation.Add(nomination);

            return RedirectToAction("Index");
        }

        private User GetCreator()
        {
            var orm = OrmFactory.Get(Application.OrmName);
            var sessionCookie = Request.Cookies["sessionId"];

            if (sessionCookie != null)
            {
                OperationFactory.GetSessionOperation(orm, Application.ConnectionString).Refresh(sessionCookie.Value);
                sessionCookie.Expires = DateTime.Now.AddDays(1);
                Response.Cookies.Add(sessionCookie);
            }

            if (sessionCookie != null)
            {
                var user = OperationFactory.GetSessionOperation(orm, Application.OrmName).GetBy(sessionCookie.Value).User;
                return user;
            }

            return OperationFactory.GetUserOperation(orm, Application.ConnectionString).GetAll().First();
        }

        public ActionResult Details(int id)
        {
            var nomination = Operation.Get(id);

            if (nomination == null)
            {
                return HttpNotFound();
            }

            return View(nomination);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Details(FormCollection collection)
        {
            var parentNomination = Operation.Get(Int32.Parse(collection["Id"]));

            if (parentNomination == null)
            {
                return HttpNotFound();
            }

            var agreements = AgreementOperation.GetAll(parentNomination.Contractor, parentNomination.AccessPointFlow.AccessPoint.GasStorage);

            if (!agreements.Any(a => a.Start <= parentNomination.GasDay && a.End >= parentNomination.GasDay))
            {
                throw new AgreementExistException();
            }

            var energyValue = collection.GetValues("value.Energy.Value")
                .Select(v => Decimal.Parse(v));

            var volumeValue = collection.GetValues("value.Volume.Value")
                .Select(v => Decimal.Parse(v));

            var cv = CalorificValueOperation.GetForGasDay(parentNomination.AccessPointFlow.AccessPoint.GasStorage, parentNomination.GasDay);

            if (cv == null)
            {
                cv = new CalorificValue()
                {
                    GasStorage = parentNomination.AccessPointFlow.AccessPoint.GasStorage,
                    GasDay = parentNomination.GasDay
                };

                var cvValues = GetEmptyCV(cv);

                cv.Values = cvValues;
            }

            var nomination = new Nomination()
            {
                Contractor = parentNomination.Contractor,
                AccessPointFlow = parentNomination.AccessPointFlow,
                CalorificValue = cv,
                Channel = parentNomination.Channel,
                Comment = parentNomination.Comment,
                CreateDateTime = parentNomination.CreateDateTime,
                Creator = GetCreator(),
                GasDay = parentNomination.GasDay,
                ParentNomination = parentNomination,
                Name = parentNomination.Name,
                MajorVersion = parentNomination.MajorVersion,
                Version = parentNomination.Version + 1
            };

            var values = new List<NominationValue>();
            for (var index = 0; index < energyValue.Count(); index++)
            {
                values.Add(new NominationValue()
                {
                    HourIndex = index,
                    Energy = new UnitValue(energyValue.ElementAt(index), Unit.Kwh),
                    Volume = new UnitValue(volumeValue.ElementAt(index), Unit.M3),
                    Nomination = nomination
                });
            }

            nomination.Values = values;

            Operation.RefreshNomiation(nomination, parentNomination.Id);

            return View(nomination);
        }

        public ActionResult Accepted(int id)
        {
            var nomination = Operation.Get(id);

            if (nomination == null)
            {
                return HttpNotFound();
            }

            Operation.AcceptedNomination(nomination);

            return RedirectToAction("Index");
        }
    }
}