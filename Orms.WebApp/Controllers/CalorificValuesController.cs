﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using Orms.DataAccessFactory;
using Orms.DataAccessFactory.OperationFactories;
using Orms.DataAccessHelper.Operations.Interfaces;
using Orms.Domains.Enums;
using Orms.Domains.Units;
using Orms.Models;

namespace Orms.WebApp.Controllers
{
    public class CalorificValuesController : Controller
    {
        private readonly IGasStorageOperation GasStorageOperation;
        private readonly ICalorificValueOperation Operation;

        public CalorificValuesController()
        {
            var orm = OrmFactory.Get(Application.OrmName);
            Operation = OperationFactory.GetCalorificValueOperation(orm, Application.ConnectionString);
            GasStorageOperation = OperationFactory.GetGasStorageOperation(orm, Application.ConnectionString);
        }

        public ActionResult Index()
        {
            var gasStorages = GasStorageOperation.GetAll();
            return View(gasStorages);
        }

        public ActionResult Details(int id, DateTime? gasDay = null)
        {
            if (gasDay == null)
            {
                gasDay = DateTime.Now;
            }

            var gasStorage = GasStorageOperation.Get(id);

            if (gasStorage == null)
            {
                return HttpNotFound();
            }

            var gd = new GasDay((DateTime)gasDay);

            var cv = Operation.GetForGasDay(gasStorage, gd.First);

            if (cv != null) return View(cv);

            cv = new CalorificValue()
            {
                GasStorage = gasStorage,
                GasDay = gd.First,
            };

            var values = new List<EnergyCalorificValue>();

            for (var index = 0; index < new GasDay(DateTime.Today).Hours.Count(); index++)
            {
                values.Add(new EnergyCalorificValue()
                {
                    CalorificValue = cv,
                    HourIndex = index,
                    Energy = new UnitValue(0, Unit.Kwh)
                });
            }

            cv.Values = values;

            return View(cv);
        }

        public ActionResult Create(FormCollection collection)
        {
            var values = collection
                .GetValues("value.Energy.Value")
                .Select(v => Decimal.Parse(v));

            var cvValues = new List<EnergyCalorificValue>();

            var gasDay = collection.GetValue("gasDay") == null
                ? GasDay.Today.First
                : TryParseDaeTime(collection.Get("gasDay"));

            var gasStorage = GasStorageOperation.Get(Int32.Parse(collection["gasStorageId"]));

            var cv = new CalorificValue()
            {
                GasStorage = gasStorage,
                GasDay = gasDay
            };

            for (var index = 0; index < values.Count(); index++)
            {
                var val = new EnergyCalorificValue()
                {
                    HourIndex = index,
                    Energy = new UnitValue(values.ElementAt(index), Unit.Kwh),
                    CalorificValue = cv
                };

                cvValues.Add(val);
            }

            cv.Values = cvValues;

            Operation.Add(cv, cv.Values);

            return RedirectToAction("Details", new { id = gasStorage.Id, gasDay = gasDay });
        }

        private static DateTime TryParseDaeTime(string value)
        {
            var gasDay = GasDay.Today.First;

            var status = DateTime.TryParse(value, out gasDay);

            if (!status)
            {
                status = DateTime.TryParseExact(value, "yyyyMMdd", null, System.Globalization.DateTimeStyles.None, out gasDay);
            }

            if (status)
            {
                gasDay = new GasDay(gasDay).First;
            }

            return gasDay;
        }
    }
}