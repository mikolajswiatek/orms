﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Orms.Models;

namespace Orms.WebApp.Models
{
    public class AgreementCreateViewModel
    {
        public IEnumerable<SelectListItem> Contractors { get; set; }

        public IEnumerable<SelectListItem> GasStorages { get; set; }

        public Agreement Agreement { get; set; }

        public int SelectedContractorId { get; set; }

        public int SelectedGasStorageId { get; set; }

        public AgreementCreateViewModel()
        {
            Contractors = new List<SelectListItem>();
            GasStorages = new List<SelectListItem>();
            Agreement = new Agreement();
            Agreement.Start = DateTime.Now;
            Agreement.End = DateTime.Now;
        }
    }
}