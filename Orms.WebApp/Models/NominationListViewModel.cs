﻿using System;
using System.Collections.Generic;
using Orms.Models;

namespace Orms.WebApp.Models
{
    public class NominationListViewModel
    {
        public IEnumerable<Nomination> Nominations { get; set; }

        public DateTime GasDay { get; set; }
    }
}