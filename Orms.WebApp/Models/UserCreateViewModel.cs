﻿using System.Collections.Generic;
using System.Web.Mvc;
using Orms.Models;

namespace Orms.WebApp.Models
{
    public class UserCreateViewModel
    {
        public User User { get; set; }

        public int ContractorId { get; set; }

        public IEnumerable<SelectListItem> Contractors { get; set; }
    }
}