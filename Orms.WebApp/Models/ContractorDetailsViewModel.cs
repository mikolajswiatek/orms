﻿using System.Collections.Generic;
using Orms.Models;

namespace Orms.WebApp.Models
{
    public class ContractorDetailsViewModel
    {
        public Contractor Contractor { get; set; }

        public ContractorContact NewContact { get; set; }

        public int ContractorId { get; set; }

        public IEnumerable<ContractorContact> Contacts { get; set; }

        public IEnumerable<Agreement> Agreements { get; set; }

        public IEnumerable<User> Users { get; set; }

        public ContractorDetailsViewModel()
        {
            Contacts = new List<ContractorContact>();
            Agreements = new List<Agreement>();
            Users = new List<User>();
            NewContact = new ContractorContact();
        }
    }
}