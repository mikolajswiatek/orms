﻿namespace Orms.WebApp.Models
{
    public class ForgotPasswordViewModel
    {
        public string Name { get; set; }

        public string Password { get; set; }
    }
}