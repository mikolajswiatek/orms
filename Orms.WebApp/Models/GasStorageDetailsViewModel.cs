﻿using Orms.Models;

namespace Orms.WebApp.Models
{
    public class GasStorageDetailsViewModel
    {
        public GasStorage GasStorage { get; set; }

        public AccessPoint NewAccessPoint { get; set; }

        public AccessPointFlow NewAccessPointFlow { get; set; }

        public int SelectedAccessPointId { get; set; }

        public int GasStorageId { get; set; }
    }
}