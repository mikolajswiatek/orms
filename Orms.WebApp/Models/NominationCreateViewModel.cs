﻿using System.Collections.Generic;
using System.Web.Mvc;
using Orms.Domains.Enums;
using Orms.Models;

namespace Orms.WebApp.Models
{
    public class NominationCreateViewModel
    {
        public IEnumerable<SelectListItem> Contractors { get; set; }

        public IEnumerable<SelectListItem> GasStorages { get; set; }

        public Nomination Nomination { get; set; }

        public DirectionType DirectionType { get; set; }

        public int SelectedGasStorageId { get; set; }

        public int SelectedContractorId { get; set; }
    }
}