﻿using System;
using System.Collections.Generic;
using Orms.Models;

namespace Orms.WebApp.Models
{
    public class GasAccountsViewModel
    {
        public IEnumerable<GasStorage> GasStorages { get; set; }

        public DateTime GasDay { get; set; }
    }
}