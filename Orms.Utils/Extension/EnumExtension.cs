﻿using System;

namespace Orms.Utils.Extension
{
    public static class EnumExtension
    {
        public static T Parse<T>(this T t, string value)
        {
            return (T)Enum.Parse(typeof(T), value, true);
        }
    }
}