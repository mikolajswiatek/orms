﻿using System;
using System.Data.SqlClient;
using NHibernate;
using Orms.DataAccessDapper;
using Orms.DataAccessDataObjectsNet;
using Orms.DataAccessEco;
using Orms.DataAccessEF;
using Orms.DataAccessFluentNHibernate;
using Orms.DataAccessHelper;
using Orms.DataAccessLLBLGenPro;
using Orms.DataAccessLLBLGenPro.DatabaseSpecific;
using Orms.DataAccessNHibernate;
using Xtensive.Orm;

namespace Orms.DataAccessFactory
{
    public static class HelperFactory
    {
        private static SessionWorker<SqlConnection> DapperHelper { get; set; }
        private static SessionWorker<Domain> DataObjectsNetHelper { get; set; }
        private static SessionWorker<EcoProject1EcoSpace> EcoProject1EcoSpaceHelper { get; set; }
        private static SessionWorker<GasContext> EnityFrameworkHelper { get; set; }
        private static SessionWorker<ISession> FluentNHibernateHelper { get; set; }
        private static SessionWorker<DataAccessAdapter> LlblGenProHelper { get; set; }
        private static SessionWorker<ISession> NHibernateHelper { get; set; }

        public static SessionWorker<SqlConnection> GetDapper(string connectionString)
        {
            if (DapperHelper == null)
            {
                DapperHelper = new DapperHelper(connectionString);
            }

            if (!DapperHelper.isOpen)
            {
                DapperHelper.OpenSession();
            }

            return DapperHelper;
        }

        public static SessionWorker<Domain> GetDataObjectsNetHelper(string connectionString)
        {
            if (DataObjectsNetHelper == null)
            {
                DataObjectsNetHelper = new DataObjectsNetHelper(connectionString);
            }

            if (!DataObjectsNetHelper.isOpen)
            {
                DataObjectsNetHelper.OpenSession();
            }

            return DataObjectsNetHelper;
        }

        public static SessionWorker<EcoProject1EcoSpace> GetEco(string connectionString)
        {
            if (EcoProject1EcoSpaceHelper == null)
            {
                EcoProject1EcoSpaceHelper = new EcoHelper(connectionString);
            }

            if (!EcoProject1EcoSpaceHelper.isOpen)
            {
                EcoProject1EcoSpaceHelper.OpenSession();
            }

            return EcoProject1EcoSpaceHelper;
        }

        public static SessionWorker<GasContext> GetEF(string connectionString)
        {
            if (EnityFrameworkHelper == null)
            {
                EnityFrameworkHelper = new EnityFrameworkHelper(connectionString);
            }

            if (!EnityFrameworkHelper.isOpen)
            {
                EnityFrameworkHelper.OpenSession();
            }

            return EnityFrameworkHelper;
        }

        public static SessionWorker<ISession> GetFluentNHibernateHelper(string connectionString)
        {
            if (FluentNHibernateHelper == null)
            {
                FluentNHibernateHelper = new FluentNHibernateHelper(connectionString);
            }

            if (!FluentNHibernateHelper.isOpen)
            {
                FluentNHibernateHelper.OpenSession();
            }

            return FluentNHibernateHelper;
        }

        public static SessionWorker<DataAccessAdapter> GetLlblGenProHelper(string connectionString)
        {
            if (LlblGenProHelper == null)
            {
                LlblGenProHelper = new LlblGenProHelper(connectionString);
            }

            if (!LlblGenProHelper.isOpen)
            {
                LlblGenProHelper.OpenSession();
            }

            return LlblGenProHelper;
        }

        public static SessionWorker<ISession> GetNHibernateHelper(string connectionString)
        {
            if (NHibernateHelper == null)
            {
                NHibernateHelper = new NHibernateHelper(connectionString);
            }

            if (!NHibernateHelper.isOpen)
            {
                NHibernateHelper.OpenSession();
            }

            return NHibernateHelper;
        }
    }
}