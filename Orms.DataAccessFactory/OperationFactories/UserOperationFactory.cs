﻿using System;
using Orms.DataAccessHelper.Operations.Interfaces;
using DapperOperations = Orms.DataAccessDapper.Operations;
using DataObjectsNetOperations = Orms.DataAccessDataObjectsNet.Operations;
using EcoOperations = Orms.DataAccessEco.Operations;
using EFOperations = Orms.DataAccessEF.Operations;
using FluentNHibernateOperations = Orms.DataAccessFluentNHibernate.Operations;
using LLBLGenProOperations = Orms.DataAccessLLBLGenPro.Operations;
using NHibernateOperations = Orms.DataAccessNHibernate.Operations;

namespace Orms.DataAccessFactory.OperationFactories
{
    public static partial class OperationFactory
    {
        public static IUserOperation GetUserOperation(Orm type, string connectionString)
        {
            IUserOperation operation;

            switch (type)
            {
                case Orm.Dapper:
                    var dapperHelper = HelperFactory.GetDapper(connectionString);
                    operation = new DapperOperations.UserOperation(dapperHelper);

                    return operation;

                case Orm.DataObjectsNet:
                    var dataBObjectsNetHelper = HelperFactory.GetDataObjectsNetHelper(connectionString);
                    operation = new DataObjectsNetOperations.UserOperation(dataBObjectsNetHelper);

                    return operation;

                case Orm.Eco:
                    var ecoHelper = HelperFactory.GetEco(connectionString);
                    operation = new EcoOperations.UserOperation(ecoHelper);

                    return operation;

                case Orm.EF:
                    var entityFrameworHelper = HelperFactory.GetEF(connectionString);
                    operation = new EFOperations.UserOperation(entityFrameworHelper);

                    return operation;

                case Orm.FluentNHibernate:
                    var fluentNHibernateHelper = HelperFactory.GetFluentNHibernateHelper(connectionString);
                    operation = new FluentNHibernateOperations.UserOperation(fluentNHibernateHelper);

                    return operation;

                case Orm.LLBLGenPro:
                    var llblGenProHelper = HelperFactory.GetLlblGenProHelper(connectionString);
                    operation = new LLBLGenProOperations.UserOperation(llblGenProHelper);

                    return operation;

                case Orm.NHibernate:
                    var nHibernateHelper = HelperFactory.GetNHibernateHelper(connectionString);
                    operation = new NHibernateOperations.UserOperation(nHibernateHelper);

                    return operation;

                default:
                    throw new NotImplementedException();
            }
        }
    }
}