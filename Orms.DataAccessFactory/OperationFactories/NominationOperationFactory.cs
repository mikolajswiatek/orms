﻿using System;
using Orms.DataAccessHelper.Operations.Interfaces;
using DapperOperations = Orms.DataAccessDapper.Operations;
using DataObjectsNetOperations = Orms.DataAccessDataObjectsNet.Operations;
using EcoOperations = Orms.DataAccessEco.Operations;
using EFOperations = Orms.DataAccessEF.Operations;
using FluentNHibernateOperations = Orms.DataAccessFluentNHibernate.Operations;
using LLBLGenProOperations = Orms.DataAccessLLBLGenPro.Operations;
using NHibernateOperations = Orms.DataAccessNHibernate.Operations;

namespace Orms.DataAccessFactory.OperationFactories
{
    public static partial class OperationFactory
    {
        public static INominationOperation GetNominationOperation(Orm type, string connectionString)
        {
            INominationOperation operation;

            switch (type)
            {
                case Orm.Dapper:
                    var dapperHelper = HelperFactory.GetDapper(connectionString);
                    operation = new DapperOperations.NominationOperation(dapperHelper);

                    return operation;

                case Orm.DataObjectsNet:
                    var dataBObjectsNetHelper = HelperFactory.GetDataObjectsNetHelper(connectionString);
                    operation = new DataObjectsNetOperations.NominationOperation(dataBObjectsNetHelper);

                    return operation;

                case Orm.Eco:
                    var ecoHelper = HelperFactory.GetEco(connectionString);
                    operation = new EcoOperations.NominationOperation(ecoHelper);

                    return operation;

                case Orm.EF:
                    var entityFrameworHelper = HelperFactory.GetEF(connectionString);
                    operation = new EFOperations.NominationOperation(entityFrameworHelper);

                    return operation;

                case Orm.FluentNHibernate:
                    var fluentNHibernateHelper = HelperFactory.GetFluentNHibernateHelper(connectionString);
                    operation = new FluentNHibernateOperations.NominationOperation(fluentNHibernateHelper);

                    return operation;

                case Orm.LLBLGenPro:
                    var llblGenProHelper = HelperFactory.GetLlblGenProHelper(connectionString);
                    operation = new LLBLGenProOperations.NominationOperation(llblGenProHelper);

                    return operation;

                case Orm.NHibernate:
                    var nHibernateHelper = HelperFactory.GetNHibernateHelper(connectionString);
                    operation = new NHibernateOperations.NominationOperation(nHibernateHelper);

                    return operation;

                default:
                    throw new NotImplementedException();
            }
        }
    }
}