﻿using System;
using Orms.DataAccessHelper.Operations.Interfaces;
using DapperOperations = Orms.DataAccessDapper.Operations;
using DataObjectsNetOperations = Orms.DataAccessDataObjectsNet.Operations;
using EcoOperations = Orms.DataAccessEco.Operations;
using EFOperations = Orms.DataAccessEF.Operations;
using FluentNHibernateOperations = Orms.DataAccessFluentNHibernate.Operations;
using LLBLGenProOperations = Orms.DataAccessLLBLGenPro.Operations;
using NHibernateOperations = Orms.DataAccessNHibernate.Operations;

namespace Orms.DataAccessFactory.OperationFactories
{
    public static partial class OperationFactory
    {
        public static IAgreementOperation GetAgreementOperation(Orm type, string connectionString)
        {
            IAgreementOperation operation;

            switch (type)
            {
                case Orm.Dapper:
                    var dapperHelper = HelperFactory.GetDapper(connectionString);
                    operation = new DapperOperations.AgreementOperation(dapperHelper);

                    return operation;

                case Orm.DataObjectsNet:
                    var dataBObjectsNetHelper = HelperFactory.GetDataObjectsNetHelper(connectionString);
                    operation = new DataObjectsNetOperations.AgreementOperation(dataBObjectsNetHelper);

                    return operation;

                case Orm.Eco:
                    var ecoHelper = HelperFactory.GetEco(connectionString);
                    operation = new EcoOperations.AgreementOperation(ecoHelper);

                    return operation;

                case Orm.EF:
                    var entityFrameworHelper = HelperFactory.GetEF(connectionString);
                    operation = new EFOperations.AgreementOperation(entityFrameworHelper);

                    return operation;

                case Orm.FluentNHibernate:
                    var fluentNHibernateHelper = HelperFactory.GetFluentNHibernateHelper(connectionString);
                    operation = new FluentNHibernateOperations.AgreementOperation(fluentNHibernateHelper);

                    return operation;

                case Orm.LLBLGenPro:
                    var llblGenProHelper = HelperFactory.GetLlblGenProHelper(connectionString);
                    operation = new LLBLGenProOperations.AgreementOperation(llblGenProHelper);

                    return operation;

                case Orm.NHibernate:
                    var nHibernateHelper = HelperFactory.GetNHibernateHelper(connectionString);
                    operation = new NHibernateOperations.AgreementOperation(nHibernateHelper);

                    return operation;

                default:
                    throw new NotImplementedException();
            }
        }


    }
}