﻿namespace Orms.DataAccessFactory
{
    public enum Orm
    {
        Dapper,
        DataObjectsNet,
        Eco,
        EF,
        FluentNHibernate,
        LLBLGenPro,
        NHibernate
    }

    public static class OrmProvider
    {
        public static string[] Names = new[]
        {
            "Dapper", "DataObjectsNet", "Eco", "EF", "FluentNHibernate", "LLBLGenPro", "NHibernate"
        };
    }
}