﻿using System;

namespace Orms.DataAccessFactory
{
    public static class OrmFactory
    {
        public static Orm Get(string name)
        {
            switch (name)
            {
                case "Dapper":
                    return Orm.Dapper;

                case "DataObjectsNet":
                    return Orm.DataObjectsNet;

                case "Eco":
                    return Orm.Eco;

                case "EF":
                    return Orm.EF;

                case "FluentNHibernate":
                    return Orm.FluentNHibernate;

                case "LLBLGenPro":
                    return Orm.LLBLGenPro;

                case "NHibernate":
                    return Orm.NHibernate;

                default:
                    throw new NotImplementedException();
            }
        }
    }
}