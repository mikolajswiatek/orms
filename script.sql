﻿CREATE TABLE [dbo].[AccessPoints]
(
	[Id] [int]             IDENTITY (1, 1) NOT NULL,
	[GasStorageId] [int] NOT NULL,
	[Code] [nvarchar](50) NOT NULL,
	PRIMARY KEY CLUSTERED ([Id] ASC),
	CONSTRAINT [FK_ACCESSPOINT_GAS_STORAGE] FOREIGN KEY ([GasStorageId]) REFERENCES [GasStorages]([Id])
)

CREATE TABLE [dbo].[AccessPointFlows]
(
	[Id] [int]             IDENTITY (1, 1) NOT NULL,
	[Direction] [nvarchar](50) NOT NULL,
	[AccessPointId] [int] NOT NULL,
	PRIMARY KEY CLUSTERED ([Id] ASC),
	CONSTRAINT [FK_ACCESSPOINTFLOW_ACCESSPOINT] FOREIGN KEY ([AccessPointId]) REFERENCES [AccessPoints]([Id])
)

CREATE TABLE [dbo].[Agreements]
(
	[Id] [int]            IDENTITY (1, 1) NOT NULL,
	[CreateDatetime] [datetime]  NOT NULL,
	[StartDatetime] [datetime]  NOT NULL,
	[EndDatetime] [datetime]  NOT NULL,
	[ContractorId] [int] NOT NULL,
	[GasStorageId] [int] NOT NULL,
	PRIMARY KEY CLUSTERED ([Id] ASC),
	CONSTRAINT [FK_AGREEMENT_CONTRACTOR] FOREIGN KEY ([ContractorId]) REFERENCES [Contractors]([Id]),
	CONSTRAINT [FK_AGREEMENT_GAS_STORAGE] FOREIGN KEY ([GasStorageId]) REFERENCES [GasStorages]([Id])
)

CREATE TABLE [dbo].[CalorificValues] (
	[Id]     [int]            IDENTITY (1, 1) NOT NULL,
	[GasDay] [datetime]  NOT NULL,
	[GasStorageId] [int] NOT NULL,
	PRIMARY KEY CLUSTERED ([Id] ASC),
	CONSTRAINT [FK_CALORIFIC_VALUE_GAS_STORAGE] FOREIGN KEY ([GasStorageId]) REFERENCES [GasStorages]([Id])
)

CREATE TABLE [dbo].[CalorificValueValues] (
	[Id]     [int]            IDENTITY (1, 1) NOT NULL,
	[EnergyValue] [decimal] NOT NULL,
	[EnergyUnit] [nvarchar](50) NOT NULL,
	[HourIndex] [int] NOT NULL,
	[CalorificValueId] [int] NOT NULL,
	PRIMARY KEY CLUSTERED ([Id] ASC),
	CONSTRAINT [FK_CALORIFIC_VALUE_VALUES_CALORIFIC_VALUE] FOREIGN KEY ([CalorificValueId]) REFERENCES [CalorificValues]([Id])
)

CREATE TABLE [dbo].[Contractors] (
	[Id]     [int]            IDENTITY (1, 1) NOT NULL,
	[Name]   [nvarchar] (255) NOT NULL,
	[Code]   [nvarchar] (255) NOT NULL,
	[Adress] [nvarchar] (255) NOT NULL,
	PRIMARY KEY CLUSTERED ([Id] ASC),
	UNIQUE NONCLUSTERED ([Code] ASC)
)

CREATE TABLE [dbo].[ContractorContacts]
(
	[Id] [int]             IDENTITY (1, 1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[FirstName] [nvarchar](50) NOT NULL,
	[LastName] [nvarchar](50) NOT NULL,
	[Adress] [nvarchar](255) NOT NULL,
	[Email] [nvarchar](255) NOT NULL,
	[Phone] [nvarchar](50) NOT NULL,
	[ContractorId] [int] NOT NULL,
	PRIMARY KEY CLUSTERED ([Id] ASC),
	CONSTRAINT [FK_CONTRACTOR_CONTACT_CONTRACTOR] FOREIGN KEY ([ContractorId]) REFERENCES [Contractors]([Id])
)

CREATE TABLE [dbo].[GasAccounts]
(
	[Id] [int]             IDENTITY (1, 1) NOT NULL,
	[EnergyValue] [decimal] NOT NULL,
	[EnergyUnit] [nvarchar](50) NOT NULL,
	[VolumeValue] [decimal] NOT NULL,
	[VolumeUnit] [nvarchar](50) NOT NULL,
	[GasDay] [datetime]  NOT NULL,
	[HourIndex] [int] NOT NULL,
	[GasStorageId] [int] NOT NULL,
	PRIMARY KEY CLUSTERED ([Id] ASC),
	CONSTRAINT [FK_GASACCOUNT_GASSTORAGE] FOREIGN KEY ([GasStorageId]) REFERENCES [GasStorages]([Id])
)

CREATE TABLE [dbo].[GasStorages]
(
	[Id] [int]             IDENTITY (1, 1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	PRIMARY KEY CLUSTERED ([Id] ASC)
)

CREATE TABLE [dbo].[Nominations]
(
	[Id] [int]             IDENTITY (1, 1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[GasDay] [datetime]  NOT NULL,
	[ContractorId] [int] NOT NULL,
	[AccessPointFlowId] [int] NOT NULL,
	[CreateDatetime] [datetime]  NOT NULL,
	[IsLast] [bit] NOT NULL,
	[IsAccepted] [bit] NOT NULL,
	[ParentNominationId] [int] NULL,
	[UserId] [int] NOT NULL,
	[Channel] [nvarchar](50) NOT NULL,
	[MajorVersion] [int] NOT NULL,
	[Version] [int] NOT NULL,
	[CalorificValueId] [int] NOT NULL,
	[Comment] [nvarchar](MAX) NOT NULL,
	PRIMARY KEY CLUSTERED ([Id] ASC),
	CONSTRAINT [FK_NOMINATION_CONTRACTOR] FOREIGN KEY ([ContractorId]) REFERENCES [Contractors]([Id]),
	CONSTRAINT [FK_NOMINATION_ACCESS_POINT_FLOW] FOREIGN KEY ([AccessPointFlowId]) REFERENCES [AccessPointFlows]([Id]),
	CONSTRAINT [FK_NOMINATION_NOMINATION] FOREIGN KEY ([ParentNominationId]) REFERENCES [Nominations]([Id]),
	CONSTRAINT [FK_NOMINATION_USER] FOREIGN KEY ([UserId]) REFERENCES [Users]([Id]),
	CONSTRAINT [FK_NOMINATION_CALORIFIC_VALUE] FOREIGN KEY ([CalorificValueId]) REFERENCES [CalorificValues]([Id])
)

CREATE TABLE [dbo].[NominationValues]
(
	[Id] [int]             IDENTITY (1, 1) NOT NULL,
	[EnergyValue] [decimal] NOT NULL,
	[EnergyUnit] [nvarchar](50) NOT NULL,
	[VolumeValue] [decimal] NOT NULL,
	[VolumeUnit] [nvarchar](50) NOT NULL,
	[HourIndex] [int] NOT NULL,
	[NominationId] [int] NOT NULL,
	PRIMARY KEY CLUSTERED ([Id] ASC),
	CONSTRAINT [FK_NOMINATION_VALUE_GASSTORAGE] FOREIGN KEY ([NominationId]) REFERENCES [Nominations]([Id])
)

CREATE TABLE [dbo].[Sessions]
(
	[Id] [int]             IDENTITY (1, 1) NOT NULL,
	[Guid] [nvarchar](MAX) NOT NULL,
	[Ip] [nvarchar](50) NOT NULL,
	[Browser] [nvarchar](50) NOT NULL,
	[CreateDatetime] [datetime]  NOT NULL,
	[LastActivityDatetime] [datetime]  NOT NULL,
	[UserId] [int] NOT NULL,
	PRIMARY KEY CLUSTERED ([Id] ASC),
	CONSTRAINT [FK_SESSION_USER] FOREIGN KEY ([UserId]) REFERENCES [Users]([Id])
)

CREATE TABLE [dbo].[Users]
(
	[Id] [int]             IDENTITY (1, 1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Password] [nvarchar](MAX) NOT NULL,
	[Email] [nvarchar](50) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreateDatetime] [datetime]  NOT NULL,
	[LastActivityDatetime] [datetime]  NOT NULL,
	[ContractorId] [int] NOT NULL,
	PRIMARY KEY CLUSTERED ([Id] ASC),
	CONSTRAINT [FK_USER_CONTRACTOR] FOREIGN KEY ([ContractorId]) REFERENCES [Contractors]([Id])
)
