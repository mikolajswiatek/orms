﻿using System.Collections.Generic;
using Orms.DataAccessEco.Providers;
using Orms.DataAccessHelper;
using Orms.DataAccessHelper.Operations.Interfaces;
using Orms.Models;
using Orms.Services;
using Orms.Services.Interfaces;

namespace Orms.DataAccessEco.Operations
{
    public class AgreementOperation : Operation<EcoProject1EcoSpace>, IAgreementOperation
    {
        private IAgreementService _service;
        private readonly SessionWorker<EcoProject1EcoSpace> _space;

        public AgreementOperation(SessionWorker<EcoProject1EcoSpace> space)
        {
            _space = space;
        }

        protected override void SetProviders(EcoProject1EcoSpace space)
        {
            _service = new AgreementService(
                new AgreementProvider(space));
        }

        public void Add(Agreement agreement)
        {
            SetProviders(_space.ReadAndWrite());
            _service.Add(agreement);
            _space.SaveChanges();
        }

        public IEnumerable<Agreement> GetAll(Contractor contractor)
        {
            SetProviders(_space.Read());
            return _service.GetAll(contractor);
        }

        public IEnumerable<Agreement> GetAll(GasStorage gasStorage)
        {
            SetProviders(_space.Read());
            return _service.GetAll(gasStorage);
        }

        public IEnumerable<Agreement> GetAll(Contractor contractor, GasStorage gasStorage)
        {
            SetProviders(_space.Read());
            return _service.GetAll(contractor, gasStorage);
        }

        public Agreement Get(int id)
        {
            SetProviders(_space.Read());
            return _service.Get(id);
        }

        public IEnumerable<Agreement> GetAll()
        {
            SetProviders(_space.Read());
            return _service.GetAll();
        }
    }
}