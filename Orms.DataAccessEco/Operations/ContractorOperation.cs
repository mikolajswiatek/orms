﻿using System.Collections.Generic;
using Orms.DataAccessEco.Providers;
using Orms.DataAccessHelper;
using Orms.DataAccessHelper.Operations.Interfaces;
using Orms.Models;
using Orms.Services;
using Orms.Services.Interfaces;

namespace Orms.DataAccessEco.Operations
{
    public class ContractorOperation : Operation<EcoProject1EcoSpace>, IContractorOperation
    {
        private IContractorService _service;
        private readonly SessionWorker<EcoProject1EcoSpace> space;

        public ContractorOperation(SessionWorker<EcoProject1EcoSpace> context)
        {
            space = context;
        }

        protected override void SetProviders(EcoProject1EcoSpace space)
        {
            _service = new ContractorService(
                new ContractorContactProvider(space),
                new ContractorProvider(space));
        }

        public IEnumerable<Contractor> GetAll()
        {
            SetProviders(space.Read());

            return _service.GetAll();
        }

        public IEnumerable<ContractorContact> GetAll(Contractor contractor)
        {
            SetProviders(space.Read());

            return _service.GetAll(contractor);
        }

        public Contractor Get(int id)
        {
            SetProviders(space.Read());

            return _service.Get(id);
        }

        public ContractorContact GetContact(int id)
        {
            SetProviders(space.Read());

            return _service.GetContact(id);
        }

        public void Update(ContractorContact contact)
        {
            SetProviders(space.ReadAndWrite());

            _service.Update(contact);
            space.SaveChanges();
        }

        public void Add(Contractor contractor)
        {
            SetProviders(space.ReadAndWrite());

            _service.Add(contractor);
            space.SaveChanges();
        }

        public void Add(ContractorContact contact, int contractorId)
        {
            SetProviders(space.ReadAndWrite());

            _service.Add(contact, contractorId);
            space.SaveChanges();
        }
    }
}