﻿using System;
using System.Collections.Generic;
using Orms.DataAccessEco.Providers;
using Orms.DataAccessHelper;
using Orms.DataAccessHelper.Operations.Interfaces;
using Orms.Models;
using Orms.Services;
using Orms.Services.Interfaces;

namespace Orms.DataAccessEco.Operations
{
    public class NominationOperation : Operation<EcoProject1EcoSpace>, INominationOperation
    {
        private INominationService _service;
        private readonly SessionWorker<EcoProject1EcoSpace> _space;

        public NominationOperation(SessionWorker<EcoProject1EcoSpace> space)
        {
            _space = space;
        }

        protected override void SetProviders(EcoProject1EcoSpace space)
        {
            _service = new NominationService(
                new NominationProvider(space),
                new NominationValueProvider(space));
        }

        public void Add(Nomination nomination)
        {
            SetProviders(_space.ReadAndWrite());

            _service.Add(nomination);
            _space.SaveChanges();
        }

        public void Add(IEnumerable<NominationValue> values)
        {
            SetProviders(_space.ReadAndWrite());

            _service.Add(values);
            _space.SaveChanges();
        }

        public void Add(Nomination nomination, IEnumerable<NominationValue> values)
        {
            SetProviders(_space.ReadAndWrite());

            _service.Add(nomination, values);
            _space.SaveChanges();
        }

        public void RefreshNomiation(Nomination nomination, int parentNominationId)
        {
            SetProviders(_space.ReadAndWrite());

            _service.RefreshNomiation(nomination, parentNominationId);
            _space.SaveChanges();
        }

        public void AcceptedNomination(Nomination nomination)
        {
            SetProviders(_space.ReadAndWrite());

            _service.AcceptedNomination(nomination);
            _space.SaveChanges();
        }

        public IEnumerable<Nomination> GetBy(Contractor contractor)
        {
            SetProviders(_space.Read());

            return _service.GetBy(contractor);
        }

        public IEnumerable<Nomination> GetBy(GasStorage gasStorage)
        {
            SetProviders(_space.Read());

            return _service.GetBy(gasStorage);
        }

        public IEnumerable<Nomination> GetBy(User user)
        {
            SetProviders(_space.Read());

            return _service.GetBy(user);
        }

        public IEnumerable<Nomination> GetBy(DateTime gasDay)
        {
            SetProviders(_space.Read());

            return _service.GetBy(gasDay);
        }

        public Nomination Get(int id)
        {
            SetProviders(_space.Read());

            return _service.Get(id);
        }

        public Nomination GetLast(Contractor contractor)
        {
            SetProviders(_space.Read());

            return _service.GetLast(contractor);
        }

        public Nomination GetLast(GasStorage gasStorage)
        {
            SetProviders(_space.Read());

            return _service.GetLast(gasStorage);
        }

        public Nomination GetLast(User user)
        {
            SetProviders(_space.Read());

            return _service.GetLast(user);
        }

        public Nomination GetLast(Contractor contractor, DateTime gasDay)
        {
            SetProviders(_space.Read());

            return _service.GetLast(contractor, gasDay);
        }

        public IEnumerable<Nomination> GetForGasDay(Contractor contractor, DateTime gasDay)
        {
            SetProviders(_space.Read());

            return _service.GetForGasDay(contractor, gasDay);
        }

        public IEnumerable<Nomination> GetForGasDay(GasStorage gasStorage, DateTime gasDay)
        {
            SetProviders(_space.Read());

            return _service.GetForGasDay(gasStorage, gasDay);
        }

        public IEnumerable<Nomination> GetForGasDay(Contractor contractor, GasStorage gasStorage, DateTime gasDay)
        {
            SetProviders(_space.Read());

            return _service.GetForGasDay(contractor, gasStorage, gasDay);
        }
    }
}