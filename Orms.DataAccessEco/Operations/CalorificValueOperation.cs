﻿using System;
using System.Collections.Generic;
using System.Linq;
using Orms.DataAccessEco.Providers;
using Orms.DataAccessHelper;
using Orms.DataAccessHelper.Operations.Interfaces;
using Orms.Models;
using Orms.Services;
using Orms.Services.Interfaces;

namespace Orms.DataAccessEco.Operations
{
    public class CalorificValueOperation : Operation<EcoProject1EcoSpace>, ICalorificValueOperation
    {
        private ICalorificValueService _service;
        private readonly SessionWorker<EcoProject1EcoSpace> _space;

        public CalorificValueOperation(SessionWorker<EcoProject1EcoSpace> space)
        {
            _space = space;
        }

        protected override void SetProviders(EcoProject1EcoSpace space)
        {
            _service = new CalorificValueService(new CalorificValueProvider(space));
        }

        public void Add(CalorificValue cv, IEnumerable<EnergyCalorificValue> values)
        {
            SetProviders(_space.ReadAndWrite());

            cv.CollectionOfValues = values.ToList();

            _service.Add(cv, values);
            _space.SaveChanges();
        }

        public CalorificValue GetLastBy(GasStorage gasStorage)
        {
            SetProviders(_space.Read());

            return _service.GetLastBy(gasStorage);
        }

        public CalorificValue GetForGasDay(GasStorage gasStorage, DateTime gasDay)
        {
            SetProviders(_space.Read());

            return _service.GetForGasDay(gasStorage, gasDay);
        }

        public CalorificValue Get(int id)
        {
            SetProviders(_space.Read());

            return _service.Get(id);
        }
    }
}