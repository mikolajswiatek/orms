﻿using System.Collections.Generic;
using Orms.DataAccessEco.Providers;
using Orms.DataAccessHelper;
using Orms.DataAccessHelper.Operations.Interfaces;
using Orms.Models;
using Orms.Services;
using Orms.Services.Interfaces;

namespace Orms.DataAccessEco.Operations
{
    public class UserOperation : Operation<EcoProject1EcoSpace>, IUserOperation
    {
        private IUserService _service;
        private readonly SessionWorker<EcoProject1EcoSpace> _space;

        public UserOperation(SessionWorker<EcoProject1EcoSpace> space)
        {
            _space = space;
        }

        protected override void SetProviders(EcoProject1EcoSpace space)
        {
            _service = new UserService(
                new UserProvider(space),
                new SessionProvider(space));
        }

        public User Exist(string nameOrEmail)
        {
            SetProviders(_space.Read());

            return _service.Exist(nameOrEmail);
        }

        public void Create(User user)
        {
            SetProviders(_space.ReadAndWrite());

            _service.Create(user);
            _space.SaveChanges();
        }

        public void Update(User user)
        {
            SetProviders(_space.ReadAndWrite());

            _service.Update(user);
            _space.SaveChanges();
        }

        public User Get(int id)
        {
            SetProviders(_space.Read());

            return _service.Get(id);
        }

        public IEnumerable<User> Get(Contractor contractor)
        {
            SetProviders(_space.Read());

            return _service.Get(contractor);
        }

        public IEnumerable<User> GetAll()
        {
            SetProviders(_space.Read());

            return _service.GetAll();
        }
    }
}