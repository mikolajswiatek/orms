﻿using System;
using System.Collections.Generic;
using Orms.DataAccessEco.Providers;
using Orms.DataAccessHelper;
using Orms.DataAccessHelper.Operations.Interfaces;
using Orms.Models;
using Orms.Services;
using Orms.Services.Interfaces;

namespace Orms.DataAccessEco.Operations
{
    public class GasAccountOperation : Operation<EcoProject1EcoSpace>, IGasAccountOperation
    {
        private IGasAccountService _service;
        private readonly SessionWorker<EcoProject1EcoSpace> _space;

        public GasAccountOperation(SessionWorker<EcoProject1EcoSpace> space)
        {
            _space = space;
        }

        protected override void SetProviders(EcoProject1EcoSpace space)
        {
            _service = new GasAccountService(
                new GasAccountProvider(space),
                new GasStorageProvider(space),
                new NominationProvider(space));
        }

        public void Update(IEnumerable<GasAccount> gasAccounts)
        {
            SetProviders(_space.ReadAndWrite());

            _service.Update(gasAccounts);
            _space.SaveChanges();
        }

        public IEnumerable<GasAccount> Get(DateTime gasDay)
        {
            SetProviders(_space.Read());

            return _service.Get(gasDay);
        }

        public IEnumerable<GasAccount> Get(GasStorage gasStorage, DateTime gasDay)
        {
            SetProviders(_space.Read());

            return _service.Get(gasStorage, gasDay);
        }

        public void Refresh(GasStorage gasStorage)
        {
            SetProviders(_space.ReadAndWrite());

            _service.Refresh(gasStorage);
            _space.SaveChanges();
        }

        public void Refresh()
        {
            SetProviders(_space.ReadAndWrite());

            _service.Refresh();
            _space.SaveChanges();
        }

        public void Refresh(GasStorage gasStorage, GasDay gasDay)
        {
            SetProviders(_space.ReadAndWrite());

            _service.Refresh(gasStorage, gasDay);
            _space.SaveChanges();
        }

        public void Refresh(GasDay gasDay)
        {
            SetProviders(_space.ReadAndWrite());

            _service.Refresh(gasDay);
            _space.SaveChanges();
        }
    }
}