﻿using Orms.DataAccessEco.Providers;
using Orms.DataAccessHelper;
using Orms.DataAccessHelper.Operations.Interfaces;
using Orms.Models;
using Orms.Services;
using Orms.Services.Interfaces;

namespace Orms.DataAccessEco.Operations
{
    public class SessionOperation : Operation<EcoProject1EcoSpace>, ISessionOperation
    {
        private ISessionService _service;
        private readonly SessionWorker<EcoProject1EcoSpace> _space;

        public SessionOperation(SessionWorker<EcoProject1EcoSpace> space)
        {
            _space = space;
        }

        protected override void SetProviders(EcoProject1EcoSpace space)
        {
            _service = new SessionService(
                new SessionProvider(space),
                new UserProvider(space));
        }

        public void Add(Session session)
        {
            SetProviders(_space.ReadAndWrite());

            _service.Add(session);
            _space.SaveChanges();
        }

        public void Refresh(string guid)
        {
            SetProviders(_space.ReadAndWrite());

            _service.Refresh(guid);
            _space.SaveChanges();
        }

        public Session GetBy(string guid)
        {
            SetProviders(_space.Read());

            return _service.GetBy(guid);
        }

        public void Delete(Session session)
        {
            SetProviders(_space.ReadAndWrite());

            _service.Delete(session);
            _space.SaveChanges();
        }
    }
}