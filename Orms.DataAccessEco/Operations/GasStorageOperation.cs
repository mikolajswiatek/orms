﻿using System.Collections.Generic;
using Orms.DataAccessEco.Providers;
using Orms.DataAccessHelper;
using Orms.DataAccessHelper.Operations.Interfaces;
using Orms.Models;
using Orms.Services;
using Orms.Services.Interfaces;

namespace Orms.DataAccessEco.Operations
{
    public class GasStorageOperation : Operation<EcoProject1EcoSpace>, IGasStorageOperation
    {
        private IGasStorageService _service;
        private readonly SessionWorker<EcoProject1EcoSpace> _space;

        public GasStorageOperation(SessionWorker<EcoProject1EcoSpace> space)
        {
            _space = space;
        }

        protected override void SetProviders(EcoProject1EcoSpace space)
        {
            _service = new GasStorageService(
                new GasStorageProvider(space));
        }

        public void Add(GasStorage gasStorage)
        {
            SetProviders(_space.ReadAndWrite());

            _service.Add(gasStorage);
            _space.SaveChanges();
        }

        public GasStorage Get(int id)
        {
            SetProviders(_space.Read());

            return _service.Get(id);
        }

        public IEnumerable<GasStorage> GetAll()
        {
            SetProviders(_space.Read());

            return _service.GetAll();
        }
    }
}