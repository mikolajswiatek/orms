﻿using System;
using Eco.ObjectRepresentation;
using Orms.DataAccessEco.Helpers;
using Orms.Models;

namespace Orms.DataAccessEco.Converters
{
    public static partial class Converter
    {
        public static EcoProject1.GasStorage ToEntity(this GasStorage gasStorage)
        {
            return new EcoProject1.GasStorage()
            {
                Name = gasStorage.Name
            };
        }

        public static GasStorage ToModel(this EcoProject1.GasStorage gasStorage)
        {
            if (gasStorage == null) return null;

            return new GasStorage()
            {
                Id = Int32.Parse(gasStorage.ExternalId().Split(ClassId.Spliter, StringSplitOptions.None)[1]),
                Name = gasStorage.Name
            };
        }
    }
}