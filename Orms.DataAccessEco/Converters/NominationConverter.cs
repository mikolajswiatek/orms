﻿using System;
using Eco.ObjectRepresentation;
using Orms.DataAccessEco.Helpers;
using Orms.Domains.Enums;
using Orms.Models;

namespace Orms.DataAccessEco.Converters
{
    public static partial class Converter
    {
        public static EcoProject1.Nomination ToEntity(this Nomination nomination)
        {
            return new EcoProject1.Nomination()
            {
                Name = nomination.Name,
                Channel = nomination.Channel.ToString(),
                GasDay = nomination.GasDay,
                MajorVersion = nomination.MajorVersion,
                Version = nomination.Version,
                Comment = nomination.Comment,
                IsAccepted = nomination.IsAccepted,
                CreateDateTime = nomination.CreateDateTime,
                IsLastOrder = nomination.IsLast
            };
        }

        public static Nomination ToModel(this EcoProject1.Nomination nomination)
        {
            if (nomination == null) return null;

            return new Nomination()
            {
                Id = Int32.Parse(nomination.ExternalId().Split(ClassId.Spliter, StringSplitOptions.None)[1]),
                Name = nomination.Name,
                AccessPointFlow = nomination.AccessPointFlow.ToModel(),
                Channel = (Channel)Enum.Parse(typeof(Channel), nomination.Channel),
                Contractor = nomination.Contractor.ToModel(),
                GasDay = nomination.GasDay,
                MajorVersion = nomination.MajorVersion,
                Version = nomination.Version,
                ParentNomination = nomination.Nomination_parent.ToModel(),
                Creator = nomination.User.ToModel(),
                IsLast = nomination.IsLastOrder,
                IsAccepted = nomination.IsAccepted,
                Comment = nomination.Comment,
                CalorificValue = nomination.CalorificValue.ToModel(),
                CreateDateTime = nomination.CreateDateTime,
            };
        }
    }
}