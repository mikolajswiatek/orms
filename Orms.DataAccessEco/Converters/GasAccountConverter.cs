﻿using System;
using Eco.ObjectRepresentation;
using Orms.DataAccessEco.Helpers;
using Orms.Domains.Enums;
using Orms.Domains.Units;
using Orms.Models;
using Orms.Models.Extensions;

namespace Orms.DataAccessEco.Converters
{
    public static partial class Converter
    {
        public static EcoProject1.GasAccount ToEntity(this GasAccount gasAccount)
        {
            return new EcoProject1.GasAccount()
            {
                GasDay = gasAccount.GasDay,
                EnergyUnit = gasAccount.Energy.Unit.ToString(),
                EnergyValue = gasAccount.Energy.Value,
                VolumeUnit = gasAccount.Volume.Unit.ToString(),
                VolumeValue = gasAccount.Volume.Value,
                HourIndex = gasAccount.HourIndex
            };
        }

        public static GasAccount ToModel(this EcoProject1.GasAccount gasAccount)
        {
            return new GasAccount()
            {
                Id = Int32.Parse(gasAccount.ExternalId().Split(ClassId.Spliter, StringSplitOptions.None)[1]),
                GasDay = gasAccount.GasDay,
                Energy = new UnitValue((decimal)gasAccount.EnergyValue, EnumHelper.ParseEnum<Unit>(gasAccount.EnergyUnit)),
                Volume = new UnitValue((decimal)gasAccount.VolumeValue, EnumHelper.ParseEnum<Unit>(gasAccount.VolumeUnit)),
                GasStorage = gasAccount.GasStorage.ToModel(),
                HourIndex = gasAccount.HourIndex
            };
        }
    }
}