﻿using System;
using System.Collections.Generic;
using System.Linq;
using Eco.ObjectImplementation;
using Eco.ObjectRepresentation;
using Orms.DataAccessEco.Helpers;
using Orms.Models;

namespace Orms.DataAccessEco.Converters
{
    public static partial class Converter
    {
        public static EcoProject1.AccessPointFlow ToEntity(this AccessPointFlow accessPointFlow)
        {
            return new EcoProject1.AccessPointFlow()
            {
                Direction = accessPointFlow.DirectionString
            };
        }

        public static AccessPointFlow ToModel(this EcoProject1.AccessPointFlow accessPointFlow)
        {
            return new AccessPointFlow()
            {
                Id = Int32.Parse(accessPointFlow.ExternalId().Split(ClassId.Spliter, StringSplitOptions.None)[1]),
                AccessPoint = accessPointFlow.AccessPoint.ToModel(),
                DirectionString = accessPointFlow.Direction
            };
        }

        public static IEnumerable<AccessPointFlow> ToModels(this IEcoList<EcoProject1.AccessPointFlow> accessPointFlows)
        {
            return accessPointFlows.ToList().Select(apf => apf.ToModel());
        }
    }
}