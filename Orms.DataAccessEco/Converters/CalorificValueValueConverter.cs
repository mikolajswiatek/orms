﻿using System;
using Eco.ObjectRepresentation;
using Orms.DataAccessEco.Helpers;
using Orms.Domains.Enums;
using Orms.Domains.Units;
using Orms.Models;
using Orms.Models.Extensions;

namespace Orms.DataAccessEco.Converters
{
    public static partial class Converter
    {
        public static EcoProject1.EnergyCalorificValue ToEntity(this EnergyCalorificValue ecv)
        {
            return new EcoProject1.EnergyCalorificValue()
            {
                CalorificValue = ecv.CalorificValue.ToEntity(),
                HourIndex = ecv.HourIndex,
                EnergyValue = ecv.Energy.Value,
                EnergyUnit = ecv.Energy.Unit.ToString(),
            };
        }

        public static EnergyCalorificValue ToModel(this EcoProject1.EnergyCalorificValue ecv)
        {
            return new EnergyCalorificValue()
            {
                Id = Int32.Parse(ecv.ExternalId().Split(ClassId.Spliter, StringSplitOptions.None)[1]),
                CalorificValue = ecv.CalorificValue.ToModel(),
                Energy = new UnitValue((decimal)ecv.EnergyValue, EnumHelper.ParseEnum<Unit>(ecv.EnergyUnit)),
                HourIndex = ecv.HourIndex
            };
        }
    }
}