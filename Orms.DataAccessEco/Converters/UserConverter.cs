﻿using System;
using Eco.ObjectRepresentation;
using Orms.DataAccessEco.Helpers;
using Orms.Models;

namespace Orms.DataAccessEco.Converters
{
    public static partial class Converter
    {
        public static EcoProject1.User ToEntity(this User user)
        {
            return new EcoProject1.User()
            {
                Name = user.Name,
                Email = user.Email,
                IsActive = user.IsActive,
                CreateDateTime = user.CreateDateTime,
                Password = user.Password,
                LastActivityDateTIme = user.LastActivityDateTime
            };
        }

        public static User ToModel(this EcoProject1.User user)
        {
            return new User()
            {
                Id = Int32.Parse(user.ExternalId().Split(ClassId.Spliter, StringSplitOptions.None)[1]),
                Name = user.Name,
                IsActive = user.IsActive,
                CreateDateTime = user.CreateDateTime,
                Password = user.Password,
                Email = user.Email,
                LastActivityDateTime = user.LastActivityDateTIme,
                Contractor = user.Contractor.ToModel()
            };
        }
    }
}