﻿using System;
using Eco.ObjectRepresentation;
using Orms.DataAccessEco.Helpers;
using Orms.Models;

namespace Orms.DataAccessEco.Converters
{
    public static partial class Converter
    {
        public static EcoProject1.Session ToEntity(this Session session)
        {
            return new EcoProject1.Session()
            {
                Guid = session.Guid,
                LastActivityDatetime = session.LastActivityDatetime,
                CreateDatetime = session.CreateDatetime,
                Browser = session.Browser,
                Ip = session.Ip
            };
        }

        public static Session ToModel(this EcoProject1.Session session)
        {
            return new Session()
            {
                Id = Int32.Parse(session.ExternalId().Split(ClassId.Spliter, StringSplitOptions.None)[1]),
                Guid = session.Guid,
                User = session.User.ToModel(),
                LastActivityDatetime = session.LastActivityDatetime,
                CreateDatetime = session.CreateDatetime,
                Browser = session.Browser,
                Ip = session.Ip
            };
        }
    }
}