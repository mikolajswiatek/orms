﻿using System;
using Eco.ObjectRepresentation;
using Orms.DataAccessEco.Helpers;
using Orms.Models;

namespace Orms.DataAccessEco.Converters
{
    public static partial class Converter
    {
        public static EcoProject1.AccessPoint ToEntity(this AccessPoint accessPoint)
        {
            return new EcoProject1.AccessPoint()
            {
                Code = accessPoint.Code
            };
        }

        public static AccessPoint ToModel(this EcoProject1.AccessPoint accessPoint)
        {
            return new AccessPoint()
            {
                Id = Int32.Parse(accessPoint.ExternalId().Split(ClassId.Spliter, StringSplitOptions.None)[1]),
                AccessPointFlows = accessPoint.AccessPointFlow.ToModels(),
                Code = accessPoint.Code,
                GasStorage = accessPoint.GasStorage.ToModel()
            };
        }
    }
}