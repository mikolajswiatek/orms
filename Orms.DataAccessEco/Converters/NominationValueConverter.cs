﻿using System;
using Eco.ObjectRepresentation;
using Orms.DataAccessEco.Helpers;
using Orms.Domains.Enums;
using Orms.Domains.Units;
using Orms.Models;
using Orms.Models.Extensions;

namespace Orms.DataAccessEco.Converters
{
    public static partial class Converter
    {
        public static EcoProject1.NominationValue ToEntity(this NominationValue value)
        {
            return new EcoProject1.NominationValue()
            {
                EnergyUnit = value.Energy.Unit.ToString(),
                EnergyValue = value.Energy.Value,
                VolumeUnit = value.Volume.Unit.ToString(),
                VolumeValue = value.Volume.Value,
                HourIndex = value.HourIndex,
            };
        }

        public static NominationValue ToModel(this EcoProject1.NominationValue value)
        {
            return new NominationValue()
            {
                Id = Int32.Parse(value.ExternalId().Split(ClassId.Spliter, StringSplitOptions.None)[1]),
                Nomination = value.Nomination.ToModel(),
                Energy = new UnitValue((decimal)value.EnergyValue, EnumHelper.ParseEnum<Unit>(value.EnergyUnit)),
                Volume = new UnitValue((decimal)value.VolumeValue, EnumHelper.ParseEnum<Unit>(value.VolumeUnit)),
                HourIndex = value.HourIndex
            };
        }
    }
}