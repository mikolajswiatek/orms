﻿using System;
using Eco.ObjectRepresentation;
using Orms.DataAccessEco.Helpers;
using Orms.Models;

namespace Orms.DataAccessEco.Converters
{
    public static partial class Converter
    {
        public static EcoProject1.Contractor ToEntity(this Contractor contractor)
        {
            return new EcoProject1.Contractor()
            {
                Name = contractor.Name,
                Code = contractor.Code,
                Adress = contractor.Adress
            };
        }

        public static Contractor ToModel(this EcoProject1.Contractor contractor)
        {
            if (contractor == null) return null;

            return new Contractor()
            {
                Id = Int32.Parse(contractor.ExternalId().Split(ClassId.Spliter, StringSplitOptions.None)[1]),
                Name = contractor.Name,
                Code = contractor.Code,
                Adress = contractor.Adress
            };
        }
    }
}