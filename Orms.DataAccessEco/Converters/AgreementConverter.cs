﻿using System;
using Eco.ObjectRepresentation;
using Orms.DataAccessEco.Helpers;
using Orms.Models;

namespace Orms.DataAccessEco.Converters
{
    public static partial class Converter
    {
        public static EcoProject1.Agreement ToEntity(this Agreement agreement)
        {
            return new EcoProject1.Agreement()
            {
                End = agreement.End,
                Start = agreement.Start
            };
        }

        public static Agreement ToModel(this EcoProject1.Agreement agreement)
        {
            return new Agreement()
            {
                Id = Int32.Parse(agreement.ExternalId().Split(ClassId.Spliter, StringSplitOptions.None)[1]),
                CreateDate = agreement.CreateDate,
                End = agreement.End,
                GasStorage = agreement.GasStorage.ToModel(),
                Start = agreement.Start,
                Contractor = agreement.Contractor.ToModel()
            };
        }
    }
}