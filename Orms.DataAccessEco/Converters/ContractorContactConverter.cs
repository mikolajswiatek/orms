﻿using System;
using Eco.ObjectRepresentation;
using Orms.DataAccessEco.Helpers;
using Orms.Models;

namespace Orms.DataAccessEco.Converters
{
    public static partial class Converter
    {
        public static EcoProject1.ContractorContact ToEntity(this ContractorContact contact)
        {
            return new EcoProject1.ContractorContact()
            {
                Name = contact.Name,
                FirstName = contact.FirstName,
                LastName = contact.LastName,
                Email = contact.Email,
                Phone = contact.Phone,
                Adress = contact.Adress
            };
        }

        public static ContractorContact ToModel(this EcoProject1.ContractorContact contact)
        {
            return new ContractorContact()
            {
                Id = Int32.Parse(contact.ExternalId().Split(ClassId.Spliter, StringSplitOptions.None)[1]),
                Name = contact.Name,
                FirstName = contact.FirstName,
                LastName = contact.LastName,
                Email = contact.Email,
                Phone = contact.Phone,
                Adress = contact.Adress,
                Contractor = contact.Contractor.ToModel()

            };
        }
    }
}