﻿using System;
using Eco.ObjectRepresentation;
using Orms.DataAccessEco.Helpers;
using Orms.Models;

namespace Orms.DataAccessEco.Converters
{
    public static partial class Converter
    {
        public static EcoProject1.CalorificValue ToEntity(this CalorificValue cv)
        {
            return new EcoProject1.CalorificValue()
            {
                GasDay = cv.GasDay
            };
        }

        public static CalorificValue ToModel(this EcoProject1.CalorificValue cv)
        {
            return new CalorificValue()
            {
                Id = Int32.Parse(cv.ExternalId().Split(ClassId.Spliter, StringSplitOptions.None)[1]),
                GasDay = cv.GasDay,
                GasStorage = cv.GasStorage.ToModel()
            };
        }
    }
}