﻿using Orms.DataAccessHelper;

namespace Orms.DataAccessEco
{
    public class EcoHelper : SessionWorker<EcoProject1EcoSpace>
    {
        private EcoProject1EcoSpace space;

        public EcoHelper(string connectionString) : base(connectionString)
        {
            space = new EcoProject1EcoSpace();
        }

        public override void CloseSession()
        {
            //space.Active = false;
            space.Dispose();
            isOpen = true;
        }

        public override void OpenSession()
        {
            isOpen = true;
            //space.Active = true;
        }

        public override EcoProject1EcoSpace Read()
        {
            return space;
        }

        public override EcoProject1EcoSpace ReadAndWrite()
        {
            return space;
        }

        public override void SaveChanges()
        {
            space.UpdateDatabase();
        }
    }
}
