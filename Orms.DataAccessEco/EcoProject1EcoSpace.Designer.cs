namespace Orms.DataAccessEco
{
    public partial class EcoProject1EcoSpace
    {
        /// <summary>
        /// Required designer variable
        /// </summary>
        private System.ComponentModel.Container components = null;

        private Eco.Persistence.PersistenceMapperSharer persistenceMapperSharer1;
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                Active = false;
                if (this.components != null)
                {
                    this.components.Dispose();
                }
            }

            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.persistenceMapperSharer1 = new Eco.Persistence.PersistenceMapperSharer();
            // 
            // persistenceMapperSharer1
            // 
            this.persistenceMapperSharer1.MapperProviderTypeName = "Orms.DataAccessEco.EcoProject1PMP";
            // 
            // EcoProject1EcoSpace
            // 
            this.PersistenceMapper = this.persistenceMapperSharer1;

        }

        protected EcoProject1.EcoProject1Package IncludeEcoPackage_EcoProject1_EcoProject1Package;
    }
}
