﻿using System.Collections.Generic;
using System.Linq;
using Orms.DataAccessEco.Converters;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;

namespace Orms.DataAccessEco.Repositories
{
    public class SessionRepository : ISessionRepository<EcoProject1EcoSpace>
    {
        public IEnumerable<Session> GetAll(EcoProject1EcoSpace space)
        {
            var sessions = EcoProject1.Session.GetAll(space);

            return sessions.Select(s => s.ToModel());
        }

        public Session Get(EcoProject1EcoSpace space, int id)
        {
            var session = EcoProject1.Session.Get(space, id);

            return session?.ToModel();
        }

        public void Update(EcoProject1EcoSpace space, Session session)
        {
            EcoProject1.Session.Update(space, session); ;
        }

        public void Add(EcoProject1EcoSpace space, Session session)
        {
            EcoProject1.Session.Add(space, session);
        }

        public Session GetBy(EcoProject1EcoSpace space, string guid)
        {
            var session = EcoProject1.Session.GetBy(space, guid);

            return session?.ToModel();
        }

        public void Delete(EcoProject1EcoSpace space, Session session)
        {
            EcoProject1.Session.Delete(space, session);
        }
    }
}
