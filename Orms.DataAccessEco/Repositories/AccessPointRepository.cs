﻿using System.Collections.Generic;
using System.Linq;
using Orms.DataAccessEco.Converters;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;

namespace Orms.DataAccessEco.Repositories
{
    public class AccessPointRepository : IAccessPointRepository<EcoProject1EcoSpace>
    {
        public IEnumerable<AccessPoint> GetAll(EcoProject1EcoSpace space)
        {
            throw new System.NotImplementedException();
        }

        public AccessPoint Get(EcoProject1EcoSpace space, int id)
        {
            var accessPoints = EcoProject1.AccessPoint.Get(space, id);

            return accessPoints?.ToModel();
        }

        public void Update(EcoProject1EcoSpace space, AccessPoint accessPoint)
        {
            throw new System.NotImplementedException();
        }

        public void Add(EcoProject1EcoSpace space, AccessPoint accessPoint)
        {
            EcoProject1.AccessPoint.Add(space, accessPoint);
        }

        public IEnumerable<AccessPoint> GetBy(EcoProject1EcoSpace space, GasStorage gasStorage)
        {
            var accessPoints = EcoProject1.AccessPoint.GetBy(space, gasStorage);

            return accessPoints.Select(a => a.ToModel());
        }
    }
}
