﻿using System.Collections.Generic;
using System.Linq;
using Orms.DataAccessEco.Converters;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;

namespace Orms.DataAccessEco.Repositories
{
    public class AgreementRepository : IAgreementRepository<EcoProject1EcoSpace>
    {
        public IEnumerable<Agreement> GetAll(EcoProject1EcoSpace space)
        {
            var agreements = EcoProject1.Agreement.GetAll(space);

            return agreements.Select(a => a.ToModel());
        }

        public Agreement Get(EcoProject1EcoSpace space, int id)
        {
            var agreement = EcoProject1.Agreement.Get(space, id);

            return agreement?.ToModel();
        }

        public void Update(EcoProject1EcoSpace space, Agreement model)
        {
            throw new System.NotImplementedException();
        }

        public void Add(EcoProject1EcoSpace space, Agreement agreement)
        {
            EcoProject1.Agreement.Add(space, agreement);
        }

        public IEnumerable<Agreement> GetBy(EcoProject1EcoSpace space, GasStorage gasStorage)
        {
            var agreements = EcoProject1.Agreement.GetBy(space, gasStorage);

            return agreements.Select(a => a.ToModel());
        }

        public IEnumerable<Agreement> GetBy(EcoProject1EcoSpace space, Contractor contractor)
        {
            var agreements = EcoProject1.Agreement.GetBy(space, contractor);

            return agreements.Select(a => a.ToModel());
        }
    }
}
