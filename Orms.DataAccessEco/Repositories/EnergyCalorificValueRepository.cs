﻿using System.Collections.Generic;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;

namespace Orms.DataAccessEco.Repositories
{
    public class EnergyCalorificValueRepository : IEnergyCalorificValueRepository<EcoProject1EcoSpace>
    {
        public IEnumerable<EnergyCalorificValue> GetAll(EcoProject1EcoSpace space)
        {
            throw new System.NotImplementedException();
        }

        public EnergyCalorificValue Get(EcoProject1EcoSpace space, int id)
        {
            throw new System.NotImplementedException();
        }

        public void Update(EcoProject1EcoSpace space, EnergyCalorificValue model)
        {
            throw new System.NotImplementedException();
        }

        public void Add(EcoProject1EcoSpace space, EnergyCalorificValue model)
        {
            throw new System.NotImplementedException();
        }

        public void Add(EcoProject1EcoSpace space, IEnumerable<EnergyCalorificValue> models)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<EnergyCalorificValue> GetAll(EcoProject1EcoSpace space, CalorificValue cv)
        {
            throw new System.NotImplementedException();
        }
    }
}