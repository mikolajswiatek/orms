﻿using System.Collections.Generic;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;

namespace Orms.DataAccessEco.Repositories
{
    public class NominationValueRepository : INominationValueRepository<EcoProject1EcoSpace>
    {
        public IEnumerable<NominationValue> GetAll(EcoProject1EcoSpace space)
        {
            throw new System.NotImplementedException();
        }

        public NominationValue Get(EcoProject1EcoSpace space, int id)
        {
            throw new System.NotImplementedException();
        }

        public void Update(EcoProject1EcoSpace space, NominationValue model)
        {
            throw new System.NotImplementedException();
        }

        public void Add(EcoProject1EcoSpace space, NominationValue model)
        {
            throw new System.NotImplementedException();
        }

        public void Add(EcoProject1EcoSpace space, IEnumerable<NominationValue> models)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<NominationValue> GetBy(EcoProject1EcoSpace space, Nomination nomination)
        {
            throw new System.NotImplementedException();
        }
    }
}
