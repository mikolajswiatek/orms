﻿using System.Collections.Generic;
using System.Linq;
using Orms.DataAccessEco.Converters;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;

namespace Orms.DataAccessEco.Repositories
{
    public class ContractorContactRepository : IContractorContactRepository<EcoProject1EcoSpace>
    {
        public IEnumerable<ContractorContact> GetAll(EcoProject1EcoSpace space)
        {
            throw new System.NotImplementedException();
        }

        public ContractorContact Get(EcoProject1EcoSpace space, int id)
        {
            throw new System.NotImplementedException();
        }

        public void Update(EcoProject1EcoSpace space, ContractorContact model)
        {
            throw new System.NotImplementedException();
        }

        public void Add(EcoProject1EcoSpace space, ContractorContact contact)
        {
            EcoProject1.ContractorContact.Add(space, contact);
        }

        public IEnumerable<ContractorContact> GetByContractor(EcoProject1EcoSpace space, Contractor contractor)
        {
            var contacts = EcoProject1.ContractorContact.GetByContractor(space, contractor);

            return contacts.Select(cc => cc.ToModel());
        }
    }
}
