﻿using System;
using System.Collections.Generic;
using System.Linq;
using Orms.DataAccessEco.Converters;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;

namespace Orms.DataAccessEco.Repositories
{
    public class NominationRepository : INominationRepository<EcoProject1EcoSpace>
    {
        public IEnumerable<Nomination> GetAll(EcoProject1EcoSpace space)
        {
            var nominations = EcoProject1.Nomination.GetAll(space);

            return nominations.Select(c => c.ToModel());
        }

        public Nomination Get(EcoProject1EcoSpace space, int id)
        {
            var nomination = EcoProject1.Nomination.Get(space, id);

            if (nomination == null) return null;

            var nom = nomination.ToModel();
            nom.Values = nomination.NominationValue.Select(v => v.ToModel());

            return nom;
        }

        public void Update(EcoProject1EcoSpace space, Nomination nomination)
        {
            EcoProject1.Nomination.Update(space, nomination);
        }

        public void Add(EcoProject1EcoSpace space, Nomination nomination)
        {
            EcoProject1.Nomination.Add(space, nomination);
        }

        public IEnumerable<Nomination> GetBy(EcoProject1EcoSpace space, Contractor contractor)
        {
            var nominations = EcoProject1.Nomination.GetBy(space, contractor);

            var results = new List<Nomination>();

            foreach (var nomination in nominations)
            {
                var n = nomination.ToModel();
                n.Values = nomination.NominationValue.Select(v => v.ToModel());
                results.Add(n);
            }

            return results;
        }

        public IEnumerable<Nomination> GetBy(EcoProject1EcoSpace space, GasStorage gasStorage)
        {
            var nominations = EcoProject1.Nomination.GetBy(space, gasStorage);

            var results = new List<Nomination>();

            foreach (var nomination in nominations)
            {
                var n = nomination.ToModel();
                n.Values = nomination.NominationValue.Select(v => v.ToModel());
                results.Add(n);
            }

            return results;
        }

        public IEnumerable<Nomination> GetBy(EcoProject1EcoSpace space, User user)
        {
            var nominations = EcoProject1.Nomination.GetBy(space, user);

            var results = new List<Nomination>();

            foreach (var nomination in nominations)
            {
                var n = nomination.ToModel();
                n.Values = nomination.NominationValue.Select(v => v.ToModel());
                results.Add(n);
            }

            return results;
        }

        public IEnumerable<Nomination> GetBy(EcoProject1EcoSpace space, DateTime gasDay)
        {
            var nominations = EcoProject1.Nomination.GetBy(space, gasDay);

            var results = new List<Nomination>();

            foreach (var nomination in nominations)
            {
                var n = nomination.ToModel();
                n.Values = nomination.NominationValue.Select(v => v.ToModel());
                results.Add(n);
            }

            return results;
        }

        public Nomination GetLast(EcoProject1EcoSpace space, Contractor contractor)
        {
            var nomination = EcoProject1.Nomination.GetLast(space, contractor);

            if (nomination == null) return null;

            var nom = nomination.ToModel();
            nom.Values = nomination.NominationValue.Select(v => v.ToModel());

            return nom;
        }

        public Nomination GetLast(EcoProject1EcoSpace space, GasStorage gasStorage)
        {
            var nomination = EcoProject1.Nomination.GetLast(space, gasStorage);

            if (nomination == null) return null;

            var nom = nomination.ToModel();
            nom.Values = nomination.NominationValue.Select(v => v.ToModel());

            return nom;
        }

        public Nomination GetLast(EcoProject1EcoSpace space, User user)
        {
            var nomination = EcoProject1.Nomination.GetLast(space, user);

            if (nomination == null) return null;

            var nom = nomination.ToModel();
            nom.Values = nomination.NominationValue.Select(v => v.ToModel());

            return nom;
        }

        public Nomination GetLast(EcoProject1EcoSpace space, Contractor contractor, DateTime gasDay)
        {
            var nomination = EcoProject1.Nomination.GetLast(space, contractor, gasDay);

            if (nomination == null) return null;

            var nom = nomination.ToModel();
            nom.Values = nomination.NominationValue.Select(v => v.ToModel());

            return nom;
        }

        public IEnumerable<Nomination> GetForGasDay(EcoProject1EcoSpace space, Contractor contractor, DateTime gasDay)
        {
            var nominations = EcoProject1.Nomination.GetForGasDay(space, contractor, gasDay);

            var results = new List<Nomination>();

            foreach (var nomination in nominations)
            {
                var n = nomination.ToModel();
                n.Values = nomination.NominationValue.Select(v => v.ToModel());
                results.Add(n);
            }

            return results;
        }

        public IEnumerable<Nomination> GetForGasDay(EcoProject1EcoSpace space, GasStorage gasStorage, DateTime gasDay)
        {
            var nominations = EcoProject1.Nomination.GetForGasDay(space, gasStorage, gasDay);

            var results = new List<Nomination>();

            foreach (var nomination in nominations)
            {
                var n = nomination.ToModel();
                n.Values = nomination.NominationValue.Select(v => v.ToModel());
                results.Add(n);
            }

            return results;
        }

        public IEnumerable<Nomination> GetForGasDay(EcoProject1EcoSpace space, Contractor contractor, GasStorage gasStorage, DateTime gasDay)
        {
            var nominations = EcoProject1.Nomination.GetForGasDay(space, contractor, gasStorage, gasDay);

            var results = new List<Nomination>();

            foreach (var nomination in nominations)
            {
                var n = nomination.ToModel();
                n.Values = nomination.NominationValue.Select(v => v.ToModel());
                results.Add(n);
            }

            return results;
        }
    }
}
