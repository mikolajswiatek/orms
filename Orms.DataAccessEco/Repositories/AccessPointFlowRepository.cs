﻿using System.Collections.Generic;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;

namespace Orms.DataAccessEco.Repositories
{
    public class AccessPointFlowRepository : IAccessPointFlowRepository<EcoProject1EcoSpace>
    {
        public IEnumerable<AccessPointFlow> GetAll(EcoProject1EcoSpace space)
        {
            throw new System.NotImplementedException();
        }

        public AccessPointFlow Get(EcoProject1EcoSpace space, int id)
        {
            throw new System.NotImplementedException();
        }

        public void Update(EcoProject1EcoSpace space, AccessPointFlow accessPointFlow)
        {
            throw new System.NotImplementedException();
        }

        public void Add(EcoProject1EcoSpace space, AccessPointFlow accessPointFlow)
        {
            EcoProject1.AccessPointFlow.Add(space, accessPointFlow);
        }
    }
}
