﻿using System;
using System.Collections.Generic;
using System.Linq;
using Orms.DataAccessEco.Converters;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;

namespace Orms.DataAccessEco.Repositories
{
    public class CalorificValueRepository : ICalorificValueRepository<EcoProject1EcoSpace>
    {
        public IEnumerable<CalorificValue> GetAll(EcoProject1EcoSpace space)
        {
            var cvs = EcoProject1.CalorificValue.GetAll(space);

            return cvs.Select(cv => cv.ToModel());
        }

        public CalorificValue Get(EcoProject1EcoSpace space, int id)
        {
            var cv = EcoProject1.CalorificValue.Get(space, id);

            return cv?.ToModel();
        }

        public void Update(EcoProject1EcoSpace space, CalorificValue cv)
        {
            EcoProject1.CalorificValue.Update(space, cv);
        }

        public void Add(EcoProject1EcoSpace space, CalorificValue cv)
        {
            EcoProject1.CalorificValue.Add(space, cv);;
        }

        public CalorificValue GetLastBy(EcoProject1EcoSpace space, GasStorage gasStorage)
        {
            var cv = EcoProject1.CalorificValue.GetLastBy(space, gasStorage);

            return cv?.ToModel();
        }

        public CalorificValue GetForGasDay(EcoProject1EcoSpace space, GasStorage gasStorage, DateTime gasDay)
        {
            var cv = EcoProject1.CalorificValue.GetForGasDay(space, gasStorage, gasDay);

            if (cv == null) return null;

            var calorificValue = cv.ToModel();
            calorificValue.Values = cv.EnergyCalorificValue.Select(v => v.ToModel());

            return calorificValue;
        }
    }
}