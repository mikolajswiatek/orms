﻿using System.Collections.Generic;
using System.Linq;
using Orms.DataAccessEco.Converters;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;

namespace Orms.DataAccessEco.Repositories
{
    public class UserRepository : IUserRepository<EcoProject1EcoSpace>
    {
        public IEnumerable<User> GetAll(EcoProject1EcoSpace space)
        {
            var users = EcoProject1.User.GetAll(space);

            return users.Select(u => u.ToModel());
        }

        public User Get(EcoProject1EcoSpace space, int id)
        {
            var user = EcoProject1.User.Get(space, id);

            return user?.ToModel();
        }

        public void Update(EcoProject1EcoSpace space, User user)
        {
            EcoProject1.User.Update(space, user);
        }

        public void Add(EcoProject1EcoSpace space, User user)
        {
            EcoProject1.User.Add(space, user);
        }

        public IEnumerable<User> GetBy(EcoProject1EcoSpace space, Contractor contractor)
        {
            var users = EcoProject1.User.GetBy(space, contractor);

            return users.Select(u => u.ToModel());
        }

        public IEnumerable<User> GetBy(EcoProject1EcoSpace space, string nameOrEmail)
        {
            var users = EcoProject1.User.GetBy(space, nameOrEmail);

            return users.Select(u => u.ToModel());
        }
    }
}
