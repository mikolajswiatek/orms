﻿using System.Collections.Generic;
using System.Linq;
using Orms.DataAccessEco.Converters;
using Orms.DataAccessEco.Helpers;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;

namespace Orms.DataAccessEco.Repositories
{
    public class GasStorageRepository: IGasStorageRepository<EcoProject1EcoSpace>
    {
        public IEnumerable<GasStorage> GetAll(EcoProject1EcoSpace space)
        {
            var gasStorages = EcoProject1.GasStorage.GetAll(space);

            return gasStorages.Select(c => c.ToModel());
        }

        public GasStorage Get(EcoProject1EcoSpace space, int id)
        {
            var gasStorage = EcoProject1.GasStorage.Get(space, $"{ClassId.GasStorage}!{id}");

            return gasStorage?.ToModel();
        }

        public void Update(EcoProject1EcoSpace space, GasStorage gasStorage)
        {
            throw new System.NotImplementedException();
        }

        public void Add(EcoProject1EcoSpace space, GasStorage gasStorage)
        {
            EcoProject1.GasStorage.Add(space, gasStorage);
        }
    }
}
