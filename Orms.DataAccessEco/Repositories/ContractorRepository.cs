﻿using System.Collections.Generic;
using System.Linq;
using Orms.DataAccessEco.Converters;
using Orms.DataAccessEco.Helpers;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;

namespace Orms.DataAccessEco.Repositories
{
    public class ContractorRepository : IContractorRepository<EcoProject1EcoSpace>
    {
        public IEnumerable<Contractor> GetAll(EcoProject1EcoSpace space)
        {
            var contractors = EcoProject1.Contractor.GetAll(space);

            return contractors.Select(c => c.ToModel());
        }

        public Contractor Get(EcoProject1EcoSpace space, int id)
        {
            var contractor = EcoProject1.Contractor.Get(space, $"{ClassId.Contractor}!{id}");

            return contractor.ToModel();
        }

        public void Update(EcoProject1EcoSpace space, Contractor model)
        {
            throw new System.NotImplementedException();
        }

        public void Add(EcoProject1EcoSpace space, Contractor contractor)
        {
            EcoProject1.Contractor.Add(space, contractor);
        }
    }
}