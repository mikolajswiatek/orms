﻿using System;
using System.Linq;
using System.Collections.Generic;
using Orms.DataAccessEco.Converters;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;

namespace Orms.DataAccessEco.Repositories
{
    public class GasAccountRepository : IGasAccountRepository<EcoProject1EcoSpace>
    {
        public IEnumerable<GasAccount> GetAll(EcoProject1EcoSpace space)
        {
            var gasAccounts = EcoProject1.GasAccount.GetAll(space);

            return gasAccounts.Select(cv => cv.ToModel());
        }

        public GasAccount Get(EcoProject1EcoSpace space, int id)
        {
            var gasAccount = EcoProject1.GasAccount.Get(space, id);

            return gasAccount?.ToModel();
        }

        public void Update(EcoProject1EcoSpace space, GasAccount gasAccount)
        {
            EcoProject1.GasAccount.Update(space, gasAccount);
        }

        public void Add(EcoProject1EcoSpace space, GasAccount gasAccount)
        {
            EcoProject1.GasAccount.Add(space, gasAccount);
        }

        public void Add(EcoProject1EcoSpace space, IEnumerable<GasAccount> gasAccounts)
        {
            EcoProject1.GasAccount.Add(space, gasAccounts);
        }

        public void Update(EcoProject1EcoSpace space, IEnumerable<GasAccount> gasAccounts)
        {
            EcoProject1.GasAccount.Update(space, gasAccounts);
        }

        public IEnumerable<GasAccount> Get(EcoProject1EcoSpace space, GasStorage gasStorage, DateTime gasDay)
        {
            var gasAccounts = EcoProject1.GasAccount.Get(space, gasStorage, gasDay);

            return gasAccounts.Select(cv => cv.ToModel());
        }

        public IEnumerable<GasAccount> Get(EcoProject1EcoSpace space, DateTime gasDay)
        {
            var gasAccounts = EcoProject1.GasAccount.Get(space, gasDay);

            return gasAccounts.Select(cv => cv.ToModel());
        }
    }
}
