﻿using System.Collections.Generic;
using Orms.DataAccessEco.Repositories;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;
using Orms.Providers;

namespace Orms.DataAccessEco.Providers
{
    public class ContractorContactProvider : IContractorContactProvider
    {
        private readonly IContractorContactRepository<EcoProject1EcoSpace> _repository;
        private readonly EcoProject1EcoSpace _space;

        public ContractorContactProvider(EcoProject1EcoSpace space)
        {
            _space = space;
            _repository = new ContractorContactRepository();
        }

        public void Add(ContractorContact contractorContact)
        {
            _repository.Add(_space, contractorContact);
        }

        public IEnumerable<ContractorContact> GetByContractor(Contractor contractor)
        {
            return _repository.GetByContractor(_space, contractor);
        }

        public ContractorContact Get(int id)
        {
            return _repository.Get(_space, id);
        }

        public IEnumerable<ContractorContact> GetAll()
        {
            return _repository.GetAll(_space);
        }

        public void Update(ContractorContact contractorContact)
        {
            _repository.Update(_space, contractorContact);
        }
    }
}