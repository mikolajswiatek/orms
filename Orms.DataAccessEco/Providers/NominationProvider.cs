﻿using System;
using System.Collections.Generic;
using Orms.DataAccessEco.Repositories;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;
using Orms.Providers;

namespace Orms.DataAccessEco.Providers
{
    public class NominationProvider : INominationProvider
    {
        private readonly INominationRepository<EcoProject1EcoSpace> _repository;
        private readonly EcoProject1EcoSpace _space;

        public NominationProvider(EcoProject1EcoSpace space)
        {
            _space = space;
            _repository = new NominationRepository();
        }

        public void Add(Nomination nomination)
        {
            _repository.Add(_space, nomination);
        }

        public Nomination Get(int id)
        {
            return _repository.Get(_space, id);
        }

        public IEnumerable<Nomination> GetAll()
        {
            return _repository.GetAll(_space);
        }

        public void Update(Nomination nomination)
        {
            _repository.Update(_space, nomination);
        }

        public IEnumerable<Nomination> GetBy(Contractor contractor)
        {
            return _repository.GetBy(_space, contractor);
        }

        public IEnumerable<Nomination> GetBy(GasStorage gasStorage)
        {
            return _repository.GetBy(_space, gasStorage);
        }

        public IEnumerable<Nomination> GetBy(User user)
        {
            return _repository.GetBy(_space, user);
        }

        public IEnumerable<Nomination> GetBy(DateTime gasDay)
        {
            return _repository.GetBy(_space, gasDay);
        }

        public Nomination GetLast(Contractor contractor)
        {
            return _repository.GetLast(_space, contractor);
        }

        public Nomination GetLast(GasStorage gasStorage)
        {
            return _repository.GetLast(_space, gasStorage);
        }

        public Nomination GetLast(User user)
        {
            return _repository.GetLast(_space, user);
        }

        public Nomination GetLast(Contractor contractor, DateTime gasDay)
        {
            return _repository.GetLast(_space, contractor);
        }

        public IEnumerable<Nomination> GetForGasDay(Contractor contractor, DateTime gasDay)
        {
            return _repository.GetForGasDay(_space, contractor, gasDay);
        }

        public IEnumerable<Nomination> GetForGasDay(GasStorage gasStorage, DateTime gasDay)
        {
            return _repository.GetForGasDay(_space, gasStorage, gasDay);
        }

        public IEnumerable<Nomination> GetForGasDay(Contractor contractor, GasStorage gasStorage, DateTime gasDay)
        {
            return _repository.GetForGasDay(_space, contractor, gasStorage, gasDay);
        }
    }
}