﻿using System.Collections.Generic;
using Orms.DataAccessEco.Repositories;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;
using Orms.Providers;

namespace Orms.DataAccessEco.Providers
{
    public class ContractorProvider : IContractorProvider
    {
        private readonly IContractorRepository<EcoProject1EcoSpace> _repository;
        private readonly EcoProject1EcoSpace _space;

        public ContractorProvider(EcoProject1EcoSpace space)
        {
            _repository = new ContractorRepository();
            _space = space;
        }

        public IEnumerable<Contractor> GetAll()
        {
            return _repository.GetAll(_space);
        }

        public Contractor Get(int id)
        {
            return _repository.Get(_space, id);
        }

        public void Update(Contractor contractor)
        {
            _repository.Update(_space, contractor);
        }

        public void Add(Contractor contractor)
        {
           _repository.Add(_space, contractor);
        }
    }
}
