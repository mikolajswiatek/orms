﻿using System.Collections.Generic;
using Orms.DataAccessEco.Repositories;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;
using Orms.Providers;

namespace Orms.DataAccessEco.Providers
{
    public class EnergyCalorificValueProvider : IEnergyCalorificValueProvider
    {
        private readonly IEnergyCalorificValueRepository<EcoProject1EcoSpace> _repository;
        private readonly EcoProject1EcoSpace _space;

        public EnergyCalorificValueProvider(EcoProject1EcoSpace space)
        {
            _space = space;
            _repository = new EnergyCalorificValueRepository();
        }

        public IEnumerable<EnergyCalorificValue> GetAll()
        {
            return _repository.GetAll(_space);
        }

        public EnergyCalorificValue Get(int id)
        {
            return _repository.Get(_space, id);
        }

        public void Update(EnergyCalorificValue ecv)
        {
            _repository.Update(_space, ecv);
        }

        public void Add(EnergyCalorificValue ecv)
        {
            _repository.Add(_space, ecv);
        }

        public void Add(IEnumerable<EnergyCalorificValue> ecvs)
        {
            _repository.Add(_space, ecvs);
        }

        public IEnumerable<EnergyCalorificValue> GetAll(CalorificValue cv)
        {
            return _repository.GetAll(_space, cv);
        }
    }
}