﻿using System;
using System.Collections.Generic;
using Orms.DataAccessEco.Repositories;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;
using Orms.Providers;

namespace Orms.DataAccessEco.Providers
{
    public class CalorificValueProvider : ICalorificValueProvider
    {
        private readonly ICalorificValueRepository<EcoProject1EcoSpace> _repository;
        private readonly EcoProject1EcoSpace _space;

        public CalorificValueProvider(EcoProject1EcoSpace space)
        {
            _space = space;
            _repository = new CalorificValueRepository();
        }

        public IEnumerable<CalorificValue> GetAll()
        {
            return _repository.GetAll(_space);
        }

        public CalorificValue Get(int id)
        {
            return _repository.Get(_space, id);
        }

        public void Update(CalorificValue cv)
        {
            _repository.Update(_space, cv);
        }

        public void Add(CalorificValue cv)
        {
            _repository.Add(_space, cv);
        }

        public CalorificValue GetLastBy(GasStorage gasStorage)
        {
            return _repository.GetLastBy(_space, gasStorage);
        }

        public CalorificValue GetForGasDay(GasStorage gasStorage, DateTime gasDay)
        {
            return _repository.GetForGasDay(_space, gasStorage, gasDay);
        }
    }
}