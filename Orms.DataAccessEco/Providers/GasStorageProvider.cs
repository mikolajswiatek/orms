﻿using System.Collections.Generic;
using Orms.DataAccessEco.Repositories;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;
using Orms.Providers;

namespace Orms.DataAccessEco.Providers
{
    public class GasStorageProvider : IGasStorageProvider
    {
        private readonly IGasStorageRepository<EcoProject1EcoSpace> _repository;
        private readonly EcoProject1EcoSpace _space;

        public GasStorageProvider(EcoProject1EcoSpace space)
        {
            _space = space;
            _repository = new GasStorageRepository();
        }

        public void Add(GasStorage gasStorage)
        {
            _repository.Add(_space, gasStorage);
        }

        public GasStorage Get(int id)
        {
            return _repository.Get(_space, id);
        }

        public IEnumerable<GasStorage> GetAll()
        {
            return _repository.GetAll(_space);
        }

        public void Update(GasStorage gasStorage)
        {
            _repository.Update(_space, gasStorage);
        }
    }
}