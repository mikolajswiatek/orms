﻿using System.Collections.Generic;
using Orms.DataAccessEco.Repositories;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;
using Orms.Providers;

namespace Orms.DataAccessEco.Providers
{
    public class UserProvider : IUserProvider
    {
        private readonly IUserRepository<EcoProject1EcoSpace> _repository;
        private readonly EcoProject1EcoSpace _space;

        public UserProvider(EcoProject1EcoSpace space)
        {
            _space = space;
            _repository = new UserRepository();
        }

        public void Add(User user)
        {
            _repository.Add(_space, user);
        }

        public User Get(int id)
        {
            return _repository.Get(_space, id);
        }

        public IEnumerable<User> GetAll()
        {
            return _repository.GetAll(_space);
        }

        public void Update(User user)
        {
            _repository.Update(_space, user);
        }

        public IEnumerable<User> GetBy(Contractor contractor)
        {
            return _repository.GetBy(_space, contractor);
        }

        public IEnumerable<User> GetBy(string nameOrEmail)
        {
            return _repository.GetBy(_space, nameOrEmail);
        }
    }
}