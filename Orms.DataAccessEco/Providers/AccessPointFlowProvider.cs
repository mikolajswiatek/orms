﻿using System.Collections.Generic;
using Orms.DataAccessEco.Repositories;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;
using Orms.Providers;

namespace Orms.DataAccessEco.Providers
{
    public class AccessPointFlowProvider : IAccessPointFlowProvider
    {
        private readonly IAccessPointFlowRepository<EcoProject1EcoSpace> _repository;
        private readonly EcoProject1EcoSpace _space;

        public AccessPointFlowProvider(EcoProject1EcoSpace space)
        {
            _space = space;
            _repository = new AccessPointFlowRepository();
        }

        public void Add(AccessPointFlow apf)
        {
            _repository.Add(_space, apf);
        }

        public AccessPointFlow Get(int id)
        {
            return _repository.Get(_space, id);
        }

        public IEnumerable<AccessPointFlow> GetAll()
        {
            return _repository.GetAll(_space);
        }

        public void Update(AccessPointFlow apf)
        {
            _repository.Update(_space, apf);
        }
    }
}