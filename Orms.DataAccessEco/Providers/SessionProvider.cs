﻿using System.Collections.Generic;
using Orms.DataAccessEco.Repositories;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;
using Orms.Providers;

namespace Orms.DataAccessEco.Providers
{
    public class SessionProvider : ISessionProvider
    {
        private readonly ISessionRepository<EcoProject1EcoSpace> _repository;
        private readonly EcoProject1EcoSpace _space;

        public SessionProvider(EcoProject1EcoSpace space)
        {
            _space = space;
            _repository = new SessionRepository();
        }

        public void Add(Session s)
        {
            _repository.Add(_space, s);

        }

        public Session GetBy(string guid)
        {
            return _repository.GetBy(_space, guid);
        }

        public void Delete(Session s)
        {
            _repository.Delete(_space, s);
        }

        public Session Get(int id)
        {
            return _repository.Get(_space, id);
        }

        public IEnumerable<Session> GetAll()
        {
            return _repository.GetAll(_space);
        }

        public void Update(Session s)
        {
            _repository.Update(_space, s);
        }
    }
}