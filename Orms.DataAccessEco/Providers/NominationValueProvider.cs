﻿using System.Collections.Generic;
using Orms.DataAccessEco.Repositories;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;
using Orms.Providers;

namespace Orms.DataAccessEco.Providers
{
    public class NominationValueProvider : INominationValueProvider
    {
        private readonly INominationValueRepository<EcoProject1EcoSpace> _repository;
        private readonly EcoProject1EcoSpace _space;

        public NominationValueProvider(EcoProject1EcoSpace space)
        {
            _space = space;
            _repository = new NominationValueRepository();
        }

        public void Add(NominationValue nominationValue)
        {
            _repository.Add(_space, nominationValue);
        }

        public void Add(IEnumerable<NominationValue> values)
        {
            _repository.Add(_space, values);
        }

        public IEnumerable<NominationValue> GetBy(Nomination nomination)
        {
            return _repository.GetBy(_space, nomination);
        }

        public NominationValue Get(int id)
        {
            return _repository.Get(_space, id);
        }

        public IEnumerable<NominationValue> GetAll()
        {
            return _repository.GetAll(_space);
        }

        public void Update(NominationValue nominationValue)
        {
            _repository.Update(_space, nominationValue);
        }
    }
}