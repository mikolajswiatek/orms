﻿using System;
using System.Collections.Generic;
using Orms.DataAccessEco.Repositories;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;
using Orms.Providers;

namespace Orms.DataAccessEco.Providers
{
    public class GasAccountProvider : IGasAccountProvider
    {
        private readonly IGasAccountRepository<EcoProject1EcoSpace> _repository;
        private readonly EcoProject1EcoSpace _space;

        public GasAccountProvider(EcoProject1EcoSpace space)
        {
            _space = space;
            _repository = new GasAccountRepository();
        }
        public void Add(GasAccount gasAccount)
        {
            _repository.Add(_space, gasAccount);
        }

        public GasAccount Get(int id)
        {
            return _repository.Get(_space, id);
        }

        public IEnumerable<GasAccount> GetAll()
        {
            return _repository.GetAll(_space);
        }

        public void Update(GasAccount gasAccount)
        {
            _repository.Update(_space, gasAccount);
        }

        public void Add(IEnumerable<GasAccount> gasAccounts)
        {
            _repository.Add(_space, gasAccounts);
        }

        public void Update(IEnumerable<GasAccount> gasAccounts)
        {
            _repository.Update(_space, gasAccounts);
        }

        public IEnumerable<GasAccount> Get(DateTime gasDay)
        {
            return _repository.Get(_space, gasDay);
        }

        public IEnumerable<GasAccount> Get(GasStorage gasStorage, DateTime gasDay)
        {
            return _repository.Get(_space, gasStorage, gasDay);
        }
    }
}