﻿using System.Collections.Generic;
using Orms.DataAccessEco.Repositories;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;
using Orms.Providers;

namespace Orms.DataAccessEco.Providers
{
    public class AccessPointProvider : IAccessPointProvider
    {
        private readonly IAccessPointRepository<EcoProject1EcoSpace> _repository;
        private readonly EcoProject1EcoSpace _space;

        public AccessPointProvider(EcoProject1EcoSpace space)
        {
            _space = space;
            _repository = new AccessPointRepository();
        }

        public void Add(AccessPoint ap)
        {
            _repository.Add(_space, ap);
        }

        public AccessPoint Get(int id)
        {
            return _repository.Get(_space, id);
        }

        public IEnumerable<AccessPoint> GetAll()
        {
            return _repository.GetAll(_space);
        }

        public void Update(AccessPoint ap)
        {
            _repository.Update(_space, ap);
        }

        public IEnumerable<AccessPoint> GetBy(GasStorage gasStorage)
        {
            return _repository.GetBy(_space, gasStorage);
        }
    }
}