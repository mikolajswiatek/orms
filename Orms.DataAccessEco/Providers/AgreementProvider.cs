﻿using System.Collections.Generic;
using Orms.DataAccessEco.Repositories;
using Orms.DataAccessHelper.Repositories.Interfaces;
using Orms.Models;
using Orms.Providers;

namespace Orms.DataAccessEco.Providers
{
    public class AgreementProvider : IAgreementProvider
    {
        private readonly IAgreementRepository<EcoProject1EcoSpace> _repository;
        private readonly EcoProject1EcoSpace _space;

        public AgreementProvider(EcoProject1EcoSpace space)
        {
            _space = space;
            _repository = new AgreementRepository();
        }

        public void Add(Agreement agreement)
        {
            _repository.Add(_space, agreement);
        }

        public Agreement Get(int id)
        {
            return _repository.Get(_space, id);
        }

        public IEnumerable<Agreement> GetAll()
        {
            return _repository.GetAll(_space);
        }

        public void Update(Agreement agreement)
        {
            _repository.Update(_space, agreement);
        }

        public IEnumerable<Agreement> GetBy(Contractor contractor)
        {
            return _repository.GetBy(_space, contractor);
        }

        public IEnumerable<Agreement> GetBy(GasStorage gasStorage)
        {
            return _repository.GetBy(_space, gasStorage);
        }
    }
}