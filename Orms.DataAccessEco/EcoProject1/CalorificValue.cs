using System.Linq;
using Orms.DataAccessEco;
using Orms.DataAccessEco.Helpers;

namespace EcoProject1
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.CodeDom.Compiler;
    using Eco.ObjectImplementation;
    using Eco.ObjectRepresentation;
    using Eco.Services;
    using Eco.Subscription;
    using Eco.UmlCodeAttributes;
    using Eco.UmlRt;


    public partial class CalorificValue
    {
        public static IEnumerable<CalorificValue> GetAll(EcoProject1EcoSpace space)
        {
            space.Active = true;

            var cvs = EcoServiceHelper.GetExtentService(space)
                .AllInstances<CalorificValue>()
                .ToList();

            return cvs;
        }

        public static CalorificValue Get(EcoProject1EcoSpace space, int id)
        {
            space.Active = true;

            var cv = EcoServiceHelper.GetExtentService(space)
                .AllInstances<CalorificValue>()
                .FirstOrDefault(c => c.ExternalId() == $"{ClassId.CalorificValue}!{id}");

            return cv;
        }

        public static void Update(EcoProject1EcoSpace space, Orms.Models.CalorificValue cv)
        {
            throw new NotImplementedException();
        }

        public static void Add(EcoProject1EcoSpace space, Orms.Models.CalorificValue cv)
        {
            space.Active = true;

            var gs = EcoServiceHelper.GetExtentService(space)
                .AllInstances<GasStorage>()
                .FirstOrDefault(c => c.ExternalId() == $"{ClassId.GasStorage}!{cv.GasStorage.Id}");

            var calorificValue = new CalorificValue(space)
            {
                GasStorage = gs,
                GasDay = cv.GasDay,
            };

            foreach (var value in cv.Values)
            {
                var val = new EnergyCalorificValue(space)
                {
                    CalorificValue = calorificValue,
                    HourIndex = value.HourIndex,
                    EnergyValue = value.Energy.Value,
                    EnergyUnit = value.Energy.UnitString
                };

                calorificValue.EnergyCalorificValue.Add(val);
            }

            space.Persistence.UpdateDatabase(calorificValue);
        }

        public static CalorificValue GetLastBy(EcoProject1EcoSpace space, Orms.Models.GasStorage gasStorage)
        {
            space.Active = true;

            var gs = EcoServiceHelper.GetExtentService(space)
                .AllInstances<GasStorage>()
                .FirstOrDefault(c => c.ExternalId() == $"{ClassId.GasStorage}!{gasStorage.Id}");

            var cv = EcoServiceHelper.GetExtentService(space)
                .AllInstances<CalorificValue>()
                .OrderByDescending(c => c.ExternalId())
                .FirstOrDefault(c => c.GasStorage == gs);

            return cv;
        }

        public static CalorificValue GetForGasDay(EcoProject1EcoSpace space, Orms.Models.GasStorage gasStorage, DateTime gasDay)
        {
            space.Active = true;

            var gs = EcoServiceHelper.GetExtentService(space)
                .AllInstances<GasStorage>()
                .FirstOrDefault(c => c.ExternalId() == $"{ClassId.GasStorage}!{gasStorage.Id}");

            var cv = EcoServiceHelper.GetExtentService(space)
                .AllInstances<CalorificValue>()
                .OrderByDescending(c => c.ExternalId())
                .FirstOrDefault(c => c.GasStorage == gs && c.GasDay == gasDay);

            return cv;
        }
    }
}