using System.Collections.Generic;
using System.Linq;
using Eco.ObjectRepresentation;
using Eco.Services;
using Orms.DataAccessEco;
using Orms.DataAccessEco.Helpers;

namespace EcoProject1
{
    public partial class Contractor
    {
        public static void Add(EcoProject1EcoSpace space, Orms.Models.Contractor contractor)
        {
            space.Active = true;

            var c = new Contractor(space)
            {
                Adress = contractor.Adress,
                Code = contractor.Code,
                Name = contractor.Name,
            };

            space.Persistence.UpdateDatabase(c);
        }

        public static IEnumerable<Contractor> GetAll(EcoProject1EcoSpace space)
        {
            space.Active = true;

            var contractors = EcoServiceHelper.GetExtentService(space)
                .AllInstances<Contractor>().ToList();

            return contractors;
        }

        public static Contractor Get(EcoProject1EcoSpace space, string id)
        {
            space.Active = true;

            var contractor = EcoServiceHelper.GetExtentService(space).AllInstances<Contractor>()
                .FirstOrDefault(c => c.ExternalId() == id);

            return contractor;
        }

        public static Contractor Get(EcoProject1EcoSpace space, int id)
        {
            space.Active = true;

            var contractor = EcoServiceHelper.GetExtentService(space).AllInstances<Contractor>()
                .FirstOrDefault(c => c.ExternalId() == $"{ClassId.Contractor}!{id}");

            return contractor;
        }
    }
}