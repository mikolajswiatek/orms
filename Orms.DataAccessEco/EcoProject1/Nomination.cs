using System;
using System.Collections.Generic;
using System.Linq;
using Eco.ObjectRepresentation;
using Eco.Services;
using Orms.DataAccessEco;
using Orms.DataAccessEco.Helpers;

namespace EcoProject1
{
    public partial class Nomination
    {
        public static IEnumerable<Nomination> GetAll(EcoProject1EcoSpace space)
        {
            space.Active = true;

            var nominations = EcoServiceHelper.GetExtentService(space)
                .AllInstances<Nomination>()
                .ToList();

            return nominations;

        }

        public static Nomination Get(EcoProject1EcoSpace space, int id)
        {
            space.Active = true;

            var nomination = EcoServiceHelper.GetExtentService(space)
                .AllInstances<Nomination>()
                .FirstOrDefault(n => n.ExternalId() == $"{ClassId.Nomination}!{id}");

            return nomination;
        }

        public static void Update(EcoProject1EcoSpace space, Orms.Models.Nomination nomination)
        {
            space.Active = true;

            var nom = EcoServiceHelper.GetExtentService(space)
                .AllInstances<Nomination>()
                .FirstOrDefault(n => n.ExternalId() == $"{ClassId.Nomination}!{nomination.Id}");

            if (nom == null) return;

            nom.IsAccepted = nomination.IsAccepted;
            nom.IsLastOrder = nomination.IsLast;

            space.Persistence.UpdateDatabase(nom);
        }

        public static void Add(EcoProject1EcoSpace space, Orms.Models.Nomination nomination)
        {
            space.Active = true;

            var contractor = Contractor.Get(space, nomination.Contractor.Id);
            var apf = AccessPointFlow.Get(space, nomination.AccessPointFlow.Id);
            var user = User.Get(space, nomination.Creator.Id);
            var cv = CalorificValue.Get(space, nomination.CalorificValue.Id);

            var nom = new Nomination(space)
            {
                Contractor = contractor,
                GasDay = nomination.GasDay,
                User = user,
                CalorificValue = cv,
                AccessPointFlow = apf,
                Name = nomination.Name,
                IsLastOrder = nomination.IsLast,
                IsAccepted = nomination.IsAccepted,
                CreateDateTime = nomination.CreateDateTime,
                Channel = nomination.ChannelString,
                Comment = nomination.Comment,
                MajorVersion = nomination.MajorVersion,
                Version = nomination.Version
            };

            if (nomination.ParentNomination != null)
            {
                var parentNomination = Get(space, nomination.ParentNomination.Id);
                nom.Nomination_parent = parentNomination;
            }

            foreach (var nominationValue in nomination.Values)
            {
                var value = new NominationValue(space)
                {
                    Nomination = nom,
                    HourIndex = nominationValue.HourIndex,
                    VolumeUnit = nominationValue.Volume.UnitString,
                    VolumeValue = nominationValue.Volume.Value,
                    EnergyValue = nominationValue.Energy.Value,
                    EnergyUnit = nominationValue.Energy.UnitString
                };

                nom.NominationValue.Add(value);
            }

            space.Persistence.UpdateDatabase(nom);

        }

        public static IEnumerable<Nomination> GetBy(EcoProject1EcoSpace space, Orms.Models.Contractor contractor)
        {
            space.Active = true;

            var contractorEntity = Contractor.Get(space, contractor.Id);

            var nominations = EcoServiceHelper.GetExtentService(space)
                .AllInstances<Nomination>()
                .Where(n => n.Contractor == contractorEntity)
                .ToList();

            return nominations;
        }

        public static IEnumerable<Nomination> GetBy(EcoProject1EcoSpace space, Orms.Models.GasStorage gasStorage)
        {
            space.Active = true;

            var apfIds = GetApfIds(space, gasStorage);

            var nominations = EcoServiceHelper.GetExtentService(space)
                .AllInstances<Nomination>()
                .Where(n => apfIds.Contains(n.AccessPointFlow.ExternalId()))
                .ToList();

            return nominations;
        }

        public static IEnumerable<Nomination> GetBy(EcoProject1EcoSpace space, Orms.Models.User user)
        {
            space.Active = true;

            var userEntity = User.Get(space, user.Id);

            var nominations = EcoServiceHelper.GetExtentService(space)
                .AllInstances<Nomination>()
                .Where(n => n.User == userEntity)
                .ToList();

            return nominations;
        }

        public static IEnumerable<Nomination> GetBy(EcoProject1EcoSpace space, DateTime gasDay)
        {
            space.Active = true;

            var nominations = EcoServiceHelper.GetExtentService(space)
                .AllInstances<Nomination>()
                .Where(n => n.GasDay == gasDay)
                .ToList();

            return nominations;
        }

        public static Nomination GetLast(EcoProject1EcoSpace space, Orms.Models.Contractor contractor)
        {
            space.Active = true;

            var contractorEntity = Contractor.Get(space, contractor.Id);

            var nomination = EcoServiceHelper.GetExtentService(space)
                .AllInstances<Nomination>()
                .Where(n => n.Contractor == contractorEntity)
                .OrderByDescending(n => n.ExternalId())
                .LastOrDefault();

            return nomination;
        }

        public static Nomination GetLast(EcoProject1EcoSpace space, Orms.Models.GasStorage gasStorage)
        {
            space.Active = true;

            var apfIds = GetApfIds(space, gasStorage);

            var nomination = EcoServiceHelper.GetExtentService(space)
                .AllInstances<Nomination>()
                .Where(n => apfIds.Contains(n.AccessPointFlow.ExternalId()))
                .OrderByDescending(n => n.ExternalId())
                .LastOrDefault();

            return nomination;
        }

        public static Nomination GetLast(EcoProject1EcoSpace space, Orms.Models.User user)
        {
            space.Active = true;

            var userEntity = User.Get(space, user.Id);

            var nomination = EcoServiceHelper.GetExtentService(space)
                .AllInstances<Nomination>()
                .Where(n => n.User == userEntity)
                .OrderByDescending(n => n.ExternalId())
                .LastOrDefault();

            return nomination;
        }

        public static Nomination GetLast(EcoProject1EcoSpace space, Orms.Models.Contractor contractor, DateTime gasDay)
        {
            space.Active = true;

            var contractorEntity = Contractor.Get(space, contractor.Id);

            var nomination = EcoServiceHelper.GetExtentService(space)
                .AllInstances<Nomination>()
                .Where(n => n.Contractor == contractorEntity && n.GasDay == gasDay)
                .OrderByDescending(n => n.ExternalId())
                .LastOrDefault();

            return nomination;
        }

        public static IEnumerable<Nomination> GetForGasDay(EcoProject1EcoSpace space, Orms.Models.Contractor contractor, DateTime gasDay)
        {
            space.Active = true;

            var contractorEntity = Contractor.Get(space, contractor.Id);

            var nominations = EcoServiceHelper.GetExtentService(space)
                .AllInstances<Nomination>()
                .Where(n => n.Contractor == contractorEntity && n.GasDay == gasDay)
                .OrderByDescending(n => n.ExternalId())
                .ToList();

            return nominations;
        }

        public static IEnumerable<Nomination> GetForGasDay(EcoProject1EcoSpace space, Orms.Models.GasStorage gasStorage, DateTime gasDay)
        {
            space.Active = true;

            var apfIds = GetApfIds(space, gasStorage);

            var nominations = EcoServiceHelper.GetExtentService(space)
                .AllInstances<Nomination>()
                .Where(n => apfIds.Contains(n.AccessPointFlow.ExternalId()) && n.GasDay == gasDay)
                .OrderByDescending(n => n.ExternalId())
                .ToList();

            return nominations;
        }

        public static IEnumerable<Nomination> GetForGasDay(EcoProject1EcoSpace space, Orms.Models.Contractor contractor, Orms.Models.GasStorage gasStorage, DateTime gasDay)
        {
            space.Active = true;

            var contractorEntity = Contractor.Get(space, contractor.Id);
            var apfIds = GetApfIds(space, gasStorage);

            var nominations = EcoServiceHelper.GetExtentService(space)
                .AllInstances<Nomination>()
                .Where(n => n.Contractor == contractorEntity && apfIds.Contains(n.AccessPointFlow.ExternalId()) && n.GasDay == gasDay)
                .OrderByDescending(n => n.ExternalId())
                .ToList();

            return nominations;
        }

        private static List<string> GetApfIds(EcoProject1EcoSpace space, Orms.Models.GasStorage gasStorage)
        {
            var aps = AccessPoint.GetBy(space, gasStorage);

            var apfIds = new List<string>();

            foreach (var ap in aps)
            {
                apfIds.AddRange(ap.AccessPointFlow.Select(apf => apf.ExternalId()));
            }

            return apfIds;
        }

    }
}