using System.Collections.Generic;
using System.Linq;
using Eco.ObjectRepresentation;
using Eco.Services;
using Orms.DataAccessEco;
using Orms.DataAccessEco.Helpers;

namespace EcoProject1
{
    public partial class User
    {
        public static IEnumerable<User> GetAll(EcoProject1EcoSpace space)
        {
            space.Active = true;

            var users = EcoServiceHelper.GetExtentService(space)
                .AllInstances<User>().ToList();

            return users;
        }

        public static User Get(EcoProject1EcoSpace space, int id)
        {
            space.Active = true;

            var u = EcoServiceHelper.GetExtentService(space).AllInstances<User>()
                .FirstOrDefault(c => c.ExternalId() == $"{ClassId.User}!{id}");

            return u;
        }

        public static void Update(EcoProject1EcoSpace space, Orms.Models.User user)
        {
            space.Active = true;

            var u = EcoServiceHelper.GetExtentService(space).AllInstances<User>()
                .FirstOrDefault(c => c.ExternalId() == $"{ClassId.User}!{user.Id}");

            u.Email = user.Email;
            u.IsActive = user.IsActive;
            u.LastActivityDateTIme = user.LastActivityDateTime;
            u.Password = user.Password;

            space.Persistence.UpdateDatabase(u);
        }

        public static void Add(EcoProject1EcoSpace space, Orms.Models.User user)
        {
            space.Active = true;

            var contractor = EcoServiceHelper.GetExtentService(space).AllInstances<Contractor>()
                .FirstOrDefault(c => c.ExternalId() == $"{ClassId.Contractor}!{user.Contractor.Id}");

            var u = new User(space)
            {
                Contractor = contractor,
                CreateDateTime = user.CreateDateTime,
                Email = user.Email,
                IsActive = user.IsActive,
                LastActivityDateTIme = user.LastActivityDateTime,
                Name = user.Name,
                Password = user.Password
            };

            space.Persistence.UpdateDatabase(u);
        }

        public static IEnumerable<User> GetBy(EcoProject1EcoSpace space, Orms.Models.Contractor contractor)
        {
            space.Active = true;

            var contractorEntity = EcoServiceHelper.GetExtentService(space).AllInstances<Contractor>()
                .FirstOrDefault(c => c.ExternalId() == $"{ClassId.Contractor}!{contractor.Id}");

            var users = EcoServiceHelper.GetExtentService(space)
                .AllInstances<User>()
                .Where(c => c.Contractor == contractorEntity)
                .ToList();

            return users;
        }

        public static IEnumerable<User> GetBy(EcoProject1EcoSpace space, string nameOrEmail)
        {
            space.Active = true;

            var users = EcoServiceHelper.GetExtentService(space)
                .AllInstances<User>()
                .Where(u => u.Name == nameOrEmail || u.Email == nameOrEmail)
                .ToList();

            return users;
        }
    }
}