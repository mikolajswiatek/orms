using System.Linq;
using Orms.DataAccessEco;
using System.Collections.Generic;
using Eco.ObjectRepresentation;
using Eco.Services;
using Orms.DataAccessEco.Helpers;

namespace EcoProject1
{
    public partial class GasStorage
    {
        public static void Add(EcoProject1EcoSpace space, Orms.Models.GasStorage contractor)
        {
            space.Active = true;

            var c = new GasStorage(space)
            {
                Name = contractor.Name
            };

            space.Persistence.UpdateDatabase(c);
        }

        public static IEnumerable<GasStorage> GetAll(EcoProject1EcoSpace space)
        {
            space.Active = true;

            var gasStorages = EcoServiceHelper.GetExtentService(space)
                .AllInstances<GasStorage>().ToList();

            return gasStorages;
        }

        public static GasStorage Get(EcoProject1EcoSpace space, string id)
        {
            space.Active = true;

            var gasStorage = EcoServiceHelper.GetExtentService(space).AllInstances<GasStorage>()
                .FirstOrDefault(c => c.ExternalId() == id);

            return gasStorage;
        }

        public static GasStorage Get(EcoProject1EcoSpace space, int id)
        {
            space.Active = true;

            var gasStorage = EcoServiceHelper.GetExtentService(space).AllInstances<GasStorage>()
                .FirstOrDefault(c => c.ExternalId() == $"{ClassId.GasStorage}!{id}");

            return gasStorage;
        }
    }
}