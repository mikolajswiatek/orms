using System.Linq;
using Orms.DataAccessEco;
using Orms.DataAccessEco.Helpers;

namespace EcoProject1
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using Eco.Services;
    using Eco.ObjectRepresentation;
    using Eco.ObjectImplementation;
    using Eco.Subscription;
    using Eco.UmlRt;
    using Eco.UmlCodeAttributes;


    public partial class AccessPointFlow
    {
        public static void Add(EcoProject1EcoSpace space, Orms.Models.AccessPointFlow apf)
        {
            space.Active = true;

            var ap = AccessPoint.Get(space, apf.AccessPoint.Id);

            var apfEntity = new AccessPointFlow(space)
            {
                Direction = apf.DirectionString
            };

            apfEntity.AccessPoint = ap;

            space.Persistence.UpdateDatabase(apfEntity);
        }

        public static AccessPointFlow Get(EcoProject1EcoSpace space, int id)
        {
            var apf = EcoServiceHelper.GetExtentService(space)
                .AllInstances<AccessPointFlow>()
                .FirstOrDefault(n => n.ExternalId() == $"{ClassId.AccessPointFlow}!{id}");

            return apf;
        }
    }
}