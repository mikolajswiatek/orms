using System.Linq;
using Orms.DataAccessEco;
using Orms.DataAccessEco.Helpers;

namespace EcoProject1
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.CodeDom.Compiler;
    using Eco.ObjectImplementation;
    using Eco.ObjectRepresentation;
    using Eco.Services;
    using Eco.Subscription;
    using Eco.UmlCodeAttributes;
    using Eco.UmlRt;


    public partial class ContractorContact
    {
        public static void Add(EcoProject1EcoSpace space, Orms.Models.ContractorContact contact)
        {
            space.Active = true;

            var contractorEntity = EcoServiceHelper.GetExtentService(space).AllInstances<Contractor>()
                .FirstOrDefault(c => c.ExternalId() == $"{ClassId.Contractor}!{contact.Contractor.Id}");

            var cc = new ContractorContact(space)
            {
                Name = contact.Name,
                FirstName = contact.FirstName,
                LastName = contact.LastName,
                Email = contact.Email,
                Phone = contact.Phone,
                Adress = contact.Adress,
                Contractor = contractorEntity
            };

            cc.Contractor = contractorEntity;

            space.Persistence.UpdateDatabase(cc);
        }

        public static IEnumerable<ContractorContact> GetByContractor(EcoProject1EcoSpace space, Orms.Models.Contractor contractor)
        {
            space.Active = true;

            var contractorEntity = EcoServiceHelper.GetExtentService(space).AllInstances<Contractor>()
                .FirstOrDefault(c => c.ExternalId() == $"{ClassId.Contractor}!{contractor.Id}");

            var contacts = EcoServiceHelper.GetExtentService(space)
                .AllInstances<ContractorContact>()
                .Where(cc => cc.Contractor == contractorEntity)
                .ToList();

            return contacts;
        }
    }
}