// This file contains all user-written code for the class

using System.Linq;
using Orms.DataAccessEco;
using Orms.DataAccessEco.Helpers;

namespace EcoProject1
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using Eco.Services;
    using Eco.ObjectRepresentation;
    using Eco.ObjectImplementation;
    using Eco.Subscription;
    using Eco.UmlRt;
    using Eco.UmlCodeAttributes;

    public partial class AccessPoint
    {
        public static void Add(EcoProject1EcoSpace space, Orms.Models.AccessPoint ap)
        {
            space.Active = true;

            var accessPoint = new AccessPoint(space)
            {
                Code = ap.Code,
            };

            var gasStorage = EcoServiceHelper.GetExtentService(space).AllInstances<GasStorage>()
                .FirstOrDefault(c => c.ExternalId() == $"{ClassId.GasStorage}!{ap.GasStorage.Id}");

            accessPoint.GasStorage = gasStorage;

            space.Persistence.UpdateDatabase(accessPoint);
        }

        public static IEnumerable<AccessPoint> GetAll(EcoProject1EcoSpace space)
        {
            space.Active = true;

            var aps = EcoServiceHelper.GetExtentService(space)
                .AllInstances<AccessPoint>().ToList();

            return aps;
        }

        public static IEnumerable<AccessPoint> GetBy(EcoProject1EcoSpace space, Orms.Models.GasStorage gasStorage)
        {
            space.Active = true;

            var gs = EcoServiceHelper.GetExtentService(space).AllInstances<GasStorage>()
                .FirstOrDefault(c => c.ExternalId() == $"{ClassId.GasStorage}!{gasStorage.Id}");

            var aps = EcoServiceHelper.GetExtentService(space)
                .AllInstances<AccessPoint>()
                .Where(ap => ap.GasStorage == gs)
                .ToList();

            return aps;
        }

        public static AccessPoint Get(EcoProject1EcoSpace space, string id)
        {
            space.Active = true;

            var accessPoint = EcoServiceHelper.GetExtentService(space).AllInstances<AccessPoint>()
                .FirstOrDefault(c => c.ExternalId() == id);

            return accessPoint;
        }

        public static AccessPoint Get(EcoProject1EcoSpace space, int id)
        {
            space.Active = true;

            var accessPoint = EcoServiceHelper.GetExtentService(space).AllInstances<AccessPoint>()
                .FirstOrDefault(c => c.ExternalId() == $"{ClassId.AccessPoint}!{id}");

            return accessPoint;
        }
    }
}
