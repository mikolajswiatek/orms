using System;
using System.Collections.Generic;
using System.Linq;
using Eco.ObjectRepresentation;
using Eco.Services;
using Orms.DataAccessEco;
using Orms.DataAccessEco.Helpers;

namespace EcoProject1
{
    public partial class GasAccount
    {
        public static IEnumerable<GasAccount> GetAll(EcoProject1EcoSpace space)
        {
            space.Active = true;

            var gasAccounts = EcoServiceHelper.GetExtentService(space)
                .AllInstances<GasAccount>()
                .ToList();

            return gasAccounts;
        }

        public static GasAccount Get(EcoProject1EcoSpace space, int id)
        {
            space.Active = true;

            var gasAccount = EcoServiceHelper.GetExtentService(space)
                .AllInstances<GasAccount>()
                .FirstOrDefault(ga => ga.ExternalId() == $"{ClassId.GasAccount}!{id}");

            return gasAccount;
        }

        public static void Update(EcoProject1EcoSpace space, Orms.Models.GasAccount gasAccount)
        {
            var ga = EcoServiceHelper.GetExtentService(space)
                .AllInstances<GasAccount>()
                .FirstOrDefault(g => g.ExternalId() == $"{ClassId.GasAccount}!{gasAccount.Id}");

            if (ga != null)
            {
                ga.EnergyValue = gasAccount.Energy.Value;
                ga.EnergyUnit = gasAccount.Energy.UnitString;
                ga.VolumeValue = gasAccount.Volume.Value;
                ga.VolumeUnit = gasAccount.Energy.UnitString;
            }
        }

        public static void Add(EcoProject1EcoSpace space, Orms.Models.GasAccount gasAccount)
        {
            var gasStorage = GasStorage.Get(space, gasAccount.GasStorage.Id);

            var ga = new GasAccount(space)
            {
                GasStorage = gasStorage,
                GasDay = gasAccount.GasDay,
                HourIndex = gasAccount.HourIndex,
                EnergyValue = gasAccount.Energy.Value,
                EnergyUnit = gasAccount.Energy.UnitString,
                VolumeValue = gasAccount.Volume.Value,
                VolumeUnit = gasAccount.Energy.UnitString
            };

            space.Persistence.UpdateDatabase(ga);
        }

        public static void Add(EcoProject1EcoSpace space, IEnumerable<Orms.Models.GasAccount> gasAccounts)
        {
            var gasStorage = GasStorage.Get(space, gasAccounts.FirstOrDefault().GasStorage.Id);

            var gas = new List<GasAccount>();

            foreach (var gasAccount in gasAccounts)
            {
                var ga = new GasAccount(space)
                {
                    GasStorage = gasStorage,
                    GasDay = gasAccount.GasDay,
                    HourIndex = gasAccount.HourIndex,
                    EnergyValue = gasAccount.Energy.Value,
                    EnergyUnit = gasAccount.Energy.UnitString,
                    VolumeValue = gasAccount.Volume.Value,
                    VolumeUnit = gasAccount.Energy.UnitString
                };

                gas.Add(ga);
            }

            space.Persistence.UpdateDatabaseWithList(gas);

        }

        public static void Update(EcoProject1EcoSpace space, IEnumerable<Orms.Models.GasAccount> gasAccounts)
        {
            var gasAccountEntities =
                Get(space, gasAccounts.FirstOrDefault().GasStorage, gasAccounts.FirstOrDefault().GasDay);
            var gas = new List<GasAccount>();

            foreach (var ga in gasAccountEntities)
            {
                var gasAccount = gasAccounts
                    .FirstOrDefault(g => ga.ExternalId() == $"{ClassId.GasAccount}!{g.Id}");

                if (gasAccount != null)
                {
                    ga.EnergyValue = gasAccount.Energy.Value;
                    ga.EnergyUnit = gasAccount.Energy.UnitString;
                    ga.VolumeValue = gasAccount.Volume.Value;
                    ga.VolumeUnit = gasAccount.Energy.UnitString;
                }

                gas.Add(ga);
            }

            space.Persistence.UpdateDatabaseWithList(gas);
        }

        public static IEnumerable<GasAccount> Get(EcoProject1EcoSpace space, Orms.Models.GasStorage gasStorage, DateTime gasDay)
        {
            space.Active = true;

            var gs = GasStorage.Get(space, gasStorage.Id);

            var gasAccounts = EcoServiceHelper.GetExtentService(space)
                .AllInstances<GasAccount>()
                .Where(ga => ga.GasStorage == gs && ga.GasDay == gasDay)
                .ToList();

            return gasAccounts;
        }

        public static IEnumerable<GasAccount> Get(EcoProject1EcoSpace space, DateTime gasDay)
        {
            space.Active = true;

            var gasAccounts = EcoServiceHelper.GetExtentService(space)
                .AllInstances<GasAccount>()
                .Where(ga => ga.GasDay == gasDay)
                .ToList();

            return gasAccounts;
        }
    }
}