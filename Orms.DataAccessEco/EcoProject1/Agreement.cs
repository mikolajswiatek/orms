using System.Linq;
using Orms.DataAccessEco;
using Orms.DataAccessEco.Helpers;

namespace EcoProject1
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.CodeDom.Compiler;
    using Eco.ObjectImplementation;
    using Eco.ObjectRepresentation;
    using Eco.Services;
    using Eco.Subscription;
    using Eco.UmlCodeAttributes;
    using Eco.UmlRt;


    public partial class Agreement
    {
        public static IEnumerable<Agreement> GetAll(EcoProject1EcoSpace space)
        {
            space.Active = true;

            var agreements = EcoServiceHelper.GetExtentService(space)
                .AllInstances<Agreement>()
                .ToList();

            return agreements;
        }

        public static Agreement Get(EcoProject1EcoSpace space, int id)
        {
            space.Active = true;

            var agreement = EcoServiceHelper.GetExtentService(space)
                .AllInstances<Agreement>()
                .FirstOrDefault(a => a.ExternalId() == $"{ClassId.Agreement}!{id}");

            return agreement;
        }

        public static void Add(EcoProject1EcoSpace space, Orms.Models.Agreement agreement)
        {
            space.Active = true;

            var contractorEntity = EcoServiceHelper.GetExtentService(space).AllInstances<Contractor>()
                .FirstOrDefault(c => c.ExternalId() == $"{ClassId.Contractor}!{agreement.Contractor.Id}");

            var gs = EcoServiceHelper.GetExtentService(space).AllInstances<GasStorage>()
                .FirstOrDefault(c => c.ExternalId() == $"{ClassId.GasStorage}!{agreement.GasStorage.Id}");

            var a = new Agreement(space)
            {
                CreateDate = agreement.CreateDate,
                End = agreement.End,
                Start = agreement.Start,
                GasStorage = gs,
                Contractor = contractorEntity
            };

            space.Persistence.UpdateDatabase(a);
        }

        public static IEnumerable<Agreement> GetBy(EcoProject1EcoSpace space, Orms.Models.GasStorage gasStorage)
        {
            space.Active = true;

            var gs = EcoServiceHelper.GetExtentService(space).AllInstances<GasStorage>()
                .FirstOrDefault(c => c.ExternalId() == $"{ClassId.GasStorage}!{gasStorage.Id}");

            var agreements = EcoServiceHelper.GetExtentService(space)
                .AllInstances<Agreement>().ToList();

            return agreements;
        }

        public static IEnumerable<Agreement> GetBy(EcoProject1EcoSpace space, Orms.Models.Contractor contractor)
        {
            space.Active = true;

            var contractorEntity = EcoServiceHelper.GetExtentService(space).AllInstances<Contractor>()
                .FirstOrDefault(c => c.ExternalId() == $"{ClassId.Contractor}!{contractor.Id}");

            var agreements = EcoServiceHelper.GetExtentService(space)
                .AllInstances<Agreement>().Where(a => a.Contractor == contractorEntity);

            return agreements;
        }
    }
}