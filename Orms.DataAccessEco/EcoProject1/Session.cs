using System.Linq;
using Orms.DataAccessEco;
using Orms.DataAccessEco.Helpers;
using System.Collections.Generic;
using Eco.ObjectRepresentation;
using Eco.Services;

namespace EcoProject1
{
    public partial class Session
    {
        public static IEnumerable<Session> GetAll(EcoProject1EcoSpace space)
        {
            space.Active = true;

            var sessions = EcoServiceHelper.GetExtentService(space)
                .AllInstances<Session>()
                .ToList();

            return sessions;
        }

        public static Session Get(EcoProject1EcoSpace space, int id)
        {
            space.Active = true;

            var session = EcoServiceHelper.GetExtentService(space)
                .AllInstances<Session>()
                .FirstOrDefault(s => s.ExternalId() == $"{ClassId.Session}!{id}");

            return session;
        }

        public static void Update(EcoProject1EcoSpace space, Orms.Models.Session session)
        {
            space.Active = true;

            var sessionEntity = Get(space, session.Id);

            sessionEntity
                .LastActivityDatetime = sessionEntity.LastActivityDatetime;

            space.Persistence.UpdateDatabase(sessionEntity);
        }

        public static void Add(EcoProject1EcoSpace space, Orms.Models.Session session)
        {
            space.Active = true;

            var user = EcoServiceHelper.GetExtentService(space)
                .AllInstances<User>()
                .FirstOrDefault(s => s.ExternalId() == $"{ClassId.User}!{session.User.Id}");

            var sessionEntity = new Session(space)
            {
                CreateDatetime = session.CreateDatetime,
                Browser = session.Browser,
                Guid = session.Guid,
                Ip = session.Ip,
                LastActivityDatetime = session.LastActivityDatetime,
                User = user
            };

            space.Persistence.UpdateDatabase(sessionEntity);
        }

        public static Session GetBy(EcoProject1EcoSpace space, string guid)
        {
            space.Active = true;

            var session = EcoServiceHelper.GetExtentService(space)
                .AllInstances<Session>()
                .FirstOrDefault(s => s.Guid == guid);

            return session;
        }

        public static void Delete(EcoProject1EcoSpace space, Orms.Models.Session session)
        {
            space.Active = true;

            var sessionEntity = Get(space, session.Id);

            EcoServiceHelper.GetExtentService(space)
                .AllInstances<Session>()
                .Remove(sessionEntity);

            space.UpdateDatabase();
        }
    }
}