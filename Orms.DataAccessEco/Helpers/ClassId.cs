﻿namespace Orms.DataAccessEco.Helpers
{
    public static class ClassId
    {
        public static string[] Spliter = new[]
        {
            "1!",
            "2!",
            "3!",
            "4!",
            "5!",
            "6!",
            "7!",
            "8!",
            "9!",
            "10!",
            "11!",
            "12!",
            "13!"
        };

        public static int AccessPoint = 1;

        public static int AccessPointFlow = 2;

        public static int Agreement = 3;

        public static int CalorificValue = 4;

        public static int Contractor = 5;

        public static int Contact = 6;

        public static int EnergyCalorificValue = 5;

        public static int GasStorage = 9;

        public static int GasAccount = 9;

        public static int Nomination = 10;

        public static int NominationValue = 9;

        public static int Session = 12;

        public static int User = 13;

    }
}
