﻿using System.Collections.Generic;
using System.Linq;
using Orms.Domains.Exceptions;
using Orms.Providers;
using Orms.Models;
using Orms.Services.Interfaces;

namespace Orms.Services
{
    public class UserService : IUserService
    {
        private readonly IUserProvider _userProvider;
        private readonly ISessionProvider _sessionProvider;

        public UserService(
            IUserProvider userProvider,
            ISessionProvider sessionProvider)
        {
            _userProvider = userProvider;
            _sessionProvider = sessionProvider;
        }

        public User Exist(string nameOrEmail)
        {
            return _userProvider.GetBy(nameOrEmail).FirstOrDefault();
        }

        public void Create(User user)
        {
            var nameStatus = Exist(user.Name) != null;
            var emailStatus = Exist(user.Email) != null;

            if (emailStatus || nameStatus)
            {
                throw new UserExistException(user.Name, user.Email);
            }

            _userProvider.Add(user);
        }

        public void Update(User user)
        {
            _userProvider.Update(user);
        }

        public User Get(int id)
        {
            return _userProvider.Get(id);
        }

        public IEnumerable<User> Get(Contractor contractor)
        {
            return _userProvider.GetBy(contractor);
        }

        public IEnumerable<User> GetAll()
        {
            return _userProvider.GetAll();
        }
    }
}