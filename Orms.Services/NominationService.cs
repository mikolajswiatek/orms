﻿using System;
using System.Collections.Generic;
using System.Linq;
using Orms.Providers;
using Orms.Models;
using Orms.Services.Interfaces;

namespace Orms.Services
{
    public class NominationService : INominationService
    {
        private readonly INominationProvider _nominationProvider;
        private readonly INominationValueProvider _nominationValueProvider;

        public NominationService(
            INominationProvider nominationProvider,
            INominationValueProvider nominationValueProvider)
        {
            _nominationProvider = nominationProvider;
            _nominationValueProvider = nominationValueProvider;
        }

        public void Add(Nomination nomination)
        {
            var lastNomination = _nominationProvider.GetForGasDay(
                nomination.Contractor,
                nomination.AccessPointFlow.AccessPoint.GasStorage,
                nomination.GasDay).FirstOrDefault();

            if (lastNomination != null)
            {
                nomination.MajorVersion = lastNomination.MajorVersion + 1;
                _nominationProvider.Update(lastNomination);
            }

            _nominationProvider.Add(nomination);
        }

        public void Add(IEnumerable<NominationValue> values)
        {
            _nominationValueProvider.Add(values);
        }

        public void Add(Nomination nomination, IEnumerable<NominationValue> values)
        {
            var lastNomination = _nominationProvider.GetForGasDay(
                nomination.Contractor,
                nomination.AccessPointFlow.AccessPoint.GasStorage,
                nomination.GasDay).LastOrDefault();

            if (lastNomination != null)
            {
                nomination.MajorVersion = lastNomination.MajorVersion + 1;
                _nominationProvider.Update(lastNomination);
            }

            foreach (var value in values)
            {
                value.Nomination = nomination;
            }

            nomination.Values = values;

            _nominationProvider.Add(nomination);

        }

        public void RefreshNomiation(Nomination nomination, int parentNominationId)
        {
            _nominationProvider.Add(nomination);
        }

        public void AcceptedNomination(Nomination nomination)
        {
            var lastNominations = _nominationProvider.GetForGasDay(
                nomination.Contractor,
                nomination.AccessPointFlow.AccessPoint.GasStorage,
                nomination.GasDay);

            var lastNomination = lastNominations.LastOrDefault(n => n.IsLast && n.Id != nomination.Id);

            if (lastNominations.Any(n => n.Id == nomination.Id))
            {
                nomination = lastNominations.FirstOrDefault(n => n.Id == nomination.Id);
            }

            if (lastNomination != null)
            {
                lastNomination.IsLast = false;
                _nominationProvider.Update(lastNomination);
            }

            nomination.IsAccepted = true;
            nomination.IsLast = true;
            _nominationProvider.Update(nomination);
        }

        public IEnumerable<Nomination> GetBy(Contractor contractor)
        {
            return _nominationProvider.GetBy(contractor);
        }

        public IEnumerable<Nomination> GetBy(GasStorage gasStorage)
        {
            return _nominationProvider.GetBy(gasStorage);
        }

        public IEnumerable<Nomination> GetBy(User user)
        {
            return _nominationProvider.GetBy(user);
        }

        public IEnumerable<Nomination> GetBy(DateTime gasDay)
        {
           return _nominationProvider.GetBy(gasDay);
        }

        public Nomination Get(int id)
        {
            return _nominationProvider.Get(id);
        }

        public Nomination GetLast(Contractor contractor)
        {
            return _nominationProvider.GetLast(contractor);
        }

        public Nomination GetLast(GasStorage gasStorage)
        {
            return _nominationProvider.GetLast(gasStorage);
        }

        public Nomination GetLast(User user)
        {
            return _nominationProvider.GetLast(user);
        }

        public Nomination GetLast(Contractor contractor, DateTime gasDay)
        {
            return _nominationProvider.GetLast(contractor, gasDay);
        }

        public IEnumerable<Nomination> GetForGasDay(Contractor contractor, DateTime gasDay)
        {
            return _nominationProvider.GetForGasDay(contractor, gasDay);
        }

        public IEnumerable<Nomination> GetForGasDay(GasStorage gasStorage, DateTime gasDay)
        {
            return _nominationProvider.GetForGasDay(gasStorage, gasDay);
        }

        public IEnumerable<Nomination> GetForGasDay(Contractor contractor, GasStorage gasStorage, DateTime gasDay)
        {
            return _nominationProvider.GetForGasDay(contractor, gasStorage, gasDay);
        }
    }
}