﻿using System;
using System.Collections.Generic;
using Orms.Providers;
using Orms.Models;
using Orms.Services.Interfaces;

namespace Orms.Services
{
    public class CalorificValueService : ICalorificValueService
    {
        private readonly ICalorificValueProvider _calorificValueProvider;

        public CalorificValueService(ICalorificValueProvider calorificValueProvider)
        {
            _calorificValueProvider = calorificValueProvider;
        }

        public void Add(CalorificValue cv, IEnumerable<EnergyCalorificValue> values)
        {
            _calorificValueProvider.Add(cv);
        }

        public CalorificValue GetLastBy(GasStorage gasStorage)
        {
            return _calorificValueProvider.GetLastBy(gasStorage);
        }

        public CalorificValue GetForGasDay(GasStorage gasStorage, DateTime gasDay)
        {
            return _calorificValueProvider.GetForGasDay(gasStorage, gasDay);
        }

        public CalorificValue Get(int id)
        {
            return _calorificValueProvider.Get(id);
        }
    }
}