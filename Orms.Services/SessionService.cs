﻿using System;
using Orms.Domains.Exceptions;
using Orms.Providers;
using Orms.Models;
using Orms.Services.Interfaces;

namespace Orms.Services
{
    public class SessionService : ISessionService
    {
        private readonly ISessionProvider _sessionProvider;
        private readonly IUserProvider _userProvider;

        public SessionService(
            ISessionProvider sessionProvider,
            IUserProvider userProvider)
        {
            _sessionProvider = sessionProvider;
            _userProvider = userProvider;
        }

        public void Add(Session session)
        {
            _sessionProvider.Add(session);
        }

        public void Refresh(string guid)
        {
            var session = _sessionProvider.GetBy(guid);

            if (session == null)
            {
                throw new SessionExpireException();
            }

            var now = DateTime.Now;

            session.LastActivityDatetime = now;
            session.User.LastActivityDateTime = now;

            _sessionProvider.Update(session);
        }

        public Session GetBy(string guid)
        {
            return _sessionProvider.GetBy(guid);
        }

        public void Delete(Session session)
        {
            _sessionProvider.Delete(session);
        }
    }
}