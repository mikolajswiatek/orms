﻿using System.Collections.Generic;
using Orms.Providers;
using Orms.Models;
using Orms.Services.Interfaces;

namespace Orms.Services
{
    public class GasStorageService : IGasStorageService
    {
        private readonly IGasStorageProvider _gasStorageProvider;

        public GasStorageService(IGasStorageProvider gasStorageProvider)
        {
            _gasStorageProvider = gasStorageProvider;
        }

        public void Add(GasStorage gasStorage)
        {
            _gasStorageProvider.Add(gasStorage);
        }

        public GasStorage Get(int id)
        {
            return _gasStorageProvider.Get(id);
        }

        public IEnumerable<GasStorage> GetAll()
        {
            return _gasStorageProvider.GetAll();
        }
    }
}