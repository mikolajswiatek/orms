﻿using System;
using System.Collections.Generic;
using System.Linq;
using Orms.Domains.Enums;
using Orms.Domains.Units;
using Orms.Providers;
using Orms.Models;
using Orms.Services.Interfaces;

namespace Orms.Services
{
    public class GasAccountService : IGasAccountService
    {
        private readonly IGasAccountProvider _gasAccountProvider;
        private readonly IGasStorageProvider _gasStorageProvider;
        private readonly INominationProvider _nominationProvider;

        public GasAccountService(
            IGasAccountProvider gasAccountProvider,
            IGasStorageProvider gasStorageProvider,
            INominationProvider nominationProvider)
        {
            _gasAccountProvider = gasAccountProvider;
            _gasStorageProvider = gasStorageProvider;
            _nominationProvider = nominationProvider;
        }

        public void Update(IEnumerable<GasAccount> gasAccounts)
        {
            _gasAccountProvider.Update(gasAccounts);
        }

        public IEnumerable<GasAccount> Get(DateTime gasDay)
        {
            return _gasAccountProvider.Get(gasDay);
        }

        public IEnumerable<GasAccount> Get(GasStorage gasStorage, DateTime gasDay)
        {
            return _gasAccountProvider.Get(gasStorage, gasDay);
        }

        public void Refresh(GasStorage gasStorage)
        {
            RefreshGasStorage(gasStorage, GasDay.Today);
        }

        public void Refresh()
        {
            var gasStorages = _gasStorageProvider.GetAll();

            foreach (var gasStorage in gasStorages)
            {
                RefreshGasStorage(gasStorage, GasDay.Today);
            }
        }

        public void Refresh(GasStorage gasStorage, GasDay gasDay)
        {
            RefreshGasStorage(gasStorage, gasDay);
        }

        public void Refresh(GasDay gasDay)
        {
            var gasStorages = _gasStorageProvider.GetAll();

            foreach (var gasStorage in gasStorages)
            {
                RefreshGasStorage(gasStorage, gasDay);
            }
        }

        private void RefreshGasStorage(GasStorage gasStorage, GasDay gasDay)
        {
            var gasAccounts = _gasAccountProvider
                .Get(gasStorage, gasDay.First)
                .OrderBy(ga => ga.HourIndex)
                .ToList();

            var previousGasAccounts = _gasAccountProvider
                .Get(gasStorage, gasDay.First.AddDays(-1))
                .OrderBy(ga => ga.HourIndex)
                .ToList();

            if (!previousGasAccounts.Any())
            {
                previousGasAccounts = CreateEmptyGasAccounts(gasStorage, new GasDay(gasDay.First.AddDays(-1)));
            }

            var nominations = _nominationProvider
                .GetForGasDay(gasStorage, GasDay.Today.First)
                .Where(n => n.IsAccepted && n.IsLast);

            if (gasAccounts.Any())
            {
                ResetGasAccountValues(gasAccounts);
                SumGasAccountsValues(gasAccounts, previousGasAccounts);
                SumNominationsValues(nominations, gasAccounts);
                _gasAccountProvider.Update(gasAccounts);
            }
            else
            {
                gasAccounts = CreateEmptyGasAccounts(gasStorage, gasDay);
                SumGasAccountsValues(gasAccounts, previousGasAccounts);
                SumNominationsValues(nominations, gasAccounts);
                _gasAccountProvider.Add(gasAccounts);
            }
        }

        private static List<GasAccount> CreateEmptyGasAccounts(GasStorage gasStorage, GasDay gasDay)
        {
            var gasAccounts = new List<GasAccount>();

            for (var hourIndex = 0; hourIndex < gasDay.Hours.Count(); hourIndex++)
            {
                gasAccounts.Add(new GasAccount()
                {
                    GasDay = gasDay.First,
                    Energy = new UnitValue(0, Unit.Kwh),
                    Volume = new UnitValue(0, Unit.M3),
                    GasStorage = gasStorage,
                    HourIndex = hourIndex
                });
            }

            return gasAccounts;
        }

        private static void ResetGasAccountValues(IEnumerable<GasAccount> gasAccounts)
        {
            foreach (var gasAccount in gasAccounts)
            {
                gasAccount.Energy.Value = 0;
                gasAccount.Volume.Value = 0;
            }
        }

        private static void SumGasAccountsValues(
            IEnumerable<GasAccount> gasAccounts,
            IEnumerable<GasAccount> previousGasAccount)
        {
            for (var index = 0; index < gasAccounts.Count(); index++)
            {
                gasAccounts.ElementAt(index).Energy.Value +=
                    previousGasAccount.ElementAt(index).Energy.Value;

                gasAccounts.ElementAt(index).Volume.Value +=
                    previousGasAccount.ElementAt(index).Volume.Value;
            }
        }

        private static void SumNominationsValues(
            IEnumerable<Nomination> nominations,
            IEnumerable<GasAccount> gasAccounts)
        {
            foreach (var nomination in nominations)
            {
                for (var index = 0; index < gasAccounts.Count(); index++)
                {
                    if (nomination.AccessPointFlow.Direction == DirectionType.Injection)
                    {
                        gasAccounts.ElementAt(index).Energy.Value +=
                            nomination.Values.ElementAt(index).Energy.Value;

                        gasAccounts.ElementAt(index).Volume.Value +=
                            nomination.Values.ElementAt(index).Volume.Value;
                    }
                    else if (nomination.AccessPointFlow.Direction == DirectionType.Withdrawal)
                    {
                        gasAccounts.ElementAt(index).Energy.Value -=
                            nomination.Values.ElementAt(index).Energy.Value;

                        gasAccounts.ElementAt(index).Volume.Value -=
                            nomination.Values.ElementAt(index).Volume.Value;
                    }
                }
            }
        }
    }
}