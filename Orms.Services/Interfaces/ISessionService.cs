﻿using Orms.Models;

namespace Orms.Services.Interfaces
{
    public interface ISessionService
    {
        void Add(Session session);

        void Refresh(string guid);

        Session GetBy(string guid);

        void Delete(Session session);
    }
}