﻿using System.Collections.Generic;
using Orms.Models;

namespace Orms.Services.Interfaces
{
    public interface IAgreementService
    {
        void Add(Agreement agreement);

        Agreement Get(int id);

        IEnumerable<Agreement> GetAll();

        IEnumerable<Agreement> GetAll(Contractor contractor);

        IEnumerable<Agreement> GetAll(GasStorage gasStorage);

        IEnumerable<Agreement> GetAll(Contractor contractor, GasStorage gasStorage);
    }
}
