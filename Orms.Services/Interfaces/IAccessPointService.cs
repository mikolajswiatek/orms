﻿using System.Collections.Generic;
using Orms.Models;

namespace Orms.Services.Interfaces
{
    public interface IAccessPointService
    {
        IEnumerable<AccessPoint> GetBy(GasStorage gasStorage);

        IEnumerable<AccessPoint> GetAll();

        AccessPoint Get(int id);

        void Add(AccessPoint accessPoint);

        void Add(AccessPointFlow apf, int accessPointId);
    }
}