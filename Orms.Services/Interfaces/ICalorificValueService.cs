﻿using System;
using System.Collections.Generic;
using Orms.Models;

namespace Orms.Services.Interfaces
{
    public interface ICalorificValueService
    {
        void Add(CalorificValue cv, IEnumerable<EnergyCalorificValue> values);

        CalorificValue GetLastBy(GasStorage gasStorage);

        CalorificValue GetForGasDay(GasStorage gasStorage, DateTime gasDay);

        CalorificValue Get(int id);
    }
}