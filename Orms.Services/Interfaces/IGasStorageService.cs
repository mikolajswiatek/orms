﻿using System.Collections.Generic;
using Orms.Models;

namespace Orms.Services.Interfaces
{
    public interface IGasStorageService
    {
        void Add(GasStorage gasStorage);

        GasStorage Get(int id);

        IEnumerable<GasStorage> GetAll();
    }
}