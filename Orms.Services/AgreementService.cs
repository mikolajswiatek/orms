﻿using System.Collections.Generic;
using System.Linq;
using Orms.Models;
using Orms.Providers;
using Orms.Services.Interfaces;

namespace Orms.Services
{
    public class AgreementService : IAgreementService
    {
        private readonly IAgreementProvider _agreementProvider;

        public AgreementService(IAgreementProvider agreementProvider)
        {
            _agreementProvider = agreementProvider;
        }

        public void Add(Agreement agreement)
        {
            _agreementProvider.Add(agreement); 
        }

        public Agreement Get(int id)
        {
            return _agreementProvider.Get(id);
        }

        public IEnumerable<Agreement> GetAll()
        {
            return _agreementProvider.GetAll();
        }

        public IEnumerable<Agreement> GetAll(Contractor contractor)
        {
            return _agreementProvider.GetBy(contractor);
        }

        public IEnumerable<Agreement> GetAll(GasStorage gasStorage)
        {
            return _agreementProvider.GetBy(gasStorage);
        }

        public IEnumerable<Agreement> GetAll(Contractor contractor, GasStorage gasStorage)
        {
            return _agreementProvider.GetBy(contractor)
                .Where(gs => gs.GasStorage.Id == gasStorage.Id);
        }
    }
}
