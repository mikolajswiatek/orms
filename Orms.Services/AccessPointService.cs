﻿using System.Collections.Generic;
using Orms.Providers;
using Orms.Models;
using Orms.Services.Interfaces;

namespace Orms.Services
{
    public class AccessPointService : IAccessPointService
    {
        private readonly IAccessPointProvider _accessPointProvider;
        private readonly IAccessPointFlowProvider _accessPointFlowProvider;

        public AccessPointService(
            IAccessPointProvider accessPointProvider,
            IAccessPointFlowProvider accessPointFlowProvider)
        {
            _accessPointProvider = accessPointProvider;
            _accessPointFlowProvider = accessPointFlowProvider;
        }

        public IEnumerable<AccessPoint> GetBy(GasStorage gasStorage)
        {
            return _accessPointProvider.GetBy(gasStorage);
        }

        public IEnumerable<AccessPoint> GetAll()
        {
            return _accessPointProvider.GetAll();
        }

        public AccessPoint Get(int id)
        {
            return _accessPointProvider.Get(id);
        }

        public void Add(AccessPoint accessPoint)
        {
            _accessPointProvider.Add(accessPoint);
        }

        public void Add(AccessPointFlow apf, int accessPointId)
        {
            var ap = _accessPointProvider.Get(accessPointId);;
            apf.AccessPoint = ap;

            _accessPointFlowProvider.Add(apf);
        }
    }
}