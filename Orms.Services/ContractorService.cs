﻿using System.Collections.Generic;
using Orms.Providers;
using Orms.Models;
using Orms.Services.Interfaces;

namespace Orms.Services
{
    public class ContractorService : IContractorService
    {
        private readonly IContractorContactProvider _contractorContactProvider;
        private readonly IContractorProvider _contractorProvider;

        public ContractorService(
            IContractorContactProvider contractorContactProvider,
            IContractorProvider contractorProvider)
        {
            _contractorContactProvider = contractorContactProvider;
            _contractorProvider = contractorProvider;
        }

        public IEnumerable<Contractor> GetAll()
        {
            return _contractorProvider.GetAll();
        }

        public IEnumerable<ContractorContact> GetAll(Contractor contractor)
        {
            return _contractorContactProvider.GetByContractor(contractor);
        }

        public Contractor Get(int id)
        {
            return _contractorProvider.Get(id);
        }

        public ContractorContact GetContact(int id)
        {
            return _contractorContactProvider.Get(id);
        }

        public void Update(ContractorContact contact)
        {
            _contractorContactProvider.Update(contact);
        }

        public void Add(Contractor contractor)
        {
            _contractorProvider.Add(contractor);
        }

        public void Add(ContractorContact contact, int contractorId)
        {
            var contractor = _contractorProvider.Get(contractorId);
            contact.Contractor = contractor;

            _contractorContactProvider.Add(contact);
        }
    }
}